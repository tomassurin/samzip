# params
OUTDIRBASE="out"
RMOUTDATA=1
MEM=3072m
#

#change time limit for processes
ulimit -t unlimited

dirbase=$PWD
mkdir $OUTDIRBASE
baseoutdir=$OUTDIRBASE/cram
mkdir $baseoutdir
cp $TESTDIR/$test/* .
source test.conf.sh

# test run
for input in `ls -1 $datadir/*.bam`
do
	# samzip run
	inpfilename=`echo $input | sed "s/.*\/\(.*\).bam/\1/"`
	outdir=$baseoutdir/$inpfilename
	mkdir $outdir
	setUp $input
	
	nice -n 19 /usr/bin/time -o compress.time.log java -Xms$MEM -Xmx$MEM -server -jar cramtools.jar cram --input-bam-file $input --reference-fasta-file $datadir/reference.fasta --output-cram-file $input.cram --capture-insertion-quality-scores --capture-substitution-quality-scores --capture-unmapped-quality-scores --capture-masked-quality-scores --capture-all-tags --include-unmapped-reads  --capture-piled-quality-scores 
	
	java -jar cramtools.jar index --input-cram-file $input.cram --reference-fasta-file $datadir/reference.fasta
	
	nice -n 19 /usr/bin/time -o decompress.time.log java -Xms$MEM -Xmx$MEM -server -jar cramtools.jar bam --input-cram-file $input.cram --reference-fasta-file $datadir/reference.fasta --output-bam-file $input.dec
	
	tearDown $input
	mv $input.cram $outdir/$input.cram
	mv $input.dec $outdir/$input.dec
	mv *.log $outdir
done