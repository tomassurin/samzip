### parameters
datadir="data"
# compression profile
profile="profile.xml"
# output directory 
outdirbase="out/"
# array with no. of SAM records to decompress
benchSize=( 100 1000 5000 )
# how many times we want to repeat decompression for corresponding no. of SAM records
benchCycles=( 100 100 100 )
# query for complete file stats
query=("Raw size" "Compressed size" "Compression time" "Decompression time" "Compressed blocks")

mkdir $outdirbase
for input in `ls -1 $datadir/*.bam`
do
	# samzip run
	inpfilename=`echo $input | sed "s/.*\/\(.*\).bam/\1/"`
	outdir=$outdirbase$inpfilename
	mkdir $outdir
	for blocksz in 100 500 1000 3000 5000 10000 20000
	do
		if [ -d $outdir/$blocksz ];
		then 
			echo "Block size directory exists -> Skipping"
		else	
			java -server -Xms2048m -Xmx2048m -ea -jar dist/samzip.jar test -h -d -t=4 -p=$profile -b=$blocksz --in=$input --out=$input.samz --dec=$input.out
			
			# query test
			samtools depth $input > depth.dat
			for i in $(seq 0 $((${#benchSize[*]}-1)))
			do
				b=${benchSize[$i]}
				c=${benchCycles[$i]}
				python queryTest.py depth.dat $input.samz $c $b > query.$b.cout.log 
				grep "Decompression time" query.$b.cout.log | 
					sed -r "s/Decompression time:[ \t]*([0-9\.]*) ms/\1/g" | 
				bash stats.sh times.tmp > query.$b.stats.log
				cat times.tmp | awk '{print $1}' > query.$b.times.log
				rm times.tmp
			done
				
			#maintenance	
			mkdir $outdir/$blocksz
			dir=$outdir/$blocksz
			mv *.log $dir/
#			rm $input.samz 
#			rm $input.out
		fi
	done
	
	#stats calculation
	cd $outdir
	statfile=$inpfilename".stats.log"
	rm *.log
	first=1
	out=""
	printf "%10s" "Profile" >> $statfile
	for i in $(seq 0 $((${#query[*]}-1)))
	do
		q=${query[$i]}
		outfile=`echo $q | sed "s/ /_/g"`
		if [ $first = 1 ]
		then
			for block in `ls -1 --ignore=*.log`
			do
				if `grep -E "$q" $block/stats.overall.log > /dev/null`;
				then
					grep -E "$q" $block/stats.overall.log | sed -r "s/.*$q:\s*([0-9]*).*/$block\t\1/" | gawk '{ printf("%10s\t%10d\n",$1,$2)}' >> stats.$outfile.log
				else
					printf "%10s\t%10s\n" $block "*" >> stats.$outfile.log
				fi
			done
			first=0
		else
			for block in `ls -1 --ignore=*.log`
			do
				if `grep -E "$q" $block/stats.overall.log > /dev/null`;
				then
					grep -E "$q" $block/stats.overall.log | sed -r "s/.*$q:\s*([0-9]*).*/\1/" | gawk '{printf("%10d\n",$1)}' >> stats.$outfile.log
				else
					printf "%10s\n" "*" >> stats.$outfile.log
				fi
			done
		fi
		printf "\t%s" "$q" >> $statfile
		out=$out" stats.$outfile.log"
	done
	for block in `ls -1 --ignore=*.log`
	do
		grep "Total size" $block/stats.block.log | sed -r 's/.*:[ \t]*([0-9]+)B/\1/g' > $block/stats.blockSize.log
	done
	printf "\n" >> $statfile
	paste $out >> $statfile
	rm stats.*.log
	cp $statfile ../../result/
	cd ../../
done