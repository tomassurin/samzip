gawk 'BEGIN{min=10000000; max=-10000000;} 
{
	count++;
	sum+=$1;
	sumsq+=$1*$1;
	if ($1 > max)
		max = $1;
	if ($1 < min)
		min = $1;
	values[$1]++;
} 
END {
printf("%d\t%d\t",min, max);
printf("%f\t", sum/NR);
printf("%f\t", sqrt(sumsq/NR - (sum/NR)^2));
printf("%d\t",count);
	for(x in values) {
		print x,"\t", values[x] > "histogram.log"
	}
}'

# median calculation
cat histogram.log | sort -n > "$1"

cat $1 | gawk '{
    count[NR] = $1;
}
END {
    if (NR % 2) {
        print count[(NR + 1) / 2];
    } else {
        print (count[(NR / 2)] + count[(NR / 2) + 1]) / 2.0;
    }
}'
