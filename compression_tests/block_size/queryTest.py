# -*- coding: utf-8 -*-
"""
Created on Sun Jun 10 00:20:10 2012
@author: Tomáš Šurín
"""

import sys
import os
import random

depths_filename = sys.argv[1]
#depths_filename = "depth.dat"
benchmarkCount = int(sys.argv[3])
querySize = int(sys.argv[4])
inputFile = sys.argv[2]

def singleQuery(query, id):
    cmd = "java -server -Xms256m -Xmx256m -jar dist/samzip.jar \
              decompress"+" --in="+inputFile+" --out=decdata.dat -q=\""+query+"\""
    print cmd+"\n"
    result = os.popen(cmd).read()
    print result
 
depths = open(depths_filename,'r')
 
ref = []
pos = []
 
for line in depths:
    l = line.split('\t')
    ref.append(l[0])
    pos.append(l[1])

n = len(ref)

for i in xrange(1,benchmarkCount):
    r = random.randint(1,n)-1
    query = ref[r]+":"+pos[r]+"+"+str(querySize)
    singleQuery(query, i)