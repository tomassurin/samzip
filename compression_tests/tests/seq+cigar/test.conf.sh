datadir="data"
blocksz=5000
query=("Stream 05" "Stream 06" "Stream 07" "Proc 04 enc" "Proc 04 dec" "Proc 05 enc" "Proc 05 dec")

setUp() {
	case $1 in
	"$datadir/HG00109.bam" | "$datadir/NA20589.bam" | "$datadir/manuel.bam")
		echo "Usign reference sequence: human"
		mv $datadir/human.fasta $datadir/reference.fasta
		mv $datadir/human.fasta.fai $datadir/reference.fasta.fai
		;;
	"$datadir/mouse.bam")
		echo "Using reference sequence: mouse"
		mv $datadir/mouse.fasta $datadir/reference.fasta
		mv $datadir/mouse.fasta.fai $datadir/reference.fasta.fai
		;;
	"$datadir/ecoli.bam")
		echo "Using reference sequence: ecoli"
		mv $datadir/ecoli.fasta $datadir/reference.fasta
		mv $datadir/ecoli.fasta.fai $datadir/reference.fasta.fai
		;;
	"$datadir/KB1454.bam")
		echo "Using reference sequence: ecoli"
		echo "Using reference sequence: hg18chr20"
		mv $datadir/hg18chr20.fasta $datadir/reference.fasta
		mv $datadir/hg18chr20.fasta.fai $datadir/reference.fasta.fai
		;;
	*)
	esac
}

tearDown() {
	case $1 in
	"$datadir/HG00109.bam" | "$datadir/NA20589.bam" | "$datadir/manuel.bam")
		mv $datadir/reference.fasta $datadir/human.fasta
		mv $datadir/reference.fasta.fai $datadir/human.fasta.fai
		;;
	"$datadir/mouse.bam")
		mv $datadir/reference.fasta $datadir/mouse.fasta
		mv $datadir/reference.fasta.fai $datadir/mouse.fasta.fai
		;;
	"$datadir/ecoli.bam")
		mv $datadir/reference.fasta $datadir/ecoli.fasta
		mv $datadir/reference.fasta.fai $datadir/ecoli.fasta.fai
		;;
	"$datadir/KB1454.bam")
		mv $datadir/reference.fasta $datadir/hg18chr20.fasta
		mv $datadir/reference.fasta.fai $datadir/hg18chr20.fasta.fai
		;;	
	*)
	esac
}