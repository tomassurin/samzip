# -*- coding: utf-8 -*-

import random
import sys

filename = sys.argv[1]
blocksz = int(sys.argv[2])
count = int(sys.argv[3])

def overlap(value, offsets):
    for off in offsets:
        if ((value >= off and value < off+blocksz)
            or (value+blocksz >= off and value+blocksz < off+blocksz)):
                return True
    return False

input = file(filename, "r")
lc=0
for line in input:
    lc+=1
lines = lc


offsets = []
for i in xrange(0,count):
    r = random.randint(300,lines)
    while (overlap(r, offsets)):
        r = random.randint(300,lines)    
    offsets.append(r)    
offsets.sort()

input = file(filename, "r")
idx = 0
lc = 0
inBlock = False
curBlockCount = 0
n = len(offsets)
for line in input:
    lc+=1
    if (idx < n and lc == offsets[idx]):
        idx += 1
        curBlockCount = 0
        inBlock = True
    if (inBlock):
        sys.stdout.write(line)
        curBlockCount+=1
    if (curBlockCount >= blocksz):
        inBlock = False
        if (idx >= n):
            break