samtools view -h $1.bam > $1.out
python create_data.py $1.out 5000 40 > out.tmp
samtools view -H $1.bam > header.tmp
cat header.tmp out.tmp > $1.sam
rm out.tmp header.tmp
mv $1.bam $1.original.bam
samtools view -Sb $1.sam > $1.bam