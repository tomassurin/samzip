# test parameters
# output directory for results output
OUTDIRBASE="out"
# directory that contains test data (*.bam files)
TESTDIR="tests"
# if 1 script removes compressed (*.samzip) and decompressed files, only logs are retained
RMOUTDATA=1
# JVM heap maximum memory
MEM=2048m
# number of threads used for compression in SamZip
THREAD_COUNT=8

# change time limit for processes
ulimit -t unlimited

# current directory
dirbase=$PWD

mkdir $OUTDIRBASE
# iterate through all tests (single test = single directory with xml 
# compression profiles and test.conf.sh file)
for test in `ls -1 $TESTDIR`
do
	echo "====================================================="
	echo "====================================================="
	echo "Running test $test"
	# test setup
	rm profile.*.xml
	rm test.conf.sh
	baseoutdir=$OUTDIRBASE/$test
	mkdir $baseoutdir
	cp $TESTDIR/$test/* .
	source test.conf.sh
	
	# iterate through all input files
	for input in `ls -1 $datadir/*.bam`
	do
		inpfilename=`echo $input | sed "s/.*\/\(.*\).bam/\1/"`
		outdir=$baseoutdir/$inpfilename
		mkdir $outdir
		setUp $input
		
		# iterate through all compression profiles
		for profile in `ls -1 profile.*.xml`
		do
			echo "====================================================="
			echo "Testing profile $profile for input file $input"
			profileDir=`echo $profile | sed -r "s/profile.(.*).xml/\1/"`
			if [ -d $outdir/$profileDir ];
			then 
				echo "Profile directory exists -> Skipping"
			else
				# run SamZip
				# must use GNU time for time output to file
				nice -n 19 /usr/bin/time -o time.log java -server -Xms$MEM -Xmx$MEM -ea -jar dist/samzip.jar test -d --bam --diffExcludeTags --stringency=SILENT -t=$THREAD_COUNT -p=$profile -b=$blocksz --in=$input --out=$input.samz --dec=$input.out 2> cerr.log | tee cout.log 
				stat -c %s $input.out > samsize.log
				mkdir $outdir/$profileDir
				dir=$outdir/$profileDir
				mv *.log $dir/
				cp $profile $dir/$profile
				if [ $RMOUTDATA -eq 1 ]
				then 
					rm $input.samz
					rm $input.out
				else 
					mv $input.samz $dir/$inpfilename.samz
					mv $input.out $dir/$inpfilename.out
				fi
			fi
		done
		tearDown $input
		
	    # overall stats calculation
		cd $outdir
		statfile=$inpfilename".stats.log"
		rm *.log
		first=1
		out=""
		printf "%10s" "Profile" >> $statfile
		for i in $(seq 0 $((${#query[*]}-1)))
		do
			q=${query[$i]}
			outfile=`echo $q | sed "s/ /_/g"`
			if [ $first = 1 ]
			then
				for profile in `ls -1 --ignore=*.log`
				do
					if `grep -E "$q" $profile/stats.overall.log > /dev/null`;
					then
						grep -E "$q" $profile/stats.overall.log | sed -r "s/.*$q:\s*([0-9]*).*/$profile\t\1/" | gawk '{ printf("%10s\t%10d\n",$1,$2)}' >> stats.$outfile.log
					else
						printf "%10s\t%10s\n" $profile "*" >> stats.$outfile.log
					fi
				done
				first=0
			else
				for profile in `ls -1 --ignore=*.log`
				do
					if `grep -E "$q" $profile/stats.overall.log > /dev/null`;
					then
						grep -E "$q" $profile/stats.overall.log | sed -r "s/.*$q:\s*([0-9]*).*/\1/" | gawk '{printf("%10d\n",$1)}' >> stats.$outfile.log
					else
						printf "%10s\n" "*" >> stats.$outfile.log
					fi
				done
			fi
			printf "\t%s" "$q" >> $statfile
			out=$out" stats.$outfile.log"
		done
		printf "\n" >> $statfile
		paste $out >> $statfile
		rm stats.*.log
		cp $statfile ../
		cd $dirbase
	done
done