README for test scripts
========================

This directory contains scripts that we used in our tests. These tests allowed 
us to choose good compression scheme for SamZip application.


Requirements
========================

Software:
	*	bash
	*	python 2.7
	* 	JVM (java virtual machine) >1.7
	*	SamZip (should be in directory "dist")
	*	CRAM <http://www.ebi.ac.uk/ena/about/cram_toolkit> (only for CRAM tests)
	
Hardware:
	*	1024 MB RAM (4 threads) or 2048 MB (8 threads)
	* 	decent CPU and enought HDD space


Data file preparation
========================

Script "data_preparation/data_preparation.sh" can be used for creation of a 
sample from a big BAM file. 

Example usage:
						bash data_preparation.sh input
						
creates random sample from input file "input.bam". Sample will contain 200000
SAM records. 

Input file is being sampled with 40 random blocks with 5000 consecutive records
each.
	
Test
========================

Each test consists of a single directory. This directory should contain test 
configuration (file "test.conf.sh") and 1 or more compression profiles. 

Example configuration file:

### FILE start ###
	# directory with input data files in bam format (relative to "test.sh" cwd)
	datadir="data"
	
	# compressed block size (no. of SAM records)
	blocksz=5000
	
	# query for grep & sed - every string in this array forms single column
	#                        in output overall table
	# in this query: table will contain second stream length in bytes and first 	
	#			     processor compression & decompression time
	query=("Stream 02" "Proc 01 enc" "Proc 01 dec")

	# if "1", compressed and decompressed files are removed after test - only 
	# logs are retained
	RMOUTDATA=1
	
	# maximum JVM heap memory
	MEM=2048m
	
	# number of threads used for compression in SamZip
	THREAD_COUNT=8
	
	# this function is called before each SamZip call
	setUp() {
		echo "";
	}

	# this function is called after each SamZip call
	tearDown() {
		echo "";
	} 
### FILE end ###	

Script "test.sh" runs all tests in directory "tests" and stores output files in
directory "out".

Script "testCRAM.sh" tests compression with CRAM (cramtools.jar must be in the
same directory). It stores output files in directory "out/cram". 

Script "block_size/test_block.sh" was used in block size testing. It also 
provides query tests.


Provided tests
========================

In directory "tests" we provide all the tests that we've been using for SamZip's
compression scheme construction. Typically each of these tests evaluates 
compression methods for a single SAM field.

Test "complete" evaluates complete compression scheme.
