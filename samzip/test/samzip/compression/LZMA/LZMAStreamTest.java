package samzip.compression.LZMA;

import java.io.IOException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import samzip.TestUtils;
import samzip.compression.codecs.LZMACodec;
import samzip.compression.util.CompressionException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LZMAStreamTest
{

	String testDataFile = "test/data/demo.dat";
	
	public LZMAStreamTest() throws InterruptedException
	{
		
	}

	@BeforeClass
	public static void setUpClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownClass() throws Exception
	{
	}

	@Test
	public void testCodec() throws CompressionException, IOException
	{
		
		LZMACodec codec = new LZMACodec();
		TestUtils.testCodec(codec, "LZMA codec",  testDataFile);
	}
}
