package samzip.compression.codecs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.junit.Test;
import samzip.TestUtils;
import samzip.compression.CompressionCodec;
import samzip.compression.util.CompressionException;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class TestCodecs
{
	String testDataFile = "test/data/demo.dat";
	
	@Test
	public void testCodecs() throws ClassNotFoundException, CompressionException, FileNotFoundException, IOException, InstantiationException, IllegalAccessException, PreferencesException
	{
		List<Class> codecClasses = new LinkedList<>();
//		codecClasses.add(Class.forName("samzip.compression.codecs.RunLengthEncodingCodec"));
//		codecClasses.add(Class.forName("samzip.compression.codecs.LZMACodec"));
//		codecClasses.add(Class.forName("samzip.compression.codecs.BZip2Codec"));
//		codecClasses.add(Class.forName("samzip.compression.codecs.EliasDeltaCodec"));
//		codecClasses.add(Class.forName("samzip.compression.codecs.EliasGammaCodec"));
//		codecClasses.add(Class.forName("samzip.compression.codecs.FibonacciCodec"));
//		codecClasses.add(Class.forName("samzip.compression.codecs.RiceCodec"));
		codecClasses.add(Class.forName("samzip.compression.codecs.GolombCodec"));
//		codecClasses.add(Class.forName("samzip.compression.codecs.DeltaEncodingCodec"));
//		codecClasses.add(Class.forName("samzip.compression.codecs.MoveToFrontCodec"));
		
		for(Class codecClass : codecClasses) {
			CompressionCodec codec = (CompressionCodec) codecClass.newInstance();
			TestUtils.testCodec(codec, codec.toString(), testDataFile);
		}
	}
}
