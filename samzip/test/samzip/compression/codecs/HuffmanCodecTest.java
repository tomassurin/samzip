package samzip.compression.codecs;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import samzip.TestUtils;
import samzip.compression.util.CompressionException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class HuffmanCodecTest
{
	String testDataFile = "test/data/demo.dat";
	
	public HuffmanCodecTest()
	{
	}

	@BeforeClass
	public static void setUpClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownClass() throws Exception
	{
	}

	@Test
	public void test() throws CompressionException, FileNotFoundException, IOException
	{
		HuffmanCodec codec = new HuffmanCodec();
		TestUtils.testCodec(codec, "Huffman codec", testDataFile);
	}
}
