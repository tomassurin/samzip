package samzip.compression.codecs;

import java.io.IOException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import samzip.TestUtils;
import samzip.compression.util.CompressionException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ArithmeticCodecTest
{
	String testDataFile = "test/data/demo.dat";
	
	public ArithmeticCodecTest()
	{
	}

	@BeforeClass
	public static void setUpClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownClass() throws Exception
	{
	}

	@Test
	public void testCodec() throws CompressionException, IOException
	{
		
		ArithmeticCodec codec = new ArithmeticCodec();
		TestUtils.testCodec(codec, "Arithmetic codec",  testDataFile);
	}
}
