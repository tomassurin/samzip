package samzip.compression.codecs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.util.StatUtil;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class EliasDeltaCodecTest
{
	final int n = 1000;
	ByteArrayOutputStream buf = new ByteArrayOutputStream(30);
	
	static StatUtil stat = StatUtil.newInstance();

	@Test
	public void testUnsigned() throws IOException
	{
		System.out.println("Unsigned test");
		List<Integer> val = new ArrayList<>();
		Random rand = new Random();
		
		EliasDeltaCodec codec = new EliasDeltaCodec(DataMode.UINT);
				
		stat.startCounter();
		
		EncoderMethod enc = codec.newEncoderMethod(buf);
		
		enc.initialize();
		int value;
		for(int i=0; i < n-1; ++i) {
			value = rand.nextInt(5000);
			val.add(value);
			enc.write(value);
		}
		// test 0
		val.add(0);
		enc.write(0);
		
		enc.close();
		
		////////////////
		
		System.out.println("Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		
		DecoderMethod dec = codec.newDecoderMethod(istream);

		dec.initialize();
		for(int i=0; i < n; ++i) {
			assertEquals((int) dec.read(), (int) val.get(i));
		}	
		
		System.out.println(stat.elapsedTime());
		System.out.println();
	}
	
	@Test
	public void testSigned() throws IOException
	{
		System.out.println("signed test");
		List<Integer> val = new ArrayList<>();
		Random rand = new Random();
		
		EliasDeltaCodec codec = new EliasDeltaCodec(DataMode.INT);
		
		stat.startCounter();
		EncoderMethod enc = codec.newEncoderMethod(buf);
		
		enc.initialize();
		int value;
		for(int i=0; i < n-1; ++i) {
			value = rand.nextInt(5000)-2500;
			val.add(value);
			enc.write(value);
		}
		// test 0
		val.add(0);
		enc.write(0);
		
		enc.close();
		
		////////////////
		
		System.out.println("Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		
		DecoderMethod dec = codec.newDecoderMethod(istream);
		
		dec.initialize();
		for(int i=0; i < n; ++i) {
			assertEquals((int) dec.read(), (int) val.get(i));
		}	
		
		System.out.println(stat.elapsedTime());
		System.out.println();
	}
}
