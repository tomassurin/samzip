package samzip.compression.standard;

import java.io.*;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;
import samzip.TestUtils;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.codecs.*;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class DeltaEncodingTest
{
	
	static File file = new File("deltaencodingtest.dat");
	
	int[] values = { 1, 1, 2, 3, 4, 5, 0,0, -300, 2, 3, 4, -33, 321, 32, 34, 23, 12, 12, 32};
	
	static DeltaEncodingCodec deltac;
	static RawCodec rawc;
	
	public DeltaEncodingTest()
	{
	}

	@BeforeClass
	public static void setUpClass() throws Exception
	{
		file.deleteOnExit();
		rawc = new RawCodec();
		rawc.setDataMode(DataMode.INT);
		deltac = new DeltaEncodingCodec(rawc);
		
	}

	@AfterClass
	public static void tearDownClass() throws Exception
	{
	}

	@Test
	public void testBYTE() throws IOException, CompressionException
	{
		deltac.setDataMode(DataMode.SHORT);
		EncoderMethod encoder = deltac.newEncoderMethod(new FileOutputStream(file));
		
		encoder.initialize();
		
		for(int v : values) {
			encoder.write(v);
		}
		
		encoder.close();
		
		DecoderMethod decoder = deltac.newDecoderMethod(new FileInputStream(file));
		
		decoder.initialize();
		
		int val;
		int i= 0;
		while ( (val = decoder.read()) != decoder.EOS()) {
			assertEquals(values[i++], val);
		}
		
		decoder.close();
	}
	
	@Test
	public void testINT() throws IOException, CompressionException
	{		
		deltac.setDataMode(DataMode.INT);
		EncoderMethod encoder = deltac.newEncoderMethod(new FileOutputStream(file));
		encoder.initialize();
		
		for(int v : values) {
			encoder.write(v);
		}
		
		encoder.close();
		
		DecoderMethod decoder = deltac.newDecoderMethod(new FileInputStream(file));

		decoder.initialize();
		
		int val;
		int i= 0;
		while ( (val = decoder.read()) != decoder.EOS()) {
			assertEquals(values[i++], val);
		}
		
		decoder.close();
	}
	
	String testDataFile = "test/data/demo.dat";
	
	@Test
	public void testDeltaCodec() throws CompressionException, FileNotFoundException, IOException 
	{
		DeltaEncodingCodec codec = new DeltaEncodingCodec(new EliasDeltaCodec());
		TestUtils.testCodec(codec, "Delta Encoder ", testDataFile);
	}
}
