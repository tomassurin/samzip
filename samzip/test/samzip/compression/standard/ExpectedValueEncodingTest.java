package samzip.compression.standard;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.codecs.ExpectedValueCodec;
import samzip.compression.codecs.RawCodec;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ExpectedValueEncodingTest
{
	
	static File file = new File("expvaluetest.dat");
	
	int[] values = { 1, 1, 2, 3, 4, 5, 0,0, -300, 2, 3, 4, -33, 321, 32, 34, 23, 12, 12, 32};
	
	static ExpectedValueCodec codec;
	static RawCodec rawc;
	

	@BeforeClass
	public static void setUpClass() throws Exception
	{
		file.deleteOnExit();
		rawc = new RawCodec();
		rawc.setDataMode(DataMode.INT);
		codec = new ExpectedValueCodec(rawc);
		
	}

	@AfterClass
	public static void tearDownClass() throws Exception
	{
	}

	@Test
	public void testBYTE() throws IOException, CompressionException
	{
		codec.setDataMode(DataMode.SHORT);
		EncoderMethod encoder = codec.newEncoderMethod(new FileOutputStream(file));
		
		encoder.initialize();
		
		for(int v : values) {
			encoder.write(v);
		}
		
		encoder.close();
		
		DecoderMethod decoder = codec.newDecoderMethod(new FileInputStream(file));
		
		decoder.initialize();
		
		int val;
		int i= 0;
		while ( (val = decoder.read()) != decoder.EOS()) {
			assertEquals(values[i++], val);
		}
		
		decoder.close();
	}
	
	@Test
	public void testINT() throws IOException, CompressionException
	{		
		codec.setDataMode(DataMode.INT);
		EncoderMethod encoder = codec.newEncoderMethod(new FileOutputStream(file));
		encoder.initialize();
		
		for(int v : values) {
			encoder.write(v);
		}
		
		encoder.close();
		
		DecoderMethod decoder = codec.newDecoderMethod(new FileInputStream(file));

		decoder.initialize();
		
		int val;
		int i= 0;
		while ( (val = decoder.read()) != decoder.EOS()) {
			assertEquals(values[i++], val);
		}
		
		decoder.close();
	}
}
