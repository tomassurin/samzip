package samzip.compression.standard;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import samzip.TestUtils;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.codecs.HuffmanCodec;
import samzip.compression.codecs.LZMACodec;
import samzip.compression.codecs.RawCodec;
import samzip.compression.codecs.RunLengthEncodingCodec;
import samzip.compression.util.CompressionException;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RunLengthEncodingTest
{	
	ByteArrayOutputStream buf = new ByteArrayOutputStream(30);
	final int n = 1000;
	final int MAX_INT = 10000;
	
	@Test
	public void standard() throws CompressionException, FileNotFoundException, IOException
	{
		List<Integer> val = new ArrayList<>();
		Random rand = new Random();
		
		RawCodec rawc = new RawCodec();
		
		RunLengthEncodingCodec runc = new RunLengthEncodingCodec(rawc, rawc);
		
		EncoderMethod encoder = runc.newEncoderMethod(buf);
		
		encoder.initialize();
		
		int value;
		for(int i=0; i < n; ++i) {
			value = rand.nextInt(MAX_INT)-MAX_INT/2;
			val.add(value);
			encoder.write(value);
		}
				
		encoder.close();
		
		System.out.println("rle+raw: Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		
		DecoderMethod decoder = runc.newDecoderMethod(istream);
		decoder.initialize();
		int res;
		for(int i=0; i < n; ++i) {
			//res = decoder.read();
			assertEquals((int)decoder.read(), (int) val.get(i));
		}	
		System.out.println();		
	}
	
	@Test
	public void rleHuffman() throws IOException
	{
		List<Integer> val = new ArrayList<>();
		Random rand = new Random();
		
		HuffmanCodec huf = new HuffmanCodec();
		
		RunLengthEncodingCodec runc = new RunLengthEncodingCodec(huf, huf);
		
		EncoderMethod encoder = runc.newEncoderMethod(buf);
		
		encoder.initialize();
		
		int value;
		for(int i=0; i < n; ++i) {
			value = rand.nextInt(MAX_INT)-MAX_INT/2;
			val.add(value);
			encoder.write(value);
		}
				
		encoder.close();
		
		System.out.println("rle+Huffman: Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		
		DecoderMethod decoder = runc.newDecoderMethod(istream);
		decoder.initialize();
		int res;
		for(int i=0; i < n; ++i) {
			//res = decoder.read();
			assertEquals((int)decoder.read(), (int) val.get(i));
		}	
		System.out.println();	
	}
	
	static String testDataFile = "test/data/demo.dat";

	@Test
	public void testUniversal() throws PreferencesException, CompressionException, FileNotFoundException, IOException
	{
		CompressionCodec codec = new RunLengthEncodingCodec();
		codec.setPreference("algorithm", "universal");
		TestUtils.testCodec(codec, "RLE universal", testDataFile);
	}
	
	@Test
	public void testStream() throws PreferencesException, CompressionException, FileNotFoundException, IOException
	{
		CompressionCodec codec = new RunLengthEncodingCodec();
		codec.setPreference("algorithm", "stream");
		TestUtils.testCodec(codec, "RLE stream", testDataFile);
	}
	
	@Test
	public void testStreamLZMA() throws PreferencesException, CompressionException, FileNotFoundException, IOException
	{
		CompressionCodec codec = new RunLengthEncodingCodec();
		codec.setPreference("algorithm", "stream");
		codec.addCodec("", new LZMACodec());
		TestUtils.testCodec(codec, "RLE stream + LZMA", testDataFile);
	}
	
	@Test
	public void testStreamLength() throws PreferencesException, CompressionException, FileNotFoundException, IOException
	{
		CompressionCodec codec = new RunLengthEncodingCodec();
		codec.setPreference("algorithm", "streamlength");
		TestUtils.testCodec(codec, "RLE stream all length", testDataFile);
	}
	
	@Test
	public void testStreamLengthLZMA() throws PreferencesException, CompressionException, FileNotFoundException, IOException
	{
		CompressionCodec codec = new RunLengthEncodingCodec();
		codec.setPreference("algorithm", "streamlength");
		codec.addCodec("", new LZMACodec());
		TestUtils.testCodec(codec, "RLE stream all length + LZMA", testDataFile);
	}
}
