package samzip.compression.EntropyCode.NumberCode;

import samzip.compression.numbercode.VByteCode;
import samzip.compression.numbercode.NumberCode;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.junit.Test;
import samzip.util.StatUtil;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class VByteCodeTest
{
	final int n = 1000000;
	ByteArrayOutputStream buf = new ByteArrayOutputStream(30);
	
	static StatUtil stat = StatUtil.newInstance();
	
	@Test
	public void testUnsigned() throws Exception
	{
		System.out.println("Unsigned test");
		List<Integer> val = new ArrayList<>();
		Random rand = new Random();
		
		stat.startCounter();
		NumberCode entr = new VByteCode(buf);
		
		int value;
		for(int i=0; i < n-1; ++i) {
			value = rand.nextInt(5000);
			val.add(value);
			entr.encode(value);
		}
		// test 0
		val.add(0);
		entr.encode(0);
		
		entr.close();
		
		////////////////
		
		System.out.println("Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		entr = new VByteCode(istream);
		for(int i=0; i < n; ++i) {
			assertEquals((int) entr.decode(), (int) val.get(i));
		}	
		
		System.out.println(stat.elapsedTime());
		System.out.println();
	}
	
	@Test
	public void testSigned() throws Exception
	{
		System.out.println("Signed test");
		List<Integer> val = new ArrayList<>();
		Random rand = new Random();
		
		stat.startCounter();
		NumberCode entr = new VByteCode(buf);
		entr.setSigned(true);
		int value;
		for(int i=0; i < n-1; ++i) {
			value = rand.nextInt(5000)-2500;
			val.add(value);
			entr.encode(value);
		}
		// test 0
		val.add(0);
		entr.encode(0);
		
		entr.close();
		
		////////////////
		
		System.out.println("Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		entr = new VByteCode(istream);
		entr.setSigned(true);
		for(int i=0; i < n; ++i) {
			assertEquals((int) val.get(i), entr.decode());
		}	
		
		System.out.println(stat.elapsedTime());
		System.out.println();
	}
}
