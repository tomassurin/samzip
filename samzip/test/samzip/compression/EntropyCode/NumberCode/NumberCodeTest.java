package samzip.compression.EntropyCode.NumberCode;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import org.junit.Test;
import samzip.TestUtils;
import samzip.compression.numbercode.NumberCode;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class NumberCodeTest
{
	final int n = 100000;
	final int maximum = 10000;

	@Test
	public void testNumberCodesUnsigned() throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException, Exception
	{
		List<Class> classes = TestUtils.findClasses(
				Paths.get("src/samzip/compression/entropycode/NumberCode").toFile(),
				"samzip.compression.entropycode.NumberCode");
		
		for(Class cls : classes) {
			if (!Class.forName("samzip.compression.entropycode.NumberCode.NumberCode").isAssignableFrom(cls)) 
				continue;
			if (cls.isInterface())
				continue;
			NumberCode inst = (NumberCode) cls.newInstance();
			
			TestUtils.testNumberCoder(inst, n, inst.toString(), false, maximum);					
		}
	}
	
	@Test
	public void testNumberCodesSigned() throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException, Exception
	{
		List<Class> classes = TestUtils.findClasses(
				Paths.get("src/samzip/compression/entropycode/NumberCode").toFile(),
				"samzip.compression.entropycode.NumberCode");
		
		for(Class cls : classes) {
			if (!Class.forName("samzip.compression.entropycode.NumberCode.NumberCode").isAssignableFrom(cls)) 
				continue;
			if (cls.isInterface())
				continue;
			NumberCode inst = (NumberCode) cls.newInstance();
			
			TestUtils.testNumberCoder(inst, n, inst.toString(), true, maximum);			
		}
	}
}
