/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.compression.EntropyCode.NumberCode;

import java.io.ByteArrayOutputStream;
import org.junit.Test;
import samzip.TestUtils;
import samzip.compression.numbercode.EliasDeltaCode;
import samzip.util.StatUtil;


/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class EliasDeltaCodeTest
{
	final int n = 1000000;
	ByteArrayOutputStream buf = new ByteArrayOutputStream(30);
	
	static StatUtil stat = StatUtil.newInstance();
	
	@Test
	public void testUnsigned() throws Exception
	{
		TestUtils.testNumberCoder(new EliasDeltaCode(), n, "Elias delta", false, 5000);
	}
	
	@Test
	public void testSigned() throws Exception
	{
		TestUtils.testNumberCoder(new EliasDeltaCode(), n, "Elias delta", true, 5000);
	}
}
