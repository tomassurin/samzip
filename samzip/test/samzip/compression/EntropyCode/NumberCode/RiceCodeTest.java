package samzip.compression.EntropyCode.NumberCode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.codecs.RiceCodec;
import samzip.compression.numbercode.NumberCode;
import samzip.compression.numbercode.RiceCode;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RiceCodeTest
{
	final int n = 10000;
	ByteArrayOutputStream buf = new ByteArrayOutputStream(30);
	
	final int MAX_INT = 10000;
	
	@Test
	public void testUnsigned() throws Exception
	{
		System.out.println("Unsigned test");
		int M = 16;
		List<Integer> val = new ArrayList<>();
		Random rand = new Random();
		
		NumberCode entr = new RiceCode(buf, M);
		
		int value;
		for(int i=0; i < n; ++i) {
			value = rand.nextInt(MAX_INT)+1;
			val.add(value);
			entr.encode(value);
		}
		
		entr.close();
		
		//////////////// decode
		
		System.out.println("Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		entr = new RiceCode(istream, M);
		for(int i=0; i < n; ++i) {
			assertEquals((int) entr.decode(), (int) val.get(i));
		}	
		System.out.println();
	}	
	
	@Test
	public void testSigned() throws Exception
	{
		System.out.println("Signed test");
		int M = 16;
		List<Integer> val = new ArrayList<>();
		Random rand = new Random();
		
		NumberCode entr = new RiceCode(buf, M);
		entr.setSigned(true);
		int value;
		for(int i=0; i < n; ++i) {
			value = rand.nextInt(MAX_INT)-MAX_INT/2;
			val.add(value);
			entr.encode(value);
		}
		
		entr.close();
		
		//////////////// decode
		
		System.out.println("Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		entr = new RiceCode(istream, M);
		entr.setSigned(true);
		for(int i=0; i < n; ++i) {
			assertEquals((int) entr.decode(), (int) val.get(i));
		}	
		System.out.println();
	}	
	
	@Test
	public void testAdaptiveM() throws Exception
	{
		System.out.println("Adaptive M test");
		int[] val = new int[n];
		Random rand = new Random();
		
		for(int i=0; i < n; ++i) {
			val[i] = rand.nextInt(MAX_INT)-MAX_INT/2;
		}
		
		int M = RiceCode.calculateRiceEncoderParam(val);
		NumberCode entr = new RiceCode(buf, 1 << M);
		entr.setSigned(true);
		for(int value : val) {
			entr.encode(value);
		}
		entr.close();
		
		//////////////// decode
		System.out.println("M = "+M);
		System.out.println("Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		entr = new RiceCode(istream, 1 << M);
		entr.setSigned(true);
		for(int i=0; i < n; ++i) {
			assertEquals((int) entr.decode(), val[i]);
		}	
		System.out.println();
	}	
	
	@Test
	public void testCodec() throws Exception
	{
		System.out.println("Adaptive Rice codec test");
		int[] val = new int[n];
		Random rand = new Random();
		
		for(int i=0; i < n; ++i) {
			val[i] = rand.nextInt(MAX_INT)-MAX_INT/2;
		}
		
		RiceCodec ricec = new RiceCodec();
//		ricec.setOption("m", "11");
		
		EncoderMethod enc = ricec.newEncoderMethod(buf);
		
		for(int value : val) {
			enc.write(value);
		}
		enc.close();
		
		//////////////// decode
		System.out.println("Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		DecoderMethod dec = ricec.newDecoderMethod(istream);
		for(int i=0; i < n; ++i) {
			assertEquals((int) dec.read(), val[i]);
		}	
		System.out.println();
	}
}
