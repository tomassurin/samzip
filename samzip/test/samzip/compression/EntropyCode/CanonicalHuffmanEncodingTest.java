package samzip.compression.EntropyCode;

import samzip.compression.numbercode.HuffmanDecoder;
import samzip.compression.numbercode.HuffmanEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import samzip.compression.util.DataMode;
import samzip.util.StatUtil;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class CanonicalHuffmanEncodingTest
{
	final int n = 1024*1024;
	ByteArrayOutputStream buf = new ByteArrayOutputStream(30);
	

	@Test
	public void test1() throws Exception
	{
		List<Integer> val = new ArrayList<>();
		Random rand = new Random();
		
		StatUtil stat = StatUtil.newInstance();
		HuffmanEncoder enc = new HuffmanEncoder(buf);
		
		stat.startCounter();
		enc.setDataMode(DataMode.INT, true);
		enc.initialize();
		
		int value;
		for(int i=0; i < n; ++i) {
			value = rand.nextInt(5000)-2500;
			val.add(value);
			enc.write(value);
		}
		
		enc.close();
		
		System.out.println("Standard Encoding time: "+stat.elapsedTime());
		System.out.println("Standard Buffer size: "+buf.size());
				
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		
		HuffmanDecoder dec = new HuffmanDecoder(istream);
		
		stat.startCounter();
		dec.initialize();
		for(int i=0; i < n; ++i) {
			assertEquals(dec.read(), val.get(i).intValue());
		}	
		assertEquals(dec.read(), dec.EOS());
		System.out.println("Decoding time: "+stat.elapsedTime());
	}	
}
