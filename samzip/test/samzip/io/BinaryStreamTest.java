/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.io;

import java.io.*;
import java.nio.charset.Charset;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BinaryStreamTest
{
	BinaryInputStream in;
	BinaryOutputStream out;
	File f;
	
	public BinaryStreamTest()
	{
	}

	@BeforeClass
	public static void setUpClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownClass() throws Exception
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of getEndian method, of class BinaryOutputStream.
	 */
	@Test
	public void testLittleEndian() throws FileNotFoundException, IOException
	{
		f = new File("binarystreamtest.dat");
		f.deleteOnExit();
		out = new BinaryOutputStream(new FileOutputStream(f), Endian.LITTLE_ENDIAN);
		
		out.writeByte(25);
		out.writeInt(1231234);
		out.writeByte(136);
		out.writeShort(38000);
		out.writeShort(-5000);
		out.writeLengthPrefixString("hello");
		out.writeShort(5555);
		out.writeNullTerminatedString("hello2alsdakf");
		out.write(15);
		out.align(4);
		
		out.close();
		
		in = new BinaryInputStream(new FileInputStream(f), Endian.LITTLE_ENDIAN);
		
		assertEquals(in.readByte(), 25);
		assertEquals(in.readInt(), 1231234);
		assertEquals(in.readUnsignedByte(), 136);
		assertEquals(in.readUnsignedShort(), 38000);
		assertEquals(in.readShort(), -5000);
		assertEquals(in.readLengthPrefixString(), "hello");
		assertEquals(in.readShort(), 5555);
		assertEquals(in.readNullTerminatedString(), "hello2alsdakf");
		assertEquals(in.read(), 15);
		assertEquals(in.available(), 3);
		assertEquals(in.read(), 0);
		assertEquals(in.read(), 0);
		assertEquals(in.read(), 0);
		assertEquals(in.available(), 0);
		
		in.close();
	}
	
	@Test
	public void testBigEndian() throws FileNotFoundException, IOException
	{
		f = new File("binarystreamtest.dat");
		f.deleteOnExit();
		out = new BinaryOutputStream(new FileOutputStream(f), Endian.BIG_ENDIAN);
		
		out.writeByte(25);
		out.writeInt(1231234);
		out.writeByte(136);
		out.writeShort(38000);
		out.writeShort(-5000);
		out.writeLengthPrefixString("hello");
		out.writeShort(5555);
		out.writeNullTerminatedString("hello2alsdakf");
		out.write(15);
		out.align(4);
		
		out.close();
		
		in = new BinaryInputStream(new FileInputStream(f), Endian.BIG_ENDIAN);
		
		assertEquals(in.readByte(), 25);
		assertEquals(in.readInt(), 1231234);
		assertEquals(in.readUnsignedByte(), 136);
		assertEquals(in.readUnsignedShort(), 38000);
		assertEquals(in.readShort(), -5000);
		assertEquals(in.readLengthPrefixString(), "hello");
		assertEquals(in.readShort(), 5555);
		assertEquals(in.readNullTerminatedString(), "hello2alsdakf");
		assertEquals(in.read(), 15);
		assertEquals(in.available(), 3);
		assertEquals(in.read(), 0);
		assertEquals(in.read(), 0);
		assertEquals(in.read(), 0);
		assertEquals(in.available(), 0);
		
		in.close();
	}
}
