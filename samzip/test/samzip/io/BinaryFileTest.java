package samzip.io;

import com.sun.xml.internal.ws.api.pipe.Fiber;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import samzip.TestUtils;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BinaryFileTest
{
	String testDataFile = "test/data/demo.dat";
	
	public BinaryFileTest()
	{
	}

	@BeforeClass
	public static void setUpClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownClass() throws Exception
	{
	}

	@Test
	public void testSomeMethod() throws FileNotFoundException, IOException
	{
		File fileBin = new File(testDataFile+".bin");
		fileBin.deleteOnExit();
		File fileOut = new File(testDataFile+".output");
		fileOut.delete();
		
		BinaryBlockFile bin = new BinaryBlockFile(testDataFile+".bin");
		
		FileInputStream in1 = new FileInputStream(testDataFile);
		int blockSz = 0;
		int blockLimit = 1 << 20;
		int res = 0;
		byte[] buf = new byte[1024];
		BinaryBlockFile.OutputBlock block = bin.newBlock();
		OutputStream stream1 = block.newStream(1 << 16);
		OutputStream stream2 = block.newStream(1 << 16);
		OutputStream stream3 = block.newStream(1 << 16);
		while((res = in1.read(buf)) >= 0) {
			stream1.write(buf, 0, res);
			stream2.write(buf, 0, res);
			stream3.write(buf, 0, res);
			blockSz += res + res;
			if (blockSz >= blockLimit) {
				block.close();
				bin.writeBlock(block);
				block = bin.newBlock();
				stream1 = block.newStream(1 << 16);
				stream2 = block.newStream(1 << 16);
				stream3 = block.newStream(1 << 16);
				blockSz = 0;
			}
		}
		
		block.close();
		bin.writeBlock(block);
		bin.close();
	
		BinaryBlockFile in = new BinaryBlockFile(testDataFile+".bin");
		BinaryBlockFile.InputBlock b;
		while( (b = in.readBlock()) != null) {
			InputStream stream = b.getStream(1);
			StreamUtils.copyStream(stream, new FileOutputStream(testDataFile+".output", true));
		}
		
		in.close();
		
		TestUtils.assertEqualsFiles(testDataFile, testDataFile+".output");
	}
}
