/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.io;

import java.io.ByteArrayInputStream;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BitInputStreamTest
{
	byte[] buf = { (byte)0x13, (byte)0x14, (byte)0xf3, (byte) 0x00, (byte) 0xff};
	byte[] bin = { 0,0,0,1,0,0,1,1, 0,0,0,1,0,1,0,0, 1,1,1,1,0,0,1,1, 
			           0,0,0,0,0,0,0,0, 1,1,1,1,1,1,1,1};
	
	@Test
	public void test1() throws Exception
	{
		BitInputStream in = new BitInputStream(new ByteArrayInputStream(buf));
		
		for(int i=0; i < bin.length; ++i) {
			assertEquals(in.readBit(), bin[i]);
		}
		
		assertEquals(in.available(), false);		
	}
	
	@Test
	public void test2() throws Exception
	{
		BitInputStream in = new BitInputStream(new ByteArrayInputStream(buf));
		
		assertEquals(in.readBit(4), 1);
		assertEquals(in.readBit(8), 49);
		assertEquals(in.readBit(28), 83034367);
	}
}
