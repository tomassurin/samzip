/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.io;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class FlagsVectorTest
{
	
	FlagsVector flags1;
	FlagsVector flags4;
	
	@Before
	public void setUp()
	{
		flags1 = new FlagsVector(1);

		flags1.set(0, true);
		flags1.set(4,false);
		flags1.set(8,true);
		
		flags4 = new FlagsVector(4);
		
		flags4.set(4, true);
		flags4.set(8, true);
		flags4.set(9, true);
		flags4.set(9, false);
		flags4.set(15, true);
		flags4.set(25, true);
	}
	
	/**
	 * Test of get method, of class FlagsVector.
	 */
	@Test
	public void testGet()
	{		
		assertEquals(flags1.get(8),false);
		assertEquals(flags1.get(4),false);
		assertEquals(flags1.get(0),true);
		assertEquals(flags1.get(4),false);
		
		assertEquals(flags4.get(25), true);
		assertEquals(flags4.get(9), false);
		assertEquals(flags4.get(15), true);
		assertEquals(flags4.get(8), true);
		assertEquals(flags4.get(4), true);
	}

	/**
	 * Test of toByteArray method, of class FlagsVector.
	 */
	@Test
	public void testToByteArray()
	{
		assertEquals(flags1.toByteArray().length, 1);
		assertEquals(flags1.toByteArray()[0], -128);
		
		assertEquals(flags4.toByteArray().length, 4);
		assertEquals(flags4.toByteArray()[0], 8);
		assertEquals(flags4.toByteArray()[3], 64);
	}
}
