/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.io;

import java.io.ByteArrayOutputStream;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BitOutputStreamTest
{
	byte[] expected = { (byte)0x13, (byte)0x14, (byte)0xf3, (byte) 0x00, (byte) 0xff, (byte) 0xe0};
	byte[] expected2 = { (byte)0x13, (byte)0x14, (byte)0xf3, (byte) 0x00, (byte) 0xff, (byte) 0xf4, (byte)0xa0};
	byte[] bin = { 0,0,0,1,0,0,1,1, 0,0,0,1,0,1,0,0, 1,1,1,1,0,0,1,1, 
			           0,0,0,0,0,0,0,0, 1,1,1,1,1,1,1,1, 1, 1, 1};
	
	
	@Test
	public void test1() throws Exception
	{
		ByteArrayOutputStream buf = new ByteArrayOutputStream(256);
		BitOutputStream out = new BitOutputStream(buf);
		
		for(int i=0; i< bin.length; ++i) {
			out.writeBit(bin[i]);
		}
		
		out.close();
		
		byte[] data = buf.toByteArray();
		
		for(int i=0; i< data.length; ++i)
			assertEquals(expected[i], data[i]);		
	}
	
	@Test
	public void test2() throws Exception
	{
		ByteArrayOutputStream buf = new ByteArrayOutputStream(256);
		BitOutputStream out = new BitOutputStream(buf);
		
		for(int i=0; i< bin.length; ++i) {
			out.writeBit(bin[i]);
		}
		
		out.writeBits(0x14, 5);
		out.writeBit(1);
		out.writeBit(0);
		out.writeBit(1);
		out.close();
		
		byte[] data = buf.toByteArray();
		
			for(int i=0; i< data.length; ++i)
				assertEquals(expected2[i], data[i]);		
	}
}
