package samzip.io;

import java.io.IOException;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RandomByteArrayOutputStreamTest
{
	
	public RandomByteArrayOutputStreamTest()
	{
	}

	@BeforeClass
	public static void setUpClass() throws Exception
	{
	}

	@AfterClass
	public static void tearDownClass() throws Exception
	{
	}
	
	@Before
	public void setUp()
	{
	}
	
	@After
	public void tearDown()
	{
	}

	/**
	 * Test of write method, of class FastByteArrayOutputStream.
	 */
	@Test
	public void test1() throws IOException
	{
		FastByteArrayOutputStream ro = new FastByteArrayOutputStream(3);
		ro.write(5);		
		byte[] b = {1,2,3,41,2,3,1,2,3};
		ro.write(b);
		
		ro.position(3);
		ro.write(16);
		ro.position(3);
		ro.position(ro.length());
		ro.write(45);
		
		ro.position(15);
		ro.write(255);
		
		assertEquals(ro.toByteArray()[3], 16);
		assertEquals(ro.toByteArray()[11], 0);
		assertEquals(ro.toByteArray()[12], 0);
		assertEquals(ro.toByteArray()[13], 0);
		assertEquals(ro.toByteArray()[14], 0);
		assertEquals(ro.toByteArray()[15], -1);
		
		assertEquals(ro.length(), 16);
	}
}
