package samzip;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import samzip.compression.CompressionCodec;
import samzip.compression.util.CompressionException;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import static org.junit.Assert.assertEquals;
import samzip.compression.numbercode.GolombCode;
import samzip.compression.numbercode.NumberCode;
import samzip.util.StatUtil;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class TestUtils
{
	public static void testCodec(CompressionCodec codec, String infoString, String testDataFile) throws CompressionException, FileNotFoundException, IOException 
	{
		EncoderMethod enc = codec.newEncoderMethod(new BufferedOutputStream(new FileOutputStream(testDataFile+".out")));
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(testDataFile));
		
		long a = System.currentTimeMillis();
		enc.initialize();
		
		byte[] buf = new byte[1024];
		int res;
		while ((res=in.read(buf)) >= 0) {
			enc.write(buf, 0, res);
		}
		
		enc.close();
		in.close();
		System.out.println(infoString+" compress time: "+(System.currentTimeMillis()-a));
				
		a = System.currentTimeMillis();
		DecoderMethod dec = codec.newDecoderMethod(new BufferedInputStream(new FileInputStream(testDataFile+".out")));
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(testDataFile+".dec"));
		
		System.out.println(infoString+" compressed size: "+Files.size(Paths.get(testDataFile+".out")));
		
		dec.initialize();
		
		while ((res=dec.read(buf)) > 0) {
			out.write(buf, 0, res);
		}
				
		dec.close();
		out.close();
		System.out.println(infoString+" decompress time: "+(System.currentTimeMillis()-a));
	
		assertEqualsFiles(testDataFile, testDataFile+".dec");
		Files.delete(Paths.get(testDataFile+".dec"));
		Files.delete(Paths.get(testDataFile+".out"));
	}
	
	public static void assertEqualsFiles(String file1, String file2) throws FileNotFoundException, IOException
	{
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(file1));
		BufferedInputStream in2 = new BufferedInputStream(new FileInputStream(file2));
		
		int data;
		while((data = in2.read()) >= 0) {
			assertEquals(data, in.read());
		}
		in.close();
		in2.close();
	}
	
		/**
     * Scans all classes accessible from the context class loader which belong to the given package and subpackages.
     *
     * @param packageName The base package
     * @return The classes
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static Class[] getClasses(String packageName)
            throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<Class>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Recursive method used to find all classes in a given directory and subdirs.
     *
     * @param directory   The base directory
     * @param packageName The package name for classes found inside the base directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    public static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".java")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 5)));
            }
        }
        return classes;
    }
	
	static StatUtil stat = StatUtil.newInstance();
	
	public static void testNumberCoder(NumberCode code, int n, String id, boolean signed, int max) throws Exception
	{
		ByteArrayOutputStream buf = new ByteArrayOutputStream(30);
		if (signed) {
			System.out.println(id+" - Signed");
		} else {
			System.out.println(id+" - Unsigned");
		}
		
		int[] val = new int[n];
		Random rand = new Random();
		
		stat.startCounter();
		code.setOutputStream(buf);
		code.setSigned(signed);
		
		int value;
		for(int i=0; i < n-1; ++i) {
			if (signed) {
				value = rand.nextInt(max)-max/2;
			} else {
				value = rand.nextInt(max);
			}
			val[i] = value;
		}
		
//		/// golomb and rice demo
//		int m = GolombCode.calculateGolombEncoderParam(val);
//		code.setOption("m", String.valueOf(m));
		
		// test 0
		val[n-1] = 0;
		for(int i=0; i < n; ++i) {
			code.encode(val[i]);
		}
		
		code.close();
		
		////////////////
		
		System.out.println("Buffer size: "+buf.size());
		
		InputStream istream = new ByteArrayInputStream(buf.toByteArray());
		code.setInputStream(istream);
		code.setSigned(signed);
		for(int i=0; i < n; ++i) {
			assertEquals((int) code.decode(), (int) val[i]);
		}	
		
		System.out.println("Elapsed Time: "+stat.elapsedTime());
		System.out.println();
	}
}
