/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.reference;

import samzip.processor.reference.FastaReferenceSequence;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ReferenceSequenceTest
{
//	static Path reference = Paths.get("d:\\diplomovka\\data\\human_g1k_v37.fasta\\human_g1k_v37.fasta");
	static Path reference = Paths.get("reference.fasta");
	
	public ReferenceSequenceTest()
	{
	}

	@Test
	public void testSomeMethod() throws IOException
	{
		FastaReferenceSequence ref = new FastaReferenceSequence(reference.toFile());
		int n = 1000;
		byte[] data = ref.getSubsequenceAt("20", 60032, n);
		for(byte b: data) {
			System.out.print((char)b);
		}
	}
}
