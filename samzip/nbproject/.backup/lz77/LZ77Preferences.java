/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.compression.lz77;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LZ77Preferences
{
	int searchBufferCapacity;
	int lookBufferCapacity;
	int overlookBufferCapacity;
	int hashSize;
	int hashTableSize;
}