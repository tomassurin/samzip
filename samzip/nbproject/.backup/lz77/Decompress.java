/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.compression.lz77;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import samzip.FORREMOVAL.lz77zip.CompressionSettings;
import samzip.FORREMOVAL.lz77zip.io.CompressedInputProvider;
import samzip.FORREMOVAL.lz77zip.io.PlainOutputProvider;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class Decompress
{
	final private CompressionSettings opts;
	final private CompressedInputProvider input;
	final private PlainOutputProvider output;

	public Decompress(CompressionSettings opts) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException
	{
		this.opts = opts;
		this.input = (CompressedInputProvider) opts.compressedInputProvider.getConstructor(CompressionSettings.class).newInstance(opts);
		this.output = (PlainOutputProvider) opts.plainOutputProvider.getConstructor(CompressionSettings.class).newInstance(opts);
	}
	
	public void decompress() throws IOException 
	{
		int windowSize = opts.searchBufferCapacity;
		byte[] window = new byte[windowSize];
		int windowPosition = 0;
		
		Arrays.fill(window, (byte)0);
		
		byte b;
		int[] tmp;
		
		int matchpos;
		MatchData match = new MatchData();
		
		while (input.read(match)) {
			if (match.onlySymbol) {
				output.write((byte)match.symbol);
				if (windowPosition == windowSize) {
					windowPosition = 0;
				}
				
				window[windowPosition++] = (byte) match.symbol;
			} else {
				matchpos = windowPosition-match.offset;
				
				if (matchpos < 0) {
					matchpos += windowSize;
				}
				
				for(int i=0; i < match.length; ++i) {
					if (matchpos >= windowSize) {
						matchpos = 0;
					}
					output.write(window[matchpos]);
					if (windowPosition == windowSize) {
						windowPosition = 0;
					}
					window[windowPosition++] = window[matchpos];
					++matchpos;
				}
			}
		}
		
		output.close();
	}
}
