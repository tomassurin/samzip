/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.compression.lz77;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import samzip.compression.FORREMOVAL.CompressionCodec;
import samzip.io.BinaryOutputStream;
import samzip.io.BitOutputStream;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
class LZ77OutputProvider
{
	int flagStreamInitialCapacity = 1 << 16;
	int offsetStreamInitialCapacity = 1 << 16;
	int lengthStreamInitialCapacity = 1 << 16;
	int symbolStreamInitialCapacity = 1 << 16;
	
	private BinaryOutputStream output;
	
	private CompressionCodec offsetCodec;
	private CompressionCodec lengthCodec;
	private CompressionCodec symbolCodec;
	
	private ByteArrayOutputStream offsetByteArray;
	private ByteArrayOutputStream lengthByteArray;
	private ByteArrayOutputStream symbolByteArray;
	
	private ByteArrayOutputStream symbolFlagsByteArray;
	private BitOutputStream symbolFlagsStream;
	
	private int uncompressedSize = 0;
		
	LZ77OutputProvider(OutputStream output, CompressionCodec offsetCodec, CompressionCodec lengthCodec, CompressionCodec symbolCodec)
	{
		this.output = new BinaryOutputStream(output);
		
		this.offsetCodec = offsetCodec;
		offsetByteArray = new ByteArrayOutputStream(offsetStreamInitialCapacity);
		this.offsetCodec.setOutputStream(offsetByteArray);
		
		this.lengthCodec = lengthCodec;
		lengthByteArray = new ByteArrayOutputStream(lengthStreamInitialCapacity);
		this.lengthCodec.setOutputStream(lengthByteArray);
		
		this.symbolCodec = symbolCodec;
		symbolByteArray = new ByteArrayOutputStream(symbolStreamInitialCapacity);
		this.symbolCodec.setOutputStream(symbolByteArray);
		
		this.symbolFlagsByteArray = new ByteArrayOutputStream(flagStreamInitialCapacity >> 3);
		this.symbolFlagsStream = new BitOutputStream(symbolFlagsByteArray);		
	}
	
	public void write(MatchData match) throws Exception
	{
//		if (data.onlySymbol)
//			System.out.println("S: "+data.symbol);
//		else 
//			System.out.println("O: "+data.offset+" L: "+data.length);
		
		if (match.onlySymbol) {
			if (match.symbol == MatchData.NOSYMBOL) {
				close();
				return;
			}
			symbolCodec.compress(match.symbol);
			symbolFlagsStream.writeBit(1);
			++uncompressedSize;
		}
		else {
			offsetCodec.compress(match.offset);
			lengthCodec.compress(match.length);
			symbolFlagsStream.writeBit(0);
			uncompressedSize += match.length;
		}
	}
	
	public void close() throws Exception
	{
		
		offsetCodec.close();
		lengthCodec.close();
		symbolCodec.close();
		symbolFlagsStream.flush(); symbolFlagsStream.close();
		
	// write header
		// uncompressed data size
		output.writeUnsignedInt(uncompressedSize);
		// flags size
		output.writeUnsignedInt(symbolFlagsByteArray.size());
		// offset stream size
		output.writeUnsignedInt(offsetByteArray.size());
		// length stream size
		output.writeUnsignedInt(lengthByteArray.size());
		// symbols stream size
		output.writeUnsignedInt(symbolByteArray.size());
		
	// write streamss
		// symbol flags stream
		output.write(symbolFlagsByteArray.toByteArray(),0,symbolFlagsByteArray.size());
		// offset stream
		output.write(offsetByteArray.toByteArray(), 0, offsetByteArray.size());
		// length stream
		output.write(lengthByteArray.toByteArray(), 0, lengthByteArray.size());
		// symbols stream
		output.write(symbolByteArray.toByteArray(), 0, symbolByteArray.size());
		
		output.close();
	}
	
	public boolean isOK()
	{
		return offsetCodec != null && lengthCodec != null && symbolCodec != null;
	}
}
