package samzip.compression.lz77;

import java.nio.charset.Charset;
import java.util.Arrays;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class IntForgettingHashTable
{
	private final int hashTableSize;
	private HashNode[] table;
	private HashNode first;
	private HashNode last;
	
	private int elements;
	private int capacity;

	public IntForgettingHashTable(int hashTableSize, int hashTableCapacity)
	{
		this.hashTableSize = hashTableSize;
		this.table = new HashNode[hashTableSize];
		this.capacity = hashTableCapacity;
		clear();
	}
	
	public void add(int[] key, int data)
	{
		int hash = hashCode(key);
		HashNode node;
		
		if (table[hash] == null) {
			node = new HashNode();
			node.hash = hash;
			node.next = null;
			node.data = data;
			table[hash] = node;
		} else {
			node = table[hash];
			while (node.next != null) {
				node = node.next;
			}
			node.next = new HashNode();
			node = node.next;
			node.hash = hash;
			node.next = null;
			node.data = data;
		}
		if (first == null) {
			first = node;
			first.nextYounger = null;
			last = node;
			last.nextYounger = null;
		} else {
			last.nextYounger = node;
			last = node;
			node.nextYounger = null;
		}
		
//		for(int i=0; i<key.length;++i) {
//			str.append((char)key[i]);
//		}
//		
//		node.orig = str.toString();
//		str.delete(0, str.length());
		
		++elements;
		
		if (elements > capacity) {
			removeOldest();
		}
	}

	public int getSize()
	{
		return elements;
	}
	
	final public HashNode find(int[] data)
	{
		int hash = hashCode(data);
		return table[hash];
	}

	final public int removeOldest()
	{
		if (first == null)
			return 0;
		
		HashNode node = table[first.hash];
		table[first.hash] = node.next;
		first = first.nextYounger;
		--elements;
		return 1;
	}
	
	/**
	 * Removes oldest {@code count} hash nodes.
	 */
	final public int removeOldest(int count)
	{
		int removed = 0;
		while (count > 0 && first != null) {
			HashNode node = table[first.hash];
			table[first.hash] = node.next;
			first = first.nextYounger;
			--count;
			--elements;
			++removed;
		}
		return removed;
	}
	
	final public void clear()
	{
		first = null;
		last = null;
		elements = 0;
		Arrays.fill(table, null);
	}
	
	final private int hashCode(int[] val)
	{
		int h = 0;
		int off = 0;

		for (int i = 0; i < val.length; i++) {
			h = 31 * h + val[off++];
		}
		return (h < 0? -h: h) % hashTableSize;
	}
	
	public class HashNode
	{
		private HashNode next;
		private HashNode nextYounger;
		
		private int hash;
		private int data;
		
		final public HashNode getNext()
		{
			return next;
		}
		
		final public int getData()
		{
			return data;
		}
	}
}