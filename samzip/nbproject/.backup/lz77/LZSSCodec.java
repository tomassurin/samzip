package samzip.compression.lz77;

import java.io.OutputStream;
import samzip.compression.FORREMOVAL.CompressionCodec;
import samzip.compression.util.Preferences;
import samzip.compression.codec.EncoderMethod;
import samzip.compression.codec.DecoderMethod;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LZSSCodec extends CompressionCodec
{
	private LZ77Compression compress;

	LZ77Preferences prefs = new LZ77Preferences();
	
	private void updatePreferences()
	{
		prefs.searchBufferCapacity = options.getInteger("lz77.searchBufferCapacity", 1 << 13);
		prefs.lookBufferCapacity = options.getInteger("lz77.lookBufferCapacity", 256);
		prefs.overlookBufferCapacity = options.getInteger("lz77.overlookBufferCapacity", prefs.lookBufferCapacity*4);
		prefs.hashSize = options.getInteger("lz77.hashSize", 3);
		prefs.hashTableSize = options.getInteger("lz77.hashTableCapacity", 1 << 12);
		
		// preferences checks
		if (prefs.hashSize <= 0)
			prefs.hashSize = 0;
		if (prefs.searchBufferCapacity <= 0)
			prefs.searchBufferCapacity = 1 << 13;
		if (prefs.lookBufferCapacity <= 0)
			prefs.lookBufferCapacity = 256;
		if (prefs.overlookBufferCapacity < prefs.lookBufferCapacity)
			prefs.overlookBufferCapacity = prefs.lookBufferCapacity*4;
	}
	
	public LZSSCodec(OutputStream output, CompressionCodec offsetCodec, CompressionCodec lengthCodec, CompressionCodec symbolCodec, Preferences options)
	{
	
		if (offsetCodec == null || lengthCodec == null || symbolCodec == null) {
			return;
		}
		
		LZ77OutputProvider out = new LZ77OutputProvider(outputStream, offsetCodec, lengthCodec, symbolCodec);
		compress = new LZ77Compression(out, prefs);
		if (options == null) {
			setPreferences(new Preferences());
		} else {
			setPreferences(options);
		}
	}

	@Override
	public boolean supportsCompression()
	{
		return true;
	}

	@Override
	public boolean supportsDecompression()
	{
		return true;
	}	
	
	@Override
	public EncoderMethod createCompressionMethod()
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public DecoderMethod createDecompressionMethod()
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}
}