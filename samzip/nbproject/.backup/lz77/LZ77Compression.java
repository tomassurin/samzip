/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.compression.lz77;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import samzip.compression.FORREMOVAL.CompressionCodec;
import samzip.compression.codec.EncoderMethod;
import samzip.io.BinaryOutputStream;
import samzip.io.BitOutputStream;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LZ77Compression implements EncoderMethod
{
	private LZ77OutputProvider output;
	private CircularWindow dataBuffer;
	
	private int searchBufferCapacity = 16;
	private int lookBufferCapacity = 8;
	private int overlookBufferCapacity = lookBufferCapacity*3;
	private int dataBufferCapacity;
	private int hashSize = 5;
	
	private int availableSize;
	private int compressionChunk;
	
	private int inputPos = 0;
	private IntForgettingHashTable hashtable;
	private int[] hashBuf;
	private MatchData match = new MatchData();
	
	private CompressionCodec offsetCoder;
	private CompressionCodec lengthCoder;
	private CompressionCodec symbolCoder;

	public LZ77Compression(LZ77OutputProvider output, LZ77Preferences prefs)
	{
		this.output = output;
		this.searchBufferCapacity = prefs.searchBufferCapacity;
		this.lookBufferCapacity = prefs.lookBufferCapacity;
		this.hashSize = prefs.hashSize;
		
		this.dataBufferCapacity = searchBufferCapacity+lookBufferCapacity+overlookBufferCapacity;
		this.compressionChunk = overlookBufferCapacity - lookBufferCapacity - hashSize;
		
		this.dataBuffer = new CircularWindow(dataBufferCapacity);
		
		hashBuf = new int[hashSize];
		hashtable = new IntForgettingHashTable(prefs.hashTableSize, searchBufferCapacity - hashSize + 1);
	}
	
	@Override
	public void write(int data) throws Exception
	{
		tryCompress();
		dataBuffer.append(data,1);
		++availableSize;
	}
	
	public void write(int[] data) throws Exception
	{
		int len = data.length;
		int startPos = 0;
		int tmplen;
		
		if (availableSize == 0) {
			tmplen = len >= dataBufferCapacity ? dataBufferCapacity : len;
			dataBuffer.append(data,startPos, tmplen);
			startPos += tmplen;
			availableSize += tmplen;
			len -= tmplen;
		}
		
		while(len > 0) {
			tryCompress();
			tmplen = compressionChunk;
			tmplen = len > tmplen? tmplen : len;
			dataBuffer.append(data, startPos, tmplen);
			startPos += tmplen;
			availableSize += tmplen;
			len -= tmplen;
		}
	}

	private void tryCompress() throws Exception
	{
		if (inputPos == 0) {
			if (availableSize > dataBufferCapacity - lookBufferCapacity) {
				initializeCompression(dataBufferCapacity - lookBufferCapacity);
			} 
		}
		else if (availableSize > inputPos + lookBufferCapacity + compressionChunk) {
			encode(inputPos + compressionChunk, false);
		}
	}
	
	public void close() throws Exception
	{
		if (inputPos == 0) {
			initializeCompression(availableSize);
		}

		encode(availableSize, true);
		
		output.close();
	}
	
	private void initializeCompression(int inputSize) throws Exception
	{
		int outsize = inputSize < hashSize ? inputSize : hashSize;
		// outputStream raw first hashSize bytes and add them to hash table
		dataBuffer.get(hashBuf, 0, outsize);
		for(int i=0; i < outsize; ++i) {
			match.onlySymbol = true;
			match.symbol = hashBuf[i];
			output.write(match);
		}
		hashtable.add(hashBuf, 0);

		inputPos = hashSize;
		
		encode(inputSize, false);
	}
	
	private void encode(int inputSize, boolean finalize) throws Exception
	{
		int lookLimit;
		while (inputPos+hashSize <= inputSize) {
			if (finalize) {
				lookLimit = inputSize;
			} else {
				lookLimit = inputPos + lookBufferCapacity;
			}
			// find longest word S that:
			//	- starts in searchBuffer
			//	- matches some suffix of look-ahead buffer
			match.reset();
			
			match = findBestMatch(inputPos, lookLimit);
			
			for (int i = inputPos - hashSize + 1; i <= inputPos + match.length + 1 - hashSize; ++i) {
				if (i+hashSize <= lookLimit) {
					dataBuffer.get(hashBuf, i, hashSize);
					hashtable.add(hashBuf, i);
				}
			}

			inputPos += match.length;
			if (match.length == 0) {
				match.onlySymbol = true;
				if (inputPos < lookLimit) {
					match.symbol = dataBuffer.get(inputPos);
				} else {
					match.symbol = MatchData.NOSYMBOL;
				}
				++inputPos;
			} else {
				match.onlySymbol = false;
			}

			output.write(match);
		}
		
		if (finalize) {
			finalizeCompression();
		}
	}
	
	private void finalizeCompression() throws Exception 
	{
		int inputSize = availableSize;
		int outsize = inputPos < inputSize ? inputSize - inputPos : 0;
		if (outsize != 0) {
			dataBuffer.get(hashBuf, inputPos, outsize);
			for(int i=0; i < outsize; ++i) {
				match.onlySymbol = true;
				match.symbol = hashBuf[i];
				output.write(match);
			}
		}
	}
	
	private int findMatchLength(int searchPos, int lookPos, int inputSize)
	{
		int matchLength = 0;
		int initialLookPos = lookPos;
		while (true) {
			if ( lookPos >= initialLookPos + lookBufferCapacity || 
				 searchPos >= inputSize || lookPos >= inputSize ) {
				break;
			}
			
			if (dataBuffer.get(searchPos++) == dataBuffer.get(lookPos++)) {
				++matchLength;
			} else {
				break;
			}
		}

		return matchLength;
	}

	private int findMatchScore(int length, int offset)
	{
		return length;
		//return length - offsetCoder.getExpectedSize(offset);
	}

	private MatchData findBestMatch(int lookPosition, int inputSize)
	{
		int outsize = lookPosition + hashSize < inputSize ? hashSize : inputSize - lookPosition;
		dataBuffer.get(hashBuf, lookPosition, outsize);
		IntForgettingHashTable.HashNode node = hashtable.find(hashBuf);

		if (node == null) {
			match.length = 0;
			match.offset = 0;
		} else {
			IntForgettingHashTable.HashNode bestNode = node;
			int bestLength = 0;
			int bestScore = Integer.MIN_VALUE;
			int tmplength, tmpscore;
			do {
				tmplength = findMatchLength(node.getData(), lookPosition, inputSize);
				tmpscore = findMatchScore(tmplength, lookPosition - node.getData());
				if (tmpscore > bestScore) {
					bestScore = tmpscore;
				//if (tmplength > bestLength) {
					bestLength = tmplength;
					bestNode = node;
				}
				node = node.getNext();
			} while (node != null );
			
			if (bestLength < hashSize) {
				match.length = 0;
				match.offset = 0;
			} else {
				match.length = bestLength;
				match.offset = lookPosition - bestNode.getData();
			}
		}

		return match;
	}
}

class MatchData
{
	public int offset;
	public int length;
	public int symbol;
	public boolean onlySymbol;

	final static public int NOSYMBOL = Integer.MIN_VALUE;
	
	public void reset()
	{
		offset = 0;
		length = 0;
		symbol = NOSYMBOL;
		onlySymbol = true;
	}
}