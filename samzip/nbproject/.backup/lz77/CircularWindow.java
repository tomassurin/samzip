package samzip.compression.lz77;

import java.util.Arrays;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class CircularWindow
{
	private int startPos;
	private int appendPos;
	private boolean windowFull;
	
	/// Virtual position of startPos (ie. position in data that have been
	/// historically in this window)
	private long virtualStartPos;
	
	final private int windowCapacity;

	final private int[] window;
		
	public CircularWindow(int capacity)
	{
		this.windowCapacity = capacity;
		window = new int[capacity];
		reset();
	}
	
	public void reset()
	{
		startPos = 0;
		appendPos = 0;
		virtualStartPos = 0;
		windowFull = false;
	}
	
	public boolean isFull()
	{
		return windowFull;
	}
	
	/** Fills window with value {@code val}. */
	public void fill(int val)
	{
		Arrays.fill(window,val);
	}
	
	/** Fills window at {@code <pos,pos+length>} with value {@code val}. */
	public void fill(int pos, int length, int val )
	{
		Arrays.fill(window, pos, pos+length, val);
	}
	
	/** Appends value {@code val}, {@code length} times. */
	public void append(int val, int length)
	{
		int[] data = new int[length];
		Arrays.fill(data, val);
		append(data, 0, length);
	}
	
	public void append(int[] data)
	{
		append(data, 0, data.length);
	}
	
	public void append(int[] data, int pos, int length)
	{
		if (windowFull) {
			for (int i = 0; i < length; ++i) {
				if (startPos >= windowCapacity) {
					startPos = 0;
				}
				window[startPos++] = data[pos+i];
				++virtualStartPos;
			}
		}
		else {
			for (int i = 0; i < length; ++i) {
				if (appendPos >= windowCapacity) {
					appendPos = 0;
					windowFull = true;
				}
				if (!windowFull) {
					window[appendPos++] = data[pos + i];
				}
				else {
					if (startPos >= windowCapacity) {
						startPos = 0;
					}
					window[startPos++] = data[pos + i];
					++virtualStartPos;
				}
			}
		}
	}
	
	public void get(int[] buf, int pos, int length)
	{
		pos -= virtualStartPos;
		copy(buf, pos, length);
	}
	
	public int get(int index)
	{		
		index -= virtualStartPos;
	
		if (index >= windowCapacity || index < 0) {
			throw new ArrayIndexOutOfBoundsException("Circular window index problem: "+index);
		}
		
		index += startPos;
		if (index >= windowCapacity) {
			index -= windowCapacity;
		}
		
		return window[index];
	}

	protected void copy(int[] buf, int pos, int length)
	{		
		if (pos >= windowCapacity || pos < 0 || length < 0) {
			throw new ArrayIndexOutOfBoundsException("Circular window pos problem: "+pos);
		}
		
		pos += startPos;
		
		for (int i = 0; i < length; ++i) {
			if (pos >= windowCapacity) {
				pos -= windowCapacity;
			}
			buf[i] = window[pos++];
		}
	}
	
	protected int[] copy(int pos, int length)
	{
		int[] out = new int[length];
		copy(out, pos, length);
		return out;
	}
	
//	public void set(int index, int value)
//	{
////		assert(index < windowCapacity && index >= 0);
//		
//		index += startPos;
//		if (index >= windowCapacity) {
//			index -= windowCapacity;
//		}
//		
//		window[index] = value;
//	}
}
