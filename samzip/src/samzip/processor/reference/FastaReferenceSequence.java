package samzip.processor.reference;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import net.sf.picard.PicardException;
import samzip.util.LOG;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class FastaReferenceSequence implements ReferenceSequence
{
	private final FastaSequenceIndex index;
	private final FileChannel channel;
	private MappedByteBuffer sequence = null;
	private FastaSequenceIndexEntry currentIndexEntry;
	
	private long sequenceSize;
	private int basesPerLine;
	private int bytesPerLine;
	private int terminatorLength;
	
	private final int MAPPED_CHUNK_LEN = 1 << 24;
	private int chunkLength = 0;
	private int chunkOffset = 0;
	private String currentContig = "";

	public FastaReferenceSequence(final File fastaFile) throws IOException
	{
		this(fastaFile, new FastaSequenceIndex(getFastaIndexFile(fastaFile)));
	}

	public FastaReferenceSequence(final File fastaFile, final FastaSequenceIndex index) throws IOException
	{
		if (index == null) {
			throw new IllegalArgumentException("Index is null for fasta file " + fastaFile);
		}

		this.index = index;
		channel = FileChannel.open(Paths.get(fastaFile.toURI()), StandardOpenOption.READ);
	}

	private static File getFastaIndexFile(File fastaFile) throws IOException
	{
		File indexFile = new File(fastaFile.getAbsolutePath() + ".fai");
		if (!indexFile.exists()) {
			throw new IOException("No index file ("+fastaFile.toString()+".fai) for fasta reference sequence."
					+ " Index file can be created with 'samtools faidx "+fastaFile.toString()+"'"); 
		}
		return indexFile;
	}

	@Override
	public void close() throws IOException
	{
		channel.close();
	}

	private void mapSequence(String contig) throws IOException
	{
		mapSequence(contig, 0, MAPPED_CHUNK_LEN);
	}

	private void mapSequence(String contig, int offset, int length) throws IOException
	{
		if (contig.equals("*")) {
			return;
		}

		try {
			currentIndexEntry = index.getIndexEntry(contig);
		} catch (PicardException ex) {
			LOG.warning("Unable to find entry for contig: "+contig);
			sequenceSize = 0;
			basesPerLine = 0;
			bytesPerLine = 0;
			terminatorLength = 0;
			return;
		}

		sequenceSize = currentIndexEntry.getSize();
		basesPerLine = currentIndexEntry.getBasesPerLine();
		bytesPerLine = currentIndexEntry.getBytesPerLine();
		terminatorLength = bytesPerLine - basesPerLine;

		// set offset to line start
		offset -= offset % basesPerLine;
		
		long indexLoc = currentIndexEntry.getLocation();
		int mapOffset = (int) (offset / basesPerLine) * bytesPerLine + offset % basesPerLine;
		
		if (offset > sequenceSize) {
			LOG.warning("Specified offset in reference sequence was bigger than contig size");
			return;
		}

		chunkOffset = offset;
		chunkLength = (int) Math.min(length, sequenceSize - offset);
		long chunkDataSize = ((chunkLength) / basesPerLine) * bytesPerLine + chunkLength % basesPerLine;

		if (sequence != null) {
//			unmap(sequence);
			
			final WeakReference bufferWeakRef = new WeakReference(sequence);
			sequence = null;

			final long startTime = System.currentTimeMillis();
			while(null != bufferWeakRef.get()) {
				if(System.currentTimeMillis() - startTime > 100)
					break;
				System.gc();
			}
		}
			
		sequence = channel.map(FileChannel.MapMode.READ_ONLY, indexLoc + mapOffset, chunkDataSize);
	}

//	private static void unmap(MappedByteBuffer buffer)
//	{
//		sun.misc.Cleaner cleaner = ((DirectBuffer) buffer).cleaner();
//		cleaner.clean();
//	}
//	
	/**
	 * 0-based
	 *
	 * @param start
	 * @param length
	 * @return
	 */
	@Override
	public byte[] getSubsequenceAt(String contig, int start, int length) throws IOException
	{
		if (!contig.equals(currentContig)) {
			mapSequence(contig, start, Math.max(MAPPED_CHUNK_LEN, length));
			currentContig = contig;
		} else {
			if (start < chunkOffset) {
				mapSequence(contig, start, Math.max(MAPPED_CHUNK_LEN, length));
			} else if (start + length > chunkOffset+chunkLength) {
				mapSequence(contig, start, Math.max(MAPPED_CHUNK_LEN, length));
			}
		}

		if (start < 0 || length < 0 || start + length > sequenceSize) {
			return new byte[length];
		}
		

		if (basesPerLine == 0)
			return new byte[length];
		
		byte[] result = new byte[length];
		int resultPos = 0;

		start -= chunkOffset;
		
		int startOffset = (int) (start / basesPerLine) * bytesPerLine + start % basesPerLine;
		sequence.position(startOffset);
		int rowOffset = (int) start % basesPerLine;

		byte data;

		while (resultPos < length) {
			data = sequence.get();
			result[resultPos++] = data;
			++rowOffset;
			if (rowOffset >= basesPerLine) {
				rowOffset = 0;
				sequence.position(sequence.position() + terminatorLength);
			}
		}

		return result;
	}
}
