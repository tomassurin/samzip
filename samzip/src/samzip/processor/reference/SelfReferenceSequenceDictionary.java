package samzip.processor.reference;

import java.io.IOException;
import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;
import net.sf.samtools.SAMRecord;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SelfReferenceSequenceDictionary implements SelfReferenceSequence
{
	private String currentContig = "";
	private byte[] data = new byte[1024];
	private int dataOffset = 0;
	private int startPos = 0;
	
	private void arrayCopy(byte[] src, int srcPos, byte[] dst, int dstPos, int length)
	{
		for(int i=0; i < length; ++i) {
			dst[dstPos+i] = src[srcPos+i];
		}
	}
	
	private void setData(int pos, byte value, int minStart)
	{
		int endPos = startPos+dataOffset;
		if (pos >= startPos && pos < endPos) {
			data[pos-startPos] = value;
		} else {
			if (startPos+dataOffset < minStart) {
				startPos = minStart;
				dataOffset = 0;
				endPos = startPos;
			}
			for(int i=0; i <= pos - endPos; ++i) {
				addData(NODATA, minStart);
			}
			data[pos-startPos] = value;
		}
	}
	
	private void addData(byte value, int minStart)
	{
		if (dataOffset >= data.length) {
			if (startPos < minStart) {
				int length = data.length - (minStart-startPos);
				arrayCopy(data, minStart-startPos, data, 0, length);
				dataOffset = length;
				startPos = minStart;
			} else {
				byte[] bufnew = new byte[data.length << 1];
				System.arraycopy(data, 0, bufnew, 0, dataOffset);
				data = bufnew;
			}
		}
		data[dataOffset++] = value;
	}
	
	@Override
	public void close() throws IOException
	{
		
	}
	
	public final static byte NODATA = '0';

	@Override
	public void addSAMRecord(SAMRecord rec)
	{
		int alignmentStart = rec.getAlignmentStart()-1;
		Cigar cig = rec.getCigar();
		byte[] seq = rec.getReadBases();
		
		if (cig == null || seq == null || alignmentStart == -1)
			return;
		
		// following code isn't working - it produces not decompressible data
		//		 if (rec.getAttribute("MD") != null)
		//				seq = SequenceUtil.makeReferenceFromAlignment(rec, true);
		int seqOffset = 0;
		
		CigarElement elem;
		int curPos = alignmentStart;
		for(int i=0; i < cig.numCigarElements(); ++i) {
			elem = cig.getCigarElement(i);
			switch (elem.getOperator()) {
				case H:
				case P:
				case D:
				case N:
					for(int j=0; j < elem.getLength(); ++j)
						setData(curPos++, NODATA, alignmentStart);
					break;
				case EQ:
				case M:
				case X:
					for(int j=0; j < elem.getLength(); ++j) {
						setData(curPos++, seq[seqOffset++], alignmentStart);
					}
					break;
				case I:
				case S:
					seqOffset += elem.getLength();
					break;
				default: { }
			}
		}
	}
	
	@Override
	public byte[] getSubsequenceAt(String contig, int start, int length) throws IOException
	{
		if (!contig.equals(currentContig)) {
			currentContig = contig;
			startPos = 0;
			dataOffset = 0;
		}
		
		byte[] result = new byte[length];
		
		int cur = 0;
		int endOffset = startPos+dataOffset;
		for(int i=start; i < start+length; ++i) {
			if (i < startPos || i >= endOffset) {
				result[cur++] = NODATA;
			} else {
				result[cur++] = data[i-startPos];
			}
		}
		
		return result;
	}
	
}
