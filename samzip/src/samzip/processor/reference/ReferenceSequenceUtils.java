package samzip.processor.reference;

import java.io.IOException;
import net.sf.samtools.*;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ReferenceSequenceUtils
{
	private ReferenceSequence reference;
	private boolean usingRealReference;
	private String currentReference = "";
	private TextCigarCodec textCigarCodec = TextCigarCodec.getSingleton();
	private int minimalEqLength = 1;

	public ReferenceSequenceUtils()
	{
		reference = new SelfReferenceSequenceDictionary();
//		reference = new SelfReferenceSequenceConsenzus();
		usingRealReference = false;
	}
	
	public void setMinimumEqualLength(int len)
	{
		this.minimalEqLength = len;
	}
	
	public ReferenceSequenceUtils(FastaReferenceSequence reference)
	{
		this.reference = reference;
		usingRealReference = true;
	}

	public class SequenceData
	{

		public Cigar cigar;
		public byte[] seq;

		public SequenceData(Cigar cigar, byte[] seq)
		{
			this.cigar = cigar;
			this.seq = seq;
		}
	}

	private void byteArrayToUpperCase(byte[] arr)
	{
		for (int i = 0; i < arr.length; ++i) {
			if (Character.isLowerCase(arr[i])) {
				arr[i] = (byte) Character.toUpperCase(arr[i]);
			}
		}
	}

	public SequenceData createDiffToReference(SAMRecord rec) throws IOException
	{		
		if (rec.getReferenceName().equals("*")) {
			return new SequenceData(rec.getCigar(), rec.getReadBases());
		}

		Cigar cigar = rec.getCigar();
		Cigar result = new Cigar();

		byte[] seq = rec.getReadBases();
		byte[] ref = reference.getSubsequenceAt(rec.getReferenceName(),
				rec.getAlignmentStart() - 1, cigar.getReferenceLength());

		byteArrayToUpperCase(seq);
		byteArrayToUpperCase(ref);

		int resultSeqLen = 0;

		int seqOffset = 0;
		int refOffset = 0;

		boolean isMatch;
		int mLen;
		CigarElement elem;

		if (cigar.numCigarElements() == 0) {
			return new SequenceData(cigar, seq);
		}

		CigarOperator MISMATCH_OPERATOR = CigarOperator.X;

		for (int j = 0; j < cigar.numCigarElements(); ++j) {
			elem = cigar.getCigarElement(j);
			switch (elem.getOperator()) {
				case M:
					mLen = 0;
					isMatch = false;
					for (int i = 0; i < elem.getLength(); ++i) {
						if (isMatch) {
							if ((seq[seqOffset] != '=') && (ref[refOffset] != seq[seqOffset])) {
								if (mLen > 0) {
									// output cigar element - match mLen bases with reference
									result.add(new CigarElement(mLen, CigarOperator.EQ));
								}
								mLen = 1;
								++resultSeqLen;
								isMatch = false;
							} else {
								++mLen;
							}
							++refOffset;
							++seqOffset;
						} else {
							if ((seq[seqOffset] == '=') || (ref[refOffset] == seq[seqOffset])) {
								if (mLen > 0) {
									// output cigar element - mismatched mLen bases with reference
									result.add(new CigarElement(mLen, MISMATCH_OPERATOR));
								}
								mLen = 1;
								isMatch = true;
							} else {
								++resultSeqLen;
								++mLen;
							}
							++refOffset;
							++seqOffset;
						}
					}
					if (mLen > 0) {
						if (isMatch) {
							result.add(new CigarElement(mLen, CigarOperator.EQ));
						} else {
							result.add(new CigarElement(mLen, MISMATCH_OPERATOR));
						}
					}
					break;
				case D:
				case N:
					result.add(elem);
					refOffset += elem.getLength();
					break;
				case I:
					result.add(elem);
					seqOffset += elem.getLength();
					resultSeqLen += elem.getLength();
					break;
				case S:
					result.add(elem);
					seqOffset += elem.getLength();
					resultSeqLen += elem.getLength();
					break;
				case EQ:
					result.add(elem);
					seqOffset += elem.getLength();
					refOffset += elem.getLength();
					break;
				case X:
					result.add(elem);
					seqOffset += elem.getLength();
					refOffset += elem.getLength();
					resultSeqLen += elem.getLength();
					break;
				case H:
				case P:
					result.add(elem);
					break;
				default: {
					assert false : "Cigar element unknown: " + elem.getOperator().toString();
				}
			}
		}

		/// expand short equalities to matches
		// 18=1X4=1X1=1X3=1X4= => 18=1X4=1X5M1X4= (for MINIMUM_EQ_LENGTH = 4)
		if (minimalEqLength > 1) {
			Cigar tmpCigar = result;
			result = new Cigar();
			CigarElement nextElem;
			int matchLength = 0;
			for (int j = 0; j < tmpCigar.numCigarElements(); ++j) {
				elem = tmpCigar.getCigarElement(j);
				if (j < tmpCigar.numCigarElements() - 1) {
					nextElem = tmpCigar.getCigarElement(j + 1);
				} else {
					nextElem = null;
				}
				switch (elem.getOperator()) {
					case EQ:
						if (nextElem == null || nextElem.getOperator() == CigarOperator.X
								|| nextElem.getOperator() == CigarOperator.M) {
							if (elem.getLength() < minimalEqLength) {
								if (nextElem != null) {
									matchLength += elem.getLength() + nextElem.getLength();
								} else {
									matchLength += elem.getLength();
								}
								resultSeqLen += elem.getLength();
								++j;
							} else {
								if (matchLength > 0) {
									result.add(new CigarElement(matchLength, CigarOperator.M));

									matchLength = 0;
								}
								result.add(elem);
							}
						} else {
							if (matchLength > 0) {
								result.add(new CigarElement(matchLength, CigarOperator.M));
								matchLength = 0;
							}
							result.add(elem);
						}
						break;
					case M:
					case X:
						if (nextElem != null && nextElem.getOperator() == CigarOperator.EQ
								&& nextElem.getLength() < minimalEqLength) {
							resultSeqLen += nextElem.getLength();
							matchLength += elem.getLength() + nextElem.getLength();
							++j;
						} else {
							if (matchLength > 0) {
								result.add(new CigarElement(matchLength, CigarOperator.M));
								matchLength = 0;
							}
							result.add(elem);
						}
						break;
					default:
						if (matchLength > 0) {
							result.add(new CigarElement(matchLength, CigarOperator.M));
							matchLength = 0;
						}
						result.add(elem);
				}
			}
			if (matchLength > 0) {
				result.add(new CigarElement(matchLength, CigarOperator.M));
			}
		}

		/// create diff sequence
		byte[] resultSeq = new byte[resultSeqLen];
		seqOffset = 0;
		int resultOffset = 0;
		for (int i = 0; i < result.numCigarElements(); ++i) {
			elem = result.getCigarElement(i);
			switch (elem.getOperator()) {
				case H:
				case P:
				case D:
				case N:
					break;
				case EQ:
					seqOffset += elem.getLength();
					break;
				case X:
				case I:
				case S:
				case M:
					for (int j = 0; j < elem.getLength(); ++j) {
						resultSeq[resultOffset++] = seq[seqOffset++];
					}
					break;
				default: {
					assert false : "Cigar element unknown: " + elem.getOperator().toString();
				}
			}
		}

		if (!usingRealReference) {
			/// add record to self reference sequence
			((SelfReferenceSequence) reference).addSAMRecord(rec);
		}

		if ((resultSeq.length + textCigarCodec.encode(result).length())
				> seq.length + textCigarCodec.encode(cigar).length()) {
			return new SequenceData(cigar, seq);
		} else {
			return new SequenceData(result, resultSeq);
		}
	}

	public static int getDiffSequenceLength(Cigar cigar)
	{
		int result = 0;
		for (int i = 0; i < cigar.numCigarElements(); ++i) {
			CigarElement elem = cigar.getCigarElement(i);
			switch (elem.getOperator()) {
				case X:
				case I:
				case S:
				case M:
					result += elem.getLength();
					break;
				default: {
				}
			}
		}
		return result;
	}

	/**
	 * Calculates edit distance to reference sequence
	 */
	public static int calculateNM(byte[] seq, byte[] ref, Cigar cigar)
	{

		int refOffset = 0;
		int seqOffset = 0;
		int result = 0;

		for (int i = 0; i < cigar.numCigarElements(); ++i) {
			CigarElement elem = cigar.getCigarElement(i);
			switch (elem.getOperator()) {
				case EQ:
					refOffset += elem.getLength();
					seqOffset += elem.getLength();
					break;
				case M:
					for (int j = 0; j < elem.getLength(); ++j) {
						if (seq[seqOffset] != '=' && seq[seqOffset] != ref[refOffset]) {
							++result;
						}
						++seqOffset;
						++refOffset;
					}
					break;
				case X:
					result += elem.getLength();
					refOffset += elem.getLength();
					seqOffset += elem.getLength();
					break;
				case I:
					seqOffset += elem.getLength();
					result += elem.getLength();
					break;
				case S:
					seqOffset += elem.getLength();
					break;
				case N:
					refOffset += elem.getLength();
					break;
				case D:
					result += elem.getLength();
					refOffset += elem.getLength();
					break;
				case H:
				case P:
					break;
				default: {
					assert false : "Cigar element unknown: " + elem.getOperator().toString();
				}
			}
		}

		return result;
	}

	public static String calculateMD(byte[] seq, byte[] ref, Cigar cigar)
	{
		StringBuilder builderMD = new StringBuilder();

		int refOffset = 0;
		int seqOffset = 0;
		boolean lastWasMatch = false;
		int matchLength = 0;

		for (int i = 0; i < cigar.numCigarElements(); ++i) {
			CigarElement elem = cigar.getCigarElement(i);
			switch (elem.getOperator()) {
				case EQ:
					refOffset += elem.getLength();
					seqOffset += elem.getLength();
					matchLength += elem.getLength();
					break;
				case M:
					for (int j = 0; j < elem.getLength(); ++j) {
						if (seq[seqOffset] == '=' || seq[seqOffset] == ref[refOffset]) {
							++matchLength;
						} else {
							builderMD.append(String.valueOf(matchLength));
							matchLength = 0;
							builderMD.append((char) ref[refOffset]);
						}
						++seqOffset;
						++refOffset;
					}
					break;
				case X:
					builderMD.append(String.valueOf(matchLength));
					matchLength = 0;

					builderMD.append((char) ref[refOffset]);
					++refOffset;
					for (int j = 1; j < elem.getLength(); ++j) {
						builderMD.append((char) '0');
						builderMD.append((char) ref[refOffset]);
						++refOffset;
					}
					seqOffset += elem.getLength();
					break;
				case I:
				case S:
					seqOffset += elem.getLength();
					break;
				case N:
					refOffset += elem.getLength();
					break;
				case D:
					builderMD.append(String.valueOf(matchLength));
					matchLength = 0;

					builderMD.append('^');
					for (int j = 0; j < elem.getLength(); ++j) {
						builderMD.append((char) ref[refOffset]);
						++refOffset;
					}
					lastWasMatch = false;
					break;
				case H:
				case P:
					break;
				default: {
					assert false : "Cigar element unknown: " + elem.getOperator().toString();
				}
			}
		}

		builderMD.append(matchLength);

		return builderMD.toString();
	}

	public void reconstructOriginalSAMRecord(Cigar cigar, byte[] seq, SAMRecord rec, boolean calculateMD, boolean calculateNM) throws IOException
	{
		byte[] ref = reference.getSubsequenceAt(rec.getReferenceName(),
				rec.getAlignmentStart() - 1, cigar.getReferenceLength());

		byteArrayToUpperCase(ref);
		byteArrayToUpperCase(seq);
		
		int seqOffset = 0;
		int refOffset = 0;
		int resultOffset = 0;

		int n = cigar.getReadLength();
		byte[] resultSeq = new byte[n];
		Cigar resultCigar = new Cigar();
		int matchRunLength = 0;

		if (cigar.isEmpty()) {
			rec.setCigar(cigar);
			rec.setReadBases(seq);
			return;
		}

		for (int i = 0; i < cigar.numCigarElements(); ++i) {
			CigarElement elem = cigar.getCigarElement(i);
			switch (elem.getOperator()) {
				case EQ:
					System.arraycopy(ref, refOffset, resultSeq, resultOffset, elem.getLength());
					resultOffset += elem.getLength();
					refOffset += elem.getLength();
					matchRunLength += elem.getLength();
					break;
				case M:
				case X:
					System.arraycopy(seq, seqOffset, resultSeq, resultOffset, elem.getLength());
					resultOffset += elem.getLength();
					seqOffset += elem.getLength();
					refOffset += elem.getLength();
					matchRunLength += elem.getLength();
					break;
				case I:
				case S:
					if (matchRunLength != 0) {
						resultCigar.add(new CigarElement(matchRunLength, CigarOperator.M));
						matchRunLength = 0;
					}
					System.arraycopy(seq, seqOffset, resultSeq, resultOffset, elem.getLength());
					seqOffset += elem.getLength();
					resultOffset += elem.getLength();
					resultCigar.add(elem);
					break;
				case D:
				case N:
					if (matchRunLength != 0) {
						resultCigar.add(new CigarElement(matchRunLength, CigarOperator.M));
						matchRunLength = 0;
					}
					refOffset += elem.getLength();
					resultCigar.add(elem);
					break;
				case H:
				case P:
					if (matchRunLength != 0) {
						resultCigar.add(new CigarElement(matchRunLength, CigarOperator.M));
						matchRunLength = 0;
					}
					resultCigar.add(elem);
					break;
				default: {
					assert false : "Cigar element unknown: " + elem.getOperator().toString();
				}
			}
		}

		if (matchRunLength != 0) {
			resultCigar.add(new CigarElement(matchRunLength, CigarOperator.M));
		}

		rec.setCigar(resultCigar);
		rec.setReadBases(resultSeq);

		if (calculateMD && usingRealReference) {
			rec.setAttribute("MD", calculateMD(resultSeq, ref, cigar));
		}

		if (calculateNM && usingRealReference) {
			rec.setAttribute("NM", calculateNM(resultSeq, ref, cigar));
		}

		if (!usingRealReference) {
			/// add this record to our reference sequence
			((SelfReferenceSequence) reference).addSAMRecord(rec);
		}

//		rec.setCigar(cigar);
//		rec.setReadBases(seq);
	}
	
	public void reset()
	{
		if (!usingRealReference && (reference instanceof SelfReferenceSequence))
			reference = new SelfReferenceSequenceDictionary();
	}
}
