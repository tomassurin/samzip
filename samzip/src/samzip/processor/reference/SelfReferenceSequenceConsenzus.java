package samzip.processor.reference;

import java.io.IOException;
import java.util.Arrays;
import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;
import net.sf.samtools.SAMRecord;
import samzip.processor.util.SequenceUtil;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SelfReferenceSequenceConsenzus implements SelfReferenceSequence
{
	private String currentContig = "";
	private int offset = 0;
	private int startPos = 0;
	
	/// matrix for storing ACGT scores
	private double[][] score = createScoreMatrix(5);	
	private static final int idxCount = 5;
	private static final int WIDTH = 6;
	
	private static final byte[] reverseColumns = new byte[128];
	{
		Arrays.fill(reverseColumns, (byte)-1);
		reverseColumns['A'] = 0;
		reverseColumns['C'] = 1;
		reverseColumns['G'] = 2;
		reverseColumns['T'] = 3;
		reverseColumns['N'] = 4;
	}
	private static final byte[] columns = new byte[] { 'A', 'C', 'G', 'T', 'N'};
	
	private static double[][] createScoreMatrix(int capacity)
	{
		double[][] res = new double[capacity][];
		for(int i=0; i < capacity; ++i) {
			res[i] = new double[WIDTH];
			Arrays.fill(res[i],-1.0);
			res[i][idxCount] = 0.0;
		}
		return res;
	}
	
	private void inplaceArrayCopy(byte[] src, int srcPos, byte[] dst, int dstPos, int length)
	{
		for(int i=0; i < length; ++i) {
			dst[dstPos+i] = src[srcPos+i];
		}
	}
	
	private void scoreCopy(double[][] src, int srcPos, double[][] dst, int dstPos, int length)
	{
		for(int i=0; i < length; ++i) {
			dst[dstPos+i] = src[srcPos+i];
		}
	}
	
	private static void setScore(byte value, byte qual, double[] dat)
	{
		if (qual <= 0)
			qual = 1;
		
		if (value < 0 || value > 127)
			return;
		int idx = reverseColumns[value];
		if (idx < 0 || idx >= WIDTH-1)
			return;
		
		dat[idxCount] += 1.0;
		if (dat[idx] < 0) {
//			dat[idx] = 1;
			dat[idx] = Math.pow(10, qual/10);
		} else {
//			dat[idx] += 1;
			dat[idx] += Math.pow(10, qual/10);
//			dat[idx] /= dat[idxCount];
		}
	}
	
	private byte getBaseFromScore(int pos)
	{
		double[] dat = score[pos];
		double max = Double.MIN_VALUE;
		int maxIdx = 0;
		for(int i=0; i < WIDTH-1; ++i) {
			if (dat[i] > max) {
				max = dat[i];
				maxIdx = i;
			}
		}
		return columns[maxIdx];
	}
	
	private void setData(int pos, byte value, byte qual, int minStart)
	{
		int endPos = startPos+offset;
		if (pos >= startPos && pos < endPos) {
			setScore(value, qual, score[pos-startPos]);
		} else {
			if (startPos+offset < minStart) {
				startPos = minStart;
				offset = 0;
				endPos = startPos;
			}
			ensureCapacity(minStart, pos-endPos+1);
			setScore(value, qual, score[pos-startPos]);
		}
	}
	
	private void ensureCapacity(int minStart, int size)
	{
		for (int i=0; i < size; ++i) {
			if (offset >= score.length) {
				if (startPos < minStart) {
					int length = score.length - (minStart-startPos);
					scoreCopy(score, minStart-startPos, score, 0, length);
					offset = length;
					startPos = minStart;
				} else {
					double[][] bufnew = createScoreMatrix(score.length << 1);
					scoreCopy(score, 0, bufnew, 0, offset);
					score = bufnew;
				}
			}
			++offset;
		}
	}
	
	@Override
	public void close() throws IOException
	{
		
	}
	
	public final static byte NODATA = '0';
	
	@Override
	public void addSAMRecord(SAMRecord rec)
	{
		int alignmentStart = rec.getAlignmentStart()-1;
		Cigar cig = rec.getCigar();
		byte[] seq = rec.getReadBases();
		byte[] qual = rec.getBaseQualities();
		
		if (cig == null || seq == null || alignmentStart == -1)
			return;
		
		SequenceUtil.canonizeSequence(seq);
		
		if (qual == null || qual.length < seq.length) {
			qual = new byte[seq.length];
			Arrays.fill(qual, (byte) 0);
		}
		
		/// following code isn't working - it produces not decompressible data
		// if (rec.getAttribute("MD") != null)
		//		seq = SequenceUtil.makeReferenceFromAlignment(rec, true);
		int seqOffset = 0;
		
		CigarElement elem;
		int curPos = alignmentStart;
		for(int i=0; i < cig.numCigarElements(); ++i) {
			elem = cig.getCigarElement(i);
			switch (elem.getOperator()) {
				case H:
				case P:
				case D:
				case N:
					for(int j=0; j < elem.getLength(); ++j)
						setData(curPos++, NODATA, (byte)  0, alignmentStart);
					break;
				case EQ:
				case M:
				case X:
					for(int j=0; j < elem.getLength(); ++j) {
						setData(curPos++, seq[seqOffset], qual[seqOffset], alignmentStart);
						++seqOffset;
					}
					break;
				case I:
				case S:
					seqOffset += elem.getLength();
					break;
				default: { }
			}
		}
	}
	
	@Override
	public byte[] getSubsequenceAt(String contig, int start, int length) throws IOException
	{
		if (!contig.equals(currentContig)) {
			currentContig = contig;
			startPos = 0;
			offset = 0;
		}
		
		byte[] result = new byte[length];
		
		int cur = 0;
		int endOffset = startPos+offset;
		for(int i=start; i < start+length; ++i) {
			if (i < startPos || i >= endOffset) {
				result[cur++] = NODATA;
			} else {
				result[cur++] = getBaseFromScore(i-startPos);
			}
		}
		
		return result;
	}
}
