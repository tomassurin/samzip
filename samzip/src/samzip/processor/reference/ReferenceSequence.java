package samzip.processor.reference;

import java.io.IOException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public interface ReferenceSequence
{
	public void close() throws IOException;
	public byte[] getSubsequenceAt(String contig, int start, int length) throws IOException;
}
