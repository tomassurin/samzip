package samzip.processor.reference;

import java.io.IOException;
import net.sf.samtools.SAMRecord;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public interface SelfReferenceSequence extends ReferenceSequence
{
	@Override
	public byte[] getSubsequenceAt(String contig, int start, int length) throws IOException;
	public void addSAMRecord(SAMRecord rec);
}
