package samzip.processor.processors;

import java.io.IOException;
import java.util.List;
import net.sf.samtools.SAMFileHeader;
import net.sf.samtools.SAMReadGroupRecord;
import net.sf.samtools.SAMRecord;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.util.PreferencesException;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;
import samzip.processor.util.SAMField;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ReadGroupProcessor extends SAMProcessor
{
	private final static SAMField processedField = SAMField.TAG_RG;

	@Override
	public SAMField[] getProcessedFields()
	{
		return new SAMField[] { processedField };
	}

	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		super.preferenceChanged(whatKey);
	}

	@Override
	public String toString()
	{
		return "ReadGroupProcessor";
	}

	@Override
	public boolean isValid()
	{
		if (getCodecFor("default") == null) {
			lastError = "Default codec wasn't specified.";
			return false;
		}
			
		return true;
	}
	
	private int getReadGroupIdx(SAMFileHeader header, String readGroupId)
	{
		List<SAMReadGroupRecord> readGroups = header.getReadGroups();
		for(int i=0; i < readGroups.size();++i) {
			if (readGroups.get(i).getId().equals(readGroupId))
				return i+1;
		}
		
		return 0;
	}
	
	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		if (block.length == 0)
			return;
		
		SAMFileHeader samHeader = block[0].getHeader();
		
		EncoderMethod enc = getCodecFor("default")
				.newEncoderMethod(outputBlock, processedField.toString(), null);
		
		for(int i=0; i < block.length; ++i) {
			String readGroupID = block[i].getStringAttribute("RG");
			if (readGroupID == null)
				enc.write(0);
			else
				enc.write(getReadGroupIdx(samHeader, readGroupID));
		}
		
		enc.close();
	}

	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		SAMFileHeader samHeader = block[0].getHeader();
		List<SAMReadGroupRecord> readGroups = samHeader.getReadGroups();
		
		DecoderMethod dec = getCodecFor("default").newDecoderMethod(inputBlock, null);
		
		for(int i=0; i < block.length; ++i) {
			int RGIdx = dec.read();
			if (RGIdx > 0 && RGIdx <= readGroups.size()) {
				SAMReadGroupRecord rg = readGroups.get(RGIdx-1);
				block[i].setAttribute("RG", rg.getId());
			}
		}
		
		dec.close();
	}	
}
