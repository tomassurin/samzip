package samzip.processor.processors;

import java.io.IOException;
import net.sf.samtools.SAMRecord;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;
import samzip.processor.util.AbstractArrayCompressor;
import samzip.processor.util.DelimiterArrayCompressor;
import samzip.processor.util.LengthArrayCompressor;
import samzip.processor.util.SequenceLengthArrayCompressor;
import samzip.util.PreferencesException;
import samzip.processor.util.SAMField;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class FieldProcessor extends SAMProcessor
{	
	private SAMField processedField = SAMField.NONE;
	
	private DecoderMethod dec;
	private EncoderMethod enc;

	private AbstractArrayCompressor arrayCompress = new DelimiterArrayCompressor(this);
	
	public FieldProcessor()
	{
		/// set default length codec
//		addCodec("length", new HuffmanCodec());
	}

	@Override
	public String toString()
	{
		return "FieldProcessor/"+processedField.toString();
	}

	@Override
	public boolean isValid()
	{
		if (getCodecFor("default") == null) {
			lastError = "Default codec wasn't specified.";
			return false;
		}
		
		if (processedField == SAMField.NONE) {
			lastError = "Field wasn't specified.";
			return false;
		}
		
		if (processedField.isTag()) {
			lastError = "Can't process tags.";
			return false;
		}
		
		if (needsArrayCompressor() && 
			 arrayCompress instanceof LengthArrayCompressor && 
			 getCodecFor("length") == null) 
		{
			lastError = "Stream for 'length' wasn't specified.";
			return false;
		}
		
		return true;
	}
	
	@Override
	public SAMField[] getProcessedFields()
	{
		if (processedField == SAMField.NONE)
			return empty;
		
		SAMField[] result = {processedField};
		return result;
	}

	@Override
	public SAMField[] getDependencies()
	{
		if (arrayCompress instanceof SequenceLengthArrayCompressor)
			return new SAMField[] {SAMField.SEQ};
		
		return empty;			
	}

	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		switch (whatKey.toLowerCase()) {
			case "field":
			{
				String value = preferences.getString(whatKey, "FLAG");
				processedField = SAMField.getSAMField(value);
				if (processedField == SAMField.NONE)
					throw new PreferencesException("Unknown field "+value, whatKey);
				break;
			}
			case "arraycode": 
			case "array":
				{
					String value = preferences.getString(whatKey, "codec").toLowerCase();
					if (value.startsWith("delimiter")) {
						int sepIdx = value.indexOf('/');
						if (sepIdx < 0 || sepIdx >= value.length()) 
							arrayCompress = new DelimiterArrayCompressor(this);
						else {
							try {
								String sub = value.substring(sepIdx+1);
								char delim = (char) (int) Integer.decode(sub);
								arrayCompress = new DelimiterArrayCompressor(this, delim);
							} catch (NumberFormatException ex) {
								throw new PreferencesException("Can't parse specified delimiter "+value.substring(sepIdx));
							}
						}
						return;
					}
					switch (value) {
						case "length":
							arrayCompress = new LengthArrayCompressor(this);
							break;
						case "sequence":
							arrayCompress = new SequenceLengthArrayCompressor(this);
							break;
						default:
							throw new PreferencesException("Unknown array coding type "+value, whatKey);
					}
					break;
				}
			default:
				super.preferenceChanged(whatKey);
				break;
		}
	}
	
	private DataMode getDataMode(SAMField processedField)
	{
		if (processedField.equals(SAMField.QNAME)) {
			return DataMode.UBYTE;
		} else if (processedField.equals(SAMField.FLAG)) {
			return DataMode.USHORT;
		} else if (processedField.equals(SAMField.RNAME)) {
			return DataMode.UINT;
		} else if (processedField.equals(SAMField.POS)) {
			return DataMode.UINT;
		} else if (processedField.equals(SAMField.MAPQ)) {
			return DataMode.UBYTE;
		} else if (processedField.equals(SAMField.CIGAR)) {
			return DataMode.UBYTE;
		} else if (processedField.equals(SAMField.RNEXT)) {
			return DataMode.UINT;
		} else if (processedField.equals(SAMField.PNEXT)) {
			return DataMode.UINT;
		} else if (processedField.equals(SAMField.TLEN)) {
			return DataMode.INT;
		} else if (processedField.equals(SAMField.SEQ)) {
			return DataMode.UBYTE;
		} else if (processedField.equals(SAMField.QUAL)) {
			return DataMode.UBYTE;
		}
		
		return DataMode.INT;
	}
	
	private void initializeCompression(SAMRecord[] data, OutputBlock outputBlock) throws IOException
	{
		if (needsArrayCompressor()) {
			arrayCompress.initializeCompression(outputBlock, processedField.toString());
		} else {
			DataMode dataMode = getDataMode(processedField);
			enc = getCodecFor("default")
					.newEncoderMethod(outputBlock, processedField.toString(), dataMode);
		}
	}
	
	@Override
	public void compressBlock(SAMRecord[] data, OutputBlock outputBlock) throws IOException
	{			
		initializeCompression(data, outputBlock);
		int len = data.length;
		
		if (processedField.equals(SAMField.QNAME)) {
			compressQNAME(data, len);
		} else if (processedField.equals(SAMField.FLAG)) {
			compressFLAG(data, len);
		} else if (processedField.equals(SAMField.RNAME)) {
			compressREF(data, len);
		} else if (processedField.equals(SAMField.POS)) {
			compressPOS(data, len);
		} else if (processedField.equals(SAMField.MAPQ)) {
			compressMAPQ(data, len);
		} else if (processedField.equals(SAMField.CIGAR)) {
			compressCIGAR(data, len);
		} else if (processedField.equals(SAMField.RNEXT)) {
			compressNEXT_REF(data, len);
		} else if (processedField.equals(SAMField.PNEXT)) {
			compressNEXT_POS(data, len);
		} else if (processedField.equals(SAMField.TLEN)) {
			compressNEXT_TEMP_LEN(data, len);
		} else if (processedField.equals(SAMField.SEQ)) {
			compressSEQ(data, len);
		} else if (processedField.equals(SAMField.QUAL)) {
			compressQUAL(data, len);
		}

		if (enc != null) {
			enc.close();
			enc = null;
		}

		if (arrayCompress != null)
			arrayCompress.close();
	}
	
	private void initializeDecompression(BinaryBlockFile.InputBlock inputBlock) throws IOException
	{
		if (needsArrayCompressor()) {
			arrayCompress.initializeDecompression(inputBlock);
		} else {
			DataMode dataMode = getDataMode(processedField);
		
			dec = getCodecFor("default").newDecoderMethod(inputBlock, dataMode);
		}
	}
	
	@Override
	public void decompressBlock(SAMRecord[] data, InputBlock inputBlock) throws IOException
	{
		initializeDecompression(inputBlock);
		int len = data.length;
		
		if (processedField.equals(SAMField.QNAME)) {
			decompressQNAME(data, len);
		} else if (processedField.equals(SAMField.RNAME)) {
			decompressREF(data, len);
		} else if (processedField.equals(SAMField.FLAG)) {
			decompressFLAG(data, len);
		} else if (processedField.equals(SAMField.MAPQ)) {
			decompressMAPQ(data, len);
		} else if (processedField.equals(SAMField.POS)) {
			decompressPOS(data, len);
		} else if (processedField.equals(SAMField.CIGAR)) {
			decompressCIGAR(data, len);
		} else if (processedField.equals(SAMField.RNEXT)) {
			decompressNEXT_REF(data, len);
		} else if (processedField.equals(SAMField.PNEXT)) {
			decompressNEXT_POS(data, len);
		} else if (processedField.equals(SAMField.TLEN)) {
			decompressNEXT_TEMP_LEN(data, len);
		} else if (processedField.equals(SAMField.SEQ)) {
			decompressSEQ(data, len);
		} else if (processedField.equals(SAMField.QUAL)) {
			decompressQUAL(data, len);
		}

		if (dec != null) {
			dec.close();
			dec = null;
		}

		if (arrayCompress != null)
			arrayCompress.close();
	}
	
	private void compressQNAME(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			arrayCompress.compressByteArray(data[i].getReadName().getBytes(AbstractArrayCompressor.charset));
		}
	}
	
	protected void compressFLAG(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			enc.write(data[i].getFlags());
		}
	}
	
	protected void compressREF(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			enc.write(data[i].getReferenceIndex().intValue()+1);
		}
	}
	
	protected void compressMAPQ(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			enc.write(data[i].getMappingQuality());
		}
	}
	
	protected void compressPOS(SAMRecord[] data, int len) throws IOException 
	{
		for(int i=0; i < len; ++i) {
			enc.write(data[i].getAlignmentStart());
		}
	}
	
	private void compressCIGAR(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			arrayCompress.compressByteArray(data[i].getCigarString().getBytes());
		}
	}
	
	private void compressNEXT_REF(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			enc.write(data[i].getMateReferenceIndex().intValue()+1);
		}
	}
	
	private void compressNEXT_POS(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			enc.write(data[i].getMateAlignmentStart());
		}
	}
	
	private void compressNEXT_TEMP_LEN(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			enc.write(data[i].getInferredInsertSize());
		}
	}
	
	private void compressSEQ(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			arrayCompress.compressByteArray(data[i].getReadBases());
		}
	}
	
	private void compressQUAL(SAMRecord[] data, int len) throws IOException
	{
		byte[] qual;
		for(int i=0; i < len; ++i) {
			qual = data[i].getBaseQualities();
			arrayCompress.compressByteArray(qual);
		}
	}
	
	private void decompressQNAME(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setReadName(new String(arrayCompress.decompressByteArray(0), AbstractArrayCompressor.charset));
		}
	}
	
	private void decompressFLAG(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setFlags(dec.read());
		}
	}
	
	private void decompressREF(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setReferenceIndex(dec.read()-1);
		}
	}
	
	private void decompressMAPQ(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setMappingQuality(dec.read());
		}
	}
	
	private void decompressPOS(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setAlignmentStart(dec.read());
		}
	}
	
	private void decompressCIGAR(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setCigarString(new String(arrayCompress.decompressByteArray(0), AbstractArrayCompressor.charset));
		}
	}
	
	private void decompressNEXT_REF(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setMateReferenceIndex(dec.read()-1);
		}
	}
	
	private void decompressNEXT_POS(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setMateAlignmentStart(dec.read());
		}
	}
	
	private void decompressNEXT_TEMP_LEN(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setInferredInsertSize(dec.read());
		}
	}
	
	private void decompressSEQ(SAMRecord[] data, int len) throws IOException
	{
		int res;
		for(int i=0; i < len; ++i) {
			data[i].setReadBases(arrayCompress.decompressByteArray(0));
		}
	}
	
	private void decompressQUAL(SAMRecord[] data, int len) throws IOException
	{
		for(int i=0; i < len; ++i) {
			data[i].setBaseQualities(arrayCompress.decompressByteArray(data[i].getReadLength()));
		}
	}

	private boolean needsArrayCompressor()
	{
		return processedField.isArray();
	}
}