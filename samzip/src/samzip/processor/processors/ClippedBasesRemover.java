package samzip.processor.processors;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;
import net.sf.samtools.CigarOperator;
import net.sf.samtools.SAMRecord;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;
import samzip.processor.util.SAMField;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ClippedBasesRemover extends SAMProcessor
{

	@Override
	public boolean isValid()
	{
		Set<SAMField> prec = new HashSet<>(Arrays.asList(precedingFields));
		lastError = "";
		if (prec.contains(SAMField.SEQ))
			if (lastError.isEmpty()) {
				lastError = "Some clipped fields were processed before clipped bases remover: SEQ ";
			} 
		if (prec.contains(SAMField.CIGAR))
			if (lastError.isEmpty()) {
				lastError = "Some clipped fields were processed before clipped bases remover: CIGAR ";
			} 
		if (prec.contains(SAMField.QUAL))
			if (lastError.isEmpty()) {
				lastError = "Some clipped fields were processed before clipped bases remover: QUAL ";
			} else
				lastError += "QUAL ";
		if (prec.contains(SAMField.TAG_BQ))
			if (lastError.isEmpty()) {
				lastError = "Some clipped fields were processed before clipped bases remover: TAG:BQ ";
			} else
				lastError += "TAG:BQ ";
		if (prec.contains(SAMField.TAG_CQ))
			if (lastError.isEmpty()) {
				lastError = "Some clipped fields were processed before clipped bases remover: TAG:CQ ";
			} else
				lastError += "TAG:CQ ";
		if (prec.contains(SAMField.TAG_CS))
			if (lastError.isEmpty()) {
				lastError = "Some clipped fields were processed before clipped bases remover: TAG:CS ";
			} else
				lastError += "TAG:CS ";
		if (prec.contains(SAMField.TAG_OQ))
			if (lastError.isEmpty()) {
				lastError = "Some clipped fields were processed before clipped bases remover: TAG:OQ ";
			} else
				lastError += "TAG:OQ ";
		
		return lastError.isEmpty();
	}
	
	@Override
	public String toString()
	{
		return "ClippedBasesRemover";
	}
	
	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		for(int i=0; i < block.length; ++i) {
			
			Cigar cigar = block[i].getCigar();
			if (cigar.isEmpty() || block[i].getReadUnmappedFlag())
				continue;
			
			byte[] bases = block[i].getReadBases();
			byte[] qual = block[i].getBaseQualities();
			String bq_str = block[i].getStringAttribute("BQ");
			byte[] baseQual = null;
			if (bq_str != null)
				baseQual = bq_str.getBytes();
			String cq_str = block[i].getStringAttribute("CQ");
			byte[] colorQual = null;
			if (cq_str != null)
				colorQual = cq_str.getBytes();
			String cs_str = block[i].getStringAttribute("CS");
			byte[] colorSequence = null;
			if (cs_str != null)
				colorSequence = cs_str.getBytes();
			String oq_str = block[i].getStringAttribute("OQ");
			byte[] originalQual = null;
			if (oq_str != null)
				originalQual = oq_str.getBytes();			
			
			int basesOffset = 0;
			// mandatory fields
			byte[] newBases = new byte[bases.length];
			byte[] newQual = new byte[bases.length];
			// attributes
			byte[] newBaseQual = baseQual != null ? new byte[bases.length] : null;
			byte[] newColorQual = colorQual != null ? new byte[bases.length] : null;
			byte[] newColorSequence = colorSequence != null ? new byte[bases.length+1] : null;
			if (colorSequence != null) {
				newColorSequence[0] = colorSequence[0];
			}
			byte[] newOriginalQual = originalQual != null ? new byte[bases.length] : null;
			
			int newBasesOffset = 0;
			Cigar newCigar = new Cigar();
			for(int j=0; j < cigar.numCigarElements(); ++j) {
				CigarElement elem = cigar.getCigarElement(j);
				switch (elem.getOperator()) {
					case S:
						newCigar.add(new CigarElement(elem.getLength(), CigarOperator.H));
						basesOffset += elem.getLength();
						break;
					default:
						newCigar.add(elem);
						if (elem.getOperator().consumesReadBases()) {
							for(int k=0; k < elem.getLength(); ++k) {
								newBases[newBasesOffset] = bases[basesOffset];
								newQual[newBasesOffset] = qual[basesOffset];
								if (baseQual != null)
									newBaseQual[newBasesOffset] = baseQual[basesOffset];
								if (colorQual != null)
									newColorQual[newBasesOffset] = colorQual[basesOffset];
								if (colorSequence != null)
									newColorSequence[newBasesOffset+1] = colorSequence[basesOffset+1];
								if (originalQual != null)
									newOriginalQual[newBasesOffset] = originalQual[basesOffset];
								newBasesOffset++;
								basesOffset++;
							}
						}
				}
			}
			bases = new byte[newBasesOffset];
			System.arraycopy(newBases, 0, bases, 0, newBasesOffset);
			qual = new byte[newBasesOffset];
			System.arraycopy(newQual, 0, qual, 0, newBasesOffset);
			block[i].setReadBases(bases);
			block[i].setCigar(newCigar);
			block[i].setBaseQualities(qual);
			
			if (baseQual != null) {
				baseQual = new byte[newBasesOffset];
				System.arraycopy(newBaseQual, 0, baseQual, 0, newBasesOffset);
				block[i].setAttribute("BQ", new String(baseQual));
			}
			
			if (colorQual != null) {
				colorQual = new byte[newBasesOffset];
				System.arraycopy(newColorQual, 0, colorQual, 0, newBasesOffset);
				block[i].setAttribute("CQ", new String(colorQual));
			}
			
			if (colorSequence != null) {
				colorSequence = new byte[newBasesOffset+1];
				System.arraycopy(newColorSequence, 0, colorSequence, 0, newBasesOffset+1);
				block[i].setAttribute("CS", new String(colorSequence));
			}
			
			if (originalQual != null) {
				originalQual = new byte[newBasesOffset];
				System.arraycopy(newOriginalQual, 0, originalQual, 0, newBasesOffset);
				block[i].setAttribute("OQ", new String(originalQual));
			}
		}
	}

	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		
	}
	
}
