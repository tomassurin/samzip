package samzip.processor.processors;

import java.io.IOException;
import net.sf.samtools.SAMRecord;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.io.BitInputStream;
import samzip.io.BitOutputStream;
import samzip.io.StreamUtils;
import samzip.processor.SAMProcessor;
import samzip.processor.util.AbstractArrayCompressor;
import samzip.processor.util.DelimiterArrayCompressor;
import samzip.processor.util.LengthArrayCompressor;
import samzip.processor.util.SequenceLengthArrayCompressor;
import samzip.api.SamzException;
import samzip.util.PreferencesException;
import samzip.processor.util.SAMField;
import samzip.processor.util.SAMField.TagDataType;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class TagProcessor extends SAMProcessor
{

	private EncoderMethod enc;
	private DecoderMethod dec;
	private BitOutputStream flagOut;
	private BitInputStream flagIn;
	private SAMField processedField = SAMField.NONE;
	private TagDataType nonStandardTagDataType = TagDataType.NONE;
	private AbstractArrayCompressor arrayCompress = new DelimiterArrayCompressor(this);
	protected final static byte[] emptyByteArray = new byte[0];

	@Override
	public String toString()
	{
		return "TAGProcessor/" + processedField.getTag();
	}

	@Override
	public boolean isValid()
	{
		if (getCodecFor("default") == null) {
			lastError = "Default codec wasn't specified.";
			return false;
		}

		if (processedField == SAMField.NONE) {
			lastError = "Processed tag wasn't specified.";
			return false;
		}

		if (needsArrayCompressor()
				&& arrayCompress instanceof LengthArrayCompressor
				&& getCodecFor("length") == null) {
			lastError = "Stream for 'length' wasn't specified.";
			return false;
		}

		if (getCodecFor("flag") == null
				&& (needsFlagCodec() || (arrayCompress instanceof SequenceLengthArrayCompressor))) {
			lastError = "Stream for 'flag' wasn't specified.";
			return false;
		}

		return true;
	}

	public TagProcessor()
	{
		// initialize standard flag and length compression
//		CompressionCodec codec = new HuffmanCodec();
//		//new RunLengthEncodingCodec(new EliasDeltaCodec(), new EliasDeltaCodec());
//		addCodec("flag", codec);
//		
//		addCodec("length", codec);
	}

	void setArrayCompressor(AbstractArrayCompressor arrayUtil)
	{
		this.arrayCompress = arrayUtil;
	}

	void setProcessedTag(String tag) throws PreferencesException
	{
		processedField = SAMField.getSAMField("TAG:" + tag);
		if (processedField == SAMField.NONE) {
			throw new PreferencesException("Unknown tag: " + tag, "tag");
		}
	}

	@Override
	public SAMField[] getProcessedFields()
	{
		if (processedField == SAMField.NONE) {
			return empty;
		}

		SAMField[] result = {processedField};
		return result;
	}

	@Override
	public SAMField[] getDependencies()
	{
		if (arrayCompress instanceof SequenceLengthArrayCompressor) {
			return new SAMField[]{SAMField.SEQ};
		}

		return empty;
	}

	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		switch (whatKey.toLowerCase()) {
			case "tag": {
				String value = preferences.getString(whatKey, "");
				setProcessedTag(value);
				break;
			}
			case "arraycode":
			case "array":
			{
				String value = preferences.getString(whatKey, "codec").toLowerCase();
				if (value.startsWith("delimiter")) {
					int sepIdx = value.indexOf('/');
					if (sepIdx < 0 || sepIdx >= value.length()) 
						arrayCompress = new DelimiterArrayCompressor(this);
					else {
						try {
							String sub = value.substring(sepIdx+1);
							char delim = (char) (int) Integer.decode(sub);
							arrayCompress = new DelimiterArrayCompressor(this, delim);
						} catch (NumberFormatException ex) {
							throw new PreferencesException("Can't parse specified delimiter "+value.substring(sepIdx));
						}
					}
					return;
				}
				switch (value) {
					case "length":
						arrayCompress = new LengthArrayCompressor(this);
						break;
					case "sequence":
						arrayCompress = new SequenceLengthArrayCompressor(this, true);
						break;
					default:
						throw new PreferencesException("Unknown array coding type "+value, whatKey);
				}
				break;
			}
			default:
				super.preferenceChanged(whatKey);
				break;
		}
	}

	private void initializeCompression(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		if (processedField.isTag() && !processedField.isStandardTag()) {
			/// handling non standard tag value
			for (int i = 0; i < block.length; ++i) {
				Object attr = block[i].getAttribute(processedField.getTag());
				if (attr != null) {
					nonStandardTagDataType = SAMField.getTagDataType(attr);
					break;
				}
			}
			StreamUtils.writeByte(TagDataType.toByteValue(nonStandardTagDataType),
					outputBlock.getStream(HEADER_STREAM));
		}

		if (needsArrayCompressor()) {
			arrayCompress.initializeCompression(outputBlock, processedField.toString());
		} else {
			DataMode dataMode = null;
			if (processedField.isInteger()
					|| (!processedField.isStandardTag() && nonStandardTagDataType.isInteger())) {
				dataMode = DataMode.INT;
			}
			enc = getCodecFor("default").newEncoderMethod(outputBlock, processedField.toString(), dataMode);
		}

		if (needsFlagCodec()) {
			EncoderMethod flagEnc = getCodecFor("flag").newEncoderMethod(outputBlock, processedField.toString() + ".flag", DataMode.UBYTE);
			flagOut = new BitOutputStream(flagEnc);
		}
	}

	@Override
	public void compressBlock(SAMRecord[] data, OutputBlock outputBlock) throws IOException
	{
		if (processedField.getTag() == null)
			return;
		
		boolean hasTagData = false;
		for(int i=0; i < data.length; ++i) {
			SAMRecord rec = data[i];
			if (rec.getAttribute(processedField.getTag()) != null) {
				hasTagData = true;
				break;
			}
		}
		
		if (!hasTagData) {
			outputBlock.writeFlag(false);
			return;
		} else {
			outputBlock.writeFlag(true);
		}
		
		
		initializeCompression(data, outputBlock);

		compressTAG(data, data.length, processedField.getTag());

		if (enc != null) {
			enc.close();
		}

		if (arrayCompress != null) {
			arrayCompress.close();
		}

		if (flagOut != null) {
			flagOut.close();
		}
	}

	private boolean needsArrayCompressor()
	{
		if (processedField.isArray()) {
			return true;
		}

		return false;
	}

	private boolean needsFlagCodec()
	{
		if (!processedField.getTagDataType().isArray()) {
			return true;
		}

		return false;
	}

	private void initializeDecompression(InputBlock inputBlock) throws IOException
	{
		if (processedField.isTag() && !processedField.isStandardTag()) {
			/// handling non standard tag value
			nonStandardTagDataType = TagDataType.parseByteValue(StreamUtils.readUnsignedByte(inputBlock.getStream(HEADER_STREAM)));
		}

		if (needsArrayCompressor()) {
			arrayCompress.initializeDecompression(inputBlock);
		} else {
			DataMode dataMode = null;
			if (processedField.isInteger()
					|| (!processedField.isStandardTag() && nonStandardTagDataType.isInteger())) {
				dataMode = DataMode.INT;
			}

			dec = getCodecFor("default").newDecoderMethod(inputBlock, dataMode);
		}

		if (needsFlagCodec()) {
			DecoderMethod flagDec = getCodecFor("flag").newDecoderMethod(inputBlock, DataMode.UBYTE);
			flagIn = new BitInputStream(flagDec);
		}
	}

	@Override
	public void decompressBlock(SAMRecord[] data, InputBlock inputBlock) throws IOException
	{
		if (!inputBlock.readFlag()) {
			return;
		}
		
		initializeDecompression(inputBlock);

		decompressTAG(data, data.length, processedField.getTag());

		if (dec != null) {
			dec.close();
		}

		if (arrayCompress != null) {
			arrayCompress.close();
		}

		if (flagIn != null) {
			flagIn.close();
		}
	}

	private void compressTAG(SAMRecord[] data, int len, String tag) throws IOException
	{
		if (SAMField.isStandardTag(tag)) {
			compressTAG(data, len, tag, SAMField.getTagDataType(tag));
		} else {
			compressTAG(data, len, tag, nonStandardTagDataType);
		}
	}

	//@TODO tag compression (array types, float)
	private void compressTAG(SAMRecord[] data, int len, String tag, SAMField.TagDataType dataType) throws IOException
	{
		switch (dataType) {
			case BYTE:
			case SHORT:
			case INT:
				compressIntegerTag(data, len, tag);
				break;
			case STRING:
				compressStringTag(data, len, tag);
				break;
			case BYTE_ARR:
				compressByteArrayTag(data, len, tag);
				break;
			case CHAR:
				compressCharacterTag(data, len, tag);
				break;
			default: {
				throw new SamzException("Unsupported tag type: " + SAMField.getTagDataType(tag));
			}
		}
	}

	private void compressByteArrayTag(SAMRecord[] data, int len, String tag) throws IOException
	{
		for (int i = 0; i < len; ++i) {
			Object attr = data[i].getAttribute(tag);
			if (attr == null) {
				arrayCompress.compressByteArray(emptyByteArray);
			} else {
				arrayCompress.compressByteArray((byte[]) attr);
			}
		}
	}

	private void compressIntegerTag(SAMRecord[] data, int len, String tag) throws IOException
	{
		for (int i = 0; i < len; ++i) {
			Object attr = data[i].getAttribute(tag);
			if (attr == null) {
				flagOut.writeBit(0);
			} else {
				flagOut.writeBit(1);
				enc.write((int) attr);
			}
		}
	}

	private void compressStringTag(SAMRecord[] data, int len, String tag) throws IOException
	{
		for (int i = 0; i < len; ++i) {
			String attr = (String) data[i].getAttribute(tag);
			if (attr == null) {
				arrayCompress.compressByteArray(emptyByteArray);
			} else {
				byte[] bytes = attr.getBytes(AbstractArrayCompressor.charset);
				arrayCompress.compressByteArray(bytes);
			}
		}
	}

	private void compressCharacterTag(SAMRecord[] data, int len, String tag) throws IOException
	{
		for (int i = 0; i < len; ++i) {
			Object attr = data[i].getAttribute(tag);
			if (attr == null) {
				flagOut.writeBit(0);
			} else {
				flagOut.writeBit(1);
				enc.write((char) attr);
			}
		}
	}

	private void decompressIntegerTag(SAMRecord[] data, int len, String tag) throws IOException
	{
		for (int i = 0; i < len; ++i) {
			if (flagIn.readBit() == 1) {
				data[i].setAttribute(tag, dec.read());
			}
		}
	}

	private void decompressCharacterTag(SAMRecord[] data, int len, String tag) throws IOException
	{
		for (int i = 0; i < len; ++i) {
			if (flagIn.readBit() == 1) {
				data[i].setAttribute(tag, (char) dec.read());
			}
		}
	}

	private void decompressStringTag(SAMRecord[] data, int len, String tag) throws IOException
	{
		for (int i = 0; i < len; ++i) {
			String result = arrayCompress.decompressString(data[i].getReadLength());
			if (!result.isEmpty()) {
				data[i].setAttribute(tag, result);
			}
		}
	}

	private void decompressByteArrayTag(SAMRecord[] data, int len, String tag) throws IOException
	{
		for (int i = 0; i < len; ++i) {
			byte[] result = arrayCompress.decompressByteArray(data[i].getReadLength());
			if (result.length != 0) {
				data[i].setAttribute(tag, result);
			}
		}
	}

	private void decompressTAG(SAMRecord[] data, int len, String tag) throws IOException
	{
		if (SAMField.isStandardTag(tag)) {
			decompressTAG(data, len, tag, SAMField.getTagDataType(tag));
		} else {
			decompressTAG(data, len, tag, nonStandardTagDataType);
		}
	}

	private void decompressTAG(SAMRecord[] data, int len, String tag, TagDataType dataType) throws IOException
	{
		switch (dataType) {
			case BYTE:
			case SHORT:
			case INT:
				decompressIntegerTag(data, len, tag);
				break;
			case STRING:
				decompressStringTag(data, len, tag);
				break;
			case BYTE_ARR:
				decompressByteArrayTag(data, len, tag);
				break;
			case CHAR:
				decompressCharacterTag(data, len, tag);
				break;
			default: {
				throw new SamzException("Unsupported tag type: " + SAMField.getTagDataType(tag));
			}
		}
	}
}
