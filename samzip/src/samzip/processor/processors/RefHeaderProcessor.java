package samzip.processor.processors;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import net.sf.samtools.SAMRecord;
import samzip.compression.numbercode.VByteCode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;
import samzip.processor.util.SAMField;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RefHeaderProcessor extends SAMProcessor
{

	private final static SAMField processedField = SAMField.RNAME;

	@Override
	public SAMField[] getProcessedFields()
	{
		return new SAMField[] { processedField };
	}

	@Override
	public String toString()
	{
		return "RefHeaderProcessor";
	}

	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		if (block.length == 0)
			return;
		
		OutputStream headerStream = outputBlock.getStream(HEADER_STREAM);
		VByteCode.encodeUnsigned(block[0].getReferenceIndex(), headerStream);
	}

	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		if (block.length == 0)
			return;
		
		InputStream headerStream = inputBlock.getStream(HEADER_STREAM);
		int refIdx = VByteCode.decodeUnsigned(headerStream);
		for(int i=0; i < block.length; ++i) {
			block[i].setReferenceIndex(refIdx);
		}
	}	
}
