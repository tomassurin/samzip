package samzip.processor.processors;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import net.sf.samtools.SAMRecord;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;
import samzip.util.PreferencesException;
import samzip.processor.util.SAMField;
import samzip.processor.util.SequenceUtil;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ColorSequenceProcessor extends SAMProcessor
{
	private static final SAMField processedField = SAMField.TAG_CS;

	@Override
	public SAMField[] getProcessedFields()
	{
		return new SAMField[] { processedField };
	}

	@Override
	public SAMField[] getDependencies()
	{
		return new SAMField[] { SAMField.SEQ, SAMField.FLAG };
	}

	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		super.preferenceChanged(whatKey);
	}

	@Override
	public String toString()
	{
		return "ColorSequenceProcessor";
	}
	
	@Override
	public boolean isValid()
	{
		if (getCodecFor("default") == null) {
			lastError = "Default codec wasn't specified.";
			return false;
		}
			
		return true;
	}
	
	private byte colorToInt(char color)
	{
		switch (color) {
			case '0':
				return 0;
			case '1':
				return 1;
			case '2':
				return 2;
			case '3':
				return 3;
			default:
				return 4;
		}
	}
	
	private char intToColor(int val)
	{
		if (val >= 4)
			return '.';
		
		switch (val) {
			case 0:
				return '0';
			case 1:
				return '1';
			case 2:
				return '2';
			case 3:
				return '3';
			default:
				return '.';
		}
	}
	
	private void compressCS(String cs, int len, OutputStream output) throws IOException
	{
		byte[] data = cs.getBytes("UTF-8");
		// write primer
		output.write(SequenceUtil.baseToInt(data[0]));
		
		/// write data
		for(int i=0; i < len; ++i) {
			output.write(colorToInt((char)data[i]));
//			data[i] = colorToInt((char)data[i]);
		}
//		output.write(data, 1, len);
	}
	
	private String decompressCS(InputStream input, int len, byte primer) throws IOException
	{
		StringBuilder ret = new StringBuilder();
		
		ret.append((char)primer);
		for(int i=0; i < len; ++i) {
			ret.append(intToColor(input.read()));
		}
		
		return ret.toString();
	}
	
	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{	
		if (block.length < 1)
			return;
		
		boolean hasTagData = false;
		for(int i=0; i < block.length; ++i) {
			SAMRecord rec = block[i];
			if (rec.getAttribute("CS") != null) {
				hasTagData = true;
				break;
			}
		}
		
		if (!hasTagData) {
			outputBlock.writeFlag(false);
			return;
		} else {
			outputBlock.writeFlag(true);
		}
		
		
		EncoderMethod enc = getCodecFor("default")
				.newEncoderMethod(outputBlock, processedField.toString(), DataMode.UBYTE);
		
		for(int i=0; i < block.length; ++i) {
			String cs = block[i].getStringAttribute("CS");
			if (cs == null)
				enc.write(SequenceUtil.MAX_BASE_ORDINAL);
			else {
				// if failed vendor qulity check output raw CS
				if (block[i].getReadFailsVendorQualityCheckFlag()) {
					compressCS(cs, block[i].getReadLength(), enc);
					continue;
				}
				// write primer
				enc.write(SequenceUtil.baseToInt((byte) cs.charAt(0)));
				
				// write CS for N bases and 1 base following run of N's
				byte[] seq = block[i].getReadBases();
				byte[] tmp = new byte[seq.length];
				System.arraycopy(seq, 0, tmp, 0, seq.length);
				if (block[i].getReadNegativeStrandFlag()) {
					net.sf.samtools.util.SequenceUtil.reverseComplement(tmp);	
				}
				SequenceUtil.canonizeSequence(tmp);
				seq = tmp;
				boolean inNBlock = false;
				for(int j=0; j < seq.length; ++j) {
					if (seq[j] == 'N') {
						enc.write(colorToInt(cs.charAt(j+1)));
						inNBlock = true;
					} else {
						if (inNBlock == true) {
							enc.write(colorToInt(cs.charAt(j+1)));
							inNBlock = false;
						}
					}
				}
			}
		}
		
		enc.close();
	}

	private String calculateCS(byte[] seq, byte primer, boolean reverseStrand) throws IOException
	{
		StringBuilder ret = new StringBuilder();
		
		byte[] tmp = new byte[seq.length];
		System.arraycopy(seq, 0, tmp, 0, seq.length);
		if (reverseStrand) {
			net.sf.samtools.util.SequenceUtil.reverseComplement(tmp);	
		}
		SequenceUtil.canonizeSequence(tmp);
		seq = tmp;
		
		ret.append((char)primer);
		byte last = primer;
		boolean inNBlock = false;
		
		for(int i=0; i < seq.length; ++i) {
			if (inNBlock && seq[i] != 'N') {
				inNBlock = false;
				ret.append(intToColor(dec.read()));
				last = seq[i];
				continue;
			}
			
			if(seq[i] == last) {
				if(seq[i] == 'N'){
					ret.append(intToColor(dec.read()));
					inNBlock = true;
				} else {
					ret.append('0');
				}
			} else if(((last == 'A') && (seq[i] == 'C')) ||
					((last == 'C') && (seq[i] == 'A')) ||
					((last == 'G') && (seq[i] == 'T')) ||
					((last == 'T') && (seq[i] == 'G'))){
					ret.append('1');
			} else if(((last == 'A') && (seq[i] == 'G')) ||
					((last == 'C') && (seq[i] == 'T')) ||
					((last == 'G') && (seq[i] == 'A')) ||
					((last == 'T') && (seq[i] == 'C'))){
					ret.append('2');
			} else if(((last == 'A') && (seq[i] == 'T')) ||
					((last == 'C') && (seq[i] == 'G')) ||
					((last == 'G') && (seq[i] == 'C')) ||
					((last == 'T') && (seq[i] == 'A'))){
					ret.append('3');
			} else if(seq[i] == 'N') {
				ret.append(intToColor(dec.read()));
				inNBlock = true;
			}
			last = seq[i];
		}
		
		return ret.toString();
	}
	
	DecoderMethod dec;
	
	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		if (!inputBlock.readFlag()) {
			return;
		}
		
		dec = getCodecFor("default")
				.newDecoderMethod(inputBlock, DataMode.UBYTE);
		
		for(int i=0; i < block.length; ++i) {
			SAMRecord rec = block[i];
			byte primer = (byte) dec.read();
			if (primer == SequenceUtil.MAX_BASE_ORDINAL)
				continue;
			
			primer = SequenceUtil.intToBase(primer);
			
			if (rec.getReadFailsVendorQualityCheckFlag()) {
				rec.setAttribute("CS", decompressCS(dec, rec.getReadLength(), primer));
			} else {
				rec.setAttribute("CS", 
						calculateCS(rec.getReadBases(), primer, rec.getReadNegativeStrandFlag()));
			}
		}
		
		dec.close();
		dec = null;
	}	
}
