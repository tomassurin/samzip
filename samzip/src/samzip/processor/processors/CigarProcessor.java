package samzip.processor.processors;

import java.io.IOException;
import java.util.List;
import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.TextCigarCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;
import samzip.processor.util.CigarUtil;
import samzip.processor.util.SAMField;
	
/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class CigarProcessor extends SAMProcessor
{	
	private boolean useSingleCigarCodec;
	private EncoderMethod cigarLenEnc;
	private EncoderMethod cigarOpEnc;
	private DecoderMethod cigarLenDec;
	private DecoderMethod cigarOpDec;
	
	private boolean calculate_TAG_NM = true;
	private boolean calculate_TAG_MD = true;

	@Override
	public String toString()
	{
		return "CigarProcessor";
	}
	
	public CigarProcessor()
	{		
	}

	@Override
	public boolean isValid()
	{				
		if (getCodecFor("cigar.len") == null) {
			lastError = "Codec for cigar length wasn't specified. (set it for 'cigar.len')";
			return false;
		}
		if (getCodecFor("cigar.op") == null) {
			lastError = "Codec for cigar operation wasn't specified. (set it for 'cigar.op')";
			return false;
		}
		return true;
	}
	
	@Override
	public SAMField[] getProcessedFields()
	{
		return new SAMField[] {SAMField.CIGAR};
	}	
	
	private void initializeCompression(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		cigarOpEnc = getCodecFor("cigar.op")
					.newEncoderMethod(outputBlock, "CIGAR.op", DataMode.UBYTE);
		cigarLenEnc = getCodecFor("cigar.len")
					.newEncoderMethod(outputBlock,"CIGAR.len", DataMode.UINT);
	}

	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{	
		initializeCompression(block, outputBlock);
		int len = block.length;
		
		TextCigarCodec textCigarCodec = TextCigarCodec.getSingleton();
		for(int i=0; i < len; ++i) {			
			Cigar cigar = block[i].getCigar();
			List<CigarElement> cigarElements = cigar.getCigarElements();
				for(CigarElement elem : cigarElements) {
					cigarOpEnc.write(CigarUtil.cigarOpToInt(elem.getOperator()));
					cigarLenEnc.write(elem.getLength());
				}
				cigarOpEnc.write(CigarUtil.noCigarOP);
			}
		
		cigarOpEnc.close();
		cigarLenEnc.close();
	}
	
	private void initializeDecompression(InputBlock inputBlock) throws IOException
	{
		cigarOpDec = getCodecFor("cigar.op")
					.newDecoderMethod(inputBlock, DataMode.UBYTE);
		cigarLenDec = getCodecFor("cigar.len")
					.newDecoderMethod(inputBlock, DataMode.UINT);
	}

	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		initializeDecompression(inputBlock);
		int len = block.length;
		
		TextCigarCodec textCigarCodec = TextCigarCodec.getSingleton();
		Cigar cigar;
		int seqLen;
		for(int i=0; i < len; ++i) {
			cigar = new Cigar();
			while(true) {
				int op = cigarOpDec.read();
				if (op == CigarUtil.noCigarOP)
					break;
				int length = cigarLenDec.read();
				cigar.add(new CigarElement(length, CigarUtil.intToCigarOp(op)));
			}
			block[i].setCigar(cigar);
		}
		
		cigarOpDec.close();
		cigarLenDec.close();
	}
}