package samzip.processor.processors;

import java.io.IOException;
import java.util.Locale;
import net.sf.samtools.SAMRecord;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;
import samzip.processor.util.AbstractArrayCompressor;
import samzip.processor.util.DelimiterArrayCompressor;
import samzip.util.LOG;
import samzip.util.PreferencesException;
import samzip.processor.util.SAMField;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class QName1000GenomeProcessor extends SAMProcessor
{
	private boolean outputRaw = true;

	public QName1000GenomeProcessor()
	{
	}

	@Override
	public SAMField[] getProcessedFields()
	{
		return new SAMField[] {SAMField.QNAME};
	}

	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		if (whatKey.toLowerCase().equals("raw")) {
			outputRaw = preferences.getBoolean(whatKey, true);
			return;
		}
		super.preferenceChanged(whatKey);
	}

	@Override
	public String toString()
	{
		return "QName1000GenomeProcessor";
	}	
	
	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		EncoderMethod enc = null;
		if (outputRaw)
			enc = getCodecFor("default").newEncoderMethod(outputBlock,"QNAME", DataMode.UBYTE);
		else
			enc = getCodecFor("default").newEncoderMethod(outputBlock,"QNAME", DataMode.UINT);
		AbstractArrayCompressor arr = null;
		if (outputRaw) {
			arr = new DelimiterArrayCompressor(enc, '\n');
		}
		
		for(int i=0; i < block.length; ++i) {
			String qname = block[i].getReadName();
			int idx = qname.indexOf('.');
			if (idx < 0)
				idx = 0;
			String rg = block[i].getStringAttribute("RG");
			if (rg != null) {
				if (!rg.equals(qname.substring(0, idx))) {
					LOG.warning("RG != QNAME/RG");
				}
			}
			String num = qname.substring(idx+1);
			if (outputRaw) {
				arr.compressString(num);
			} else {
				int num_val = 0;
				try {
					num_val = Integer.valueOf(num);
				} catch (NumberFormatException ex) {
					LOG.warning("Bad format for qname.");
				}
				enc.write(num_val);
			}
		}
		
		enc.close();
	}

	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		DecoderMethod dec = null;
		if (outputRaw)
			dec = getCodecFor("default").newDecoderMethod(inputBlock, DataMode.UBYTE);
		else
			dec = getCodecFor("default").newDecoderMethod(inputBlock, DataMode.UINT);
		AbstractArrayCompressor arr = null;
		if (outputRaw) {
			arr = new DelimiterArrayCompressor(dec, '\n');
		}
		
		for(int i=0; i < block.length; ++i) {
			String rg = block[i].getStringAttribute("RG");
			if (outputRaw) {
				String num = arr.decompressString();
				if (rg != null && num != null) {
					block[i].setReadName(String.format(Locale.US, "%1$s.%2$s", rg, num));
				}
			} else {
				int num_val = dec.read();
				if (rg != null && num_val != 0) {
					block[i].setReadName(String.format(Locale.US,"%1$s.%2$d", rg, num_val));
				}
			}
		}
		
		dec.close();
	}
	
}
