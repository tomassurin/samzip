package samzip.processor.processors;

import java.io.IOException;
import java.io.InputStream;
import net.sf.samtools.SAMRecord;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.io.BitInputStream;
import samzip.io.BitOutputStream;
import samzip.processor.SAMProcessor;
import samzip.processor.util.SAMField;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class NextSegmentProcessor extends SAMProcessor
{
	private static final SAMField[] processedFields = {SAMField.PNEXT, SAMField.RNEXT, SAMField.TLEN};
	private static final SAMField[] dependsOnFields = {SAMField.FLAG, SAMField.POS, SAMField.RNAME};
	private EncoderMethod refEnc;
	private BitOutputStream refFlagOut;
	private EncoderMethod posEnc;
	private EncoderMethod tempLenEnc;
	private DecoderMethod refDec;
	private BitInputStream refFlagIn;
	private DecoderMethod posDec;
	private DecoderMethod tempLenDec;

	@Override
	public SAMField[] getProcessedFields()
	{
		return processedFields;
	}

	@Override
	public SAMField[] getDependencies()
	{
		return dependsOnFields;
	}

	@Override
	public String toString()
	{
		return "NextSegmentProcessor";
	}

//	{
//		addCodec("ref", new EliasDeltaCodec());
//		addCodec("ref.flag", new HuffmanCodec());
//		addCodec("pos", new VByteCodec());
//		addCodec("tempLen", new HuffmanCodec());
//	}
	@Override
	public boolean isValid()
	{
		if (getCodecFor("ref") == null) {
			lastError = "Stream for 'ref' field wasn't specified.";
			return false;
		}

		if (getCodecFor("ref.flag") == null) {
			lastError = "Stream for 'ref.flag' wasn't specified.";
			return false;
		}

		if (getCodecFor("pos") == null) {
			lastError = "Stream for 'pos' wasn't specified.";
			return false;
		}

		if (getCodecFor("tempLen") == null) {
			lastError = "Stream for 'tempLen' wasn't specified.";
			return false;
		}

		return true;
	}

	private void initializeCompression(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		refEnc = getCodecFor("ref").newEncoderMethod(outputBlock, "NEXT_REF", DataMode.UINT);

		EncoderMethod refFlagEnc = getCodecFor("ref.flag").newEncoderMethod(outputBlock, "NEXT_REF.flag", DataMode.UBYTE);
		refFlagOut = new BitOutputStream(refFlagEnc);

		posEnc = getCodecFor("pos").newEncoderMethod(outputBlock, "NEXT_POS", DataMode.INT);

		tempLenEnc = getCodecFor("tempLen").newEncoderMethod(outputBlock, "NEXT_TEMP_LEN", DataMode.INT);
	}

	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		int len = block.length;

		boolean hasNextData = false;
		for(int i=0; i < len; ++i) {
			SAMRecord rec = block[i];
			if (rec.getMateReferenceIndex() != -1 || 
					rec.getMateAlignmentStart() != 0 ||
					rec.getInferredInsertSize() != 0) {
				hasNextData = true;
				break;
			}
		}
		
		if (!hasNextData) {
			outputBlock.writeFlag(false);
			return;
		} else {
			outputBlock.writeFlag(true);
		}
	
		initializeCompression(block, outputBlock);
		
		for (int i = 0; i < len; ++i) {
			SAMRecord rec = block[i];

			/// store mate's reference
			if (rec.getReferenceIndex() == rec.getMateReferenceIndex()) {
				refFlagOut.writeBit(0);
			} else {
				refFlagOut.writeBit(1);
				refEnc.write(rec.getMateReferenceIndex() + 1);
			}

			/// store mate's position
			if (rec.getReadPairedFlag() && rec.getProperPairFlag()) {
				int diff = rec.getAlignmentStart() - rec.getMateAlignmentStart();
				posEnc.write(diff);
//				posEnc.write(diff < 0 ? -diff : diff);
			} else {
				posEnc.write(rec.getMateAlignmentStart());
			}

			int tempLen = rec.getInferredInsertSize();
//			tempLen = tempLen > 0 ? tempLen : -tempLen;
			tempLenEnc.write(tempLen);
		}

		refEnc.close();
		refFlagOut.close();
		posEnc.close();
		tempLenEnc.close();
	}

	private void initializeDecompression(InputBlock inputBlock) throws IOException
	{
		refDec = getCodecFor("ref").newDecoderMethod(inputBlock, DataMode.UINT);

		DecoderMethod refFlagDec = getCodecFor("ref.flag").newDecoderMethod(inputBlock, DataMode.UBYTE);
		refFlagIn = new BitInputStream(refFlagDec);

		posDec = getCodecFor("pos")
				   .newDecoderMethod(inputBlock, DataMode.INT);

		tempLenDec = getCodecFor("tempLen")
				      .newDecoderMethod(inputBlock, DataMode.INT);
	}

	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		InputStream headerStream = inputBlock.getStream(HEADER_STREAM);
		if (!inputBlock.readFlag()) {
			return;
		}
		
		initializeDecompression(inputBlock);
		int len = block.length;

		for (int i = 0; i < len; ++i) {
			SAMRecord rec = block[i];

			/// reference
			if (refFlagIn.readBit() == 0) {
				rec.setMateReferenceIndex(rec.getReferenceIndex());
			} else {
				rec.setMateReferenceIndex(refDec.read() - 1);
			}

			/// position
			if (rec.getReadPairedFlag() && rec.getProperPairFlag()) {
				int diff = posDec.read();
//				diff = rec.getReadNegativeStrandFlag() ? diff : -diff;
				rec.setMateAlignmentStart(rec.getAlignmentStart() - diff);
			} else {
				rec.setMateAlignmentStart(posDec.read());
			}

			/// tempLen
//			if (rec.getMateAlignmentStart() < rec.getAlignmentStart()) {
//				rec.setInferredInsertSize(-tempLenDec.read());
//			} else {
				rec.setInferredInsertSize(tempLenDec.read());
//			}
		}

		refDec.close();
		refFlagIn.close();
		posDec.close();
		tempLenDec.close();
	}
}
