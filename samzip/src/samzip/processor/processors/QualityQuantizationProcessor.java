package samzip.processor.processors;

import java.io.IOException;
import net.sf.samtools.SAMRecord;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class QualityQuantizationProcessor extends SAMProcessor
{

	private int quantizeLevel = 8;
	
	@Override
	public String toString()
	{
		return "QualityQuantizationProcessor";
	}
	
	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		switch (whatKey.toLowerCase()) {
			case "level":
				{
					quantizeLevel = preferences.getInteger(whatKey, 1);
					break;
				}
			default:
				super.preferenceChanged(whatKey);
		}
	}

	
	private static byte quantize(byte val, int level)
	{
		return (byte) (Math.round(val / level)*level);
	}
	
	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		for(int i=0; i < block.length; ++i) 
		{		
			byte[] qual = block[i].getBaseQualities();
			byte[] newQual = new byte[qual.length];
			for(int j=0; j < qual.length; ++j) {
				newQual[j] = quantize(qual[j], quantizeLevel);
			}
			block[i].setBaseQualities(newQual);
			
		}
	}

	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		
	}
	
}
