package samzip.processor.processors;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.SAMRecord.SAMTagAndValue;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.io.BinaryInputStream;
import samzip.io.BinaryOutputStream;
import samzip.processor.SAMProcessor;
import samzip.processor.util.SAMField;
import samzip.processor.util.SAMField.TagDataType;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RawTagProcessor extends SAMProcessor
{
	public final static Charset charset = Charset.forName("UTF-8");
	
	private Set<String> excludedTags = new HashSet<>();
	
	private EncoderMethod enc;
	private DecoderMethod dec;
	
	public RawTagProcessor()
	{
	}

	@Override
	public SAMField[] getProcessedFields()
	{
		SAMField[] stdTags = SAMField.getStandardTags();
		List<SAMField> result = new LinkedList<>();
		for(SAMField f : stdTags) {
			if (!excludedTags.contains(f.getTag()))
				result.add(f);
		}
		return result.toArray(new SAMField[0]);
	}

	@Override
	public String toString()
	{
		return "RAWTagProcessor";
	}

	@Override
	public boolean isValid()
	{
		if (getCodecFor("default") == null) {
			lastError = "Default codec wasn't specified.";
			return false;
		}
		
		return true;
	}
	
	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		switch (whatKey.toLowerCase()) {
			case "excluded":
				{
					String value = preferences.getString(whatKey, "");
					String[] exc = value.split("\\s");
					excludedTags.addAll(Arrays.asList(exc));
					break;
				}
			default:
				super.preferenceChanged(whatKey);
		}
	}

	private void initializeCompression(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		enc = getCodecFor("default").newEncoderMethod(outputBlock, "TAGS", DataMode.UBYTE);
	}

	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		initializeCompression(block, outputBlock);
		int len = block.length;
		
		BinaryOutputStream output = new BinaryOutputStream(enc);
		int n;
		for(int i=0; i < len; ++i) {
			List<SAMTagAndValue> attributes = block[i].getAttributes();
			
			n = 0;
			for(SAMTagAndValue attr : attributes) {
				if (excludedTags.contains(attr.tag) || attr.tag.length() < 2) 
					continue;
				++n;
			}
			output.writeByte(n);
			
			for(SAMTagAndValue attr : attributes) {
				if (excludedTags.contains(attr.tag) || attr.tag.length() < 2) 
					continue;
				
				output.writeByte(attr.tag.charAt(0));
				output.writeByte(attr.tag.charAt(1));
				
				TagDataType dataType = TagDataType.getType(attr.value);
				output.write(TagDataType.toByteValue(dataType));
				
				switch(dataType) {
					case INT:
						output.writeInt((int)attr.value);
						break;
					case BYTE:
						output.write((byte)attr.value);
						break;
					case SHORT:
						output.writeShort((short) attr.value);
						break;
					case CHAR:
						output.write((char)attr.value);
						break;
					case FLOAT:
						output.writeFloat((float)attr.value);
						break;
					case STRING:
					{
						byte[] data = ((String)attr.value).getBytes(charset);
						output.writeShort(data.length);
						output.write(data);
						break;
					}
					case BYTE_ARR:
					{
						byte[] data = (byte[]) attr.value;						
						output.writeShort(data.length);
						output.write(data);
						break;
					}
					case INT_ARR:
					{
						int[] data = (int[]) attr.value;
						output.writeShort(data.length);
						for(int k=0; k < data.length; ++i) {
							output.writeInt(data[k]);
						}
						break;
					}
					case SHORT_ARR:
					{
						short[] data = (short[]) attr.value;
						output.writeShort(data.length);
						for(int k=0; k < data.length; ++i) {
							output.writeShort(data[k]);
						}
						break;
					}
					case FLOAT_ARR:
					{
						float[] data = (float[]) attr.value;						
						output.writeShort(data.length);
						for(int k=0; k < data.length; ++i) {
							output.writeFloat(data[k]);
						}
						break;
					}
					default: { }
				}
			}
		}
		
		output.close();
	}

	private void initializeDecompression(InputBlock inputBlock) throws IOException
	{
		dec = getCodecFor("default").newDecoderMethod(inputBlock, DataMode.UBYTE);
	}

	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		int len = block.length;
		initializeDecompression(inputBlock);
		
		byte[] buf = new byte[2];
		BinaryInputStream input = new BinaryInputStream(dec);
		for(int i=0; i < len; ++i) {
			int n = input.readUnsignedByte();
			
			for(int j=0; j < n; ++j) {
				buf[0] = input.readByte();
				buf[1] = input.readByte();
				String tag = new String(buf,charset);
				
				TagDataType dataType = SAMField.TagDataType.parseByteValue(input.readByte());
				
				switch(dataType) {
					case INT:
						block[i].setAttribute(tag, (int) input.readInt());
						break;
					case BYTE:
						block[i].setAttribute(tag, input.readByte());
						break;
					case SHORT:
						block[i].setAttribute(tag, input.readShort());
						break;
					case CHAR:
						block[i].setAttribute(tag, (char) input.readByte());
						break;
					case FLOAT:
						block[i].setAttribute(tag, input.readFloat());
						break;
					case STRING:
					{
					    int l = input.readUnsignedShort();
						byte[] data = new byte[l];
						input.read(data, 0, l);
						block[i].setAttribute(tag, new String(data, charset));
						break;
					}
					case BYTE_ARR:
					{
						int l = input.readUnsignedShort();
						byte[] data = new byte[l];
						input.read(data);
						block[i].setAttribute(tag, data);
						break;
					}
					case INT_ARR:
					{
						int l = input.readUnsignedShort();
						int[] data = new int[l];
						for(int k=0; k < l; ++k) {
							data[k] = input.readInt();
						}
						block[i].setAttribute(tag, data);
						break;
					}
					case SHORT_ARR:
					{
						int l = input.readUnsignedShort();
						short[] data = new short[l];
						for(int k=0; k < l; ++k) {
							data[k] = input.readShort();
						}
						block[i].setAttribute(tag, data);
						break;
					}
					case FLOAT_ARR:
					{
						int l = input.readUnsignedShort();
						float[] data = new float[l];
						for(int k=0; k < l; ++k) {
							data[k] = input.readFloat();
						}
						block[i].setAttribute(tag, data);
						break;
					}
					default: { }
				}
			}
		}
		input.close();
	}

	@Override
	public void addPrecedingFields(SAMField[] fields)
	{
		super.addPrecedingFields(fields);
		addPreceedingTagsToExcluded();
	}
	
	private void addPreceedingTagsToExcluded()
	{
		for(SAMField f : precedingFields) {
			if (f.isTag()) {
				excludedTags.add(f.getTag());
			}
		}
	}
}