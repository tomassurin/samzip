package samzip.processor.processors;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.TextCigarCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;
import samzip.processor.reference.FastaReferenceSequence;
import samzip.processor.reference.ReferenceSequenceUtils;
import samzip.processor.reference.ReferenceSequenceUtils.SequenceData;
import samzip.processor.util.AbstractArrayCompressor;
import samzip.processor.util.DelimiterArrayCompressor;
import samzip.util.PreferencesException;
import samzip.processor.util.CigarUtil;
import samzip.processor.util.SAMField;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ReferenceSequenceProcessor extends SAMProcessor
{	
	protected static final SAMField[] dependentFields = {SAMField.RNAME, SAMField.POS};
	
	private FastaReferenceSequence reference;
	private String referenceFile = "";
	
	private ReferenceSequenceUtils diffSeq = new ReferenceSequenceUtils();

	private EncoderMethod seqEnc;
	private DecoderMethod seqDec;	
	
	private AbstractArrayCompressor arrc;
	private static final char delimiter = '\n';

	private boolean useSingleCigarCodec;
	private EncoderMethod cigarLenEnc;
	private EncoderMethod cigarOpEnc;
	private DecoderMethod cigarLenDec;
	private DecoderMethod cigarOpDec;
	
	private boolean calculate_TAG_NM = true;
	private boolean calculate_TAG_MD = true;
	private int minimalEqLength = 1;

	@Override
	public String toString()
	{
		return "SEQProcessorWithReference";
	}
	
	public ReferenceSequenceProcessor()
	{		
	}

	@Override
	public boolean isValid()
	{	
		if (getCodecFor("seq") == null) {
			lastError = "Seq codec wasn't specified. (set codec for 'seq')";
			return false;
		}
		
		if ((getCodecFor("cigar.len") == null || getCodecFor("cigar.op") == null) 
				&& getCodecFor("cigar") == null) {
			lastError = "Cigar codec wasn't specified. (set codec for 'cigar' or codecs for 'cigar.len' and 'cigar.op')";
			return false;
		}
		
		return true;
	}

	protected void mapReferenceFile(String path, String whatKey) throws PreferencesException
	{
		if (!path.equals(referenceFile)) {
			if (reference != null) {
				try {
					reference.close();
				} catch (IOException ex) {
					throw new PreferencesException("Can't close existing fasta reference file", ex, whatKey);
				}
			}
			if (!Files.exists(Paths.get(path)))
				throw new PreferencesException("Reference file "+path+" does not exist", whatKey);
			try {
				this.reference = new FastaReferenceSequence(new File(path));
			} catch (IOException | IllegalArgumentException ex) {
				throw new PreferencesException("Problem with reference file: "+path, ex, whatKey);
			}
			diffSeq = new ReferenceSequenceUtils(reference);
			diffSeq.setMinimumEqualLength(minimalEqLength);
			referenceFile = path;
		}
	}
	
	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		switch (whatKey.toLowerCase()) {
			case "reference":
			case "ref":
			{
				String value = preferences.getString(whatKey, referenceFile);
				value = Paths.get(preferences.getString(ENVIRONMENT_VAR_PREFIX+"input_dir", ""), value).normalize().toString();
				mapReferenceFile(value, whatKey);
				break;
			}
			case ENVIRONMENT_VAR_PREFIX+"reference":
			case ENVIRONMENT_VAR_PREFIX+"ref":
			{
//				System.out.println(preferences.getString(ENVIRONMENT_VAR_PREFIX+"cwd", ""));
				String value = preferences.getString(whatKey, referenceFile);
//				value = Paths.get(preferences.getString(ENVIRONMENT_VAR_PREFIX+"input_dir", ""), value).normalize().toString();
				mapReferenceFile(value, whatKey);
				break;
			}
			case "calculateNM":
			case "calcNM":
				this.calculate_TAG_NM = preferences.getBoolean(whatKey, true);
				break;
			case "calculateMD":
			case "calcMD":
				this.calculate_TAG_MD = preferences.getBoolean(whatKey, true);
				break;
			case "mineqlen":
				this.minimalEqLength = preferences.getInteger(whatKey, 1);
				diffSeq.setMinimumEqualLength(minimalEqLength);
				break;
			default: {
				super.preferenceChanged(whatKey); 
			}
		}
		
	}
	
	public ReferenceSequenceProcessor(File referenceSequence) throws IOException, PreferencesException
	{
		setPreference("reference", referenceSequence.toString());
	}
	
	private boolean calculate_TAG_NM()
	{
		return calculate_TAG_NM && reference != null;
	}
	
	private boolean calculate_TAG_MD()
	{
		return calculate_TAG_MD && reference != null;
	}
	
	@Override
	public SAMField[] getProcessedFields()
	{
		List<SAMField> data = new ArrayList<>();
		data.add(SAMField.SEQ);
		data.add(SAMField.CIGAR);
		if (calculate_TAG_NM()) {
			data.add(SAMField.TAG_NM);
		}
		if (calculate_TAG_MD()) {
			data.add(SAMField.TAG_MD);
		}
		return data.toArray(new SAMField[0]);
	}	
	
	@Override
	public SAMField[] getDependencies()
	{
		return dependentFields;
	}
	
	private void initializeCompression(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{
		seqEnc = getCodecFor("seq")
				.newEncoderMethod(outputBlock,"SEQ", DataMode.UBYTE);
		
		useSingleCigarCodec = (getCodecFor("cigar.op") == null ||  getCodecFor("cigar.len") == null);
		
		if (useSingleCigarCodec) {
			EncoderMethod cigarEnc = getCodecFor("cigar")
					.newEncoderMethod(outputBlock, "CIGAR", DataMode.UBYTE);
			arrc = new DelimiterArrayCompressor(cigarEnc, delimiter);
		} else {
			cigarOpEnc = getCodecFor("cigar.op")
					.newEncoderMethod(outputBlock, "CIGAR.op", DataMode.UBYTE);
			cigarLenEnc = getCodecFor("cigar.len")
					.newEncoderMethod(outputBlock,"CIGAR.len", DataMode.UINT);
		}
	}

	@Override
	public void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException
	{	
		initializeCompression(block, outputBlock);
		int len = block.length;
		
		TextCigarCodec textCigarCodec = TextCigarCodec.getSingleton();
		for(int i=0; i < len; ++i) {			
			SequenceData diff = diffSeq.createDiffToReference(block[i]);
			
			if (useSingleCigarCodec) {
				if (diff.cigar.isEmpty()) {
					arrc.compressString("*"+diff.seq.length);
				} else {
					arrc.compressString(textCigarCodec.encode(diff.cigar));
				}
			} else {
				List<CigarElement> cigarElements = diff.cigar.getCigarElements();				
				for(CigarElement elem : cigarElements) {
					cigarOpEnc.write(CigarUtil.cigarOpToInt(elem.getOperator()));
					cigarLenEnc.write(elem.getLength());
				}
				cigarOpEnc.write(CigarUtil.noCigarOP);
				if (diff.cigar.isEmpty()) {
					cigarLenEnc.write(diff.seq.length);
				}
			}
			seqEnc.write(diff.seq);
		}
		
		if (useSingleCigarCodec) {
			arrc.close();
		}
		else {
			cigarOpEnc.close();
			cigarLenEnc.close();
		}
		seqEnc.close();
	}
	
	private void initializeDecompression(InputBlock inputBlock) throws IOException
	{
		seqDec = getCodecFor("seq")
				.newDecoderMethod(inputBlock, DataMode.UBYTE);
		
		useSingleCigarCodec = ( getCodecFor("cigar.op") == null || getCodecFor("cigar.len") == null);
		
		if (useSingleCigarCodec) {
			DecoderMethod cigarDec = getCodecFor("cigar")
					.newDecoderMethod(inputBlock, DataMode.UBYTE);
			arrc = new DelimiterArrayCompressor(cigarDec, delimiter);
		} else {
			cigarOpDec = getCodecFor("cigar.op")
					.newDecoderMethod(inputBlock, DataMode.UBYTE);
			cigarLenDec = getCodecFor("cigar.len")
					.newDecoderMethod(inputBlock, DataMode.UINT);
		}
	}
	
	@Override
	public void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException
	{
		initializeDecompression(inputBlock);
		int len = block.length;
		
		TextCigarCodec textCigarCodec = TextCigarCodec.getSingleton();
		Cigar cigar;
		int seqLen;
		for(int i=0; i < len; ++i) {
			if (useSingleCigarCodec) {
				String cigarStr = arrc.decompressString();
				if (cigarStr.startsWith("*")) {
					cigar = new Cigar();
					seqLen = Integer.parseInt(cigarStr.substring(1));
				} else {
					cigar = textCigarCodec.decode(cigarStr);
					seqLen = ReferenceSequenceUtils.getDiffSequenceLength(cigar);
				}
			} else {
				cigar = new Cigar();
				while(true) {
					int op = cigarOpDec.read();
					if (op == CigarUtil.noCigarOP)
						break;
					int length = cigarLenDec.read();
					cigar.add(new CigarElement(length, CigarUtil.intToCigarOp(op)));
				}
				
				if (cigar.isEmpty()) {
					seqLen = cigarLenDec.read();
				} else {
					seqLen = ReferenceSequenceUtils.getDiffSequenceLength(cigar);
				}
			}
			byte[] seq = new byte[seqLen];
			seqDec.read(seq);
			
			diffSeq.reconstructOriginalSAMRecord(cigar, seq, block[i], 
					calculate_TAG_MD() && !block[i].getReadUnmappedFlag(), 
					calculate_TAG_NM() && !block[i].getReadUnmappedFlag());
		}
		
		if (useSingleCigarCodec)
			arrc.close();
		else {
			cigarOpDec.close();
			cigarLenDec.close();
		}
		seqDec.close();
	}

	@Override
	public void reset() throws IOException
	{
		
		diffSeq.reset();
		super.reset();
	}
}