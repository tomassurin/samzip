package samzip.processor;

import java.io.IOException;
import java.util.*;
import net.sf.samtools.SAMRecord;
import samzip.compression.CompressionCodec;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.util.SAMField;
import samzip.processor.util.SAMProcessorException;
import samzip.util.MethodWithPreferences;
import samzip.util.PreferencesException;

/**
 * Base class for SAM file processing elements. 
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public abstract class SAMProcessor extends MethodWithPreferences
{	
	public static final int HEADER_STREAM = 0;
	public static final String ENVIRONMENT_VAR_PREFIX = "env_";
	
	protected static final SAMField[] empty = {};
	protected static final String[] emptyStr = {};
	
	protected SAMField[] precedingFields;
	protected String lastError = "";
		
	private Map<String, CompressionCodec> codecs;
		
	public SAMProcessor()
	{	
	}
	
	public abstract void compressBlock(SAMRecord[] block, OutputBlock outputBlock) throws IOException;
	public abstract void decompressBlock(SAMRecord[] block, InputBlock inputBlock) throws IOException;	
	
	public void initialize() throws SAMProcessorException
	{
		try {
			preferencesChanged();
		} catch (PreferencesException ex) {
			throw new SAMProcessorException("Problem with processor option: "+ex.getOption(), ex, this);
		}
		
		/// shared codecs validation
		if (codecs == null)
			return;
		
		Set<CompressionCodec> sharedCodecs = new HashSet<>();
		for(CompressionCodec codec : codecs.values()) {
			Set<CompressionCodec> sharedTmp = codec.getSharedCodecs();
			StringBuilder error = new StringBuilder();
			for(CompressionCodec str : sharedTmp) {
				if (sharedCodecs.contains(str)) {
					error.append(str.getId()).append(" ");
				}
			}
			if (!error.toString().isEmpty()) {
				throw new SAMProcessorException("Shared codecs are being used more than once in a single processor ("+error.toString().trim()+")", this);
			}
			sharedCodecs.addAll(sharedTmp);
		}
	}
	
	public void reset() throws IOException
	{
		if (codecs != null) {
			for(CompressionCodec codec : codecs.values()) {
				codec.reset();
			}
		}
	}
	
	/** {@inheritDoc} */
	@Override
	public void addCodec(String codecID, CompressionCodec codec)
	{
		if (codec == null || codecID == null)
			return;
		
		codecID = codecID.toLowerCase();
		if (codecs == null) {
			codecs = new HashMap<>();
		}
		codecs.put(codecID, codec);
	}
		
	public CompressionCodec getCodecFor(String id)
	{
		id = id.toLowerCase();
		
		if (codecs == null)
			return null;
		
		if (codecs.containsKey(id)) {
			return codecs.get(id);
		}

		return null;
	}
	
	@Override
	public void setPreference(String key, String value) throws PreferencesException
	{
		preferences.setString(key, value);
	}
	
	public SAMField[] getProcessedFields()
	{
		return empty;
	}
	
	public SAMField[] getDependencies()
	{
		return empty;
	}
	
	public boolean isValid()
	{
		return true;
	}
	
	public String getLastError()
	{
		return lastError;
	}	
	
	public static String getValidationErrors(SAMProcessor[] processors)
	{
		StringBuilder configError = new StringBuilder();
		
		/// ckeck if processors are correctly configured - isValid returns true
		for(SAMProcessor p : processors) {
			if (!p.isValid()) {
				configError.append("Processor ")
					.append(p.toString())
					.append(" is not valid. Reason: ")
					.append(p.getLastError())
					.append("\n");
			}
		}
		
		return configError.toString();
	}
	
	public static String getDependencyErrors(SAMProcessor[] processors)
	{
		StringBuilder dependencyError = new StringBuilder();
		Set<SAMField> samFields = new HashSet<>();
		
		/// check dependencies
		for(SAMProcessor p: processors) {
			samFields.addAll(Arrays.asList(p.getProcessedFields()));
			Set<SAMField> unsatisfiedDependencies = new HashSet<>(Arrays.asList(p.getDependencies()));
			unsatisfiedDependencies.removeAll(samFields);
			if (!unsatisfiedDependencies.isEmpty()) {
				dependencyError.append("Dependencies are not met for processor ")
					.append(p.toString())
					.append(":");
				for(SAMField f : unsatisfiedDependencies) {
					dependencyError.append(" ");
					dependencyError.append(f.toString());
				}
				dependencyError.append("\n");
			}
		}
		
		return dependencyError.toString();
	}
	
	public static String getMandatoryFieldsErrors(SAMProcessor[] processors)
	{
		StringBuilder mandatoryError = new StringBuilder();
		Set<SAMField> mandatoryFields = new HashSet<>();
		mandatoryFields.addAll(Arrays.asList(SAMField.getMandatoryFields()));
		
		/// remove processed fields from mandatory fields
		for(SAMProcessor p: processors) {
			
			mandatoryFields.removeAll(Arrays.asList(p.getProcessedFields()));
		}
		
		/// check mandatory fields
		if (!mandatoryFields.isEmpty()) {
			mandatoryError.append("Some mandatory fields are not processed: ");
			for(SAMField f : mandatoryFields) {
				mandatoryError.append(f.toString());
				mandatoryError.append(" ");
			}
			mandatoryError.append("\n");
		}
		
		return mandatoryError.toString();
	}
	
	public static String getMultipleProcessingErrors(SAMProcessor[] processors)
	{
		StringBuilder error = new StringBuilder();
		Set<SAMField> fields = new HashSet<>();
		
		for(SAMProcessor p : processors) {
			SAMField[] processed = p.getProcessedFields();
			for(SAMField f : processed) {
				if (fields.contains(f)) {
					error.append("Field ")
						 .append(f)
						 .append(" is being processed more than once\n");
				}
			}
			fields.addAll(Arrays.asList(processed));
		}
		
		return error.toString();
	}

	public void addPrecedingFields(SAMField[] fields)
	{
		this.precedingFields = fields;
	}
}
