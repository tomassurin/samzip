package samzip.processor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import net.sf.samtools.SAMFileHeader;
import net.sf.samtools.SAMRecord;
import samzip.api.SamzException;
import samzip.compression.numbercode.VByteCode;
import samzip.io.BinaryBlockFile;
import samzip.processor.SAMZipProfileReader.ProfileException;
import samzip.processor.util.SAMField;
import samzip.processor.util.SAMProcessorException;
import samzip.util.PreferencesException;
import samzip.util.SAMZipStats.BlockCompressionStats;
import samzip.util.StatUtil;

/**
 * Class representing SAM processor pipeline. It handles compression/decompression of single block.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public final class SAMProcessorPipeline
{
	/// array of SAMProcessors which will be sequentially called while compressing
	private SAMProcessor[] processors;
	/// stats
	private BlockCompressionStats stats;
	private StatUtil stat = StatUtil.newInstance();
	private boolean calculateStats = false;
	
	private int gcNotRunFor = 0;
	private int GC_NOT_RUN_LIMIT = 10;
	
	/// pipeline environment variables
	private Map<String, String> environment = new HashMap<>();
	
	private SAMFileHeader samHeader;
	private Set<SAMField> processedFields = new HashSet<>();

	public SAMProcessorPipeline(InputStream profileStream) throws IOException, ProfileException
	{
		this(SAMZipProfileReader.readProfile(profileStream));
	}

	public SAMProcessorPipeline(byte[] profileData) throws ProfileException, IOException
	{
		this(SAMZipProfileReader.readProfile(profileData));
	}

	public SAMProcessorPipeline(ProfileConfiguration config)
	{
		processors = new SAMProcessor[config.processors.size()];
		for (int i = 0; i < config.processors.size(); ++i) {
			SAMProcessor unit = config.processors.get(i);
			processors[i] = unit;
		}
	}

	public void setEnvironmentVar(String key, String value)
	{
		this.environment.put(key, value);
		for (SAMProcessor p : processors) {
			try {
				p.setPreference(SAMProcessor.ENVIRONMENT_VAR_PREFIX + key, value);
			} catch (PreferencesException ex) {
				throw new SamzException("Illegal environment variable (" + ex.getMessage() + ")", ex);
			}
		}
	}

	/** Initialize SAM processors in pipeline. */
	public void initialize() throws SAMProcessorException
	{
		for (SAMProcessor p : processors) {
			p.addPrecedingFields(processedFields.toArray(new SAMField[0]));
			p.initialize();
			processedFields.addAll(Arrays.asList(p.getProcessedFields()));
		}
	}

	/** Compress array of records {@code data} to output block {@code block}. */
	public void compress(SAMRecord[] data, BinaryBlockFile.OutputBlock block) throws IOException
	{
		OutputStream headerStream = block.newStream("header");
		VByteCode.encodeUnsigned(data.length, headerStream);
		
		if (calculateStats) {
			stats = new BlockCompressionStats();
			for (int i = 0; i < processors.length; ++i) {
				stats.processorName.add(processors[i].toString());
			}

			stats.alignmentsCount = data.length;
			
			for (final SAMProcessor p : processors) {
				stat.startCounter();
					p.compressBlock(data, block);
				stats.compressionTime.add(stat.elapsedTime());
			}

			int i = 0;
			long compressTime;
			for (SAMProcessor p : processors) {
				stat.startCounter();
					p.reset();
				compressTime = stats.compressionTime.get(i) + stat.elapsedTime();
				stats.compressionTime.set(i, compressTime);
				++i;
			}
			
			for (BinaryBlockFile.MemoryStream stream : block.getStreams()) {
				stats.streamSize.add((long)stream.getLength());
				stats.streamName.add(stream.getId());
			}
		} else {
			for (SAMProcessor p : processors) {
				p.compressBlock(data, block);
			}
			for (SAMProcessor p : processors) {
				p.reset();
			}
		}

		if (gcNotRunFor++ >= GC_NOT_RUN_LIMIT) {
			// run garbage collector
			gcNotRunFor = 0;
			System.gc();
		}
	}

	/** Decompress array of records from input block {@code block}. */
	public SAMRecord[] decompress(BinaryBlockFile.InputBlock block) throws IOException
	{
		// get number of alignments in the current block
		InputStream headerStream = block.getStream();
		int blockSize = VByteCode.decodeUnsigned(headerStream);

		if (blockSize == 0) {
			return null;
		}

		// create current block SAMRecords (this array will be filled later by SAMProcessors)
		SAMRecord[] data = new SAMRecord[blockSize];
		for (int i = 0; i < blockSize; ++i) {
			try {
				data[i] = (SAMRecord) emptySAMRecord.clone();
			} catch (CloneNotSupportedException ex) {
				data[i] = createEmptySAMRecord();
			}
		}
		if (calculateStats) {
			stats = new BlockCompressionStats();
			for (SAMProcessor p : processors) {
				stat.startCounter();
				p.decompressBlock(data, block);
				stats.decompressionTime.add(stat.elapsedTime());
			}

			int i = 0;
			long compressTime;
			for (SAMProcessor p : processors) {
				stat.startCounter();
				p.reset();
				compressTime = stats.decompressionTime.get(i) + stat.elapsedTime();
				stats.decompressionTime.set(i, compressTime);
				++i;
			}
		} else {
			for (SAMProcessor p : processors) {
				p.decompressBlock(data, block);
			}

			for (SAMProcessor p : processors) {
				p.reset();
			}
		}

		if (gcNotRunFor++ >= GC_NOT_RUN_LIMIT) {
			// run garbage collector
			gcNotRunFor = 0;
			System.gc();
		}
		return data;
	}
	private SAMRecord emptySAMRecord;

	private SAMRecord createEmptySAMRecord()
	{
		SAMRecord rec = new SAMRecord(samHeader);

		rec.setReadName("*");
		rec.setFlags(0);
		rec.setReferenceName("*");
		rec.setAlignmentStart(0);
		rec.setMappingQuality(0);
		rec.setCigarString("*");
		rec.setMateReferenceName("*");
		rec.setMateAlignmentStart(0);
		rec.setInferredInsertSize(0);
		rec.setReadString("*");
		rec.setBaseQualityString("*");
		return rec;
	}

	/** Set SAM header that will be used in decompressed SAMRecords. */
	public void setSamHeader(SAMFileHeader samheader)
	{
		this.samHeader = samheader;
		emptySAMRecord = createEmptySAMRecord();
	}

	/** @return block statistics for last processed block. */
	public BlockCompressionStats getStatistics()
	{
		return stats;
	}

	public void setCalculateStats(boolean calculateStats)
	{
		this.calculateStats = calculateStats;
	}

	/** Validate SAM processors in pipeline. */
	public void validate() throws SAMProcessorException
	{
		String validationError = SAMProcessor.getValidationErrors(processors);
		String dependencyError = SAMProcessor.getDependencyErrors(processors);
		String mandatoryError = SAMProcessor.getMandatoryFieldsErrors(processors);
		String multipleError = SAMProcessor.getMultipleProcessingErrors(processors);

		String res = validationError + dependencyError + mandatoryError + multipleError;
		if (!res.isEmpty()) {
			throw new SAMProcessorException(res);
		}
	}

	/** @return string that contains validation errors for processors in pipeline (errors are separated with newlines). */
	public String getValidationErrors()
	{
		return SAMProcessor.getValidationErrors(processors);
	}

	/** @return string that contains not met dependecies errors for processors in pipeline (errors are separated with newlines). */
	public String getDependencyErrors()
	{
		return SAMProcessor.getDependencyErrors(processors);
	}

	/** @return string that contains mandatory fields that are not processed in pipeline */
	public String getMandatoryFieldsErrors()
	{
		return SAMProcessor.getMandatoryFieldsErrors(processors);
	}

	/** @return string that contains errors that multiple processors are processing single field more than once. */
	public String getMultipleProcessingErrors()
	{
		return SAMProcessor.getMultipleProcessingErrors(processors);
	}
}
