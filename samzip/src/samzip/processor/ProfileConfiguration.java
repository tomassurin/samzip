package samzip.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import samzip.compression.CompressionCodec;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ProfileConfiguration {
	public Map<String, CompressionCodec> codecs = new HashMap<>();
	public List<SAMProcessor> processors = new ArrayList<>();
	

	public void clear()
	{
		codecs.clear();
		processors.clear();
	}
}
