package samzip.processor;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.*;
import samzip.compression.CompressionCodec;
import samzip.util.MethodWithPreferences;
import samzip.util.PreferencesException;
import samzip.util.StatUtil;

/**
 * Reader for XML profile that contains configuration for compression.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMZipProfileReader
{
	public static class ProfileException extends Exception
	{
		public ProfileException(String message)
		{
			super(message);
		}

		public ProfileException(Throwable cause)
		{
			super(cause);
		}

		public ProfileException(String message, Throwable cause)
		{
			super(message, cause);
		}
	}

	/** Based on code from http://dzone.com/snippets/add-jar-file-java-load-path */
	private static class JarFileLoader extends URLClassLoader
	{
		private static final String PREFIX_STRING = getPrefix();
		
		private static String getPrefix()
		{
			if (OSValidator.isWindows()) {
				return "jar:file:/";
			} else 
				return "jar:file://"; 
		}
		
		public JarFileLoader (URL[] urls, ClassLoader parent)
		{
			super (urls,parent);
		}

		public void addFile (String path) throws MalformedURLException
		{
			String urlPath = PREFIX_STRING + path + "!/";
			addURL (new URL (urlPath));
		}
		
		public void addFile (File path) throws MalformedURLException
		{
			String urlPath = PREFIX_STRING + path.getAbsolutePath() + "!/";
			addURL (new URL (urlPath));
		}
	}
	
	private static class OSValidator 
	{
		public static boolean isWindows() 
		{
			String os = System.getProperty("os.name").toLowerCase();
			// windows
			return (os.indexOf("win") >= 0);

		}

		public static boolean isMac() {

			String os = System.getProperty("os.name").toLowerCase();
			// Mac
			return (os.indexOf("mac") >= 0);

		}

		public static boolean isUnix() 
		{
			String os = System.getProperty("os.name").toLowerCase();
			// linux or unix
			return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);

		}

		public static boolean isSolaris() 
		{
			String os = System.getProperty("os.name").toLowerCase();
			// Solaris
			return (os.indexOf("sunos") >= 0);
		}
	}
	
	private JarFileLoader jarFileLoader;

	private List<File> getJarsInDirectory(File directory)
	{
		List<File> ret = new ArrayList<>();
        if (!directory.exists()) {
            return ret;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                ret.addAll(getJarsInDirectory(file));
            } else if (file.getName().endsWith(".jar")) {
                ret.add(file);
            }
        }
        return ret;
	}
	
	private void addDirToCP(String directory)
	{
		File dir = new File(directory);
		List<File> jars = getJarsInDirectory(dir);
		for(File jar : jars) {
			try {
				jarFileLoader.addFile(jar);
			} catch (MalformedURLException ex) {
			}
		}
	}
	
	public SAMZipProfileReader()
	{
//		ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();
//		jarFileLoader = new JarFileLoader(new URL[]{}, currentThreadClassLoader);
//
//		String jarDir = StatUtil.getJarDir();
//		addDirToCP(jarDir+"/plugin");
//		addDirToCP(jarDir);
//		addDirToCP(".");
////		addDirToCP("plugin");
////		addDirToCP(".");
////		addDirToCP("lib");
//		
//		// Replace the thread classloader - assumes
//		// you have permissions to do so
//		Thread.currentThread().setContextClassLoader(jarFileLoader);
	}
	
	/** Utility class for resolving DTD. */
	private static class JarEntityManager implements EntityResolver
	{
		@Override
		public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException
		{
			URL dtd = getClass().getResource("samzipProfile.dtd");
			InputSource in = new InputSource(dtd.openStream());
			return in;
		}		
	}
	
	private Document doc;
	private final static String PROCESSOR_PACKAGE = "samzip.processor.processors";
	private final static String CODEC_PACKAGE = "samzip.compression.codecs";
	
	private class SimpleErrorHandler implements ErrorHandler
	{
		@Override
		public void warning(SAXParseException exception) throws SAXException
		{
			throw new SAXException(exception.getMessage()+" at line: "+exception.getLineNumber());
		}

		@Override
		public void error(SAXParseException exception) throws SAXException
		{
			throw new SAXException(exception.getMessage()+" at line: "+exception.getLineNumber());
		}

		@Override
		public void fatalError(SAXParseException exception) throws SAXException
		{
			throw new SAXException(exception.getMessage()+" at line: "+exception.getLineNumber());
		}
	}
	
	private ProfileConfiguration config = new ProfileConfiguration();

	private void parseMethods(Node method) throws ProfileException
	{
		NodeList children = method.getChildNodes();
		Node node;
		for(int i=0; i < children.getLength(); ++i) {
			node = children.item(i);
			if (node.getNodeName().equals("codec")) 
				parseCodec(node, null);
		}
	}
	
	private String getNodeAttributeValue(Node node, String key)
	{
		NamedNodeMap attrs = node.getAttributes();
		if (attrs == null)
			return null;
		
		Node attr = attrs.getNamedItem(key);
		
		if (attr == null)
			return null;
	
		return attr.getNodeValue();
	}
	
	private String expandStandardClass(String className, String pkg)
	{
		if (className == null)
			return null;
		
		if (className.startsWith("@")) {
			return className.replace("@", pkg+".");
		}
		
		return className;
	}
	
	private CompressionCodec parseCodec(Node codec, MethodWithPreferences parentCodec) throws ProfileException
	{
		String id = getNodeAttributeValue(codec, "id");
		String name = getNodeAttributeValue(codec, "name");
		String codecClass = getNodeAttributeValue(codec, "class");
		codecClass = expandStandardClass(codecClass, CODEC_PACKAGE);
		String whatFor = getNodeAttributeValue(codec, "for");
		String ref = getNodeAttributeValue(codec, "ref");
		String sharedValue = getNodeAttributeValue(codec, "shared");
		boolean shared = Boolean.parseBoolean(sharedValue);
		
		CompressionCodec codecInstance = null;
		if (ref != null) {
			if (id != null)
				throw new ProfileException("Codec id specified while referencing");
			if (codecClass != null)
				throw new ProfileException("Codec class specified while referencing");
			if (sharedValue != null)
				throw new ProfileException("Parameter 'shared' specified while referencing codec");
			if (name != null)
				throw new ProfileException("Parameter 'name' specified while referencing codec");
			
			codecInstance = config.codecs.get(ref);
			if (codecInstance == null)
				throw new ProfileException("Codec with id "+ref+" does not exist");
		} else {
			if (codecClass == null)
				throw new ProfileException("Class name for codec wasn't specified");
			try {
				if (jarFileLoader == null)
					codecInstance = (CompressionCodec) Class.forName(codecClass).newInstance();
				else {
					codecInstance = (CompressionCodec) jarFileLoader.loadClass(codecClass).newInstance();
				}
				codecInstance.setId(id);
				codecInstance.setName(name);
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
				throw new ProfileException("Problem creating instance of compression codec for class:"+codecClass, ex);
			}
		}
		
		if (parentCodec != null) {
			if (whatFor != null)
				parentCodec.addCodec(whatFor, codecInstance);
			else 
				parentCodec.addCodec("default", codecInstance);
		}
		
		if (ref != null)
			return codecInstance;
			
		NodeList children = codec.getChildNodes();
		Node node;
		for(int i=0; i < children.getLength(); ++i) {
			node = children.item(i);
			switch (node.getNodeName()) {
				case "param":
					String key = getNodeAttributeValue(node, "key");
					String value = getNodeAttributeValue(node, "value");
					if (key != null && value != null)
						try {
							codecInstance.setPreference(key, value);
						} catch (PreferencesException ex) {
							throw new ProfileException("Problem with option for codec '"+id+"'",ex);
						}
					break;
				case "codec":
					parseCodec(node, codecInstance);
					break;
			}
		}
		
		if (shared) {
			codecInstance.setShared(shared);
		}
		
		if (id != null) {
			config.codecs.put(id, codecInstance);
		}
		
		return codecInstance;
	}
	
	private void parseProcessingUnits(Node processors) throws ProfileException
	{
		NodeList children = processors.getChildNodes();
		Node node;
		for(int i=0; i < children.getLength(); ++i) {
			node = children.item(i);
			if (node.getNodeName().equals("processor")) 
				parseProcessor(node);
		}
	}
	
	private void parseProcessor(Node processor) throws ProfileException
	{
		String processorClass = getNodeAttributeValue(processor, "class");
		processorClass = expandStandardClass(processorClass, PROCESSOR_PACKAGE);
		SAMProcessor processorInstance = null;
		if (processorClass == null)
				throw new ProfileException("Class name for SAM processor was not specified.");
		try {
			if (jarFileLoader == null)
				processorInstance = (SAMProcessor) Class.forName(processorClass).newInstance();
			else {
				processorInstance = (SAMProcessor) jarFileLoader.loadClass(processorClass).newInstance();
			}
		} catch (NoClassDefFoundError | InstantiationException | ClassNotFoundException | IllegalAccessException ex) {
			throw new ProfileException("Problem creating instance of SAM "
					+ "processor for class: '"+processorClass+"'", ex);
		}
		
		NodeList children = processor.getChildNodes();
		Node node;
		for(int i=0; i < children.getLength(); ++i) {
			node = children.item(i);
			switch (node.getNodeName()) {
				case "param":
					String key = getNodeAttributeValue(node, "key");
					String value = getNodeAttributeValue(node, "value");
					if (key != null && value != null)
						try {
							processorInstance.setPreference(key, value);
						} catch (PreferencesException ex) {
							throw new ProfileException("Problem with option of SAM "
									+ "processor for class: '"+processorClass+"'",ex);
						}
					break;
				case "codec":
					parseCodec(node, processorInstance);
					break;
			}
		}
		
		config.processors.add(processorInstance);		
	}
		
	public ProfileConfiguration parse(InputStream inputStream) throws ProfileException
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true); 
		factory.setValidating(true);
		
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			builder.setErrorHandler(new SimpleErrorHandler());
			builder.setEntityResolver(new JarEntityManager());
		} catch (ParserConfigurationException ex) {
			throw new ProfileException(ex);
		}
		try {
			doc = builder.parse(inputStream);
		} catch (SAXException ex) {
			throw new ProfileException(ex.getMessage());
		} catch (IOException ex) {
			throw new ProfileException(ex);
		}
		
		config.clear();
		
		NodeList children = doc.getDocumentElement().getChildNodes();
		Node node;
		
		for(int i=0; i < children.getLength(); ++i) {
			node = children.item(i);
			if (node.getNodeName().equals("methods"))
				parseMethods(node);
			
			if (node.getNodeName().equals("processing"))
				parseProcessingUnits(node);
		}

		return config;
	}
	
	public static byte[] readProfileData(InputStream profileStream) throws IOException
	{
		byte[] buf = new byte[255];
		ByteArrayOutputStream output = new ByteArrayOutputStream(2048);
		int res;
		while((res = profileStream.read(buf)) >= 0) {
			output.write(buf,0, res);
		}
		
		return output.toByteArray();
	}
	
	public static ProfileConfiguration readProfile(InputStream profileStream) throws IOException, ProfileException 
	{
		SAMZipProfileReader profilReader = new SAMZipProfileReader();
		ProfileConfiguration config;
				
		config = profilReader.parse(profileStream);
		
		return config;
	}
	
	public static ProfileConfiguration readProfile(byte[] profileData) throws ProfileException, IOException
	{
		ByteArrayInputStream input = new ByteArrayInputStream(profileData);
		return readProfile(input);
	}
}
