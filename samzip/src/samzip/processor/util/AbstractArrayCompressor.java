package samzip.processor.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import samzip.compression.util.CodecException;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile;
import samzip.processor.SAMProcessor;
import samzip.util.ByteBuffer;

/**
 * Helper class for array data compression. Works in 2 distinct modes:
 * <ul>
 *	<li> <b> delimiter mode</b> - uses delimiter to distinguish arrays</li>
 *  <li> <b> codec mode</b> - uses other compression codec for coding array lengths</li>
 * </ul>
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public abstract class AbstractArrayCompressor
{	
	public final static Charset charset = Charset.forName("UTF-8");
	protected ByteBuffer byteBuffer = new ByteBuffer(255);
	
	protected OutputStream enc;
	protected InputStream dec;
	
	protected SAMProcessor processor;
	
	public AbstractArrayCompressor(SAMProcessor proc)
	{
		this.processor = proc;
	}

	public AbstractArrayCompressor(OutputStream enc)
	{
		this.enc = enc;
	}

	public AbstractArrayCompressor(InputStream dec)
	{
		this.dec = dec;
	}
	
	public void initializeCompression(BinaryBlockFile.OutputBlock block, String streamName) throws CodecException, IOException
	{
		if (processor == null)
			return;
		
		enc = processor.getCodecFor("default")
				.newEncoderMethod(block, streamName, DataMode.UBYTE);
	}
	
	public void initializeDecompression(BinaryBlockFile.InputBlock block) throws CodecException, IOException
	{
		if (processor == null)
			return;
		
		dec = processor.getCodecFor("default")
				.newDecoderMethod(block, DataMode.UBYTE);
	}
	
	public void close() throws IOException
	{
		if (enc != null)
			enc.close();
		if (dec != null)
			dec.close();
	}

	protected byte[] decompressByteArray(InputStream dec, int n) throws IOException
	{	
		byte[] result = new byte[n];
		int res = dec.read(result);
		if (res < 0)
			return new byte[0];
		if (res < n) {
			byte[] tmp = new byte[res];
			System.arraycopy(result, 0, tmp, 0, res);
			result = tmp;
		}
		return result;
	}
	
	public abstract void compressByteArray(byte[] array) throws IOException;
	
	public abstract byte[] decompressByteArray(int seqLen) throws IOException;
	
	public byte[] decompressByteArray() throws IOException
	{
		return decompressByteArray(0);
	}
	
	public void compressString(String data) throws IOException
	{	
		byte[] array = data.getBytes(charset);
		compressByteArray(array);
	}
	
	public String decompressString(int seqLen) throws IOException
	{		
		return new String(decompressByteArray(seqLen), charset);
	}
	
	public String decompressString() throws IOException
	{
		return decompressString(0);
	}
}
