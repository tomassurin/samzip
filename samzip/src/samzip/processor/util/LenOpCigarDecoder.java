package samzip.processor.util;

import java.io.IOException;
import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;
import samzip.compression.DecoderMethod;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LenOpCigarDecoder implements CigarDecoder
{
	private DecoderMethod cigarOp;
	private DecoderMethod cigarLen;

	public LenOpCigarDecoder(DecoderMethod cigarOp, DecoderMethod cigarLen)
	{
		this.cigarOp = cigarOp;
		this.cigarLen = cigarLen;
	}
	
	@Override
	public Cigar decompress() throws IOException
	{
		Cigar cigar = new Cigar();
		while(true) {
			int op = cigarOp.read();
			if (op == CigarUtil.noCigarOP)
				break;
			int length = cigarLen.read();
			cigar.add(new CigarElement(length, CigarUtil.intToCigarOp(op)));
		}
		
		return cigar;
	}
	
}
