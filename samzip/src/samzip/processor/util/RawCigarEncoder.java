
package samzip.processor.util;

import java.io.IOException;
import java.io.OutputStream;
import net.sf.samtools.Cigar;
import net.sf.samtools.TextCigarCodec;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RawCigarEncoder implements CigarEncoder
{
	private AbstractArrayCompressor arrc;
	private TextCigarCodec textCigarCodec = TextCigarCodec.getSingleton();
	
	public RawCigarEncoder(OutputStream cigarEnc)
	{
		this.arrc = new DelimiterArrayCompressor(cigarEnc, '\n');
	}
	
	@Override
	public void compress(Cigar cigar) throws IOException
	{
		if (cigar.isEmpty()) {
			arrc.compressString("*");
		} else {
			arrc.compressString(textCigarCodec.encode(cigar));
		}
	}
}
