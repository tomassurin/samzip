package samzip.processor.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import samzip.processor.SAMProcessor;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class DelimiterArrayCompressor extends AbstractArrayCompressor
{
	protected char delimiter = '\n';

	public DelimiterArrayCompressor(SAMProcessor proc, char delimiter)
	{
		super(proc);
		this.delimiter = delimiter;
	}

	public DelimiterArrayCompressor(SAMProcessor proc)
	{
		super(proc);
	}

	public DelimiterArrayCompressor(InputStream dec, char delimiter)
	{
		super(dec);
		this.delimiter = delimiter;
	}

	public DelimiterArrayCompressor(OutputStream enc, char delimiter)
	{
		super(enc);
		this.delimiter = delimiter;
	}
	
	@Override
	public void compressByteArray(byte[] array) throws IOException
	{
		enc.write(array);
		enc.write(delimiter);
	}

	@Override
	public void compressString(String data) throws IOException
	{
		byte[] array = data.getBytes(charset);
		compressByteArray(array);
	}
	
	@Override
	public byte[] decompressByteArray(int seqLen) throws IOException
	{
		return decompressByteArray(dec);
	}
	
	protected byte[] decompressByteArray(InputStream dec) throws IOException
	{
		int res;
		byteBuffer.clear();
		while ((res = dec.read()) != delimiter) {
			byteBuffer.add((byte) res);
		}
		byte[] result = new byte[byteBuffer.length()];
		System.arraycopy(byteBuffer.buffer(), 0, result, 0, byteBuffer.length());
		return result;
	}
}
