package samzip.processor.util;

import samzip.processor.SAMProcessor;

/**
 * Exceptions used in SAM processor implementations. Could indicated problem 
 * with SAM processor configuration.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMProcessorException extends Exception
{
	private final SAMProcessor processor;
	
	public SAMProcessorException(String message)
	{
		super(message);
		this.processor = null;
	}

	public SAMProcessorException(String message, SAMProcessor processor)
	{
		super(message);
		this.processor = processor;
	}

	
	
	public SAMProcessorException(Throwable cause)
	{
		super(cause);
		processor = null;
	}
	
	public SAMProcessorException(Throwable cause, SAMProcessor processor)
	{
		super(cause);
		this.processor = processor;
	}

	public SAMProcessorException(String message, Throwable cause)
	{
		super(message, cause);
		this.processor = null;
	}

	public SAMProcessorException(String message, Throwable cause, SAMProcessor processor)
	{
		super(message, cause);
		this.processor = processor;
	}
	
	public SAMProcessor getSAMProcessor()
	{
		return processor;
	}
}
