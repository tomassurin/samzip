package samzip.processor.util;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representation of single SAM field.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMField
{
	/** SAM field types */
	public static enum SAMFieldType {
		QNAME,
		FLAG,
		RNAME,
		POS,
		MAPQ,
		CIGAR,
		RNEXT,
		PNEXT,
		TLEN,
		SEQ,
		QUAL,
		/** optional fields */
		TAG,
		/** error type */
		NONE;
	}
	
	/** Field type */
	private final SAMFieldType type;
	/** 2 char TAG string (if type == TAG) */
	private final String tag;
	/** tag data type (if type == TAG) */
	private final TagDataType tagDataType;
	
	//// static fields for known fields
	// none field -> indicates error
	public static final SAMField NONE = new SAMField(SAMFieldType.NONE);
	// mandatory fields
	public static final SAMField QNAME = new SAMField(SAMFieldType.QNAME);
	public static final SAMField FLAG = new SAMField(SAMFieldType.FLAG);
	public static final SAMField RNAME = new SAMField(SAMFieldType.RNAME, null);
	public static final SAMField POS = new SAMField(SAMFieldType.POS);
	public static final SAMField MAPQ = new SAMField(SAMFieldType.MAPQ);
	public static final SAMField CIGAR = new SAMField(SAMFieldType.CIGAR);
	public static final SAMField RNEXT = new SAMField(SAMFieldType.RNEXT);
	public static final SAMField PNEXT = new SAMField(SAMFieldType.PNEXT);
	public static final SAMField TLEN = new SAMField(SAMFieldType.TLEN);
	public static final SAMField SEQ = new SAMField(SAMFieldType.SEQ);
	public static final SAMField QUAL = new SAMField(SAMFieldType.QUAL);
	// standard tags
	public static final SAMField TAG_AM = new SAMField(SAMFieldType.TAG, "AM", TagDataType.INT);
	public static final SAMField TAG_AS = new SAMField(SAMFieldType.TAG, "AS", TagDataType.INT);
	public static final SAMField TAG_BC = new SAMField(SAMFieldType.TAG, "BC", TagDataType.STRING);
	public static final SAMField TAG_BQ = new SAMField(SAMFieldType.TAG, "BQ", TagDataType.STRING);
	public static final SAMField TAG_CC = new SAMField(SAMFieldType.TAG, "CC", TagDataType.STRING);
	public static final SAMField TAG_CM = new SAMField(SAMFieldType.TAG, "CM", TagDataType.INT);
	public static final SAMField TAG_CP = new SAMField(SAMFieldType.TAG, "CP", TagDataType.INT);
	public static final SAMField TAG_CQ = new SAMField(SAMFieldType.TAG, "CQ", TagDataType.STRING);
	public static final SAMField TAG_CS = new SAMField(SAMFieldType.TAG, "CS", TagDataType.STRING);
	public static final SAMField TAG_E2 = new SAMField(SAMFieldType.TAG, "E2", TagDataType.STRING);
	public static final SAMField TAG_FI = new SAMField(SAMFieldType.TAG, "FI", TagDataType.INT);
	public static final SAMField TAG_FS = new SAMField(SAMFieldType.TAG, "FS", TagDataType.SHORT_ARR);
	public static final SAMField TAG_LB = new SAMField(SAMFieldType.TAG, "LB", TagDataType.STRING);
	public static final SAMField TAG_H0 = new SAMField(SAMFieldType.TAG, "H0", TagDataType.INT);
	public static final SAMField TAG_H1 = new SAMField(SAMFieldType.TAG, "H1", TagDataType.INT);
	public static final SAMField TAG_H2 = new SAMField(SAMFieldType.TAG, "H2", TagDataType.INT);
	public static final SAMField TAG_HI = new SAMField(SAMFieldType.TAG, "HI", TagDataType.INT);
	public static final SAMField TAG_IH = new SAMField(SAMFieldType.TAG, "IH", TagDataType.INT);
	public static final SAMField TAG_MD = new SAMField(SAMFieldType.TAG, "MD", TagDataType.STRING);
	public static final SAMField TAG_MQ = new SAMField(SAMFieldType.TAG, "MQ", TagDataType.INT);
	public static final SAMField TAG_NH = new SAMField(SAMFieldType.TAG, "NH", TagDataType.INT);
	public static final SAMField TAG_NM = new SAMField(SAMFieldType.TAG, "NM", TagDataType.INT);
	public static final SAMField TAG_OQ = new SAMField(SAMFieldType.TAG, "OQ", TagDataType.STRING);
	public static final SAMField TAG_OP = new SAMField(SAMFieldType.TAG, "OP", TagDataType.INT);
	public static final SAMField TAG_OC = new SAMField(SAMFieldType.TAG, "OC", TagDataType.STRING);
	public static final SAMField TAG_PG = new SAMField(SAMFieldType.TAG, "PG", TagDataType.STRING);
	public static final SAMField TAG_PQ = new SAMField(SAMFieldType.TAG, "PQ", TagDataType.INT);
	public static final SAMField TAG_PU = new SAMField(SAMFieldType.TAG, "PU", TagDataType.STRING);
	public static final SAMField TAG_Q2 = new SAMField(SAMFieldType.TAG, "Q2", TagDataType.STRING);
	public static final SAMField TAG_R2 = new SAMField(SAMFieldType.TAG, "R2", TagDataType.STRING);
	public static final SAMField TAG_RG = new SAMField(SAMFieldType.TAG, "RG", TagDataType.STRING);
	public static final SAMField TAG_SM = new SAMField(SAMFieldType.TAG, "SM", TagDataType.INT);
	public static final SAMField TAG_TC = new SAMField(SAMFieldType.TAG, "TC", TagDataType.INT);
	public static final SAMField TAG_U2 = new SAMField(SAMFieldType.TAG, "U2", TagDataType.STRING);
	public static final SAMField TAG_UQ = new SAMField(SAMFieldType.TAG, "UQ", TagDataType.INT);	
	
	private static final SAMField[] mandatoryFields = { 
		QNAME, FLAG, RNAME, POS, MAPQ, CIGAR, RNEXT, 
		PNEXT, TLEN, SEQ, QUAL 
	};
	
	private static final SAMField[] standardTags = {
		TAG_AM, TAG_AS, TAG_BC, TAG_BQ, 
		TAG_CC, TAG_CM, TAG_CP, TAG_CQ, 
		TAG_CS, TAG_E2, TAG_FI, TAG_FS,
		TAG_LB, TAG_H0, TAG_H1, TAG_H2, 
		TAG_HI, TAG_IH, TAG_MD, TAG_MQ, 
		TAG_NH, TAG_NM, TAG_OQ, TAG_OP, 
		TAG_OC, TAG_PG, TAG_PQ, TAG_PU, 
		TAG_Q2, TAG_R2, TAG_RG, TAG_SM, 
		TAG_TC, TAG_U2, TAG_UQ
	};
	
	protected SAMField(SAMFieldType type)
	{
		this(type, null, null);
	}
	
	protected SAMField(SAMFieldType type, String tag)
	{
		this(type, tag, null);
	}
	
	protected SAMField(SAMFieldType type, String tag, TagDataType tagType)
	{
		this.type = type;
		if (this.type == SAMFieldType.TAG) {
			this.tag = tag == null ? new String() : tag;
			this.tagDataType = tagType == null ? TagDataType.NONE : tagType;
		} else {
			this.tag = null;
			this.tagDataType = null;
		}
	}
	
	public static SAMField[] getMandatoryFields()
	{
		return mandatoryFields;
	}

	public static SAMField[] getStandardTags()
	{
		return standardTags;
	}
	
	/** Get field type - mandatory fields, NONE or TAG */
	public SAMFieldType getType()
	{
		return type;
	}
	
	/** Return 2 character tag string if any */
	public String getTag()
	{
		return tag;
	}
	
	/** Get standard optional field data type for tag {@code tag}. */
	public static TagDataType getTagDataType(String tag)
	{
		for(SAMField f : standardTags) {
			if (f.getTag().equals(tag))
				return f.getTagDataType();
		}
		
		return TagDataType.NONE;
	}
	
	/** If type == TAG return TAG data type */
	public TagDataType getTagDataType()
	{
		if (isTag()) {
			return tagDataType;
		}
		
		return TagDataType.NONE;
	}
	
	public boolean isTag()
	{
		if (type == SAMFieldType.TAG && tag != null)
			return true;
		
		return false;
	}
	
	public boolean isStandardTag()
	{
		return isStandardTag(tag);
	}
	
	public static boolean isStandardTag(String tag)
	{
		for( SAMField f : standardTags) {
			if (f.getTag().equals(tag))
				return true;
		}
		
		return false;
	}
	
	
	/** @return true if field value is integer type. */
	public boolean isInteger()
	{
		if (isTag()) {
			return getTagDataType().isInteger();
		}
		
		return type == SAMFieldType.FLAG ||
			   type == SAMFieldType.PNEXT || type == SAMFieldType.POS;
	}
	
	/** @return true if field value is array type - string or array of values. */
	public boolean isArray()
	{
		return type == SAMFieldType.CIGAR || type == SAMFieldType.QNAME || type == SAMFieldType.QUAL || type == SAMFieldType.SEQ
				|| (isTag() && getTagDataType().isArray());
	}
	
	public static TagDataType getTagDataType(Object obj)
	{
		if (obj instanceof Integer)
			return TagDataType.INT;
		else if (obj instanceof String)
			return TagDataType.STRING;
		else if (obj instanceof Character)
			return TagDataType.CHAR;
		else if (obj instanceof Float)
			return TagDataType.FLOAT;
		else if (obj instanceof byte[])
			return TagDataType.BYTE_ARR;
		else if (obj instanceof short[])
			return TagDataType.SHORT_ARR;
		else if (obj instanceof int[])
			return TagDataType.INT_ARR;
		else if (obj instanceof float[])
			return TagDataType.FLOAT_ARR;
		else if (obj instanceof Byte)
			return TagDataType.BYTE;
		else if (obj instanceof Short)
			return TagDataType.SHORT;
			
		return TagDataType.NONE;
	}
		
	/** Parse string {@code field} that contains mandatory field identifier (equal to enum constants)
	 * or optional field in format "TAG:XY" where "XY" is 2 char code of optional field.
	 * @return instance of SAMField class for parsed field
	 */
	public static SAMField getSAMField(String field)
	{
		switch (field.toLowerCase()) {
			case "qname":
				return QNAME;
			case "flag":
				return FLAG;
			case "ref":
			case "rname":
				return RNAME;
			case "pos":
				return POS;
			case "map_qual":
			case "mapq":
				return MAPQ;
			case "cigar":
				return CIGAR;
			case "next_ref":
			case "rnext":
				return RNEXT;
			case "next_pos":
			case "pnext":
				return PNEXT;
			case "next_temp_len":
			case "tlen":
				return TLEN;
			case "seq":
				return SEQ;
			case "qual":
				return QUAL;
			default: { 
				Pattern pat = Pattern.compile("(tag|TAG):([a-zA-Z0-9][a-zA-Z0-9])");
				Matcher matcher = pat.matcher(field);	
				if (!matcher.matches()) {
					return NONE;
				} else {
					String tag = matcher.group(2);
					if (!isStandardTag(tag)) {
						return new SAMField(SAMFieldType.TAG, tag);
					}
					for(SAMField f : standardTags) {
						if (f.getTag().equals(tag))
							return f;
					}
					return new SAMField(SAMFieldType.TAG, tag);
				}
			}
		}
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == this)
			return true;
		
		if (obj instanceof SAMField) {
			SAMField other = (SAMField) obj;
			if (this.type == other.type) {
				if (this.type == SAMFieldType.TAG) {
					return this.tag.equals(other.tag);
				}
				return true;
			}
			else {
				return false;
			}
		} else if (obj instanceof String) {
			return getSAMField((String) obj).equals(this);
		}
		return false;
	}

	@Override
	public int hashCode()
	{
		int hash = 7;
		hash = 79 * hash + (this.type != null ? this.type.hashCode() : 0);
		hash = 79 * hash + Objects.hashCode(this.tag);
		return hash;
	}

	@Override
	public String toString()
	{
		if (type == SAMFieldType.TAG) {
			return "TAG:"+tag;
		} else {
			return type.toString();
		}
	}
	
	/** Enum for all possible optional field data types. */
	public static enum TagDataType {
		INT,
		STRING,
		CHAR,
		BYTE,
		SHORT,
		FLOAT,
		INT_ARR,
		BYTE_ARR,
		SHORT_ARR,
		FLOAT_ARR,
		NONE;
		
		public boolean isArray()
		{
			return this == STRING || this == BYTE_ARR || this == SHORT_ARR || this == INT_ARR
					|| this == FLOAT_ARR;
		}
		
		public boolean isInteger()
		{
			return this == INT || this == SHORT;
		}
		
		public static TagDataType getType(Object cl)
		{
			if (cl instanceof Integer) 
				return INT;
			else if (cl instanceof String) 
				return STRING;
			else if (cl instanceof Character)
				return CHAR;
			else if (cl instanceof Short)
				return SHORT;
			else if (cl instanceof Byte)
				return BYTE;
			else if (cl instanceof Float)
				return FLOAT;
			else if (cl instanceof byte[])
				return BYTE_ARR;
			else if (cl instanceof int[])
				return INT_ARR;
			else if (cl instanceof short[])
				return SHORT_ARR;
			else if (cl instanceof float[])
				return FLOAT_ARR;
			return NONE;
		}
		
		public static byte toByteValue(TagDataType data)
		{
			return (byte) data.ordinal();
		}
		
		public static TagDataType parseByteValue(int val)
		{
			if (val < values().length && val >= 0)
				return values()[val];
			
			return NONE;
		}
	}
}
