package samzip.processor.util;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SequenceUtil
{
	public static void canonizeSequence(byte[] arr)
	{
		for(int i=0; i < arr.length; ++i) {
			if (Character.isLowerCase(arr[i]))
				arr[i] = (byte) Character.toUpperCase(arr[i]);
			if (arr[i] != 'A' && arr[i] != 'C' && arr[i] != 'G' &&
				arr[i] != 'T' && arr[i] != 'N') 
			{
				arr[i] = 'N';
			}
		}
	}
	
	public static final byte MAX_BASE_ORDINAL = 5;
			
	public static byte baseToInt(byte base)
	{
		switch (base) {
			case 'A':
			case 'a':
				return 0;
			case 'C':
			case 'c':
				return 1;
			case 'G':
			case 'g':
				return 2;
			case 'T':
			case 't':
				return 3;
			case 'N':
			case 'n':
				return 4;
			default: assert false;
		}
		assert false;
		return 0;
	}
	
	public static byte intToBase(byte value)
	{
		switch (value) {
			case 0:
				return 'A';
			case 1:
				return 'C';
			case 2:
				return 'G';
			case 3:
				return 'T';
			case 4:
				return 'N';
			default: assert false : value;
		}
		assert false;
		return 'N';
	}
	
	public static byte[] basesToIntArray(byte[] seq)
	{
		byte[] ret = new byte[seq.length];
		
		for(int i=0; i < seq.length; ++i) {
			ret[i] = baseToInt(seq[i]);
		}
		
		return ret;
	}
	
	public static byte[] intToBasesArray(byte[] values)
	{
		byte[] ret = new byte[values.length];
		
		for(int i=0; i < values.length; ++i) {
			ret[i] = intToBase(values[i]);
		}
		
		return ret;
	}
}
