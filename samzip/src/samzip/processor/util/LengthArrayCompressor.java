package samzip.processor.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.util.CodecException;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.processor.SAMProcessor;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LengthArrayCompressor extends AbstractArrayCompressor
{
	private OutputStream lenEnc;
	private InputStream lenDec;

	public LengthArrayCompressor(SAMProcessor proc)
	{
		super(proc);
	}

	@Override
	public void initializeCompression(OutputBlock block, String streamName) throws CodecException, IOException
	{
		super.initializeCompression(block, streamName);
		
		if (processor == null)
			return;
		
		lenEnc = processor.getCodecFor("length")
				.newEncoderMethod(block, streamName+".len", DataMode.UINT);
	}

	@Override
	public void initializeDecompression(InputBlock block) throws CodecException, IOException
	{
		super.initializeDecompression(block);
		
		if (processor == null)
			return;
		
		lenDec = processor.getCodecFor("length")
				.newDecoderMethod(block, DataMode.UINT);
	}
	
	@Override
	public void close() throws IOException
	{
		if (lenEnc != null)
			lenEnc.close();
		if (lenDec != null)
			lenDec.close();
		super.close();
	}

	@Override
	public void compressByteArray(byte[] array) throws IOException
	{
		lenEnc.write(array.length);
		enc.write(array);
	}

	@Override
	public void compressString(String data) throws IOException
	{
		byte[] array = data.getBytes(charset);
		compressByteArray(array);
	}
	
	public byte[] decompressByteArray(int seqLen) throws IOException
	{
		int n = lenDec.read();
		return decompressByteArray(dec, n);				
	}

	@Override
	public String decompressString(int seqLen) throws IOException
	{
		return new String(decompressByteArray(seqLen), charset);
	}
}
