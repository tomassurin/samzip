package samzip.processor.util;

import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;
import net.sf.samtools.CigarOperator;
import net.sf.samtools.SAMRecord;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class CigarUtil
{
	public static int cigarOpToInt(CigarOperator op)
	{
		switch (op) {
			case EQ:
				return 0;
			case X:
				return 1;
			case S:
				return 2;
			case D:
				return 3;
			case I:
				return 4;
			case H:
				return 5;
			case N:
				return 6;
			case P:
				return 7;
			case M:
				return 8;
			default:
				return noCigarOP;
		}
	}
	
	private final static CigarOperator[] operators = { 
		CigarOperator.EQ, CigarOperator.X,
		CigarOperator.S, CigarOperator.D,
		CigarOperator.I, CigarOperator.H, 
		CigarOperator.N, CigarOperator.P,
		CigarOperator.M	
	};
	
	public static CigarOperator intToCigarOp(int op)
	{
		return operators[op];
	}
	
	public final static int noCigarOP = 9;

}
