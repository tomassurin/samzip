package samzip.processor.util;

import java.io.IOException;
import java.io.InputStream;
import net.sf.samtools.Cigar;
import net.sf.samtools.TextCigarCodec;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RawCigarDecoder implements CigarDecoder
{
	private AbstractArrayCompressor arrc;
	private TextCigarCodec textCigarCodec = TextCigarCodec.getSingleton();
	
	public RawCigarDecoder(InputStream cigarDec)
	{
		this.arrc = new DelimiterArrayCompressor(cigarDec, '\n');
	}
	
	@Override
	public Cigar decompress() throws IOException
	{
		Cigar cigar;
		String cigarStr = arrc.decompressString();
		if (cigarStr.startsWith("*")) {
			cigar = new Cigar();
		} else {
			cigar = textCigarCodec.decode(cigarStr);
		}
		
		return cigar;
	}
	
}
