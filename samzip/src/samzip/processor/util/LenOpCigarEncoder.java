package samzip.processor.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import net.sf.samtools.Cigar;
import net.sf.samtools.CigarElement;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LenOpCigarEncoder implements CigarEncoder
{
	private OutputStream cigarOp;
	private OutputStream cigarLen;

	public LenOpCigarEncoder(OutputStream cigarOp, OutputStream cigarLen)
	{
		this.cigarOp = cigarOp;
		this.cigarLen = cigarLen;
	}
	
	@Override
	public void compress(Cigar cigar) throws IOException
	{
		List<CigarElement> cigarElements = cigar.getCigarElements();
		for(CigarElement elem : cigarElements) {
			cigarOp.write(CigarUtil.cigarOpToInt(elem.getOperator()));
			cigarLen.write(elem.getLength());
		}
		cigarOp.write(CigarUtil.noCigarOP);
	}
}
