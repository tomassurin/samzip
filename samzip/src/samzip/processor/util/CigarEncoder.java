package samzip.processor.util;

import java.io.IOException;
import net.sf.samtools.Cigar;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public interface CigarEncoder
{
	public void compress(Cigar cigar) throws IOException;
}
