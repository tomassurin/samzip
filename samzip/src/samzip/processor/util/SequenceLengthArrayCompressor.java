package samzip.processor.util;

import java.io.IOException;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.util.CodecException;
import samzip.compression.util.DataMode;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.io.BinaryBlockFile.OutputBlock;
import samzip.io.BitInputStream;
import samzip.io.BitOutputStream;
import samzip.processor.SAMProcessor;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SequenceLengthArrayCompressor extends AbstractArrayCompressor
{
	private final boolean outputFlags;
	private BitOutputStream flagOut;
	private BitInputStream flagIn;
	
	public SequenceLengthArrayCompressor(SAMProcessor proc, boolean outputFlags)
	{
		super(proc);
		this.outputFlags = outputFlags;
	}
	
	public SequenceLengthArrayCompressor(SAMProcessor proc)
	{
		super(proc);
		this.outputFlags = false;
	}

	@Override
	public void initializeCompression(OutputBlock block, String streamName) throws CodecException, IOException
	{
		super.initializeCompression(block, streamName);
		
		if (processor == null)
			return;
		
		if (outputFlags) {
			EncoderMethod flagEnc = processor.getCodecFor("flag")
				.newEncoderMethod(block, streamName+".flag", DataMode.UBYTE);
			flagOut = new BitOutputStream(flagEnc);
		}
	}

	@Override
	public void close() throws IOException
	{
		if (flagOut != null)
			flagOut.close();
		if (flagIn != null)
			flagIn.close();
		
		super.close();
	}	

	@Override
	public void initializeDecompression(InputBlock block) throws CodecException, IOException
	{
		super.initializeDecompression(block);
		
		if (processor == null)
			return;
		
		if (outputFlags) {
			DecoderMethod flagDec = processor.getCodecFor("flag")
					.newDecoderMethod(block, DataMode.UBYTE);
			flagIn = new BitInputStream(flagDec);
		}
	}
	
	@Override
	public void compressByteArray(byte[] array) throws IOException
	{
		if (outputFlags) {
			if (array.length > 0) 
				flagOut.writeBit(1);
			else
				flagOut.writeBit(0);
		}
		if (array.length > 0)
			enc.write(array);
	}

	@Override
	public void compressString(String data) throws IOException
	{
		byte[] array = data.getBytes(charset);
		compressByteArray(array);
	}
	
	protected byte[] decompressByteArrayWithFlags(int seqLen) throws IOException
	{	
		if (flagIn.readBit() == 1)
			return decompressByteArray(dec, seqLen);
		else
			return new byte[0];
	}
	
	@Override
	public byte[] decompressByteArray(int seqLen) throws IOException
	{
		if (outputFlags) {
			return decompressByteArrayWithFlags(seqLen);
		} else {
			return decompressByteArray(dec, seqLen);
		}
	}
}
