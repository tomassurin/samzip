package samzip.compression;

import java.io.IOException;
import java.io.InputStream;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 * <p>This abstract class is the superclass of all implementations of decoder methods. 
 * In essence this class works almost like InputStream would. Exceptions are
 * that user needs to initialize this method via {@link initialize()} 
 * (in order to allow configuration of data mode and output stream after method creation)
 * and use of data modes in method {@link read()}.
 * </p>
 *
 * <p>Recommended method order: 
 * <ol> 
 *	<li> {@link setInputStream(InputStream)} - configuration of input stream</li> 
 *	<li> {@link setDataMode(DataMode)} - data mode configuration</li>
 *	<li> {@link initialize()} - initialization of decompression method</li> 
 *	<li> {@link read()} - decompression of data</li> 
 *	<li> {@link close()} - finalize decompression</li> 
 * </ul>
 * </p>
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public abstract class DecoderMethod extends InputStream
{
	/** methods data mode */
	protected DataMode dataMode = DataMode.getDefault();
	/** indicates that data mode was configured via {@link setDataMode(DataMode mode)} */
	protected boolean dataModeConfigured = false;
	/** end of stream symbol - depends on data mode */
	protected int EOS = dataMode.getEOSSymbol();
	/** indicates that decoder method is initialized */
	protected boolean initialized = false;
	/** input stream that is used as compressed data input (can be another DecoderMethod) */
	protected InputStream inputStream;
	/** indicates that EOS was already reached */
	protected boolean endOfStream = false;
	
	/**
	 * @return {@code 0} if we are at the end of the stream, else return {@code 1}
	 * @throws IOException 
	 */
	@Override
	public int available() throws IOException
	{
		return endOfStream? 0 : 1;
	}
	
	/**
	 * Sets data mode to {@code mode}. Data mode can be specified only once through this method.
	 * <p>Can't be used after successful initialization.</p>
	 * @throws CompressionException if specified data mode wasn't valid
	 */
	public void setDataMode(DataMode mode)
	{
		setDataMode(mode, false);
	}
	
	/** Sets data mode to {@code mode}. If {@code override} is true overrides already
	 * configured data mode, else leaves it as is.
	 * <p>Can't be used after successful initialization.</p>
	 * @throws CompressionException if specified data mode wasn't valid
	 */
	public void setDataMode(DataMode mode, boolean override)
	{
		if (initialized)
			return;
		if (!override && dataModeConfigured) 
			return;
		configureDataMode(mode);
		dataModeConfigured = true;
	}
	
	/** Bypass high level methods {@link setDataMode(DataMode)} and {@link setDataMode(dataMode, boolean)}.
	 * and sets data mode without hard configured data modes. Still checks if data mode is valid.
	 * <p>Can't be used after successful initialization.</p>
	 * @throws CompressionException if specified data mode wasn't valid 
	 */
	final protected void configureDataMode(DataMode mode)
	{
		if (initialized) // can't change mode after initialization
			return;
		if (!isValidDataMode(mode)) {
			throw new CompressionException("Data mode "+mode+" is not valid for this method.", this);
		}
		this.dataMode = mode;
		EOS = dataMode.getEOSSymbol();
	}

	/** Get currently configured data mode. */
	public DataMode getDataMode()
	{
		return dataMode;
	}
	
	/**
	 * Set input stream that will be used as methods input.
	 * InputStream can be another DecoderMethod. This class also handles 
	 * initialization of such method.
	 * <p>Can't be used after successful initialization.</p>
	 */
	public void setInputStream(InputStream stream)
	{
		if (initialized)
			return;
		
		if (this.inputStream instanceof DecoderMethod) {
			((DecoderMethod)this.inputStream).setInputStream(stream);
		} else {
			this.inputStream = stream;
		}
	}
	
	/**
	 * Get currently configured input stream.
	 */
	public InputStream getInputStream()
	{
		return inputStream;
	}	

	/**
	 * Initialize method. After this method ends without exception. All 
	 * subsequent calls to {@code setInputStream()} are ineffective.
	 *
	 * @throws CompressionException if there's problem with method configuration
	 */
	public void initialize()
	{
		// should be used for safety reasons
		if (initialized)
			return;
		
		// must be used if you don't want to have som strange behavior in 
		// situation when input stream is instance of DecoderMethod
		initializeInputStream();
		
		if (inputStream != null) {
			initialized = true;
			endOfStream = false;
		} else {
			throw new CompressionException(Exceptions.DECODER_INITIALIZE, this);
		}
	}
	
	/** Initialize input stream. It's safer to use this method in {@link initialize()} 
	 * because it handles situation where input stream is instance of DecoderMethod.
	 */
	protected final void initializeInputStream()
	{
		if (inputStream != null) {
			if (inputStream instanceof DecoderMethod) {
				((DecoderMethod)inputStream).initialize();
			}
		}
	}

	/**
	 * Decodes single value with this method. It should be thoughtful about 
	 * configured data mode.
	 *
	 * <p>Can be used only after successful initialization via {@link initialize()}.</p>
	 * 
	 * @return decoded value or EOS symbol, that is equal to return value of 
	 *        {@link EOS()} and is guaranteed to be negative
	 *
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	public abstract int read() throws IOException;
	
	/** Same behavior as {@link InputStream#read(byte[])}. Framework does
	 * not expect that this method uses data mode (i.e. uses mode UBYTE).
	 * <p>Can be used only after successful initialization via {@link initialize()}.</p>
	 */
	@Override
	public int read(byte[] data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
			
		return read(data, 0, data.length);
	}

	/** Same behavior as {@link InputStream#read(byte[],int,int)}. Framework does
	 * not expect that this method uses data mode (i.e. uses mode UBYTE).
	 * <p>Can be used only after successful initialization via {@link initialize()}.</p>
	 */
	@Override
	public int read(byte[] data, int off, int len) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		if (data == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > data.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }
		
		if (endOfStream)
			return EOS;
		
		int result = 0;
		int tmp;
		for(int i=0; i < len; ++i) {
			tmp = read();
			if (tmp == EOS) {
				endOfStream = true;
				break;
			}
			data[off+i] = (byte) tmp;
			++result;
		}
		
		return result;
	}
	
	/**
	 * Close DecoderMethod and clean after yourself. 
	 * <p>Can be used only after successful initialization via {@link initialize()}.</p>
	 * @throws IOException
	 */
	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		inputStream.close();
		inputStream = null;
		initialized = false;
		endOfStream = true;
	}

	/**
	 * @return the EOS symbol
	 */
	public int EOS()
	{
		return EOS;
	}
	
	/** Override this method to provide valid data modes for 
	 * specific DecoderMethod implementation. 
	 * @return true if specified data mode is valid, else false
	 */
	protected boolean isValidDataMode(DataMode mode)
	{
		return true;
	}
}