package samzip.compression.standard;

import java.io.IOException;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class NullDecoder extends DecoderMethod
{

	public NullDecoder()
	{
	}

	@Override
	public int available()
	{
		return 0;
	}
	
	@Override
	public int read() throws IOException, CompressionException
	{
		return EOS();
	}

	@Override
	public void close() throws IOException
	{
	}
}
