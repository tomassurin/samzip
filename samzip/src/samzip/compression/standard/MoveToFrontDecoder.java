package samzip.compression.standard;

import java.io.IOException;
import java.io.InputStream;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class MoveToFrontDecoder extends DecoderMethod
{
	private DecoderMethod input = null;
	private Deque<Integer> symbols = new LinkedList<>();
	
	public MoveToFrontDecoder(InputStream inputStream, DecoderMethod inputMethod)
	{
		if (inputMethod == null)
			throw new IllegalArgumentException(Exceptions.INPUT_METHOD_NULL);
		
		configureDataMode(DataMode.INT);
		this.input = inputMethod;
		setInputStream(inputStream);
	}
	
	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null && input != null) {
			input.setInputStream(inputStream);
			input.setDataMode(dataMode);
			input.initialize();
			initialized = true;
		} else {
			if (input == null)
				throw new CompressionException(Exceptions.DECODER_INITIALIZE+
						": "+ Exceptions.INPUT_METHOD_NULL, this);
			if (inputStream == null)
				throw new CompressionException(Exceptions.DECODER_INITIALIZE+
						": "+Exceptions.INPUT_STREAM_NULL, this);
		}
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		input.close();
		input = null;
		super.close();
	}
	
	@Override
	public synchronized int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		if (endOfStream)
			return EOS;
		
		int res = input.read();
		if (res == input.EOS()) {
			return EOS;
		}
		
		if (res < symbols.size()) {
			Iterator<Integer> it = symbols.iterator();
			for(int i=0; i < res; ++i) {
				it.next();
			}
			res = it.next();
			it.remove();
			symbols.addFirst(res);
			return res;
		} else if (res == symbols.size()) {
			res = input.read();
			symbols.addFirst(res);
			return res;
		}
		
		endOfStream = true;
		return EOS;
	}

	@Override
	public String toString()
	{
		return "MoveToFrontDecoder";
	}
	
	
}
