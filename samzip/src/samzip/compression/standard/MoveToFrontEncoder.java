package samzip.compression.standard;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import samzip.compression.EncoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class MoveToFrontEncoder extends EncoderMethod
{
	private Deque<Integer> symbols = new LinkedList<>();
	
	private EncoderMethod output;

	public MoveToFrontEncoder(OutputStream outputStream, EncoderMethod outputMethod)
	{
		if (outputMethod == null) {
			throw new IllegalArgumentException(Exceptions.OUTPUT_METHOD_NULL);
		}

		configureDataMode(DataMode.INT);
		this.output = outputMethod;
		setOutputStream(outputStream);
	}

	@Override
	public void initialize()
	{
		if (initialized) 
			return;

		super.initializeOutputStream();
		
		if (output != null && outputStream != null) {
			output.setOutputStream(outputStream);
			output.setDataMode(dataMode);
			output.initialize();
			initialized = true;
		} else {
			if (output == null)
				throw new CompressionException(Exceptions.ENCODER_INITIALIZE+
						": "+Exceptions.OUTPUT_METHOD_NULL+".", this);
			if (outputStream == null)
				throw new CompressionException(Exceptions.ENCODER_INITIALIZE+
						": "+Exceptions.OUTPUT_STREAM_NULL+".", this);
		}
	}
	
	@Override
	public void write(int data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		data = dataMode.convertType(data);
		
		if (!symbols.contains(data)) {
			output.write(symbols.size());
			output.write(data);
			symbols.addFirst(data);
		} else {
			int i=0;
			Iterator<Integer> it = symbols.iterator();
			while(it.hasNext()) {
				if (it.next() == data) {
					output.write(i);
					it.remove();
					symbols.addFirst(data);
					break;
				}
			++i;
			}
		}
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		output.close();
		output = null;
		super.close();
	}

	@Override
	public String toString()
	{
		return "MoveToFrontEncoder";
	}
}
