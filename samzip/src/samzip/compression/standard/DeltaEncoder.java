package samzip.compression.standard;

import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.numbercode.VByteCode;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
final public class DeltaEncoder extends EncoderMethod
{
	public final static int NOVALUE = Integer.MIN_VALUE;
	/// Holds the value in the current run.
	private int lastValue = NOVALUE;
	private EncoderMethod output = null;
	private boolean encodeFirstValueAsRaw = true;
	private boolean unsignedOutput = false;

	public void setOutputUnsigned(boolean value)
	{
		this.unsignedOutput = value;
	}

	public DeltaEncoder(OutputStream outputStream, EncoderMethod outputMethod)
	{
		if (outputMethod == null) {
			throw new IllegalArgumentException(Exceptions.OUTPUT_METHOD_NULL);
		}

		configureDataMode(DataMode.INT);
		this.output = outputMethod;
		setOutputStream(outputStream);
	}

	@Override
	public void initialize()
	{
		if (initialized) 
			return;
		
		super.initializeOutputStream();
		
		if (output != null && outputStream != null) {
			output.setOutputStream(outputStream);
			if (unsignedOutput)
				output.setDataMode(DataMode.UINT);
			else
				output.setDataMode(DataMode.INT);
			if (!encodeFirstValueAsRaw)
				output.initialize();
			initialized = true;
		} else {
			if (output == null)
				throw new CompressionException(Exceptions.ENCODER_INITIALIZE+
						": "+Exceptions.OUTPUT_METHOD_NULL+".", this);
			if (outputStream == null)
				throw new CompressionException(Exceptions.ENCODER_INITIALIZE+
						": "+Exceptions.OUTPUT_STREAM_NULL+".", this);
		}
	}

	@Override
	public void write(int data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		data = dataMode.convertType(data);
		
		if (lastValue == NOVALUE) {
			if (encodeFirstValueAsRaw) {
				if (dataMode.isSigned())
					VByteCode.encodeSigned(data, outputStream);
				else
					VByteCode.encodeUnsigned(data, outputStream);
				output.initialize();
				output.write(0);
			} else {
				output.write(data);
			}
			lastValue = data;
		} else {
			output.write(data - lastValue);
			lastValue = data;
		}
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized) 
			return;

		output.close();
		output = null;
		super.close();
	}

	@Override
	public String toString()
	{
		return "DeltaEncoder";
	}
}
