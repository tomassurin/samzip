package samzip.compression.standard;

import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class StreamRunLengthDecoder extends DecoderMethod
{
	public final static int NOVALUE = Integer.MIN_VALUE;
	/// Holds the value in the current run.
	private int lastValue = NOVALUE;
	/// Holds current run length.
	private int lastLength = NOVALUE;

	private final boolean alwaysWriteLength;
	
	public StreamRunLengthDecoder(InputStream inputStream, boolean alwaysWriteLength)
	{
		configureDataMode(DataMode.UBYTE);
		this.alwaysWriteLength = alwaysWriteLength;
		setInputStream(inputStream);
	}
	
	@Override
	protected boolean isValidDataMode(DataMode mode)
	{
		if (mode != DataMode.UBYTE) 
			return false;
		
		return true;
	}

	private final static int RUN_TRESHOLD = 4;
	private final static int MAX_LENGTH = 255+RUN_TRESHOLD-1;
	
	@Override
	public int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		if ( lastLength <= 0 ) {
			if (alwaysWriteLength) {
				lastLength = inputStream.read();
				lastValue = inputStream.read();
			} else {
				int res = inputStream.read();
				if (res == 255) {
					res = inputStream.read();
					if (res == 255) {
						return res;
					} else {
						lastLength = res + RUN_TRESHOLD;
						lastValue = inputStream.read();
					}
				} else {
					return res;
				}
			}
		}
		lastLength--;
		return lastValue;
	}

	@Override
	public String toString()
	{
		return "StreamRunLengthDecoder";
	}
}
