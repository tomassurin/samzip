package samzip.compression.standard;

import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.io.BinaryBlockFile;
import samzip.io.BinaryBlockFile.InputBlock;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RunLengthDecoder extends DecoderMethod
{
	public final static int NOVALUE = Integer.MIN_VALUE;
	/// Holds the value in the current run.
	private int lastValue = NOVALUE;
	/// Holds current run length.
	private int lastLength = NOVALUE;
	
	private DecoderMethod valueInput;
	private DecoderMethod lengthInput;
	private boolean singleInput = false;
		
	public RunLengthDecoder(InputStream inputStream, DecoderMethod decoderValue, DecoderMethod decoderLength)
	{
		if (decoderValue == null || decoderLength == null) {
			throw new IllegalArgumentException(Exceptions.INPUT_METHODS_NULL);
		}
		
		setInputStream(inputStream);
		
		if (decoderValue == decoderLength)
			singleInput = true;
		
		this.valueInput = decoderValue;
		this.lengthInput = decoderLength;
		
		configureDataMode(DataMode.INT);
	}
	
	public RunLengthDecoder(InputStream inputStream, DecoderMethod decoderMethod)
	{
		if (decoderMethod == null) {
			throw new IllegalArgumentException(Exceptions.INPUT_METHOD_NULL);
		}
	
		setInputStream(inputStream);
		singleInput = true;
		valueInput = decoderMethod;
		lengthInput = decoderMethod;		
		
		dataMode = DataMode.INT;
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null) {
			if (singleInput) {
				valueInput.setInputStream(inputStream);
				valueInput.setDataMode(DataMode.INT);
				valueInput.initialize();
			} else {
				InputBlock inputBlock = BinaryBlockFile.readBlock(inputStream);
				
				valueInput.setInputStream(inputBlock.getStream());
				valueInput.setDataMode(dataMode);
				
				lengthInput.setInputStream(inputBlock.getStream());
				lengthInput.setDataMode(DataMode.UINT);
				
				valueInput.initialize();
				lengthInput.initialize();
			}
			initialized = true;
		} else {
			throw new CompressionException(Exceptions.DECODER_INITIALIZE, this);
		}
	}

	@Override
	public int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		if ( lastLength <= 0 ) {
			lastValue = valueInput.read();
			lastLength = lengthInput.read();
		}
		lastLength--;
		return lastValue;
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		if (valueInput != null)
			valueInput.close();
		if (lengthInput != null)
			lengthInput.close();
		valueInput = null;
		lengthInput = null;
		super.close();
	}

	@Override
	public String toString()
	{
		return "RunLengthDecoder";
	}
}
