package samzip.compression.standard;

import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.io.BinaryBlockFile;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RunLengthEncoder extends EncoderMethod
{
	public final static int NOVALUE = Integer.MIN_VALUE;
	
	{
		configureDataMode(DataMode.INT);
	}
	
	/// Holds the value in the current run.
	private int lastValue = NOVALUE;
	/// Holds current run length.
	private int lastLength = NOVALUE;
	
	private EncoderMethod valueOutput;
	private EncoderMethod lengthOutput;
	private boolean singleOutput = false;
	
	private BinaryBlockFile.OutputBlock block;
	
	public RunLengthEncoder(OutputStream outputStream, EncoderMethod encoderValue, EncoderMethod encoderLength)
	{
		if (encoderValue == null || encoderLength == null) {
			throw new IllegalArgumentException("Output methods must not be null.");
		}
		
		setOutputStream(outputStream);
		
		if (encoderValue == encoderLength)
			singleOutput = true;
		
		this.valueOutput = encoderValue;
		this.lengthOutput = encoderLength;
	}

	public RunLengthEncoder(OutputStream outputStream, EncoderMethod encoderMethod)
	{
		if (encoderMethod == null) {
			throw new IllegalArgumentException("Output method must not be null.");
		}
		
		setOutputStream(outputStream);
		singleOutput = true;
		valueOutput = encoderMethod;
		lengthOutput = encoderMethod;
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		super.initializeOutputStream();
		
		if (outputStream != null) {
			if (singleOutput) {
				valueOutput.setOutputStream(outputStream);
				valueOutput.setDataMode(DataMode.INT);
				valueOutput.initialize();
			} else {
				block = new BinaryBlockFile.OutputBlock();
				valueOutput.setOutputStream(block.newStream());
				valueOutput.setDataMode(dataMode);
				
				lengthOutput.setOutputStream(block.newStream());
				lengthOutput.setDataMode(DataMode.UINT);
				
				valueOutput.initialize();
				lengthOutput.initialize();
			}
			initialized = true;
		} else {
			throw new CompressionException(Exceptions.ENCODER_INITIALIZE, this);
		}
	}

	@Override
	public void write(int data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		data = dataMode.convertType(data);
		
		if (lastValue == NOVALUE) {
			lastValue = data;
			lastLength = 1;
		} else {
			if (lastValue == data) {
				++lastLength;
			} else {
				valueOutput.write(lastValue);
				lengthOutput.write(lastLength);
				lastValue = data;
				lastLength = 1;
			}
		}
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		if (lastValue != NOVALUE) {
			valueOutput.write(lastValue);
			lengthOutput.write(lastLength);
			lastValue = NOVALUE;
			lastLength = NOVALUE;
		}
		
		if (singleOutput) {
			valueOutput.close();
			outputStream.close();
		} else {
			valueOutput.close();
			lengthOutput.close();
			
			block.close();
			
			block.writeBlock(outputStream);
			outputStream.close();
		}
		block = null;
		valueOutput = null;
		lengthOutput = null;
		outputStream = null;
		initialized = false;
	}

	@Override
	public String toString()
	{
		return "RunLengthEncoder";
	}
}
