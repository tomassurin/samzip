package samzip.compression.standard;

import java.io.IOException;
import samzip.compression.EncoderMethod;
import samzip.compression.util.CompressionException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class NullEncoder extends EncoderMethod
{
	public NullEncoder()
	{
		initialized = true;
	}
	
	@Override
	public void write(int data) throws IOException, CompressionException
	{
		
	}

	@Override
	public void close() throws IOException
	{
	}
}
