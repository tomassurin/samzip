package samzip.compression.standard;

import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class StreamRunLengthEncoder extends EncoderMethod
{
	public final static int NOVALUE = Integer.MIN_VALUE;
	
	/// Holds the value in the current run.
	private int lastValue = NOVALUE;
	/// Holds current run length.
	private int lastLength = NOVALUE;

	private final boolean alwaysWriteLength;
	
	public StreamRunLengthEncoder(OutputStream outputStream, boolean alwaysWriteLength)
	{
		setOutputStream(outputStream);
		this.alwaysWriteLength = alwaysWriteLength;
		configureDataMode(DataMode.UBYTE);
	}

	@Override
	protected boolean isValidDataMode(DataMode mode)
	{
		if (mode != DataMode.UBYTE) 
			return false;
		
		return true;
	}

	private final static int RUN_TRESHOLD = 4;
	private final static int MAX_LENGTH = 255+RUN_TRESHOLD-1;
	
	@Override
	public void write(int data) throws IOException
	{	
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		if (lastValue == NOVALUE) {
			lastValue = data;
			lastLength = 1;
		} else {
			if (lastValue == data && lastLength < MAX_LENGTH) {
				++lastLength;
			} else {
				writeRun();
				lastValue = data;
				lastLength = 1;
			}
		}
	}
	
	private void writeRun() throws IOException
	{
		if (alwaysWriteLength) {
			outputStream.write(lastLength);
			outputStream.write(lastValue & 0xff);
		} else {
			if (lastLength >= RUN_TRESHOLD) {
				outputStream.write(255);
				outputStream.write(lastLength-RUN_TRESHOLD);
				outputStream.write(lastValue & 0xff);
			} else {
				for(int i=0; i < lastLength; ++i) {
					if (lastValue == 255) {
						outputStream.write(255);
						outputStream.write(255);
					} else {
						outputStream.write(lastValue & 0xff);
					}
				}
			}
		}
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		if (lastValue != NOVALUE) {
			writeRun();
			lastValue = NOVALUE;
			lastLength = NOVALUE;
		}
		
		super.close();
	}

	@Override
	public String toString()
	{
		return "StreamRunLengthEncoder";
	}
}
