package samzip.compression.standard;

import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.numbercode.VByteCode;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
final public class DeltaDecoder extends DecoderMethod
{
	public final static int NOVALUE = Integer.MAX_VALUE;
	
	/// Holds the value in the current run.
	private int lastValue = NOVALUE;
	
	private boolean encodeFirstValueAsRaw = true;
	private boolean unsignedOutput = false;
		
	private DecoderMethod input = null;
	
	public void setOutputUnsigned(boolean value)
	{
		this.unsignedOutput = value;
	}
	
	public DeltaDecoder(InputStream inputStream, DecoderMethod inputMethod)
	{
		if (inputMethod == null)
			throw new IllegalArgumentException(Exceptions.INPUT_METHOD_NULL);
		
		
		configureDataMode(DataMode.INT);
		this.input = inputMethod;
		setInputStream(inputStream);
	}
	
	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null && input != null) {
			if (encodeFirstValueAsRaw) {
				try {
					if (dataMode.isSigned())
						lastValue = VByteCode.decodeSigned(inputStream);
					else
						lastValue = VByteCode.decodeUnsigned(inputStream);
				} catch (IOException ex) {
					throw new CompressionException(Exceptions.DECODER_INITIALIZE, ex, this);
				}
			}
			if (unsignedOutput)
				input.setDataMode(DataMode.UINT);
			else
				input.setDataMode(DataMode.INT);
			
			input.setInputStream(inputStream);
			input.initialize();
			initialized = true;
		} else {
			if (input == null)
				throw new CompressionException(Exceptions.DECODER_INITIALIZE+
						": "+ Exceptions.INPUT_METHOD_NULL, this);
			if (inputStream == null)
				throw new CompressionException(Exceptions.DECODER_INITIALIZE+
						": "+Exceptions.INPUT_STREAM_NULL, this);
		}
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		input.close();
		input = null;
		super.close();
	}
	
	@Override
	public synchronized int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		if (endOfStream)
			return EOS();
		
		int newdata = input.read();
		if (newdata == input.EOS()) {
			endOfStream = true;
			return EOS;
		}
		
		int out = newdata;
		if (lastValue != NOVALUE)
			out += lastValue;
		lastValue = out;
		return out;	
	}

	@Override
	public String toString()
	{
		return "DeltaDecoder";
	}
}

