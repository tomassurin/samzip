package samzip.compression.standard;

import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.io.StreamUtils;

/**
 * Raw encoder - takes data and writes them in original form to output stream.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
final public class RawEncoder extends EncoderMethod
{		
	public RawEncoder(OutputStream output)
	{
		setOutputStream(output);
		configureDataMode(DataMode.UBYTE);
	}
	
	public RawEncoder(OutputStream output, DataMode mode)
	{
		setDataMode(mode, true);
		setOutputStream(output);
	}

	@Override
	public void write(int data) throws IOException, CompressionException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		switch (dataMode) {
			case UBYTE:
			case BYTE:
				outputStream.write(data & 0xff);
				break;
			case INT:
			case UINT:
				StreamUtils.writeInt(data, outputStream);
				break;
			case SHORT:
			case USHORT:
				StreamUtils.writeShort((short)data, outputStream);
				break;
			default: { }
		}
	}

	@Override
	public void write(byte[] data) throws IOException, CompressionException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		outputStream.write(data);
	}

	@Override
	public void write(byte[] data, int off, int len) throws IOException, CompressionException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		outputStream.write(data, off, len);
	}
}
