package samzip.compression.standard;

import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.io.StreamUtils;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
final public class RawDecoder extends DecoderMethod
{
	private byte[] buf;

	public RawDecoder()
	{
		configureDataMode(DataMode.UBYTE);
		buf = null;
	}
	
	public RawDecoder(InputStream input)
	{
		this(input, DataMode.getDefault());
	}
	
	public RawDecoder(InputStream input, DataMode mode)
	{
		setDataMode(mode, true);
		setInputStream(input);
	}

	@Override
	public void setDataMode(DataMode mode, boolean override)
	{
		if (initialized)
			return;
		
		if (!override && dataModeConfigured)
			return;
		
		super.setDataMode(mode, override);
		
		switch(dataMode) {
			case BYTE:
			case UBYTE:
				buf = null;
				break;
			case SHORT:
			case USHORT:
				buf = new byte[2];
				break;
			case INT:
			case UINT:
				buf = new byte[4];
				break;
			default: { buf = null; }
		}
	}

	@Override
	public int read() throws IOException, CompressionException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		if (endOfStream)
			return EOS();
		
		int result;
		switch (dataMode) {
			case BYTE:
				result = inputStream.read();
				if (result == -1) {
					endOfStream = true;
					return EOS();
				}
				return result;
			case UBYTE:
				result = inputStream.read();
				if (result == -1) {
					endOfStream = true;
					return EOS();
				}
				return StreamUtils.getUnsignedByte(result);
			case INT:
				if (inputStream.read(buf) < buf.length) {
					endOfStream = true;
					return EOS();
				}
				return StreamUtils.getInt(buf);
			case UINT:
				if (inputStream.read(buf) < buf.length) {
					endOfStream = true;
					return EOS();
				}
				return (int) StreamUtils.getUnsignedInt(buf);
			case SHORT:
				if (inputStream.read(buf) < buf.length) {
					endOfStream = true;
					return EOS();
				}
				return StreamUtils.getShort(buf);
			case USHORT:
				if (inputStream.read(buf) < buf.length) {
					endOfStream = true;
					return EOS();
				}
				return StreamUtils.getUnsignedShort(buf);
			default: { return EOS();}
		}
	}

	@Override
	public int read(byte[] data) throws IOException, CompressionException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return inputStream.read(data);
	}

	@Override
	public int read(byte[] data, int off, int len) throws IOException, CompressionException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return inputStream.read(data, off, len);
	}
}
