package samzip.compression.standard;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import samzip.compression.EncoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ExpectedValueEncoder extends EncoderMethod
{
	public static enum ExpectedValue {
		ARITHMETIC_MEAN,
		MEDIAN;
	}
	
	private int expectedValue;
	private ExpectedValue method;
	private EncoderMethod output = null;
	private IntList dataBuf;
	private boolean encodeFirstValueAsRaw = true;
	
	{
		configureDataMode(DataMode.INT);
	}
	
	public ExpectedValueEncoder(OutputStream outputStream, EncoderMethod outputMethod)
	{
		this(outputStream, outputMethod, ExpectedValue.ARITHMETIC_MEAN);
	}

	public ExpectedValueEncoder(OutputStream outputStream, EncoderMethod outputMethod, ExpectedValue method)
	{
		if (outputMethod == null) {
			throw new IllegalArgumentException("Output method shouldn't be null.");
		}
		
		this.method = method;
		this.output = outputMethod;
		setOutputStream(outputStream);
	}

	@Override
	public void initialize()
	{
		if (initialized) {
			return;
		}

		super.initializeOutputStream();

		
		if (output != null && outputStream != null) {
			dataBuf = new IntArrayList(1024);
			output.setOutputStream(outputStream);
			output.setDataMode(DataMode.INT);
			if (!encodeFirstValueAsRaw)
				output.initialize();
			initialized = true;
		} else {
			super.initialize();
		}
	}

	@Override
	public void write(int data) throws CompressionException, IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		data = dataMode.convertType(data);
		dataBuf.add(data);
	}
	
	private int calculateArithmeticMean(List<Integer> dataBuf)
	{ 
		int sum = 0;
		for(int data : dataBuf) {
			sum += data;
		}
		return (int) sum/dataBuf.size();
	}
	
	private int calculateMedian(List<Integer> dataBuf)
	{
		Integer[] bufCopy = dataBuf.toArray(new Integer[0]);
		Arrays.sort(bufCopy);		
		if (bufCopy.length % 2 == 0) {
			int idx = (int) bufCopy.length/2;
			return (int)bufCopy[idx-1]+bufCopy[idx]/2;
		} else {
			return bufCopy[(int)bufCopy.length/2];
		}
	}
	
	@Override
	public void close() throws IOException, CompressionException
	{
		if (!initialized) {
			return;
		}
		
		switch (method) {
			case ARITHMETIC_MEAN:
				expectedValue =	calculateArithmeticMean(dataBuf);
				break;
			case MEDIAN:
				expectedValue =	calculateMedian(dataBuf);
				break;
		}
			
		if (encodeFirstValueAsRaw) {
			dataMode.writeToStream(outputStream, expectedValue);
			output.initialize();
		} else {
			output.write(expectedValue);
		}
		
		for(int data : dataBuf) {
			output.write(data-expectedValue);
		}
		
		dataBuf = null;
		output.close();
		output = null;
		super.close();
	}
}
