package samzip.compression.standard;

import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ExpectedValueDecoder extends DecoderMethod
{
	private int expectedValue;
	
	private boolean encodeFirstValueAsRaw = true;
	private DecoderMethod input = null;
	
	public ExpectedValueDecoder(InputStream inputStream, DecoderMethod inputMethod)
	{
		if (inputMethod == null)
			throw new IllegalArgumentException("Input method should not be null.");
					
		this.input = inputMethod;
		setInputStream(inputStream);
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null && input != null) {
			try {
				if (encodeFirstValueAsRaw) {
					expectedValue = dataMode.readFromStream(inputStream);
				}
				input.setInputStream(inputStream);
				input.initialize();
				if (!encodeFirstValueAsRaw) {
					expectedValue = input.read();
				}
			} catch (IOException ex) {
				throw new CompressionException(Exceptions.DECODER_INITIALIZE, ex);
			}
			initialized = true;
		} else {
			super.initialize();
		}
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		input.close();
		input = null;
		super.close();
	}
	
	@Override
	public synchronized int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		if (endOfStream)
			return EOS();
		
		int newdata = input.read();
		if (newdata == input.EOS()) {
			endOfStream = true;
			return EOS;
		}
		
		return newdata+expectedValue;
	}
}
