package samzip.compression.other;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.itadaki.bzip2.BZip2InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BZip2Decoder extends DecoderMethod
{
	private InputStream decoder;
	
	public BZip2Decoder()
	{
		
	}
	
	public BZip2Decoder(InputStream inputStream)
	{
		setInputStream(inputStream);
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null) {
			decoder = new BufferedInputStream(new BZip2InputStream(inputStream, false));
			initialized = true;
		} else {
			super.initialize();
		}
	}

	@Override
	protected boolean isValidDataMode(DataMode mode)
	{
		if (mode != DataMode.UBYTE)
			return false;
		
		return true;
	}
	
	@Override
	public int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return decoder.read();
	}

	@Override
	public int read(byte[] data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return decoder.read(data);
	}

	@Override
	public int read(byte[] data, int off, int len) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return decoder.read(data, off, len);
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		decoder.close();
		super.close();
	}

	@Override
	public int available() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return decoder.available();
	}

	@Override
	public String toString()
	{
		return "BZip2Decoder";
	}	
}
