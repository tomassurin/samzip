package samzip.compression.other;

import java.io.IOException;
import java.io.OutputStream;
import org.itadaki.bzip2.BZip2OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BZip2Encoder extends EncoderMethod
{
	private BZip2OutputStream encoder;
	
	public BZip2Encoder()
	{
	}

	public BZip2Encoder(OutputStream outputStream)
	{
		setOutputStream(outputStream);
	}
	
	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		super.initializeOutputStream();
		
		if (outputStream != null) {
			try {
				encoder = new BZip2OutputStream(outputStream);
			} catch (IOException ex) {
				throw new CompressionException(Exceptions.ENCODER_INITIALIZE, ex, this);
			}
			initialized = true;
		} else {
			super.initialize();
		}
	}

	@Override
	protected boolean isValidDataMode(DataMode mode)
	{
		if (mode != DataMode.UBYTE) 
			return false;
		
		return true;
	}
	
	@Override
	public void write(int data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		encoder.write(data);
	}

	@Override
	public void write(byte[] data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		encoder.write(data);
	}

	@Override
	public void write(byte[] data, int off, int len) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		encoder.write(data, off, len);
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		encoder.close();
		outputStream.flush();
		outputStream.close();
		initialized = false;
	}

	@Override
	public String toString()
	{
		return "BZip2Encoder";
	}
}
