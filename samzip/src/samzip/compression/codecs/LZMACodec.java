package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.dictionary.LZMADecoder;
import samzip.compression.dictionary.LZMAEncoder;
import samzip.compression.util.CodecException;
import samzip.compression.util.CompressionException;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LZMACodec extends CompressionCodec
{
	protected LZMAEncoder.SevenZipParams params = new LZMAEncoder.SevenZipParams();

	public LZMACodec()
	{
	}
	
	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		try {
			if (outputStream != null)
				return new LZMAEncoder(outputStream, params);
			else
				return new LZMAEncoder(params);
		} catch (CompressionException ex) {
			throw new CodecException("Invalid LZMA parameters", ex);
		}
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		if (inputStream != null)
			return new LZMADecoder(inputStream, params);
		else
			return new LZMADecoder(params);
	}

	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{		
		switch (whatKey.toLowerCase()) {
			case "dictionarysize":
				params.DictionarySize = preferences.getInteger(whatKey, params.DictionarySize);
				break;
			case "endmarkermode":
				params.endMarkerMode = preferences.getBoolean(whatKey, false);
				break;
			default: { }
		}
		
		super.preferenceChanged(whatKey);
	}

	@Override
	public String toString()
	{
		return "LZMA Codec";
	}
}
