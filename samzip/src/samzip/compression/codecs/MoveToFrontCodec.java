package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.standard.MoveToFrontDecoder;
import samzip.compression.standard.MoveToFrontEncoder;
import samzip.compression.util.CodecException;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public final class MoveToFrontCodec extends CompressionCodec
{
	private CompressionCodec nextMethod;
	
	public MoveToFrontCodec()
	{
//		addCodec("default", new RawCodec(dataMode));
	}
	
	public MoveToFrontCodec(CompressionCodec nextMethod)
	{
		if (nextMethod == null)
			throw new IllegalArgumentException(Exceptions.OUTPUT_CODEC_NULL);
		
		addCodec("default", nextMethod);
	}

	@Override
	public void addCodec(String pipeID, CompressionCodec other)
	{
		nextMethod = other;
	}
	
	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		if (nextMethod == null) 
			throw new CodecException(Exceptions.REQUIRED_CODEC, this);
		
		return new MoveToFrontEncoder(outputStream, nextMethod.newEncoderMethod());
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		if (nextMethod == null) 
			throw new CodecException(Exceptions.REQUIRED_CODEC, this);
		
		return new MoveToFrontDecoder(inputStream, nextMethod.newDecoderMethod());
	}

	@Override
	public String toString()
	{
		return "MoveToFrontCodec";
	}
}
