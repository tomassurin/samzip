package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.numbercode.NumberDecoder;
import samzip.compression.numbercode.NumberEncoder;
import samzip.compression.numbercode.VByteCode;
import samzip.compression.util.CodecException;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class VByteCodec extends CompressionCodec
{
	public VByteCodec()
	{
	}

	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		NumberEncoder result;
		try {
			result = new NumberEncoder(VByteCode.class, outputStream);
		} catch (InstantiationException | IllegalAccessException ex) {
			throw new CodecException(Exceptions.ENCODER_METHOD_CREATION, ex, this);
		}
		return result;
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		NumberDecoder result;
		try {
			result = new NumberDecoder(VByteCode.class, inputStream);
		} catch (InstantiationException | IllegalAccessException ex) {
			throw new CodecException(Exceptions.DECODER_METHOD_CREATION, ex, this);
		} 
		return result;
	}
}
