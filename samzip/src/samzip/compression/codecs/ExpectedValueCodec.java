package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.standard.ExpectedValueDecoder;
import samzip.compression.standard.ExpectedValueEncoder;
import samzip.compression.util.CodecException;
import samzip.compression.util.Exceptions;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public final class ExpectedValueCodec extends CompressionCodec
{
	protected ExpectedValueEncoder.ExpectedValue expectedValueMethod = ExpectedValueEncoder.ExpectedValue.ARITHMETIC_MEAN;
	private CompressionCodec nextMethod;
	
	public ExpectedValueCodec()
	{
	}

	@Override
	public void addCodec(String pipeID, CompressionCodec other)
	{
		nextMethod = other;
	}
	
	public ExpectedValueCodec(CompressionCodec nextMethod) throws CodecException
	{
		if (nextMethod == null)
			throw new IllegalArgumentException(Exceptions.OUTPUT_CODEC_NULL);
		
		addCodec("default", nextMethod);
	}
	
	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		if (whatKey.toLowerCase().equals("statistics")) {
			try {
				this.expectedValueMethod = ExpectedValueEncoder.ExpectedValue.valueOf(
						preferences.getString(whatKey, "ARITHMETIC_MEAN"));
			} catch (IllegalArgumentException ex) {
				throw new PreferencesException("Bad expected value encoder statistics", whatKey);
			}
			return;
		}
		super.preferenceChanged(whatKey);
	}
	
	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		if (nextMethod == null) 
			throw new CodecException(Exceptions.REQUIRED_CODEC);
		
		return new ExpectedValueEncoder(outputStream, nextMethod.newEncoderMethod(), expectedValueMethod);
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		if (nextMethod == null) 
			throw new CodecException(Exceptions.REQUIRED_CODEC);
		
		return new ExpectedValueDecoder(inputStream, nextMethod.newDecoderMethod()); 
	}
}
