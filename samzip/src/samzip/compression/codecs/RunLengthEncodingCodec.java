package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.standard.RunLengthDecoder;
import samzip.compression.standard.RunLengthEncoder;
import samzip.compression.standard.StreamRunLengthDecoder;
import samzip.compression.standard.StreamRunLengthEncoder;
import samzip.compression.util.CodecException;
import samzip.compression.util.Exceptions;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public final class RunLengthEncodingCodec extends CompressionCodec
{
	private CompressionCodec valueCodec;
	private CompressionCodec lengthCodec;
	private boolean singlePipe = true;
	
	private RLEAlgorithm algorithm = RLEAlgorithm.UNIVERSAL;
	
	private static enum RLEAlgorithm
	{
		UNIVERSAL,
		STREAM,
		STREAM_LENGTH;
	}

	@Override
	public String toString()
	{
		return "Run Length Encoding Codec";
	}
	
	public RunLengthEncodingCodec() throws CodecException
	{
//		addCodec("default", new HuffmanCodec());
	}

	public RunLengthEncodingCodec(CompressionCodec valueCodec, CompressionCodec lengthCodec) throws CodecException
	{
		this();
		addCodec("value", valueCodec);
		addCodec("length", lengthCodec);
	}

	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		if (whatKey.toLowerCase().equals("algorithm") || 
			whatKey.toLowerCase().equals("algo")) {
			String alg = preferences.getString(whatKey, "UNIVERSAL");
			switch (alg.toLowerCase()) {
				case "stream":
					this.algorithm = RLEAlgorithm.STREAM;
					break;
				case "streamlength":
					this.algorithm = RLEAlgorithm.STREAM_LENGTH;
					break;
				case "universal":
					this.algorithm = RLEAlgorithm.UNIVERSAL;
					break;
				default:
					this.algorithm = RLEAlgorithm.UNIVERSAL;
			}
			return;
		}
		super.preferenceChanged(whatKey);
	}
	
	@Override
	public void addCodec(String codecID, CompressionCodec other)
	{
		switch (codecID) {
			case "value":
				valueCodec = other;
				singlePipe = false;
				break;
			case "all":
			case "both":
				valueCodec = other;
				singlePipe = true;
				break;
			case "length":
				lengthCodec = other;
				singlePipe = false;
				break;
			default: { 
				super.addCodec(codecID, other);
			}
		}
	}

	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		if (algorithm == RLEAlgorithm.STREAM) {
			return new StreamRunLengthEncoder(outputStream, false);
		} else if (algorithm == RLEAlgorithm.STREAM_LENGTH) {
			return new StreamRunLengthEncoder(outputStream, true);
		} else {
			if (singlePipe) {
				if (valueCodec == null)
					throw new CodecException(Exceptions.REQUIRED_CODEC, this);

				EncoderMethod result = new RunLengthEncoder(outputStream, valueCodec.newEncoderMethod());
				return result;
			} else {
				if (valueCodec == null)
					throw new CodecException("Required codec for coding value was not specified",this);
				if (lengthCodec == null)
					throw new CodecException("Required codec for coding length was not specified", this);

				EncoderMethod result = new RunLengthEncoder(outputStream, valueCodec.newEncoderMethod(), lengthCodec.newEncoderMethod());
				return result;
			}
		} 
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		if (algorithm == RLEAlgorithm.STREAM) {
			return new StreamRunLengthDecoder(inputStream, false);
		} else if (algorithm == RLEAlgorithm.STREAM_LENGTH)  {
			return new StreamRunLengthDecoder(inputStream, true);
		} else {
			if (singlePipe) {
				if (valueCodec == null)
					throw new CodecException(Exceptions.REQUIRED_CODEC, this);

				DecoderMethod result = new RunLengthDecoder(inputStream, valueCodec.newDecoderMethod());
				return result;
			} else {
				if (valueCodec == null)
					throw new CodecException("Required codec for coding value was not specified", this);
				if (lengthCodec == null)
					throw new CodecException("Required codec for coding length was not specified", this);

				DecoderMethod result = new RunLengthDecoder(inputStream, valueCodec.newDecoderMethod(), lengthCodec.newDecoderMethod());
				return result;
			}
		}
	}
}
