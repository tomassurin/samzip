package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.numbercode.AdaptiveGolombDecoder;
import samzip.compression.numbercode.AdaptiveGolombEncoder;
import samzip.compression.numbercode.GolombCode;
import samzip.compression.numbercode.NumberDecoder;
import samzip.compression.numbercode.NumberEncoder;
import samzip.compression.numbercode.RiceCode;
import samzip.compression.util.CodecException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class GolombCodec extends CompressionCodec
{
	private boolean signed = false;
	private boolean adaptive = true;
	private int M = 1024;
	
	public GolombCodec()
	{		
	}
	
	public GolombCodec(DataMode mode, int m) throws CodecException
	{
		setDataMode(mode);
		this.M = m;
		adaptive = false;
	}
	
	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		try {
			if (adaptive) {
				EncoderMethod result = new AdaptiveGolombEncoder(outputStream);
				return result;
			} else {
				NumberEncoder result;
				if (M % 2 == 0) {
					result = new NumberEncoder(RiceCode.class, outputStream);
				} else {
					result = new NumberEncoder(GolombCode.class, outputStream);
				}
				try {
					result.setCoderOption("m", String.valueOf(M));
				} catch (PreferencesException ex) {
					throw new CodecException(ex, this);
				}
				return (EncoderMethod) result;
			}
		} catch (InstantiationException | IllegalAccessException ex) {
			throw new CodecException(Exceptions.ENCODER_METHOD_CREATION, ex, this);
		}
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		try {
			if (adaptive) {
				DecoderMethod result = new AdaptiveGolombDecoder(inputStream);
				return result;
			} else {
				NumberDecoder result;
				if (M % 2 == 0) {
					result = new NumberDecoder(RiceCode.class, inputStream);
				} else {
					result = new NumberDecoder(GolombCode.class, inputStream);
				}
				try {
					result.setCoderOption("m", String.valueOf(M));
				} catch (PreferencesException ex) {
					throw new CodecException(ex, this);
				}
				return (DecoderMethod) result;
			}
		} catch (InstantiationException | IllegalAccessException ex) {
			throw new CodecException(Exceptions.DECODER_METHOD_CREATION, ex, this);
		}
	}	

	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		switch (whatKey.toLowerCase()) {
			case "adaptive":
				adaptive = preferences.getBoolean(whatKey, true);
				break;
			case "m":
				adaptive = false;
				M = preferences.getInteger(whatKey, 1024);
				break;
			default: { 
				super.preferenceChanged(whatKey); 
			}
		}
	}

	@Override
	public String toString()
	{
		return "Golomb Codec";
	}	
}
