package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.util.CodecException;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.standard.NullDecoder;
import samzip.compression.standard.NullEncoder;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class NullCodec extends CompressionCodec
{

	public NullCodec()
	{
		
	}
	
	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		return new NullEncoder();
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		return new NullDecoder();
	}
}
