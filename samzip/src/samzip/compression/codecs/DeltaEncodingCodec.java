package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.standard.DeltaDecoder;
import samzip.compression.standard.DeltaEncoder;
import samzip.compression.util.CodecException;
import samzip.compression.util.Exceptions;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public final class DeltaEncodingCodec extends CompressionCodec
{	
	private boolean unsignedOutput = false;
	private CompressionCodec outputMethod;
	
	public DeltaEncodingCodec()
	{
//		addCodec("default", new RawCodec(dataMode));
	}

	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		if (whatKey.toLowerCase().contains("unsigned")) {
			unsignedOutput = preferences.getBoolean(whatKey, false);
			return;
		}
		super.preferenceChanged(whatKey);
	}
	
	public DeltaEncodingCodec(CompressionCodec nextMethod)
	{
		this();
		if (nextMethod == null)
			throw new IllegalArgumentException(Exceptions.OUTPUT_CODEC_NULL);
		
		addCodec("default", nextMethod);
	}

	@Override
	public void addCodec(String pipeID, CompressionCodec other)
	{
		outputMethod = other;
	}
	
	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		if (outputMethod == null) 
			throw new CodecException(Exceptions.REQUIRED_CODEC, this);
	
		DeltaEncoder enc = new DeltaEncoder(outputStream, outputMethod.newEncoderMethod());
		enc.setOutputUnsigned(unsignedOutput);
		return enc;
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		if (outputMethod == null) 
			throw new CodecException(Exceptions.REQUIRED_CODEC, this);
		
		DeltaDecoder dec = new DeltaDecoder(inputStream, outputMethod.newDecoderMethod());
		dec.setOutputUnsigned(unsignedOutput);
		return dec;
	}

	@Override
	public String toString()
	{
		return "DeltaEncodingCodec";
	}
}
