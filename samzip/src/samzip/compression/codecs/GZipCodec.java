package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.dictionary.GZIPDecoder;
import samzip.compression.dictionary.GZIPEncoder;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class GZipCodec extends CompressionCodec
{
	public GZipCodec()
	{
	}

	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream)
	{
		if (outputStream != null)
			return new GZIPEncoder(outputStream);
		else
			return new GZIPEncoder(); 
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream)
	{
		if (inputStream != null)
			return new GZIPDecoder(inputStream);
		else 
			return new GZIPDecoder();
	}
}
