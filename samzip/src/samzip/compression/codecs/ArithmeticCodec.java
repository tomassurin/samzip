package samzip.compression.codecs;

import com.colloquial.arithcode.AdaptiveUnigramModel;
import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.numbercode.ArithmeticDecoder;
import samzip.compression.numbercode.ArithmeticEncoder;
import samzip.compression.util.CodecException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ArithmeticCodec extends CompressionCodec
{
	public ArithmeticCodec()
	{	
	}
	
	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		return new ArithmeticEncoder(outputStream, new AdaptiveUnigramModel());
//		return new ArithmeticEncoder(outputStream, new PPMModel(10));
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		return new ArithmeticDecoder(inputStream, new AdaptiveUnigramModel());
//		return new ArithmeticDecoder(inputStream, new PPMModel(10));
	}
}
