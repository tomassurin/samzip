package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.standard.RawDecoder;
import samzip.compression.standard.RawEncoder;
import samzip.compression.util.CodecException;
import samzip.compression.util.DataMode;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RawCodec extends CompressionCodec
{	
	public RawCodec() 
	{
	}
	
	public RawCodec(DataMode mode) throws CodecException
	{
		setDataMode(mode);
	}

	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		return new RawEncoder(outputStream);
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		return new RawDecoder(inputStream);
	}
}
