package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.other.BZip2Decoder;
import samzip.compression.other.BZip2Encoder;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BZip2Codec extends CompressionCodec
{
	public BZip2Codec()
	{
	}

	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream)
	{
		if (outputStream != null)
			return new BZip2Encoder(outputStream);
		else
			return new BZip2Encoder(); 
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream)
	{
		if (inputStream != null)
			return new BZip2Decoder(inputStream);
		else 
			return new BZip2Decoder();
	}
}
