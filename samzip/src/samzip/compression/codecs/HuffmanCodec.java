package samzip.compression.codecs;

import java.io.InputStream;
import java.io.OutputStream;
import samzip.compression.CompressionCodec;
import samzip.compression.DecoderMethod;
import samzip.compression.EncoderMethod;
import samzip.compression.numbercode.HuffmanDecoder;
import samzip.compression.numbercode.HuffmanEncoder;
import samzip.compression.util.CodecException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class HuffmanCodec extends CompressionCodec
{
	public HuffmanCodec()
	{	 
	}
	
	@Override
	protected EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException
	{
		return new HuffmanEncoder(outputStream);
	}

	@Override
	protected DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException
	{
		return new HuffmanDecoder(inputStream);
	}
	
}
