package samzip.compression;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import samzip.compression.util.CodecException;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.io.BinaryBlockFile;
import samzip.util.MethodWithPreferences;
import samzip.util.PreferencesException;
import samzip.api.SamzException;

/**
 * <p>Abstract class providing base implementation for compression codecs  (pairs of Encoder/Decoder method).
 * It's main responsibility is creation of correctly configured EncoderMethod and DecoderMethod instances. </p>
 * 
 * <p>It also handles codec configuration, creation of connected methods (next method uses previous method's output as it's input)
 * and provides methods that are being used in implementation of shared codec.</p>
 * 
 * <p>Shared codec creates only singleton instance of it's Encoder/Decoder method. 
 * New instance of Encoder/Decoder method can be constructed with call to method {@code reset()}.</p>
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public abstract class CompressionCodec extends MethodWithPreferences
{
	/** codec's data mode, overrides data modes specified in newEncoderMethod and newDecoderMethod */
	private DataMode codecDataMode = null;//= DataMode.getDefault();
	/** next method instance, can be configured with {@code addCodec('default', method)} */
	private CompressionCodec nextMethod;
	/** codec display name */
	protected String codecName;
	/** codec identifier that was specified in compression profile */
	protected String codecId;
	
	public CompressionCodec()
	{
	}
	
	public final DataMode getDataMode()
	{
		return codecDataMode;
	}

	public void setDataMode(DataMode dataMode)
	{			
		this.codecDataMode = dataMode;
	}
	
	public final void setName(String name)
	{
		this.codecName = name;
	}

	public final String getName()
	{
		return codecName;
	}
	
	public final void setId(String id)
	{
		this.codecId = id;
	}

	public final String getId()
	{
		return codecId;
	}
	
	/**
	 * Connects codec {@code codec} to named pipe with id {@code id}.
	 * Depending on codec implementation, there can be any number of named pipes.
	 * 
	 * @param pipeID named pipe id
	 * @param codec connected codec
	 */
	@Override
	public void addCodec(String pipeID, CompressionCodec codec)
	{
		nextMethod = codec;
	}
	
	/** {@inheritDoc} */
	@Override
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
		switch(whatKey.toLowerCase()) {
			case "mode":
				DataMode mode;
				try {
					mode = DataMode.valueOf(preferences.getString(whatKey, "BYTE"));
				} catch (IllegalArgumentException ex) {
					throw new PreferencesException("Bad data mode value: "+preferences.getString("mode", "BYTE"), whatKey);
				}
				setDataMode(mode);
				break;
			default: { 
				super.preferenceChanged(whatKey);
			}	
		}
	}
	
	/**
	 * Framework expects that this method returns unitialized instance of codec's EncoderMethod. 
	 * 
	 * @param outputStrem output stream that will be used as output for EncoderMethod. 
	 * In some situations can be null.
	 * @throws CodecException if there is some problem with codec configuration
	 */
	protected abstract EncoderMethod createRawEncoderMethod(OutputStream outputStream) throws CodecException;
	
	/**
	 * Framework expects that this method returns unitialized instance of codec's DecoderMethod.
	 * 
	 * @param inputStream input stream that will be used as input for returned instance of DecoderMethod. 
	 * In some situations can be null.
	 * @throws CodecException if there is some problem with codec configuration
	 */
	protected abstract DecoderMethod createRawDecoderMethod(InputStream inputStream) throws CodecException;
	
	/** 
	 * Creates codec's EncoderMethod instance that will be using stream {@code outputStream} 
	 * as it's output. Returned method will be configured with codec configuration, default data mode
	 * and will be correctly initialized.
	 * 
	 * @throws CodecException if problem occurs while creating EncoderMethod (i.e wrong codec configuration)
	 */
	public final EncoderMethod newEncoderMethod(OutputStream outputStream) throws CodecException
	{
		return newEncoderMethod(outputStream, codecDataMode);
	}
	
	/** 
	 * Creates codec's EncoderMethod instance that will be using stream {@code outputStream} 
	 * as it's output. Returned method will be configured with codec configuration, data mode {@code dataMode}
	 * and will be correctly initialized.
	 * 
	 * @throws CodecException if problem occurs while creating EncoderMethod (i.e wrong codec configuration)
	 */
	public final EncoderMethod newEncoderMethod(OutputStream outputStream, DataMode dataMode) throws CodecException
	{
		if (outputStream == null)
			throw new IllegalArgumentException(Exceptions.OUTPUT_STREAM_NULL);
		
		if (nextMethod != null) {
			EncoderMethod nextEnc =	nextMethod.newEncoderMethod();
			nextEnc.setDataMode(DataMode.UBYTE);
			nextEnc.setOutputStream(outputStream);
			initializeEncoderMethod(nextEnc);
			outputStream = nextEnc;
		}
		
		EncoderMethod enc = createRawEncoderMethod(outputStream); 
		if (codecDataMode != null)
			enc.setDataMode(codecDataMode, true);
		else if (dataMode != null)
			enc.setDataMode(dataMode, true);
			
		initializeEncoderMethod(enc);
		return enc;
	}
	
	/** 
	 * Creates codec's DecoderMethod instance that will be using stream {@code inputStream} 
	 * as it's input. Returned method will be configured with codec configuration, default data mode
	 * and will be correctly initialized.
	 * 
	 * @throws CodecException if problem occurs while creating DecoderMethod (i.e wrong codec configuration)
	 */
	public final DecoderMethod newDecoderMethod(InputStream inputStream) throws CodecException
	{
		return newDecoderMethod(inputStream, codecDataMode);
	}
	
	/** 
	 * Creates codec's DecoderMethod instance that will be using stream {@code inputStream} 
	 * as it's input. Returned method will be configured with codec configuration, data mode {@code dataMode}
	 * and will be correctly initialized.
	 * 
	 * @throws CodecException if problem occurs while creating DecoderMethod (i.e wrong codec configuration)
	 */
	public final DecoderMethod newDecoderMethod(InputStream inputStream, DataMode dataMode) throws CodecException
	{
		if (inputStream == null)
			throw new IllegalArgumentException(Exceptions.INPUT_STREAM_NULL);

		if (nextMethod != null) {
			DecoderMethod nextDec =	nextMethod.newDecoderMethod();
			nextDec.setDataMode(DataMode.UBYTE);
			nextDec.setInputStream(inputStream);
			initializeDecoderMethod(nextDec);
			inputStream = nextDec;
		}
		
		DecoderMethod dec = createRawDecoderMethod(inputStream);
		if (codecDataMode != null)
			dec.setDataMode(codecDataMode, true);
		else if (dataMode != null)
			dec.setDataMode(dataMode, true);
		initializeDecoderMethod(dec);
		return dec;
	}
	
		
	/** 
	 * Creates unitialized EncoderMethod instance. Returned method will be configured with codec configuration
	 * and will have set default data mode.
	 * 
	 * @throws CodecException if problem occurs while creating EncoderMethod (i.e wrong codec configuration)
	 */
	public final EncoderMethod newEncoderMethod() throws CodecException
	{
		OutputStream outputStream = null;
		if (nextMethod != null) {
			EncoderMethod nextEnc =	nextMethod.newEncoderMethod();
			nextEnc.setDataMode(DataMode.UBYTE);
			outputStream = nextEnc;
		}
		
		EncoderMethod method = createRawEncoderMethod(outputStream);
		if (codecDataMode != null)
			method.setDataMode(codecDataMode, true);
		return method;
	}

	/** 
	 * Creates unitialized DecoderMethod instance. Returned method will be configured with codec configuration
	 * and will have set default data mode.
	 * 
	 * @throws CodecException if problem occurs while creating DecoderMethod (i.e wrong codec configuration)
	 */
	public final DecoderMethod newDecoderMethod() throws CodecException
	{
		InputStream inputStream = null;
		if (nextMethod != null) {
			DecoderMethod nextDec =	nextMethod.newDecoderMethod();
			nextDec.setDataMode(DataMode.UBYTE);
			inputStream = nextDec;
		}
		
		DecoderMethod method = createRawDecoderMethod(inputStream);
		if (codecDataMode != null)
			method.setDataMode(codecDataMode, true);
		return method;
	}	
	
	protected final void initializeEncoderMethod(EncoderMethod enc) throws CodecException
	{
		try {
			enc.initialize();
		} catch (CompressionException ex) {
			throw new CodecException(Exceptions.ENCODER_INITIALIZE, ex, this);
		}
	}
	
	protected final void initializeDecoderMethod(DecoderMethod dec) throws CodecException
	{
		try {
			dec.initialize();
		} catch (CompressionException ex) {
			throw new CodecException(Exceptions.DECODER_INITIALIZE, ex, this);
		}
	}
    /******************************************************************************/                              
	/** shared codec methods and helpers */
	private boolean shared = false;
	/** singleton instance of codec's EncoderMethod */
	private EncoderMethod sharedEncoderMethod;
	/** singleton instance of codec's DecoderMethod */
	private DecoderMethod sharedDecoderMethod;
	/** encoder method reference counter */
	private int sharedEncRefCount = 0;
	/** decoder method reference counter */
	private int sharedDecRefCount = 0;
	
	/**
	 * Sets shared preference. 
	 * 
	 * <p>Shared codec has only singleton instance of it's Encoder and Decoder methods. 
	 * New instance of Encoder/Decoder method can be constructed with call to method {@code reset()}.</p>
     * 
	 * @param shared 
	 */
	public final void setShared(boolean shared)
	{
		this.shared = shared;
	}
	
	/**
	 * Returns set of shared codecs that are connected via pipe to this codec.
	 */
	public final Set<CompressionCodec> getSharedCodecs()
	{
		Set<CompressionCodec> sharedCodecs = new HashSet<>();
		getSharedCodecs(sharedCodecs);
		return sharedCodecs;
	}
	
	private final void getSharedCodecs(Set<CompressionCodec> sharedCodecs)
	{
		if (shared) {
			if (sharedCodecs.contains(this)) {
				throw new SamzException("Cycle in shared codecs definition for codec with id "+codecId);
			}
			sharedCodecs.add(this);
		}
		
		if (nextMethod != null) {
			nextMethod.getSharedCodecs(sharedCodecs);
		}
	}
	
	/**
	 * High level method that creates initialized EncoderMethod instance.
	 * This method also implements shared codec behavior. 
	 * 
	 * @param block output block that is being used for creation of new OutputStream 
	 *        (if codec is shared and there is already created EncoderMethod instance 
	 *	      no stream is created)
	 * @param defaultName display name for created stream (this is shown in statistics)
	 * @param dataMode requested data mode for EncoderMethod, if codec data mode was already set
	 *		  via {@code setDataMode(mode)} this modes are compared for compatibility (ok if 
	 *	      codec mode have bigger range than requested codec)
	 * @return initialized and configured instance of EncoderMethod
	 * @throws CodecException if there is any problem with codec configuration or requested
	 *			              data mode is not compatible with codec's data mode
	 * @throws IOException 
	 */
	public final EncoderMethod newEncoderMethod(BinaryBlockFile.OutputBlock block, String defaultName, DataMode dataMode) throws CodecException, IOException
	{
		String blockName = codecName == null ? defaultName : codecName;
		dataMode = codecDataMode == null ? dataMode : codecDataMode;
		
		if (shared) {
			if (sharedEncoderMethod == null) { // shared encoder method wasn't created yet
				OutputStream outputStream = block.newStream(blockName);
				sharedEncoderMethod = newEncoderMethod(outputStream, dataMode);
			}	
			if (sharedEncoderMethod != null) {
				if (dataMode != null) {
					if (!dataMode.compatibleWith(sharedEncoderMethod.getDataMode())) {
						throw new CodecException("Incompatible data modes for shared codec with id "+
								                codecId+" (requested: "+dataMode.toString()+" was: "+
								                sharedEncoderMethod.getDataMode().toString()+")");
					}
				}
			}
			++sharedEncRefCount;
			return new SharedEncoderMethod(sharedEncoderMethod);
		} else {
			OutputStream outputStream = block.newStream(blockName);
			return newEncoderMethod(outputStream, dataMode);
		}
	}
	
	/**
	 * High level method that creates initialized DecoderMethod instance.
	 * This method also implements shared codec behavior. 
	 * 
	 * @param block input block that is requested for next InputStream
	 *        (if codec is shared and there is already created DecoderMethod instance 
	 *	      no stream is requested)
	 * @param dataMode requested data mode for DecoderMethod. If codec data mode was already set
	 *		  via {@code setDataMode(mode)} this modes are compared for compatibility (ok if 
	 *	      codec mode have bigger range than requested codec)
	 * @return initialized and configured instance of DecoderMethod
	 * @throws CodecException if there is any problem with codec configuration or requested
	 *			              data mode is not compatible with codec's data mode
	 * @throws IOException 
	 */
	public final DecoderMethod newDecoderMethod(BinaryBlockFile.InputBlock block, DataMode dataMode) throws CodecException, IOException
	{	
		dataMode = codecDataMode == null ? dataMode : codecDataMode;
		if (shared) {
			if (sharedDecoderMethod == null) { 
				InputStream inputStream = block.getStream();
				sharedDecoderMethod = newDecoderMethod(inputStream, dataMode);
			}
			if (sharedDecoderMethod != null) {
				if (dataMode != null) {
					if (!dataMode.compatibleWith(sharedDecoderMethod.getDataMode())) {
						throw new CodecException("Incompatible data modes for shared codec with id "+
												codecId+" (requested: "+dataMode.toString()+" was: "+
												sharedDecoderMethod.getDataMode().toString()+")");
					}
				}
			}
			++sharedDecRefCount;
			return new SharedDecoderMethod(sharedDecoderMethod);
		} else {
			InputStream inputStream = block.getStream();
			return newDecoderMethod(inputStream, dataMode);
		}
	}
	
	/**
	 * If this codec is shared decrements encoder and decoder method reference counts.
	 * If reference counts hit zero shared Encoder/Decoder methods are closed and destroyed.
	 * @throws IOException if problem occurs while closing Encoder/Decoder methods
	 */
	public final void reset() throws IOException
	{
		if (sharedEncRefCount > 1 || sharedDecRefCount > 1) {
			--sharedEncRefCount;
			--sharedDecRefCount;
		} else {
			sharedEncRefCount = 0; sharedDecRefCount = 0;
			
			if (sharedEncoderMethod != null) {
				sharedEncoderMethod.close();
			}
			if (sharedDecoderMethod != null) {
				sharedDecoderMethod.close();
			}	
			
			if (sharedEncoderMethod != null && sharedEncoderMethod instanceof SharedEncoderMethod) {
				((SharedEncoderMethod) sharedEncoderMethod).closeShared();		
			}
			if (sharedDecoderMethod != null && sharedDecoderMethod instanceof SharedDecoderMethod) {
				((SharedDecoderMethod) sharedDecoderMethod).closeShared();
			}

			sharedEncoderMethod = null;
			sharedDecoderMethod = null;

			if (nextMethod != null) {
				nextMethod.reset();
			}
		}
	}
	
	/** Wraps EncoderMethod instance and don't allow it to close with standard 
	 * {@link close()} call in order to implement shared encoder method. 
	 */
	private final class SharedEncoderMethod extends EncoderMethod
	{
		private final EncoderMethod enc;

		public SharedEncoderMethod(EncoderMethod enc)
		{
			this.enc = enc;
		}

		@Override
		public void flush() throws IOException
		{
			enc.flush();
		}

		@Override
		public void write(byte[] data, int off, int len) throws IOException
		{
			enc.write(data, off, len);
		}

		@Override
		public void write(byte[] data) throws IOException
		{
			enc.write(data);
		}

		@Override
		public void write(int data) throws IOException
		{
			enc.write(data);
		}

		@Override
		public void setOutputStream(OutputStream outputStream)
		{
			enc.setOutputStream(outputStream);
		}

		@Override
		public void setDataMode(DataMode mode)
		{
			enc.setDataMode(mode);
		}

		@Override
		public void initialize()
		{
			enc.initialize();
		}

		@Override
		public OutputStream getOutputStream()
		{
			return enc.getOutputStream();
		}

		@Override
		public DataMode getDataMode()
		{
			return enc.getDataMode();
		}

		@Override
		public void close() throws IOException
		{
//			enc.close();
		}
		
		public void closeShared() throws IOException
		{
			enc.close();
		}
	}
	
	/** Wraps DecoderMethod instance and don't allow it to close with standard 
	 * {@link close()} call in order to implement shared decoder method.
	 */
	private final class SharedDecoderMethod extends DecoderMethod
	{
		private final DecoderMethod dec;
		
		public SharedDecoderMethod(DecoderMethod dec)
		{
			this.dec = dec;
		}

		@Override
		public long skip(long n) throws IOException
		{
			return dec.skip(n);
		}

		@Override
		public synchronized void reset() throws IOException
		{
			dec.reset();
		}

		@Override
		public boolean markSupported()
		{
			return dec.markSupported();
		}

		@Override
		public synchronized void mark(int readlimit)
		{
			dec.mark(readlimit);
		}

		@Override
		public void setInputStream(InputStream inputStream)
		{
			dec.setInputStream(inputStream);
		}

		@Override
		public void setDataMode(DataMode mode)
		{
			dec.setDataMode(mode);
		}

		@Override
		public int read(byte[] data, int off, int len) throws IOException
		{
			return dec.read(data, off, len);
		}

		@Override
		public int read(byte[] data) throws IOException
		{
			return dec.read(data);
		}

		@Override
		public int read() throws IOException
		{
			return dec.read();
		}

		@Override
		public void initialize()
		{
			dec.initialize();
		}

		@Override
		public InputStream getInputStream()
		{
			return dec.getInputStream();
		}

		@Override
		public DataMode getDataMode()
		{
			return dec.getDataMode();
		}

		@Override
		public void close() throws IOException
		{
//			dec.close();
		}
		
		public void closeShared() throws IOException
		{
			dec.close();
		}

		@Override
		public int available() throws IOException
		{
			return dec.available();
		}		
	}
}
