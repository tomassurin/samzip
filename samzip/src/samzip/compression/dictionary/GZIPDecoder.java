package samzip.compression.dictionary;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class GZIPDecoder extends DecoderMethod
{
	private InputStream decoder;
	private int gzipBufferSize = 2048;

	public GZIPDecoder()
	{
		
	}
	
	public GZIPDecoder(InputStream inputStream)
	{
		setInputStream(inputStream);
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null) {
			try {
				decoder = new BufferedInputStream(new GZIPInputStream(inputStream, gzipBufferSize), 1 << 14);
			} catch (IOException ex) {
				throw new CompressionException(Exceptions.DECODER_INITIALIZE, ex, this);			
			}
			initialized = true;
		} else {
			super.initialize();
		}
	}

	@Override
	protected boolean isValidDataMode(DataMode mode)
	{
		if (mode != DataMode.UBYTE)
			return false;
		
		return true;
	}
	
	@Override
	public int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return decoder.read();
	}

	@Override
	public int read(byte[] data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return decoder.read(data);
	}

	@Override
	public int read(byte[] data, int off, int len) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return decoder.read(data, off, len);
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		decoder.close();
		super.close();
	}

	@Override
	public int available() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return decoder.available();
	}

	@Override
	public String toString()
	{
		return "GZIPDecoder";
	}	
}
