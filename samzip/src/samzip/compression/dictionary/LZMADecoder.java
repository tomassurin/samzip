package samzip.compression.dictionary;

import SevenZip.Compression.LZMA.Decoder;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.io.FastByteArrayOutputStream;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LZMADecoder extends DecoderMethod
{	
	protected Decoder encoder = new Decoder();
	
	private final int INITIAL_BUFFER_SIZE = 2048;
	
	private ByteArrayInputStream bufInput;
	private boolean endMarkerMode = false;
	
	{
		configureDataMode(DataMode.UBYTE);
	}
	
	@Override
	protected boolean isValidDataMode(DataMode mode)
	{
		if (mode != DataMode.UBYTE) 
			return false;
		
		return true;
	}
	
	public LZMADecoder(final InputStream inputStream, final LZMAEncoder.SevenZipParams params)
	{
		endMarkerMode = params.endMarkerMode;
		setInputStream(inputStream);
	}

	public LZMADecoder()
	{
		this(null, new LZMAEncoder.SevenZipParams());
	}
	
	public LZMADecoder(final LZMAEncoder.SevenZipParams params)
	{
		this(null, params);
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null) {
			try {
				int propertiesSize = 5;
				byte[] properties = new byte[propertiesSize];
				if (inputStream.read(properties, 0, propertiesSize) != propertiesSize)
					throw new CompressionException("Input lzma stream is too short", this);
				final SevenZip.Compression.LZMA.Decoder decoder = new SevenZip.Compression.LZMA.Decoder();
				if (!decoder.SetDecoderProperties(properties))
					throw new CompressionException("Incorrect stream properties", this);
				
				long outSz = -1;
				if (endMarkerMode) {
					long outSize = 0;
					for (int i = 0; i < 8; i++) {
						int v = inputStream.read();
						if (v < 0)
							throw new CompressionException("Can't read stream size", this);
						outSize |= ((long)v) << (8 * i);
					}
	
					outSz = outSize;
				}

				FastByteArrayOutputStream dataBuf;
				if (endMarkerMode) {
					dataBuf = new FastByteArrayOutputStream((int)outSz);
				} else {
					dataBuf = new FastByteArrayOutputStream();
				}
				
				decoder.Code(inputStream, dataBuf, outSz);
				bufInput = new ByteArrayInputStream(dataBuf.toByteArray(), 0, dataBuf.length());

				inputStream.close();
			} catch (IOException ex) {
				throw new CompressionException(Exceptions.DECODER_INITIALIZE, ex, this);
			}
			
			initialized = true;
		} else {
			super.initialize();
		}
	}
	
	@Override
	public int available() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return endOfStream ? 0 : 1;
	}
	
	@Override
	public int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return bufInput.read();
	}

	@Override
	public int read(byte[] b) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return bufInput.read(b);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return bufInput.read(b, off, len);
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		bufInput.close();	
		bufInput = null;
		super.close();
	}

	@Override
	public String toString()
	{
		return "LZMADecoder";
	}
}

