package samzip.compression.dictionary;

import SevenZip.Compression.LZMA.Encoder;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.io.FastByteArrayOutputStream;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LZMAEncoder extends EncoderMethod
{
	private final int INITIAL_BUFFER_SIZE = 2048;
	
	private FastByteArrayOutputStream dataBuf = new FastByteArrayOutputStream(INITIAL_BUFFER_SIZE);
	
	private boolean endMarkerMode = false;
	
	public static class SevenZipParams
	{
		//@TODO
		public int DictionarySize = 1 << 20;

		public int Lc = 3;
		public int Lp = 0;
		public int Pb = 2;

		public int Fb = 128;

		public int Algorithm = 2;
		public int MatchFinder = 1;
		public boolean endMarkerMode = false;
	}
	
	Encoder encoder = new Encoder();
	
	@Override
	protected boolean isValidDataMode(DataMode mode)
	{
		if (mode != DataMode.UBYTE) 
			return false;
		
		return true;
	}
	
	public LZMAEncoder(final SevenZipParams params) throws CompressionException 
	{
		this(null, params);
	}
	
	public LZMAEncoder(final OutputStream outputStream) throws CompressionException
	{
		this(outputStream, new LZMAEncoder.SevenZipParams());
	}
	
	public LZMAEncoder(final OutputStream outputStream, final SevenZipParams params) throws CompressionException
	{
		setOutputStream(outputStream);
		configureDataMode(DataMode.UBYTE);
		this.endMarkerMode = params.endMarkerMode;
		// fill params
		if (!encoder.SetAlgorithm(params.Algorithm))
			throw new CompressionException("Incorrect compression mode");
		if (!encoder.SetDictionarySize(params.DictionarySize))
			throw new CompressionException("Incorrect dictionary size");
		if (!encoder.SetNumFastBytes(params.Fb))
			throw new CompressionException("Incorrect -fb value");
		if (!encoder.SetMatchFinder(params.MatchFinder))
			throw new CompressionException("Incorrect -mf value");
		if (!encoder.SetLcLpPb(params.Lc, params.Lp, params.Pb))
			throw new CompressionException("Incorrect -lc or -lp or -pb value");
		
		if (endMarkerMode)
			encoder.SetEndMarkerMode(false);
		else
			encoder.SetEndMarkerMode(true);
	}
	
	@Override
	public void write(int b) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		dataBuf.write(b & 0xff);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		dataBuf.write(b, off, len);
	}

	@Override
	public void write(byte[] b) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		dataBuf.write(b);
	}

	@Override
	public void flush() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		dataBuf.flush();
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		encoder.WriteCoderProperties(outputStream);
		long fileSize = -1;
		if (endMarkerMode) {
			fileSize = dataBuf.length();
			for (int i = 0; i < 8; i++)
				outputStream.write((int)(fileSize >>> (8 * i)) & 0xFF);
		}
		
		ByteArrayInputStream inputStream = new ByteArrayInputStream(dataBuf.toByteArray(), 0, dataBuf.length());
		
		encoder.Code(inputStream , outputStream, fileSize, -1, null);
		
		outputStream.flush();
		outputStream.close();	
		
		outputStream = null;
		encoder = null;
		initialized = false;
	}

	@Override
	public String toString()
	{
		return "LZMAEncoder";
	}
}