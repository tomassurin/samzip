package samzip.compression.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import samzip.io.StreamUtils;

/**
 * Represents data mode (range of values) of the compression codec or the Encoder/Decoder method.
 * This is needed for implementation of compression methods that needs to code data 
 * from ranges bigger than [0,255].
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public enum DataMode
{	
	/** [-127,127] */
	BYTE, 
	/** [0,255] */
	UBYTE, 
	/** [-32768, 32767] */
	SHORT, 
	/** [0, 65535] */
	USHORT, 
	/** [0, 2^{31}-1] */
	UINT, 
	/** [-2^{31}+1,2^{31}-1] */
	INT; 

	/** Returns default data mode */
	public static DataMode getDefault()
	{
		return DataMode.UBYTE;
	}
	
	/**
	 * @return true if data mode is signed
	 */
	public boolean isSigned()
	{
		return this == BYTE || this == SHORT || this == INT;
	}

	/**
	 * @return signed data mode that is represented with same number of bits.
	 */
	public DataMode getSigned()
	{
		switch (this) {
			case BYTE:
			case UBYTE:
				return BYTE;
			case INT:
			case UINT:
				return INT;
			case SHORT:
			case USHORT:
				return SHORT;
		}
		return INT;
	}
	
	/**
	 * @return unsigned data mode that is represented with same number of bits.
	 */
	public DataMode getUnsigned()
	{
		switch (this) {
			case BYTE:
			case UBYTE:
				return UBYTE;
			case INT:
			case UINT:
				return UINT;
			case SHORT:
			case USHORT:
				return USHORT;
		}
		return UINT;
	}
	
	/**
	 * Returns EOS symbol for this data mode.
	 */
	public int getEOSSymbol()
	{
		switch (this) {
			case INT:
			case SHORT:
			case BYTE:
				return Integer.MIN_VALUE;
			default:
				return -1;
		}
	}
	
	/**
	 * @param mode requested data mode
	 * @return true if data mode is compatible with requested data mode 
	 *	       (range of this data mode is subset of the range of the requested data mode)
	 */
	public boolean compatibleWith(DataMode mode)
	{
		if (this.isSigned()) {
			if (mode.isSigned())
				return this.ordinal() <= mode.ordinal();
			else
				return false;
		} else {
			return this.ordinal() <= mode.ordinal();
		}
	}
	
	/**
	 * Converts input {@code data} to this data mode's range (cut bits)
	 */
	public final int convertType(int data)
	{
		switch (this) {
			case BYTE:
				return (byte) data;
			case UBYTE:
				return (data & 0xff);
			case SHORT:
				return (short) data;
			case USHORT:
				return (data & 0xffff);
			case INT:
			case UINT:
				return data;
			default: return data;
		}
	}
	
	/**
	 * Converts input {@code data} to this data mode's range (cut bits)
	 */
	public int convertType(byte data)
	{
		switch (this) {
			case BYTE:
				return data;
			case UBYTE:
				return (data & 0xff);
			case SHORT:
				return (short) data;
			case USHORT:
				return (data & 0xffff);
			case INT:
				return data;
			default: return data;
		}
	}

	/** Writes {@code data} to output stream {@code outputStream} in current data mode. */
	public void writeToStream(OutputStream outputStream, int data) throws IOException
	{
		switch (this) {
			case BYTE:
				outputStream.write(data);
				break;
			case INT:
				StreamUtils.writeInt(data, outputStream);
				break;
			case SHORT:
				StreamUtils.writeShort((short)data, outputStream);
				break;
			case UBYTE:
				StreamUtils.writeByte(data, outputStream);
				break;
			case USHORT:
				StreamUtils.writeUnsignedShort(data, outputStream);
				break;
			case UINT:
				StreamUtils.writeUnsignedInt(data, outputStream);
				break;
		}
	}
	
		/** Reads data from input stream {@code inputStream} in current data mode. */
	public int readFromStream(InputStream inputStream) throws IOException
	{
		int result = 0;
		switch (this) {
			case BYTE:
				result = (byte) inputStream.read();
				break;
			case INT:
				result = StreamUtils.readInt(inputStream);
				break;
			case SHORT:
				result = (short) StreamUtils.readShort(inputStream);
				break;
			case UBYTE:
				result = StreamUtils.readUnsignedByte(inputStream);
				break;
			case USHORT:
				result = StreamUtils.readUnsignedShort(inputStream);
				break;
			case UINT:
				result = (int) StreamUtils.readUnsignedInt(inputStream);
				break;
		}
		return result;
	}
		
	/** Exception concerning data modes. */
	public static class DataModeException extends CompressionException
	{

		public DataModeException(String message)
		{
			super(message);
		}

		public DataModeException(Throwable cause)
		{
			super(cause);
		}

		public DataModeException(String message, Throwable cause)
		{
			super(message, cause);
		}
	}
}
