package samzip.compression.util;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class Exceptions
{
	public static final String ENCODER_METHOD_CREATION = "Problem while creating new encoder method";
	public static final String DECODER_METHOD_CREATION = "Problem while creating new decoder method";
	public static final String ENCODER_INITIALIZE = "Problem with initialization of an encoder method";
	public static final String DECODER_INITIALIZE = "Problem with initialization of a decoder method";
	
	public static final String REQUIRED_CODEC = "Required output codec was not specified";
		
	public static final String OUTPUT_STREAM_NULL = "Output stream was null";
	public static final String INPUT_STREAM_NULL = "Input stream was null";
	
	public static final String INPUT_METHOD_NULL = "Input method was null";
	public static final String OUTPUT_METHOD_NULL = "Output method was null";
	
	public static final String INPUT_METHODS_NULL = "Some input methods were null";
	public static final String OUTPUT_METHODS_NULL = "Some output methods were null";
	
	public static final String OUTPUT_CODEC_NULL = "Output compression codec was null";
	public static final String INPUT_CODEC_NULL = "Input compression codec was null";
	
	public static final String NOT_INITIALIZED_ENCODER = "Using not initialized encoder method";
	public static final String NOT_INITIALIZED_DECODER = "Using not initialized decoder method";
}
