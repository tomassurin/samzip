package samzip.compression.util;

import java.io.IOException;

/**
 * Indicates problem with compression codec configuration.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class CodecException extends IOException
{

	public CodecException()
	{
	}

	public CodecException(String message)
	{
		super(message);
	}

	public CodecException(Throwable cause)
	{
		super(cause);
	}
	
	public CodecException(Throwable cause, Object origin)
	{
		super(cause);
		this.origin = origin;
	}

	public CodecException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public CodecException(String message, Throwable cause, Object origin)
	{
		super(message, cause);
		this.origin = origin;
	}

	public CodecException(String message, Object origin)
	{
		super(message);
		this.origin = origin;
	}
	
		
	Object origin;
	
	@Override
	public String getMessage()
	{
		String message = super.getMessage();
		if (origin != null) {
			return "["+origin.toString()+"] "+message;
		} else {
			return message;
		}
	}
}
