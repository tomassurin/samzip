package samzip.compression.util;

/**
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class CompressionException extends RuntimeException
{
	public CompressionException()
	{
	}

	public CompressionException(String message)
	{
		super(message);
	}

	public CompressionException(Throwable cause)
	{
		super(cause);
	}
	
	public CompressionException(Throwable cause, Object origin)
	{
		super(cause);
		this.origin = origin;
	}

	public CompressionException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public CompressionException(String message, Throwable cause, Object origin)
	{
		super(message, cause);
		this.origin = origin;
	}

	public CompressionException(String message, Object origin)
	{
		super(message);
		this.origin = origin;
	}
	
	Object origin;
	
	@Override
	public String getMessage()
	{
		String message = super.getMessage();
		if (origin != null) {
			return "["+origin.toString()+"] "+message;
		} else {
			return message;
		}
	}
}
