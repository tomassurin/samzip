package samzip.compression;

import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 * <p>This abstract class is the superclass of implementations of encoder methods. 
 * In essence this class works almost like OutputStream would. Exceptions are
 * need for initialization with method {@link initialize()} (in order to allow user 
 * configuration of data mode and output stream) and use of data modes in method 
 * {@link write(int data)}.
 * </p>
 *
 * <p>Recommended method order: 
 * <ol> 
 *	<li> {@link setOutputStream(OutputStream)} - configuration of output stream</li> 
 *	<li> {@link setDataMode(DataMode)} - data mode configuration</li>
 *	<li> {@link initialize()} - initialization of compression method</li> 
 *	<li> {@link write(int)} - compression of data</li> 
 *	<li> {@link close()} - finalize compression</li> 
 * </ul>
 * </p>
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public abstract class EncoderMethod extends OutputStream
{
	/** methods data mode */
	protected DataMode dataMode = DataMode.UBYTE;
	/** end of stream symbol - depends on data mode */
	protected int EOS = dataMode.getEOSSymbol();
	/** output stream that is used for compressed data output (can be another EncoderMethod) */
	protected OutputStream outputStream;
	/** indicates if encoder method is initialized */
	protected boolean initialized = false;
	/** indicates if data mode was yet configured via {@link setDataMode(DataMode mode)} */
	protected boolean dataModeConfigured = false;

	/**
	 * Sets data mode to {@code mode}. Data mode can be specified only once through this method.
	 * <p>Can't be used after successful initialization.</p>
	 * @throws CompressionException if specified data mode wasn't valid
	 */
	public void setDataMode(DataMode mode)
	{
		setDataMode(mode, false);
	}

	/** Sets data mode to {@code mode}. If {@code override} is true overrides already
	 * configured data mode, else leaves it as is.
	 * <p>Can't be used after successful initialization.</p>
	 * @throws CompressionException if specified data mode wasn't valid
	 */
	public void setDataMode(DataMode mode, boolean override)
	{
		if (initialized) // can't change mode after initialization
			return;
		if (!override && dataModeConfigured) {
			return;
		}
		configureDataMode(mode);
		dataModeConfigured = true;
	}

	/** Bypass high level methods {@link setDataMode(DataMode)} and {@link setDataMode(dataMode, boolean)}.
	 * and sets data mode without hard configured data modes. Still checks if data mode is valid.
	 * <p>Can't be used after successful initialization.</p>
	 * @throws CompressionException if specified data mode wasn't valid 
	 */
	final protected void configureDataMode(DataMode mode)
	{
		if (initialized)
			return;
		if (!isValidDataMode(mode)) {
			throw new CompressionException("Data mode " + mode + " is not valid for this method.", this);
		}
		this.dataMode = mode;
		EOS = dataMode.getEOSSymbol();
	}

	/** Get currently configured data mode. */
	public DataMode getDataMode()
	{
		return dataMode;
	}

	/**
	 * Set output stream that will be used for output of encoded data.
	 * OutputStream can be another EncoderMethod. This class also handles 
	 * initialization of such method.
	 * <p>Can't be used after successful initialization.</p>
	 */
	public void setOutputStream(OutputStream stream)
	{
		if (initialized) {
			return;
		}

		if (this.outputStream instanceof EncoderMethod) {
			((EncoderMethod) this.outputStream).setOutputStream(stream);
		} else {
			this.outputStream = stream;
		}
	}

	/**
	 * Get currently configured output stream.
	 */
	public OutputStream getOutputStream()
	{
		return outputStream;
	}

	/**
	 * Initialize method. After this method ends without exception. All 
	 * subsequent calls to {@code setOutputStream()} are ineffective.
	 *
	 * @throws CompressionException if there's problem with method configuration
	 */
	public void initialize()
	{
		// should be used for safety reasons
		if (initialized) {
			return;
		}

		// must be used if you don't want to have som strange behavior in 
		// situation when output stream is instance of EncoderMethod
		initializeOutputStream();

		if (outputStream != null) {
			initialized = true;
		} else {
			throw new CompressionException(Exceptions.ENCODER_INITIALIZE
					+ ": " + Exceptions.OUTPUT_STREAM_NULL + ".", this);
		}
	}

	/** Initialize outputStream. It's safer to use this method in {@link initialize()} 
	 * because it handles situation where output stream is instance of EncoderMethod.
	 */
	protected final void initializeOutputStream()
	{
		if (outputStream != null) {
			if (outputStream instanceof EncoderMethod) {
				((EncoderMethod) outputStream).initialize();
			}
		}
	}

	/**
	 * Encodes single value with this method. 
	 *
	 * <p>Can be used only after successful initialization via {@link initialize()}.</p>
	 * 
	 * @param data value to encode, it should be represented according to 
	 *        configured data mode
	 *
	 * @throws IOException if an I/O error occurs
	 */
	@Override
	public abstract void write(int data) throws IOException;

	/** Same behavior as {@link OutputStream#write(byte[])}. Framework does
	 * not expect that this method uses data mode (i.e. uses mode UBYTE).
	 * <p>Can be used only after successful initialization via {@link initialize()}.</p>
	 */
	@Override
	public void write(byte[] data) throws IOException
	{
		write(data, 0, data.length);
	}

	/** Same behavior as {@link OutputStream#write(byte[],int,int)}. Framework does
	 * not expect that this method uses data mode (i.e. uses mode UBYTE).
	 * <p>Can be used only after successful initialization via {@link initialize()}.</p>
	 */
	@Override
	public void write(byte[] data, int off, int len) throws IOException
	{
		if (!initialized) {
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		}

		if (data == null) {
			throw new NullPointerException();
		} else if ((off < 0) || (off > data.length) || (len < 0)
				|| ((off + len) > data.length) || ((off + len) < 0)) {
			throw new IndexOutOfBoundsException();
		} else if (len == 0) {
			return;
		}

		for (int i = 0; i < len; ++i) {
			write(data[off + i] & 0xff);
		}
	}

	/**
	 * Finish still unfinished compression tasks and write remaining data to
	 * output stream. Can be safely used only after initialization via {@link initialize()}.
	 * <p>Can be used only after successful initialization via {@link initialize()}.</p>
	 * @throws IOException
	 */
	@Override
	public void close() throws IOException
	{
		if (!initialized) {
			return;
		}
		
		outputStream.close();
		outputStream = null;
		initialized = false;
	}

	/** Override this method to provide valid data modes for 
	 * specific EncoderMethod implementation. 
	 * @return true if specified data mode is valid, else false
	 */
	protected boolean isValidDataMode(DataMode mode)
	{
		return true;
	}
}