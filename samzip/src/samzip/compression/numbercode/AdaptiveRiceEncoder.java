package samzip.compression.numbercode;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class AdaptiveRiceEncoder extends EncoderMethod
{
	protected IntArrayList data = new IntArrayList();
	protected boolean signed = false;
	
	public AdaptiveRiceEncoder()
	{
		configureDataMode(DataMode.INT);
	}
	
	public AdaptiveRiceEncoder(OutputStream outputStream)
	{
		this();
		setOutputStream(outputStream);
	}
	
	@Override
	public void write(int data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		if (data < 0)
			signed = true;
		
		this.data.add(dataMode.convertType(data));
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		int m = RiceCode.calculateRiceEncoderParam(data.toIntArray());
		
		outputStream.write(m & 0xff);
		RiceCode coder = new RiceCode(outputStream, 1 << m);
		if (signed) {
			coder.encode(1);
		} else {
			coder.encode(0);
		}
		coder.setSigned(signed);
		
		for(int d : data) {
			coder.encode(d);
		}
	
		coder.close();
		outputStream.close();
		data = null;
		super.close();
	}

	@Override
	public String toString()
	{
		return "AdaptiveRiceEncoder";
	}
}
