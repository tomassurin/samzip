package samzip.compression.numbercode;

import it.unimi.dsi.fastutil.ints.Int2IntLinkedOpenHashMap;
import it.unimi.dsi.fastutil.ints.Int2IntMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntSet;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Comparator;
import samzip.compression.util.CompressionException;
import samzip.io.BitInputStream;
import samzip.io.BitOutputStream;
import samzip.io.FastByteArrayOutputStream;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */

class RawFrequencyData
{
	int symbol = 0;
	int count = 0;
}

public class CanonicalHuffmanEncoding
{
	protected Int2IntMap countTable = new Int2IntLinkedOpenHashMap();
	protected Int2IntMap reverseSymbol = new Int2IntOpenHashMap();

	protected int[] symbol;
	
	protected int maxCodeLength;
    protected int[] count;
	protected long[] base;
	protected int[] offset;
	protected long[] limit;
	protected byte[] bitConversionBuffer;

	protected OutputStream outputStream;
	protected InputStream inputStream;
	
	protected BitOutputStream bitoutput;
	protected BitInputStream bitinput;
	
	protected NumberCode numberCoder;
	
	protected long decremainder;
	protected int decremainderSize;
	
	public static final int EOS = Integer.MAX_VALUE;
	
	protected int riceEncoderParam;
	
	public CanonicalHuffmanEncoding(InputStream input, OutputStream output)
	{
		this.inputStream = input;
		this.outputStream = output;	
	}
	
	public CanonicalHuffmanEncoding(InputStream input) 
	{
		this(input, null);
	}
	
	public CanonicalHuffmanEncoding(OutputStream output) 
	{
		this(null, output);
	}
	
	public void increment(int s)
	{
		int val = 0;
		if (countTable.containsKey(s)) {
			val = countTable.get(s);
		}
		countTable.put(s, val + 1);
	}
	
	private void calculateCodeWordsLengths()
	{
		// A. create symbol counts descending array
		int n = countTable.size();
		if (n < 2) {
			symbol = new int[1];
			symbol[0] = EOS;
			count = new int[1];
			count[0] = 1;
			maxCodeLength = 1;
			return;
		}
		
		RawFrequencyData[] table = new RawFrequencyData[n];
		
		IntSet keySet = countTable.keySet();
		int j=0;
		RawFrequencyData val;
		for( int k : keySet) {
			table[j] = new RawFrequencyData();
			table[j].symbol = k;
			table[j++].count = countTable.get(k);
		}
		
		// sort symbol counts
		Arrays.sort(table, new Comparator<RawFrequencyData>() {
			@Override
			public int compare(RawFrequencyData o1, RawFrequencyData o2)
			{
				if (o1.symbol == EOS)
					return 1;
				if (o1.count < o2.count) {
					return 1;
				} else if (o1.count == o2.count) {
					return 0;
				} else {
					return -1;
				}
			}		
		});
		
		symbol = new int[n];  // contains symbols
		int[] P = new int[n]; // contains
		
		for(int i=0; i < n; ++i) {
			symbol[i] = table[i].symbol;
			reverseSymbol.put(symbol[i], i);
			P[i] = table[i].count; 
		}	
		
		/// A. calculate code word lengths
		
		// 1. huffman tree construction
		int r = n-1;
		int s = n-1;
		for(int x=n-1; x >= 1; --x) {
			if ((s < 0) || ((r > x) && (P[r] < P[s]))) {
				P[x] = P[r];
				P[r] = x;
				--r;
			} else {
				P[x] = P[s];
				--s;
			}
			if ((s < 1) || ((r > x) && (P[r] < P[s]))) {
				P[x] = P[x] + P[r];
				P[r] = x;
				--r;
			} else {
				P[x] = P[x] + P[s];
				--s;
			}
		}
		
		// 2. depth of inner nodes
		P[1] = 0;
		for(int x=2; x < n; ++x) {
			P[x] = P[P[x]] + 1;
		}
		
		// 3. lengths of code words
		int a = 1;
		int u = 0;
		int d = 0;
		r = 1;
		int x = 0;
		while (a > 0) {
			// a = # nodes of depth d
			while ((r < n) && (P[r] == d)) {
				++u;
				++r;
			}
			// u = # inner nodes of depth d
			while ((a > u)) {
				P[x] = d;
				++x;
				--a;
			}
			a = u << 1;
			++d;
			u = 0;
		}	
		
		// 4. # of code words of given length
		maxCodeLength = P[P.length-1];
		count = new int[maxCodeLength];
		int c = 1; // last length count
		int last = P[0];
		for(int i=1; i < P.length; ++i) {
			if (P[i] != last) {
				count[last-1] = c;
				last = P[i];
				c = 1;
			} else {
				++c;
			}
		}
		count[last-1] = c;
	}
	
	private void calculate()
	{
		calculateCodeWordsLengths();
		calculateStructs();
	}
	
	private void calculateStructs()
	{
		// B. calculate structures for encoding/decoding
		
		// 1. calculate base array
			// base[i] = integer value of first i-bit code word
//		if (maxCodeLength == 0)
//			return;
		
		base = new long[maxCodeLength];
		base[0] = 0;
		for(int i=1; i < maxCodeLength; ++i) {
			base[i] = (base[i-1]+count[i-1]) << 1;
		}
		
		// 2. calculate offset array
			// offset[i] = index of first i-bit code word
		offset = new int[maxCodeLength];
		offset[0] = 0;
		for(int i=1; i < maxCodeLength; ++i) {
			offset[i] = offset[i-1] + count[i-1];
		}
		
		// 3. calculate limit array
		limit = new long[maxCodeLength];
		for(int i=0; i < maxCodeLength-1; ++i) {
			limit[i] = base[i+1] << (maxCodeLength-i-2);
		}
		limit[maxCodeLength-1] = 1 << maxCodeLength;
		
		// allocate bit conversion buffer
		bitConversionBuffer = new byte[maxCodeLength];
	}
	
	public void initializeEncoding() throws IOException
	{
		if (outputStream != null) {
			increment(EOS);
			calculateCodeWordsLengths();
			calculateStructs();
			writeCodeBook();
			bitoutput = new BitOutputStream(outputStream);
			if (maxCodeLength > 30)
				throw new CompressionException("Huffman code max code length is bigger than 30.");
		}
	}
	
	public void initializeDecoding() throws IOException
	{		
		if (inputStream != null) {
			readCodeBook();
			calculateStructs();
			bitinput = new BitInputStream(inputStream);
		}
	}
	
	private int calculateRiceEncoderParam()
	{
		// calculate mean absolute value of symbol data and lengths data
		int meanabs = 0;
		int sym;
		int n = 0;
		for(int i=0; i < symbol.length-1; ++i) {
			sym = symbol[i] < 0 ? -symbol[i] : symbol[i];
			
			meanabs += sym;
			++n;
		}
		
		for(int i=0; i < count.length; ++i) {
			if (count[i] != 0) {
				meanabs += count[i];
				++n;
			}
		}
		
		meanabs /= n;
		
		// calculate floor(log2(meanabs))
		int log2m = 0;
		for(int t=meanabs; t > 1; t >>= 1)
			++log2m;
		
		return log2m > 0? log2m : 1;
	}
	
	private void writeCodeBook() throws IOException
	{
		FastByteArrayOutputStream buf = new FastByteArrayOutputStream();
		
		// encode symbols
		NumberCode numEnc = new VByteCode(buf);
		
		// warning! Rice code isn't universal, 
		// encoding big numbers can cause out of memory error!
		//riceEncoderParam = calculateRiceEncoderParam();
		//StreamUtils.writeByte(riceEncoderParam, outputStream);
		//numberCoder = new RiceCode(buf, 1 << riceEncoderParam);		
		
		if (hasNegativeSymbol()) {
			// if there are negative symbols set rice code to signed output mode
			numEnc.encode(1);
			numEnc.setSigned(true);
		} else {
			numEnc.encode(0);
			numEnc.setSigned(false);
		}
		numEnc.encode(symbol.length-1);
		for(int i=0; i < symbol.length; ++i) {
			if (symbol[i] == EOS) // exclude EOS symbol
				continue;
			
			numEnc.encode(symbol[i]);
		}
		
		numEnc.setSigned(false);
		numEnc.encode(count.length);
		for(int i=0; i < count.length; ++i) {
			numEnc.encode(count[i]);
		}
		
		numEnc.close();
		
		outputStream.write(buf.toByteArray(), 0, buf.length());
	}
	
	private void readCodeBook() throws IOException
	{
		countTable.clear();
		
		//riceEncoderParam = StreamUtils.readUnsignedByte(inputStream);
		//numberCoder = new RiceCode(inputStream, 1 << riceEncoderParam);
		numberCoder = new VByteCode(inputStream);
		
		int data = numberCoder.decode();
		
		boolean signed = data == 1 ? true : false;
		numberCoder.setSigned(signed);
		
		int sym;
		int c;
		int n = numberCoder.decode();
		symbol = new int[n+1]; // +1 for EOS
		for(int i=0; i < n; ++i) {
			symbol[i] = numberCoder.decode();
		}
		symbol[n] = EOS;
		
		numberCoder.setSigned(false);
		n = numberCoder.decode();
		count = new int[n];
		for(int i=0; i < n; ++i) {
			count[i] = numberCoder.decode();
		}
		maxCodeLength = n;
	}
	
	public void encode(final int sym) throws IOException
	{		
		int s = reverseSymbol.get(sym);
		int i=0;
		while ((i+1 < offset.length) && (s >= offset[i+1])) {
			++i;
		}
		
		long c = s - offset[i] + base[i];
		
		// output i+1 bits representing c to bit stream
		for(int j = i; j >= 0 ; --j) {
			bitConversionBuffer[j] = (byte) (c & 1); // mod
			c >>= 1;
		}
		for(int j = 0; j <= i; ++j) {
			bitoutput.writeBit(bitConversionBuffer[j]);
		}
	}
	
	public int decode() throws IOException
	{
		long v = bitinput.readBit(maxCodeLength - decremainderSize);
		decremainder |= v;
		int i=0; 
		while ( decremainder >= limit[i]) {
			++i;
		}
		
		decremainderSize = maxCodeLength - i - 1;
		// decremainder[1..i] - base[i] + offset[i]
		int result = (int) ((decremainder >>> decremainderSize) - base[i] + offset[i]);
		
		// clear bits decremainder[1..i]
		for(int j=maxCodeLength-1; j >= decremainderSize; --j)
			decremainder &= ~(1 << j);
		
		// move remaining bits in decremainder for future use
		decremainder = decremainder << (i+1);
		
		return symbol[result];
	}
	
	public void close() throws IOException
	{
		if (bitinput != null) {
			bitinput.close();
			inputStream.close();
		}
		if (bitoutput != null) {
			encode(EOS);
			bitoutput.close();
			outputStream.close();
		}
	}

	private boolean hasNegativeSymbol()
	{
		for(int i=0; i < symbol.length-1; ++i) {
			if (symbol[i] < 0)
				return true;
		}
		return false;
	}
}
