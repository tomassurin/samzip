package samzip.compression.numbercode;

import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class HuffmanDecoder extends DecoderMethod
{
	protected CanonicalHuffmanEncoding decoder;
	
	{
		configureDataMode(DataMode.INT);
	}
	
	public HuffmanDecoder(InputStream inputStream)
	{
		setInputStream(inputStream);
	}
	
	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null) {
			decoder = new CanonicalHuffmanEncoding(inputStream);
			try {
				decoder.initializeDecoding();
			} catch (IOException ex) {
				throw new CompressionException(Exceptions.DECODER_INITIALIZE, ex);
			}
			initialized = true;
		} else {
			super.initialize();
		}
	}
	
	@Override
	public int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		if (endOfStream)
			return EOS;
		
		int result = dataMode.convertType(decoder.decode());
		if (result == CanonicalHuffmanEncoding.EOS) {
			endOfStream = true;
			return EOS;
		}
		
		return result;
	}
	
	@Override
	public void close() throws IOException, CompressionException
	{
		if (!initialized)
			return;
		
		decoder.close();
		decoder = null;
		super.close();
	}
}
