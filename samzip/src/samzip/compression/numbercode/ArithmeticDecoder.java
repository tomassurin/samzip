package samzip.compression.numbercode;

import com.colloquial.arithcode.AdaptiveUnigramModel;
import com.colloquial.arithcode.ArithCodeInputStream;
import com.colloquial.arithcode.ArithCodeModel;
import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ArithmeticDecoder extends DecoderMethod
{
	ArithCodeInputStream in;
	
	ArithCodeModel model;
	
	public ArithmeticDecoder(InputStream inputStream)
	{
		this(inputStream, new AdaptiveUnigramModel());
	}
	
	public ArithmeticDecoder(InputStream inputStream, ArithCodeModel model)
	{
		setInputStream(inputStream);
		this.model = model;
	}
	
	@Override
	protected boolean isValidDataMode(DataMode mode)
	{
		if (mode != DataMode.UBYTE) 
			return false;
		
		return true;
	}
	
	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null) {
			try {
				in = new ArithCodeInputStream(inputStream, model);
			} catch (IOException ex) {
				throw new CompressionException(Exceptions.DECODER_INITIALIZE, ex, this);
			}
			initialized = true;
		} else {
			super.initialize();
		}
	}
	
	@Override
	public int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return in.read();
	}

	@Override
	public int read(byte[] data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return in.read(data);
	}

	@Override
	public int read(byte[] data, int off, int len) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		return in.read(data, off, len);
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		in.close();
		in = null;
		model = null;
		super.close();
	}
	
	
	@Override
	public String toString()
	{
		return "ArithmeticDecoder";
	}
}
