package samzip.compression.numbercode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import samzip.io.BitInputStream;
import samzip.io.BitOutputStream;
import samzip.util.PreferencesException;

/**
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
final public class RiceCode implements NumberCode
{
	protected BitInputStream input;
	protected BitOutputStream output;
	protected int m = 12;
	protected boolean signed = false;

	public RiceCode()
	{
	}

	public RiceCode(InputStream input, OutputStream output, int m, boolean signed)
	{
		setM(m);
		setSigned(signed);
		
		setInputStream(input);
		setOutputStream(output);
	}
	
	private void setM(int m)
	{
		if (m % 2 != 0 || m <= 0) {
			throw new IllegalArgumentException("Rice code parameter m should be power of 2.");
		}
		
		int log2m = 0;
		for(int t= m; t > 1; t >>= 1)	// calculate log2(m)
			++log2m;
		this.m = log2m;
	}
	
	public RiceCode(InputStream input, int m)
	{
		this(input, null, m, false);
	}
	
	public RiceCode(OutputStream output, int m)
	{
		this(null, output, m, false);
	}
	
	@Override
	public void close() throws IOException
	{
		if (output != null)
			output.close();
		if (input != null)
			input.close();
	}

	@Override
	public int decode() throws IOException
	{
		int q = 0;
		int nr = 0;
		int sign = 1;
		int res;
		if (signed) {
			res = input.readBit();
			if (res == -1)
				return EOS;
			sign = res == 1 ? -1 : 1;
		}
		
		// rice code
		while ((res = input.readBit()) == 1) {
			++q;
		}
		
		if (res == -1)
			return EOS;
		
		for( int a = 0; a < m; ++a) { // read log2(m) bits
			if ((res = input.readBit()) == 1)
				nr += 1 << a;
			if (res == -1)
				return EOS;
		}
		nr += q << m;
		
		return sign > 0 ? nr : -nr;
	}
	
	@Override
	public void encode(int data) throws IOException
	{
		// sign extension
		if (signed) {
			// output data in format: sign_bit|ricecode
			if (data < 0) {
				output.writeBit(1);
				data = -data;
			}
			else
				output.writeBit(0);
		}
		
		int q = data >> m;
		
		// unary code
		for(int i = 0; i < q; ++i)
			output.writeBit(1);    // write q ones
		output.writeBit(0);		   // write 1 zero
		
		// binary representation of data
		int v = 1;
		for(int i = 0; i < m; ++i) {
			output.writeBit(v & data);
			v = v << 1;
		}
	}
	
	@Override
	public void setInputStream(InputStream input)
	{
		if (input != null) {
			try {
				this.input = new BitInputStream(input);
			} catch(IOException ex)	{
				this.input = null;
			}
		} else {
			this.input = null;
		}
	}

	@Override
	public void setOutputStream(OutputStream output)
	{
		if (output != null) {
			this.output = new BitOutputStream(output);
		} else {
			this.output = null;
		}
	}

	@Override
	public void setSigned(boolean signed)
	{
		this.signed = signed;
	}

	@Override
	public void setOption(String key, String value) throws PreferencesException
	{
		if (key.toLowerCase().equals("m")) {
			try {
				m = Integer.parseInt(value);
				setM(m);
			} catch (NumberFormatException ex) {
				throw new PreferencesException("Rice code: error setting option 'm' with value: "+value, key);
			}
			if (m < 2 || m >= 1 << 29)
				throw new PreferencesException("Rice code: parameter 'm' should be from <2;2^29> was "+m, key);
		} else {
//			throw new PreferencesException("Unknown option", key);
		}
	}
	
	public static int calculateRiceEncoderParam(int[] symbol)
	{
		// calculate mean absolute value of symbol data and lengths data
		int meanabs = 0;
		int sym;
		int n = 1;
		for(int i=0; i < symbol.length; ++i) {
			sym = symbol[i] < 0 ? -symbol[i] : symbol[i];
			
			meanabs += sym;
			++n;
		}
		
		meanabs /= n;
		
		// calculate floor(log2(meanabs))
		int log2m = 0;
		for(int t=meanabs; t > 1; t >>= 1)
			++log2m;
		
		if (log2m > 29)
			log2m = 29;
		
		return log2m > 0? log2m : 1;
	}
	
	public static int calculateRiceEncoderParamMedian(int[] symbol)
	{
		// calculate median of symbols array
		int median;
		Arrays.sort(symbol);
		if (symbol.length % 2 == 0) {
			int idx = (int) symbol.length/2;
			median = (int)symbol[idx-1]+symbol[idx]/2;
		} else {
			median = symbol[(int)symbol.length/2];
		}
		
		// calculate floor(log2(meanabs))
		int log2m = 0;
		for(int t=median; t > 1; t >>= 1)
			++log2m;
		
		if (log2m > 29)
			log2m = 29;
		
		return log2m > 0? log2m : 1;
	}

	@Override
	public String toString()
	{
		return "Rice code";
	}
}
