package samzip.compression.numbercode;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.io.StreamUtils;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class AdaptiveGolombEncoder extends EncoderMethod
{
	protected IntArrayList data = new IntArrayList();
	protected boolean signed = false;
	protected double golombConstant = 1.0;
	
	public AdaptiveGolombEncoder()
	{
		configureDataMode(DataMode.INT);
	}
	
	public AdaptiveGolombEncoder(OutputStream outputStream)
	{
		this();
		setOutputStream(outputStream);
	}
	
	public void setGolombConstant(double constant)
	{
		this.setGolombConstant(constant);
	}
	
	@Override
	public void write(int data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		if (data < 0)
			signed = true;
		
		this.data.add(dataMode.convertType(data));
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		int m = GolombCode.calculateGolombEncoderParam(data.toIntArray(), golombConstant);
		if (m <= 0)
			m = 1;
		StreamUtils.writeUnsignedInt(m, outputStream);
		GolombCode coder = new GolombCode(outputStream, m);
		if (signed) {
			coder.encode(1);
		} else {
			coder.encode(0);
		}
		coder.setSigned(signed);
		
		for(int d : data) {
			coder.encode(d);
		}
	
		coder.close();
		outputStream.close();
		data = null;
		super.close();
	}
	
	@Override
	public String toString()
	{
		return "AdaptiveGolombEncoder";
	}
}
