package samzip.compression.numbercode;

import com.colloquial.arithcode.AdaptiveUnigramModel;
import com.colloquial.arithcode.ArithCodeModel;
import com.colloquial.arithcode.ArithCodeOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ArithmeticEncoder extends EncoderMethod
{
	ArithCodeOutputStream out;
	ArithCodeModel model;
	
	public ArithmeticEncoder(OutputStream outputStream, ArithCodeModel model)
	{
		setOutputStream(outputStream);
		this.model = model;
	}
	
	public ArithmeticEncoder(OutputStream outputStream)
	{
		this(outputStream, new AdaptiveUnigramModel());
	}

	@Override
	protected boolean isValidDataMode(DataMode mode)
	{
		if (mode != DataMode.UBYTE) 
			return false;
		
		return true;
	}
	
	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		super.initializeOutputStream();
		
		if (outputStream != null) {
			out = new ArithCodeOutputStream(outputStream, model);
			initialized = true;
		} else {
			super.initialize();
		}
	}
	
	@Override
	public void write(int data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		out.write(data);
	}

	@Override
	public void write(byte[] data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		out.write(data);
	}

	@Override
	public void write(byte[] data, int off, int len) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		out.write(data, off, len);
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		out.close();
		out = null;
		super.close();
	}

	@Override
	public String toString()
	{
		return "ArithmeticEncoder";
	}
}
