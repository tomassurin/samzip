package samzip.compression.numbercode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import samzip.io.BitInputStream;
import samzip.io.BitOutputStream;

/** 
 * Implementation of the elias delta code.
 * 
 *  @author Tomáš Šurín <tomas.surin at gmail.com>
 */
final public class EliasDeltaCode implements NumberCode
{		
	protected BitInputStream input = null;
	protected BitOutputStream output = null;
	protected boolean signed = false;
	
	public EliasDeltaCode(InputStream input, OutputStream output, boolean signed)
	{
		setSigned(signed);
		setInputStream(input);
		setOutputStream(output);
	}
	
	public EliasDeltaCode(InputStream input)
	{
		this(input, null, false);
	}
	
	public EliasDeltaCode(OutputStream output)
	{
		this(null, output, false);
	}
	
	public EliasDeltaCode()
	{
	}

	@Override
	public void setOption(String key, String val)
	{
		
	}
	
	@Override
	public void setInputStream(InputStream input)
	{
		if (input != null) {
			try {
				this.input = new BitInputStream(input);
			} catch(IOException ex)	{
				this.input = null;
			}
		} else {
			this.input = null;
		}
	}

	@Override
	public void setOutputStream(OutputStream output)
	{
		if (output != null) {
			this.output = new BitOutputStream(output);
		} else {
			this.output = null;
		}
	}	
	
//	@Override
//	public int getExpectedSize(int data)
//	{
//		int len = 0;
//		int lengthOfLen = 0;
//	
//		for(int t = data; t > 0; t >>= 1) // calculate 1+floor(log2(data))
//			++len;
//		for(int t= len; t > 1; t >>= 1)	// calculate floor(log2(len))
//			++lengthOfLen;
//		
//		// (lengthOfLen -> zeros) + (lengthOfLen+1 -> binary representation of len) + (len-1 -> binary representation of data without leading bit)
//		return (lengthOfLen<<1) + len;
//	}
	
	@Override
	public void close() throws IOException
	{		
		if (output != null)
			output.close();
		if (input != null)
			input.close();
	}
	
	@Override
	public void encode(int data) throws IOException
	{
		if (signed) {
			// output data in format: sign_bit|elias_code
			if (data < 0) {
				output.writeBit(1);
				data = -data;
			}
			else
				output.writeBit(0);
		}
		
		++data; // encoding 0 as 1
		
		int len = 0;
		int lengthOfLen = 0;
		for(int t = data; t > 0; t >>= 1) // calculate 1+floor(log2(data))
			++len;
		for(int t= len; t > 1; t >>= 1)	// calculate floor(log2(len))
			++lengthOfLen;
		for(int i = lengthOfLen; i > 0; --i) // output lengthOfLen 0's
			output.writeBit(0);
		for(int i = lengthOfLen; i >= 0; --i) // output binary representation of len
			output.writeBit((len >> i) & 1); 
		for(int i = len-2; i >= 0; i--) 
			output.writeBit((data >> i) & 1);
	}

	@Override
	public int decode() throws IOException
	{
		int num = 1;
		int len = 1;
		int lengthOfLen = 0;
		int sign = 1;
		int res;	
		if (signed) {
			res = input.readBit();
			if (res < 0)
				return EOS;
			sign = res == 1 ? -1 : 1;
		}
		
		while ((res = input.readBit()) == 0)
			++lengthOfLen;
		
		if (res < 0)
			return EOS;
		
		for(int i = 0; i < lengthOfLen; ++i) {
			len <<= 1;
			if ((res = input.readBit()) == 1) {
				len |= 1;
			} else if (res < 0) {
				return EOS;
			}
		}
		for(int i=0; i < len-1; ++i) {
			num <<= 1;
			if ((res = input.readBit()) == 1) {
				num |= 1;
			} else if (res < 0) {
				return EOS;
			}
		}
		
		--num; // for decoding 0
		
		return sign > 0 ? num : -num;
	}

	@Override
	public void setSigned(boolean signed)
	{
		this.signed = signed;
	}

	@Override
	public String toString()
	{
		return "Elias Delta Code";
	}
}
