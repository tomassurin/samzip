package samzip.compression.numbercode;

import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public final class NumberEncoder extends EncoderMethod
{
	protected NumberCode coder;
	
	{
		configureDataMode(DataMode.INT);
	}
	
	public NumberEncoder(Class<? extends NumberCode> numberCoder) throws InstantiationException, IllegalAccessException
	{
		coder = numberCoder.newInstance();
	}
	
	public NumberEncoder(Class<?extends NumberCode> numberCoder, OutputStream outputStream) throws InstantiationException, IllegalAccessException 
	{
		coder = numberCoder.newInstance();
		setOutputStream(outputStream);
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		super.initializeOutputStream();
		
		if (outputStream != null && coder != null) {
			coder.setOutputStream(outputStream);
			coder.setSigned(dataMode.isSigned());
			initialized = true;
		} else {
			super.initialize();
		}
	}
	
	@Override
	public void write(int data) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		coder.encode(dataMode.convertType(data));
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		coder.close();
	}
	
	public void setCoderOption(String key, String value) throws PreferencesException
	{
		if (initialized)
			return;
		
		coder.setOption(key, value);
	}

	@Override
	public String toString()
	{
		if (coder != null)
			return "NumberEncoder/"+coder.toString();
		else
			return "NumberEncoder";
	}
}
