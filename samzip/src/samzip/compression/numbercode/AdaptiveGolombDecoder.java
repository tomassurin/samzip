package samzip.compression.numbercode;

import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.io.StreamUtils;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class AdaptiveGolombDecoder extends DecoderMethod
{
	private GolombCode decoder;
	
	public AdaptiveGolombDecoder()
	{
		configureDataMode(DataMode.INT);
	}
	
	public AdaptiveGolombDecoder(InputStream inputStream)
	{
		this();
		setInputStream(inputStream);
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null) {
			try {
				int M = (int) StreamUtils.readUnsignedInt(inputStream);
				decoder = new GolombCode(inputStream, M);
				if (decoder.decode() == 1)
					decoder.setSigned(true);
				else
					decoder.setSigned(false);
			} catch (IOException ex) {
				throw new CompressionException(Exceptions.DECODER_INITIALIZE, ex, this);

			}
			initialized = true;
		} else {
			super.initialize();
		}
	}
	
	@Override
	public int read() throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		int res = decoder.decode();
		
		if (res == NumberCode.EOS)
			return EOS();
		
		return res;
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		decoder.close();
		decoder = null;
		super.close();
	}

	@Override
	public String toString()
	{
		return "AdaptiveGolombDecoder";
	}
}
