package samzip.compression.numbercode;

import java.io.*;

/**
 * Variable length number coder. Uses variable number of bytes where 
 * 1st bit represents flag and remaining 7 bits represent value. Flag
 * indicates if there are more bytes in variable code.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public final class VByteCode implements NumberCode
{
	private InputStream inputStream;
	private OutputStream outputStream;
	private boolean signed = false;

	public VByteCode()
	{
		
	}
	
	public VByteCode(OutputStream outputStream)
	{
		setOutputStream(outputStream);
	}
	
	public VByteCode(InputStream inputStream)
	{
		setInputStream(inputStream);
	}
	
	@Override
	public void close() throws IOException
	{
		if (inputStream != null)
			inputStream.close();
		if (outputStream != null)
			outputStream.close();
	}
	
	@Override
	public int decode() throws IOException
	{
		int sign = 1;
		boolean inNumber = true;
		int result = 0;
		int addPos = 0;
		
		if (signed) {
			int data = inputStream.read();
			if (data < 0)
				return EOS;
			if ((data & 0x40) != 0x0) {
				sign = -1;
			}
			if ((data & 0x80) == 0x0) {
				inNumber = false;
			}
			result |= (data & 0x3f) << addPos;
			addPos += 6;
		}
		
		while (inNumber) {
			int data = inputStream.read();
			if (data < 0) {
				return 0;
			}
			if ((data & 0x80) == 0) {
				inNumber = false;
			} 
			result |= (data & 0x7f) << addPos;
			addPos += 7;
		}
		return sign > 0 ? result : -result;
	}
	
	@Override
	public void encode(int data) throws IOException
	{
		if (signed) {
			boolean negative = false;
			if (data < 0) {
				negative = true;
				data = -data;
			}
			int num = data & 0x3f;
			data = data >> 6;
			if (negative) {
				num = num | 0x40;
			}
			if (data != 0) {
				num = num | 0x80;
			}
			outputStream.write(num);
			if (data == 0)
				return;
		} 
		
		do {
			int num = data & 0x7f;
			data = data >> 7;
			if (data != 0) {
				num = num | 0x80;
			}
			outputStream.write(num);
		} while (data != 0);
	}

	@Override
	public void setInputStream(InputStream input)
	{
		this.inputStream = input;
	}

	@Override
	public void setOutputStream(OutputStream output)
	{
		this.outputStream = output;
	}

	@Override
	public void setSigned(boolean signed)
	{
		this.signed = signed;
	}

	@Override
	public void setOption(String key, String value)
	{
		
	}

	@Override
	public String toString()
	{
		return "VByte code";
	}
	
	public static void encodeUnsigned(int data, OutputStream outputStream) throws IOException
	{
		do {
			int num = data & 0x7f;
			data = data >> 7;
			if (data != 0) {
				num = num | 0x80;
			}
			outputStream.write(num);
		} while (data != 0);
	}
	
	public static int decodeUnsigned(InputStream inputStream) throws IOException
	{
		boolean inNumber = true;
		int result = 0;
		int addPos = 0;
		
		while (inNumber) {
			int data = inputStream.read();
			if (data < 0) {
				return 0;
			}
			if ((data & 0x80) == 0) {
				inNumber = false;
			} 
			result |= (data & 0x7f) << addPos;
			addPos += 7;
		}
		return result;
	}
	
	public static void encodeSigned(int data, OutputStream outputStream) throws IOException
	{
		boolean negative = false;
		if (data < 0) {
			negative = true;
			data = -data;
		}
		int num = data & 0x3f;
		data = data >> 6;
		if (negative) {
			num = num | 0x40;
		}
		if (data != 0) {
			num = num | 0x80;
		}
		outputStream.write(num);
		if (data == 0)
			return;
			
		do {
			num = data & 0x7f;
			data = data >> 7;
			if (data != 0) {
				num = num | 0x80;
			}
			outputStream.write(num);
		} while (data != 0);
	}
	
	public static int decodeSigned(InputStream inputStream) throws IOException
	{
		int sign = 1;
		boolean inNumber = true;
		int result = 0;
		int addPos = 0;
		
		int data = inputStream.read();
		if (data < 0)
			return EOS;
		if ((data & 0x40) != 0x0) {
			sign = -1;
		}
		if ((data & 0x80) == 0x0) {
			inNumber = false;
		}
		result |= (data & 0x3f) << addPos;
		addPos += 6;
		
		while (inNumber) {
			data = inputStream.read();
			if (data < 0) {
				return 0;
			}
			if ((data & 0x80) == 0) {
				inNumber = false;
			} 
			result |= (data & 0x7f) << addPos;
			addPos += 7;
		}
		return sign > 0 ? result : -result;
	}
}
