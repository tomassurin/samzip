package samzip.compression.numbercode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import samzip.io.BitInputStream;
import samzip.io.BitOutputStream;

/** 
 * Implementation of the elias delta code.
 * 
 *  @author Tomáš Šurín <tomas.surin at gmail.com>
 */
final public class EliasGammaCode implements NumberCode
{		
	protected BitInputStream input = null;
	protected BitOutputStream output = null;
	protected boolean signed = false;
	
	public EliasGammaCode(InputStream input, OutputStream output, boolean signed)
	{
		setSigned(signed);
		setInputStream(input);
		setOutputStream(output);
	}
	
	public EliasGammaCode(InputStream input)
	{
		this(input, null, false);
	}
	
	public EliasGammaCode(OutputStream output)
	{
		this(null, output, false);
	}
	
	public EliasGammaCode()
	{
	}

	@Override
	public void setOption(String key, String val)
	{
		
	}
	
	@Override
	public void setInputStream(InputStream input)
	{
		if (input != null) {
			try {
				this.input = new BitInputStream(input);
			} catch(IOException ex)	{
				this.input = null;
			}
		} else {
			this.input = null;
		}
	}

	@Override
	public void setOutputStream(OutputStream output)
	{
		if (output != null) {
			this.output = new BitOutputStream(output);
		} else {
			this.output = null;
		}
	}	
	
	@Override
	public void close() throws IOException
	{		
		if (output != null)
			output.close();
		if (input != null)
			input.close();
	}
	
	@Override
	public void encode(int data) throws IOException
	{
		if (signed) {
			// output data in format: sign_bit|eliascode
			if (data < 0) {
				output.writeBit(1);
				data = -data;
			}
			else
				output.writeBit(0);
		}
		
		++data; // for encoding 0
		
		int len = 0;
		// calculate floor(log2(data))
		for(int t = data; t > 1; t >>= 1)
			++len;
		// prepend len 0's (unary encoding)
		for(int i = len; i > 0; --i)
			output.writeBit(0);
		/// write binary representation of data
		for(int i = len; i >= 0; i--) 
			output.writeBit((data >> i) & 1);
	}

	@Override
	public int decode() throws IOException
	{
		int num = 1;
		int len = 0;
		int lengthOfLen = 0;
		int sign = 1;
		int res;	
		if (signed) {
			res = input.readBit();
			if (res < 0)
				return EOS;
			sign = res == 1 ? -1 : 1;
		}
		
		while ((res = input.readBit()) == 0)
			++len;
		
		if (res < 0)
			return EOS;
		
		for(int i=0; i < len; ++i) {
			num <<= 1;
			if ((res = input.readBit()) == 1) {
				num |= 1;
			} else if (res < 0) {
				return EOS;
			}
		}
		
		--num; // for decoding 0
		
		return sign > 0 ? num : -num;
	}

	@Override
	public void setSigned(boolean signed)
	{
		this.signed = signed;
	}
	
	@Override
	public String toString()
	{
		return "Elias Gamma Code";
	}
}
