package samzip.compression.numbercode;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import samzip.util.PreferencesException;

/**
 * Interface for number codes (elias, rice, golomb ...).
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public interface NumberCode
{
	public static int EOS = Integer.MIN_VALUE;
	
	void close() throws IOException;

	int decode() throws IOException;

	void encode(int data) throws IOException;

//	int getExpectedSize(int data);

	void setInputStream(InputStream input);

	void setOutputStream(OutputStream output);

	void setSigned(boolean signed);
	
	void setOption(String key, String value) throws PreferencesException;	
}
