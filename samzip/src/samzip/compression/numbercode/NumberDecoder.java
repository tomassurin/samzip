package samzip.compression.numbercode;

import java.io.IOException;
import java.io.InputStream;
import samzip.compression.DecoderMethod;
import samzip.compression.util.CompressionException;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;
import samzip.util.PreferencesException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class NumberDecoder extends DecoderMethod
{
	protected NumberCode coder;
	
	{
		configureDataMode(DataMode.INT);
	}
	
	public NumberDecoder(Class<? extends NumberCode> numberCoder) throws InstantiationException, IllegalAccessException
	{
		coder = numberCoder.newInstance();
	}

	public NumberDecoder(Class<? extends NumberCode> numberCoder, InputStream inputStream) throws InstantiationException, IllegalAccessException
	{
		coder = numberCoder.newInstance();
		setInputStream(inputStream);
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		initializeInputStream();
		
		if (inputStream != null && coder != null) {
			coder.setInputStream(inputStream);
			coder.setSigned(dataMode.isSigned());
			initialized = true;
		} else {
			super.initialize();
		}
	}
	
	@Override
	public int read() throws IOException, CompressionException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_DECODER);
		
		int res = coder.decode();
		
		if (res == NumberCode.EOS)
			return EOS();
		
		return res;
	}

	@Override
	public void close() throws IOException, CompressionException
	{
		if (!initialized)
			return;
		
		coder.close();
		coder = null;
		super.close();
	}
	
	public void setCoderOption(String key, String value) throws PreferencesException
	{
		if (initialized)
			return;
		
		coder.setOption(key, value);
	}
	
	@Override
	public String toString()
	{
		if (coder != null)
			return "NumberDecoder/"+coder.toString();
		else
			return "NumberDecoder";
	}
}
