package samzip.compression.numbercode;

import java.io.*;
import samzip.io.BitInputStream;
import samzip.io.BitOutputStream;

/**
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
final public class FibonacciCode implements NumberCode
{
	protected BitInputStream input;
	protected BitOutputStream output;
	private boolean signed;
	
	private int[] fibs;
	
	public FibonacciCode()
	{
		calculateFibs(1024);
	}

	public FibonacciCode(InputStream input, OutputStream output)
	{
		this();
		setInputStream(input);
		setOutputStream(output);
	}
	
	public FibonacciCode(InputStream input)
	{
		this(input, null);
	}
	
	public FibonacciCode(OutputStream output)
	{
		this(null, output);
	}
	
	@Override
	public void close() throws IOException
	{
		if (output != null)
			output.close();
		if (input != null)
			input.close();
	}

	@Override
	public int decode() throws IOException
	{
		int res;
		int sign = 1;
		boolean bit = false;
		int idx = 0;
		int num = 0;
		
		if (signed) {
			res = input.readBit();
			if (res < 0)
				return EOS;
			sign = res == 1 ? -1 : 1;
		}
		
		while (true) {
			if (idx >= fibs.length) {
				calculateFibs();
			}
			res = input.readBit();
			
			if (res < 0)
				return EOS;
			
			if (bit && res == 1) {
				break;
			}
			if (res == 1) {
				num += fibs[idx];
				bit = true;
			} else {
				bit = false;
			}
			++idx;
		}
		
		--num; // decoding 0
		
		return sign > 0 ? num : -num;
	}

	private byte[] buf = new byte[256];
	
	@Override
	public void encode(int data) throws IOException
	{
		if (signed) {
			// output data in format: sign_bit|eliascode
			if (data < 0) {
				output.writeBit(1);
				data = -data;
			}
			else
				output.writeBit(0);
		}
		
		++data; // encoding 0 as 1
		
		int f = fibs[0];
		int idx = 1;
		while (data >= f) {
			if (idx == fibs.length) {
				calculateFibs();
			}
			f = fibs[idx++];
		}
		
		int k = 1;
		buf[0] = 1;
		for(int i=idx-2; i >=0; --i) {
			if (data >= fibs[i]) {
				data = data - fibs[i];
				buf[k++] = 1;
			} else {
				buf[k++] = 0;
			}
		}
		
		for(int i=k-1; i >=0; --i) {
			output.writeBit(buf[i]);
		}
	}

	public static void main(String[] args) throws IOException
	{
		ByteArrayOutputStream buf = new ByteArrayOutputStream(10);
		FibonacciCode code = new FibonacciCode(buf);
		
		code.encode(65);
		code.close();
		ByteArrayInputStream in = new ByteArrayInputStream(buf.toByteArray());
		
		code.setInputStream(in);
		System.out.println(code.decode());
		code.close();
	}
	
	@Override
	public void setInputStream(InputStream input)
	{
		if (input != null) {
			try {
				this.input = new BitInputStream(input);
			} catch(IOException ex)	{
				this.input = null;
			}
		} else {
			this.input = null;
		}
	}

	@Override
	public void setOutputStream(OutputStream output)
	{
		if (output != null) {
			this.output = new BitOutputStream(output);
		} else {
			this.output = null;
		}
	}	

	@Override
	public void setOption(String key, String value)
	{
		
	}

	@Override
	public void setSigned(boolean signed)
	{
		this.signed = signed;
	}

	private void calculateFibs(int count)
	{
		fibs = new int[count];
		fibs[0] = 1;
		fibs[1] = 2;
		for(int i=2; i < count; ++i) {
			fibs[i] = fibs[i-2]+fibs[i-1];
		}
	}
	
	private void calculateFibs()
	{
		int n = fibs.length << 1;
		int[] newFibs = new int[n];
		System.arraycopy(fibs, 0, newFibs, 0, fibs.length);
		for(int i=fibs.length; i < n; ++i) {
			newFibs[i] = newFibs[i-2]+newFibs[i-1];
		}
		fibs = newFibs;
	}

	@Override
	public String toString()
	{
		return "Fibonacci Code";
	}
}
