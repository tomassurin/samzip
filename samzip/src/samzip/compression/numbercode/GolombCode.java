package samzip.compression.numbercode;

import java.io.*;
import samzip.io.BitInputStream;
import samzip.io.BitOutputStream;
import samzip.util.PreferencesException;

/**
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
final public class GolombCode implements NumberCode
{
	protected BitInputStream input;
	protected BitOutputStream output;
	protected boolean signed = false;
	
	protected int m;
	protected int log2m;
	protected int b;
	protected boolean isRiceCode;
	private long truncatedEncTreshold;

	public GolombCode()
	{
		setM(4096);
	}

	public GolombCode(InputStream input, OutputStream output, int m, boolean signed)
	{
		setM(m);
		setSigned(signed);
		
		setInputStream(input);
		setOutputStream(output);
	}
	
	private void setM(int m)
	{
		if (m <= 0) {
			throw new IllegalArgumentException("Golomb code parameter m should be positive.");
		}
		
		int log2m = 0;
		for(int t= m; t > 1; t >>= 1)	// calculate floor(log2(m))
			++log2m;
		
//		b = (int) Math.ceil(Math.log((double)m)/Math.log(2.0));
		this.m = m;
		this.log2m = log2m;
		b = log2m+1;
		this.isRiceCode = m % 2 == 0;
		this.truncatedEncTreshold = (1 << (log2m+1)) - m;
//		this.truncatedEncTreshold = (1 << (b)) - m;
	}
	
	public GolombCode(InputStream input, int m)
	{
		this(input, null, m, false);
	}
	
	public GolombCode(OutputStream output, int m)
	{
		this(null, output, m, false);
	}
	
	@Override
	public void close() throws IOException
	{
		if (output != null)
			output.close();
		if (input != null)
			input.close();
	}

	@Override
	public int decode() throws IOException
	{
		int q = 0;
		int nr = 0;
		int sign = 1;
		int res;
		if (signed) {
			res = input.readBit();
			if (res == -1)
				return EOS;
			sign = res == 1 ? -1 : 1;
		}
		
		while ((res = input.readBit()) == 1) {
			++q;
		}
		
		if (res == -1)
			return EOS;
		
		if (isRiceCode) {
			for( int a = 0; a < log2m; ++a) { // read log2(m) bits
				if ((res = input.readBit()) == 1)
					nr += 1 << a;
				if (res == -1)
					return EOS;
			}	
			nr += q << log2m;
		} else {
			/// read b-1 bits from input stream
			for(int i=0; i < b-1; ++i) {
				nr <<= 1;
				if ((res = input.readBit()) == 1)
					nr |= 1;
				if (res < 0)
					return EOS;
			}
			if (nr >= truncatedEncTreshold) {
				nr <<= 1;
				if ((res = input.readBit()) == 1)
					nr |= 1;
				if (res < 0)
					return EOS;
				nr -= truncatedEncTreshold;
			}
			nr += q*m;
		}
		
		return sign > 0 ? nr : -nr;
	}
	
	public static void main(String[] argv) throws IOException
	{
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		GolombCode code = new GolombCode(buf, 17);
		code.encode(15);
		code.encode(10);
		code.encode(32);
		code.close();
		ByteArrayInputStream in = new ByteArrayInputStream(buf.toByteArray());
		code = new GolombCode(in, 17);
		int res = code.decode();
		System.out.println(res);
		res = code.decode();
		System.out.println(res);
		res = code.decode();
		System.out.println(res);
		code.close();
	}
	
	@Override
	public void encode(int data) throws IOException
	{
		// sign extension
		if (signed) {
			// output data in format: sign_bit|GolombCode
			if (data < 0) {
				output.writeBit(1);
				data = -data;
			}
			else
				output.writeBit(0);
		}
		int q;
		int r = 0;
		if (isRiceCode) {
			q = data >> log2m;
		} else {
			q = (int) data / m;
			r = data % m;
		}
		
		// unary code for quotient
		for(int i = 0; i < q; ++i)
			output.writeBit(1);    // write q ones
		output.writeBit(0);		   // write 1 zero
		
		// binary representation of remainder
		if (isRiceCode) {
			// if m is power of 2 this algorithm is equivalent to rice coding
			long v = 1;
			for(int i = 0; i < log2m; ++i) {
				output.writeBit((int) (v & data));
				v = v << 1;
			}
		} else {
//			int b = log2m+1;
			if (r < truncatedEncTreshold) {
				// use truncated binary encoding (b-1) bits
				for(int i = b-2; i >= 0; --i) {
					output.writeBit((r >> i) & 1);
				}
			} else {
				// use binary encoding of r+2^b-m using b bits
				r += truncatedEncTreshold;
				for(int i = b-1; i >= 0; --i) {
					int bit = (r >> i) & 1;
					output.writeBit((r >> i) & 1);
				}
			}
		}
	}
	
	@Override
	public void setInputStream(InputStream input)
	{
		if (input != null) {
			try {
				this.input = new BitInputStream(input);
			} catch(IOException ex)	{
				this.input = null;
			}
		} else {
			this.input = null;
		}
	}

	@Override
	public void setOutputStream(OutputStream output)
	{
		if (output != null) {
			this.output = new BitOutputStream(output);
		} else {
			this.output = null;
		}
	}

	@Override
	public void setSigned(boolean signed)
	{
		this.signed = signed;
	}

	@Override
	public void setOption(String key, String value) throws PreferencesException
	{
		if (key.toLowerCase().equals("m")) {
			try {
				m = Integer.parseInt(value);
				setM(m);
			} catch (NumberFormatException ex) {
				throw new PreferencesException("Golomb code: error setting option 'm' with value: "+value, key);
			}
			if (m <= 0)
				throw new PreferencesException("Golomb code: parameter 'm' should be positive, was "+m, key);
		} else {
//			throw new PreferencesException("Unknown option");
		}
	}
	
	public static int calculateGolombEncoderParam(int[] symbol, double constant)
	{
		return (int) (calculateGolombEncoderParam(symbol) * constant);
	}
	
	public static int calculateGolombEncoderParam(int[] symbol)
	{
		// calculate mean absolute value of symbol data and lengths data
		int meanabs = 0;
		int sym;
		int n = 1;
		for(int i=0; i < symbol.length; ++i) {
			sym = symbol[i] < 0 ? -symbol[i] : symbol[i];
			
			meanabs += sym;
			++n;
		}
		
		meanabs /= n;

		return meanabs;
	}

	@Override
	public String toString()
	{
		return "Golomb code";
	}
}
