package samzip.compression.numbercode;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import java.io.IOException;
import java.io.OutputStream;
import samzip.compression.EncoderMethod;
import samzip.compression.util.DataMode;
import samzip.compression.util.Exceptions;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class HuffmanEncoder extends EncoderMethod
{
	protected CanonicalHuffmanEncoding encoder;
	protected IntArrayList dataBuf = new IntArrayList();
	
	public HuffmanEncoder(OutputStream outputStream)
	{
		configureDataMode(DataMode.INT);
		setOutputStream(outputStream);
	}
	
	@Override
	public void write(int d) throws IOException
	{
		if (!initialized)
			throw new IOException(Exceptions.NOT_INITIALIZED_ENCODER);
		
		d = dataMode.convertType(d);
		encoder.increment(d);
		dataBuf.add(d);
	}

	@Override
	public void initialize()
	{
		if (initialized)
			return;
		
		super.initializeOutputStream();
		
		if (outputStream != null) {
			encoder = new CanonicalHuffmanEncoding(outputStream);
			initialized = true;
		} else {
			super.initialize();
		}
	}

	@Override
	public void close() throws IOException
	{
		if (!initialized)
			return;
		
		encoder.initializeEncoding();
		for(int d: dataBuf) {
			encoder.encode(d);
		}
		encoder.close();
		encoder = null;
		dataBuf = null;
		
		super.close();
	}
}
