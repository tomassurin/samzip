package samzip.util;

import java.io.PrintStream;
import java.util.logging.Level;

/**
 * Class representing standard output and error streams. Also contains utilities
 * for pretty printing exceptions/errors.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ConsoleWriter
{
	public final PrintStream out;
	public final PrintStream err;

	protected static ConsoleWriter instance = new ConsoleWriter();
	private int level;
	private final String LEVEL_SEP = "   ";
	private final String ERROR_PREFIX = "ERROR: ";
	private final String WARNING_PREFIX = "WARNING: ";
	
	public ConsoleWriter()
	{
		out = System.out;
		err = System.err;
	}
	
	public ConsoleWriter(PrintStream out, PrintStream err)
	{
		this.out = out;
		this.err = err;
	}
	
	public static ConsoleWriter instance()
	{		
		return instance;
	}
	
	public synchronized void info(String msg)
	{
		for(int i=0; i < level; ++i) {
			out.append(LEVEL_SEP);
		}
		out.println(msg);
	}
	
	public synchronized void warning(String msg)
	{
		for(int i=0; i < level; ++i) {
			err.append(LEVEL_SEP);
		}
		err.println(WARNING_PREFIX+msg);
		err.flush();
	}
	
	private synchronized void writeLevel(PrintStream out)
	{
		for(int i=0; i < level; ++i) {
			out.append(LEVEL_SEP);
		}
	}
	
	public synchronized void error(String msg)
	{
		writeLevel(err);
		err.println(msg);
		err.flush();
	}
	
	public synchronized void error(Throwable ex)
	{
		error(null, ex, false);
	}
	
	public synchronized void error(String msg, Throwable ex)
	{
		error(msg, ex, false);
	}
	
	public synchronized void error(String msg, Throwable ex, boolean multiline)
	{
		if (msg != null) {
			writeLevel(err);
			err.print(msg);
			err.print(" ");
			increaseLevel();
		}
		if (ex == null) {
			err.println();
		}
		else {
			if (multiline) {
				if (msg != null)
					err.println();
				
				String[] lines = ex.toString().split("\n");
				for(String s : lines) {
					writeLevel(err);
					err.println(s);
				}
				Throwable cause = ex.getCause();
				while (cause != null) {
					writeLevel(err);
					err.println("Cause: "+cause.toString());
					cause = cause.getCause();
				}
			} else {
				if (msg == null)
					err.print(ERROR_PREFIX);
				err.println(ex.toString());
				
				Throwable cause = ex.getCause();
				while (cause != null) {
					writeLevel(err);
					err.println("Cause: "+cause.toString());
					cause = cause.getCause();
				}
			}
		}
		if (msg != null) {
			decreaseLevel();
		}
		
		err.flush();
		LOG.log(Level.SEVERE, null, ex);
	}
	
	public synchronized void flush()
	{
		err.flush();
		out.flush();
	}

	public synchronized void close()
	{
		err.close();
		out.close();
	}

	public void increaseLevel()
	{
		level++;
	}

	public void decreaseLevel()
	{
		level--;
	}
}
