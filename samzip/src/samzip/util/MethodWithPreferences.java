package samzip.util;

import samzip.compression.CompressionCodec;

/**
 * Abstract class representing object that has configurable preferences. 
 * Preferences are represented as (key, value) pairs.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public abstract class MethodWithPreferences
{
	/** object which holds preferences */
	protected Preferences preferences = new Preferences();
		
	/**
	 * Sets single preference (key, value) pair. This method calls {@code preferenceChanged(key)}
	 * which indicates that preferences changed.
	 *
	 * @param key
	 * @param value
	 */
	public void setPreference(String key, String value) throws PreferencesException
	{
		preferences.setString(key, value);

		preferenceChanged(key);
	}

	/**
	 * Gets preferences value for key {@code key}.
	 *
	 * @param key
	 */
	public String getPreference(String key)
	{
		return preferences.getString(key, null);
	}

	/**
	 * Gets method preferences object.
	 */
	public Preferences getPreferences()
	{
		return preferences;
	}

	/**
	 * This method is called whenever preference with key {@code whatKey} changes.
	 * This method should be overriden whenever children class needs to know about
	 * preferences changes.
	 * 
	 * @param whatKey key of changed preference
	 * @throws PreferencesException if there was any problem with options
	 */
	protected void preferenceChanged(String whatKey) throws PreferencesException
	{
//		LOG.warning("Can't recognize option: "+whatKey);
		//throw new PreferencesException("Can't recognize option: "+whatKey);
	}

	/**
	 * This method calls {@code preferenceChanged(key)} for all contained keys.
	 * @throws PreferencesException if there was any problem with configuration
	 */
	protected void preferencesChanged() throws PreferencesException
	{
		for(String key: preferences.getKeys()) {
			preferenceChanged(key);
		}
	}
	
	/**
	 * <p>Connects codec {@code codec} to named pipe with id {@code id}.
	 * Depending on method implementation, there can be any number of named pipes.</p>
	 * 
	 * <pre> 
	 *       pipeA
	 * this -------> codec1
	 * this -------> codec2
	 *       pipeB
	 * </pre>
	 * 
	 * <p>This codecs are used for creation of EncoderMethod and DecoderMethod instances.
	 * </p>
	 * <pre>
	 *       write                  write
	 * this -------> EncoderMethod -----> OutputStream
	 * 
	 *        read                  read 
	 * this &lt;------- DecoderMethod &lt;----- InputStream
	 * </pre>
	 * 
	 * @param pipeID named pipe id
	 * @param codec connected codec
	 */
	public void addCodec(String pipeID, CompressionCodec codec)
	{
	}
}
