package samzip.util;

import java.util.Locale;
import net.sf.samtools.SAMFileHeader;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.SAMSequenceDictionary;
import net.sf.samtools.SAMSequenceRecord;

/**
 * Utility class that outputs compression/decompression progress. Progress and 
 * remaining time is calculated from position in reference sequence and so it's 
 * not very accurate.
 * 
 * This approach was chosen because we wasn't able to find any way to find position 
 * in input SAM/BAM file. Probably this is limitation of format and/or Picard tools 
 * which are used for processing SAM/BAM files.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMZipProgress
{
	private ConsoleWriter out;
	
	private SAMFileHeader header;
	private SAMSequenceDictionary seqDict;
	
	private long totalRefLength;
	private long processedSize;
	private long curRefTime;
	
	private int curRef = 0;
	private int curRefLength;
	private int seqCount;
	
	private final int outputInterval;
	private StatUtil stat = new StatUtil();

	protected ConsoleWriter console = new ConsoleWriter(System.out, System.err);
	
	public SAMZipProgress(ConsoleWriter writer, int outputInterval)
	{
		this.out = writer;
		this.outputInterval = outputInterval;
	}

	public SAMZipProgress()
	{
		this.out = new ConsoleWriter();
		this.outputInterval = 100;
	}
	
	/**
	 * Initialize with SAM file header. This header is mandatory because it's
	 * used for querying reference sequence length.
	 */
	public void initialize(SAMFileHeader header)
	{
		this.header = header;
		seqDict = header.getSequenceDictionary();
		totalRefLength = seqDict.getReferenceLength();		
		seqCount = seqDict.size();
		processedSize = 0;
		curRefLength = seqDict.getSequence(curRef).getSequenceLength();
		stat.startCounter();
	}

	public void writeProgress(SAMRecord[] block)
	{
		if (block.length < 1)
			return;
		
		SAMRecord rec = block[block.length-1];
		writeProgress(rec);
	}
	
	public void writeProgress(SAMRecord rec)
	{	
		int refIdx = rec.getReferenceIndex();
		if (refIdx == -1)
			return;
		if (refIdx != curRef) {
			for(; curRef < refIdx;++curRef) {
				processedSize += seqDict.getSequence(curRef).getSequenceLength();
			}
			curRef = refIdx;
			curRefTime = stat.elapsedTime();
//			return;
		}
		
		SAMSequenceRecord refSeq = rec.getHeader().getSequence(refIdx);
		double refProgress = (double)rec.getAlignmentStart()/refSeq.getSequenceLength();
		double totalProgress = (double)(rec.getAlignmentStart()+processedSize)/totalRefLength;
		
		long totalTime = stat.elapsedTime()/1000000;
		long refTime = (stat.elapsedTime()-curRefTime)/1000000;
		
		/// progress in current sequence
		double expectedTime = (double)refTime/(refProgress)*(1-refProgress);
		String res = String.format(Locale.US, "Seq %1$d/%2$d %3$04.2f%% ETA:%4$s", 
				                    refIdx+1, seqCount, refProgress*100, 
									StatUtil.createTimeString((long)expectedTime));
		
		/// total progress
		expectedTime = (double)totalTime/(totalProgress)*(1-totalProgress);
		String res2 = String.format(Locale.US, " (%1$04.2f%% ETA:%2$s)", 
									totalProgress*100,
				                    StatUtil.createTimeString((long)expectedTime));
		
		out.info(res+res2);		
	}

}
