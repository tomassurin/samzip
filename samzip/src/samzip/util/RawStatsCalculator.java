package samzip.util;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import net.sf.samtools.SAMRecord.SAMTagAndValue;
import net.sf.samtools.*;
import samzip.processor.util.SAMField;
import samzip.processor.util.Seq4bitUtils;

/**
 * Dirty class that calculates field stats for input SAM/BAM file.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class RawStatsCalculator
{

	private static Charset charset = Charset.forName("UTF-8");
	
	public boolean outputSAMData = true;
	public boolean outputBAMData = false;
	
	public void calculate(String input, OutputStream outputStream) throws FileNotFoundException, IOException
	{
		PrintWriter output = new PrintWriter(outputStream);
		
		File inputf = new File(input);
		if (!inputf.exists()) {
			throw new FileNotFoundException("Input file \"" + input + "\" does not exist");
		}
		
		SAMFileReader reader = new SAMFileReader(inputf);
		reader.setValidationStringency(SAMFileReader.ValidationStringency.LENIENT);
		SAMRecordIterator it = reader.iterator();
		
		byte[] tmp;
		String str;
		SAMFieldRawStats st;
		
		// initialize
		final String[] fields = new String[] 
		         {"QNAME", "FLAG", "REF", "POS", "MAPQ", "CIGAR",
				  "SEQ", "QUAL", "RNEXT", "PNEXT", "TLEN"};
		Map<String, SAMFieldRawStats> stats = new TreeMap<>(new Comparator<String>() {

			@Override
			public int compare(String o1, String o2)
			{
				if (o1.startsWith("TAG")) {
					if (o2.startsWith("TAG")) {
						return o1.compareTo(o2);
					} else
						return 1;
				} 
				if (o2.startsWith("TAG")) {
					if (o1.startsWith("TAG")) 
						return o1.compareTo(o2);
					else
						return -1;
				}
				int idx1 = -1;
				int idx2 = -1;
				for(int i=0; i < fields.length; ++i) {
					if (o1.equals(fields[i]))
						idx1 = i;
					if (o2.equals(fields[i]))
						idx2 = i;
				}
				return idx1 == idx2 ? 0 : (idx1 < idx2 ? -1 : 1);
			}
			
		});
		
		for(String name : fields) {
			stats.put(name, new SAMFieldRawStats(name));
		}
		
		st = stats.get("QNAME");
		st.setSamBamEqual();
		
		long alignmentCount = 0;
		long unmappedCount = 0;
		long secondaryAlignments = 0;
		
		// calculate data
		while (it.hasNext()) {
			SAMRecord rec = it.next();
			
			/// global
			++alignmentCount;
			if (rec.getReadUnmappedFlag())
				++unmappedCount;
			if (rec.getNotPrimaryAlignmentFlag())
				++secondaryAlignments;
			
			/// QNAME
			st = stats.get("QNAME");
			str = rec.getReadName();
			if (str == null)
				str = "*";
			tmp = str.getBytes(charset);
			st.writeSAM(tmp);
			st.bamSize += tmp.length + 1;
			/// FLAG
			st = stats.get("FLAG");
			int flags = rec.getFlags();
			tmp = Integer.toString(flags).getBytes(charset);
			st.writeSAM(tmp);
			st.writeBAM(getShortData(flags));
			/// REF
			st = stats.get("REF");
			str = rec.getReferenceName();
			if (str == null)
				str = "*";
			tmp = str.getBytes(charset);
			int refIdx = rec.getReferenceIndex();
			st.writeSAM(tmp);
			st.writeBAM(getIntData(refIdx));
			/// POS
			st = stats.get("POS");
			int pos = rec.getAlignmentStart();
			tmp = String.valueOf(pos).getBytes();
			st.writeSAM(tmp);
			st.writeBAM(getIntData(pos));
			/// MAPQ
			st = stats.get("MAPQ");
			int mapq = rec.getMappingQuality();
			tmp = Integer.toString(mapq).getBytes(charset);
			st.writeSAM(tmp);
			st.writeBAM(new byte[] {(byte)(mapq & 0xff)});
			/// CIGAR
			st = stats.get("CIGAR");
			Cigar cig = rec.getCigar();
			if (cig != null)
				tmp = TextCigarCodec.getSingleton().encode(cig).getBytes(charset);
			else
				tmp = "*".getBytes(charset);
			st.writeSAM(tmp);
			if (cig != null) {
				int[] binaryCigar = encodeCigarToBinary(cig);
				for(int data : binaryCigar) {
					st.writeBAM(getIntData(data));
				}
			}
			st.bamSize += 2; // cigar numops
			/// SEQ
			st = stats.get("SEQ");
			tmp = rec.getReadBases();
			st.writeSAM(tmp);
			Seq4bitUtils.normalizeBases(tmp);
			tmp = Seq4bitUtils.bytesToCompressedBases(tmp);
			st.writeBAM(tmp);
			/// QUAL
			st = stats.get("QUAL");
			tmp = rec.getBaseQualityString().getBytes(charset);
			st.writeSAM(tmp);
			tmp = rec.getBaseQualities();
			st.writeBAM(tmp);
			/// RNEXT
			st = stats.get("RNEXT");
			str = rec.getMateReferenceName();
			if (str == null)
				str = "*";
			refIdx = rec.getReferenceIndex();
			if (refIdx == rec.getReferenceIndex()) {
				str = "=";
			}
			tmp = str.getBytes(charset);
			st.writeSAM(tmp);
			st.writeBAM(getIntData(refIdx));
			/// PNEXT
			st = stats.get("PNEXT");
			pos = rec.getMateAlignmentStart();
			tmp = String.valueOf(pos).getBytes();
			st.writeSAM(tmp);
			st.writeBAM(getIntData(pos));
			/// TLEN
			st = stats.get("TLEN");
			int tlen = rec.getInferredInsertSize();
			tmp = String.valueOf(tlen).getBytes();
			st.writeSAM(tmp);
			st.writeBAM(getIntData(tlen));
			List<SAMTagAndValue> attrs = rec.getAttributes();
			for(SAMTagAndValue attr : attrs) {
				String id = "TAG."+attr.tag;
				if (!stats.containsKey(id)) {
					stats.put(id, new SAMFieldRawStats(id));
				}
				
				st = stats.get(id);
				st.calcTAG(attr);
			}
		}
		// write data
		output.printf("Alignments:\t%1$d\n",alignmentCount);
		output.printf("Unmapped alignments:\t%1$d\n",unmappedCount);
		output.printf("Duplicate alignments:\t%1$d\n",secondaryAlignments);
		output.println("================================================================");
		writeStatsHeader(output);
		for(String key : stats.keySet()) {
			writeStatsForField(stats.get(key), output, key);
		}
		output.close();
	}
	
	private static byte[] getShortData(int data)
	{
		return new byte[] { (byte) (data & 0xff), (byte) (data & 0xff00)};
	}
	
	private static byte[] getIntData(int data)
	{
		return new byte[] { (byte) (data & 0xff), (byte) (data & 0xff00), 
							(byte) (data &0xff0000), (byte) (data & 0xff000000)};
	}
	
	private void writeStatsHeader(PrintWriter output)
	{
		output.printf("Field\tSAM size\tBAM size\tSAM entropy\tBAM entropy\n");
	}
	
	private void writeStatsForField(SAMFieldRawStats stats, PrintWriter output, String displayName) throws IOException
	{
		stats.close();
		
		output.printf("%1$s\t", displayName);
		output.printf("%1$d\t", stats.samSize);
		output.printf("%1$d\t", stats.bamSize);
		output.printf("%1$1.5f\t", stats.samEnt.getEntropy());
		output.printf("%1$1.5f\n", stats.bamEnt.getEntropy());
	}
	
	private class SAMFieldRawStats 
	{
		final String id;

		final String SAM_EXT = ".dat";
		final String FILE_PREFIX = "field.";
		final String SAM_FILE_SEP = "\n";
		final String BAM_EXT = ".bam.dat";
		
		EntropyOutputStream samEnt = new EntropyOutputStream();
		EntropyOutputStream bamEnt = new EntropyOutputStream();	
		
		long samSize = 0;
		long bamSize = 0;
		long samSizeWithTAG = 0;
		long bamSizeWithTAG = 0;
		boolean isTag = false;
		
		private OutputStream samOut;
		private OutputStream bamOut;
		
		public SAMFieldRawStats(String id) throws FileNotFoundException, IOException
		{
			this.id = id;
			if (outputSAMData) {
				samOut = new BufferedOutputStream(
						new FileOutputStream(FILE_PREFIX+id+SAM_EXT));
			}
			if (outputBAMData) {
				bamOut = new BufferedOutputStream(
						new FileOutputStream(FILE_PREFIX+id+BAM_EXT));
			}
		}
		
		public void writeSAM(byte[] data) throws IOException
		{
			samSize += data.length;
			samSizeWithTAG += data.length;
			samEnt.write(data);
			
			if (outputSAMData) {
				samOut.write(data);
				samOut.write(SAM_FILE_SEP.getBytes());
			}
		}
		
		public void writeBAM(byte[] data) throws IOException
		{
			bamSize += data.length;
			bamSizeWithTAG += data.length;
			bamEnt.write(data);
			if (outputBAMData)
				bamOut.write(data);
		}
		
		void close() throws IOException
		{
			if (outputSAMData) {
				samOut.close();
			}
			
			if (outputBAMData) {
				bamOut.close();
			}
		}

		void setSamBamEqual()
		{
			bamEnt = samEnt;
		}

		private void calcTAG(SAMTagAndValue attr) throws IOException
		{
			String str;
			byte[] tmp;
			isTag = true;
			bamSizeWithTAG += 2+1; // two character tag and its value type
			samSizeWithTAG += 5; // TT:i:

			switch (SAMField.TagDataType.getType(attr.value)) {
				case BYTE:
				{
					byte data = (byte) attr.value;
					str = String.valueOf(data);
					writeSAM(str.getBytes(charset));
					writeBAM(new byte[] { data } );
					break;
				}
				case SHORT:
				{
					short data = (short) attr.value;
					str = String.valueOf(data);
					writeSAM(str.getBytes(charset));
					writeBAM(getShortData(data));
					break;
				}
				case INT:
				{
					int data = (int) attr.value;
					str = String.valueOf(data);
					writeSAM(str.getBytes(charset));
					writeBAM(getIntData(data));
					break;
				}
				case CHAR:
					char c = (char)attr.value;
					writeSAM(new byte[] {(byte) c});
					writeBAM(new byte[] {(byte) c});
					break;
				case STRING:
					str = (String) attr.value;
					tmp = str.getBytes(charset);
					writeSAM(tmp);
					writeBAM(tmp);
					break;
				case FLOAT:
				case BYTE_ARR:
				case FLOAT_ARR:
				case INT_ARR:
				case SHORT_ARR:
					break;
				default: { }
			}
		}
	}
	
	public static class EntropyOutputStream extends OutputStream
	{

		long[] counts = new long[256];
		long count = 0;
		
		@Override
		public void close()
		{
			for(int i=0; i < 255; ++i) {
				counts[i] = 0;
			}
			count = 0;
		}
		
		@Override
		public void write(int b) throws IOException
		{
			counts[b & 0xff]++;
			count++;
		}
		
		
		public double getEntropy()
		{
			if (count <= 0)
				return 0;
			
			double ent = 0;
			double freq;
			for(int i=0; i < 255; ++i) {
				if (counts[i] == 0)
					continue;
				freq = (double)counts[i] / count;
				ent += freq*Math.log(freq);
			}
			return -ent/Math.log(2);
		}
	}
	
	/**
     * Original source: Picard SDK
     */
    private static int[] encodeCigarToBinary(final Cigar cigar) {
        if (cigar.numCigarElements() == 0) {
            return new int[0];
        }

        // Binary rep can be no longer than 1/2 of text rep
        // Although this is documented as uint, I think lengths will never get that long,
        // and it's a pain in Java.
        final int[] binaryCigar = new int[cigar.numCigarElements()];
        int binaryCigarLength = 0;
        for (int i = 0; i < cigar.numCigarElements(); ++i) {
            final CigarElement cigarElement = cigar.getCigarElement(i);
            final int op = CigarOperator.enumToBinary(cigarElement.getOperator());
            binaryCigar[binaryCigarLength++] = cigarElement.getLength() << 4 | op;
        }
        return binaryCigar;
    }
}
