package samzip.util;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class Touple<F,S>
{
	protected final F first;
	protected final S second;

	public Touple(F first, S second)
	{
		this.first = first;
		this.second = second;
	}
	
	public F first()
	{
		return first;
	}
	
	public S second()
	{
		return second;
	}
}
