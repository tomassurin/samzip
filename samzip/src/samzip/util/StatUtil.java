package samzip.util;

import java.io.File;
import java.net.URISyntaxException;
import java.security.CodeSource;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class StatUtil
{	
	private long startTime;
	private long lastTime;
	
	/** 
	 * Starts internal nanosecond counter.
	 * @return this instance
	 */
	public StatUtil startCounter()
	{
		startTime = System.nanoTime();
		lastTime = startTime;
		return this;
	}
	
	/**
	 * Stops counter.
	 */
	public void stopCounter()
	{
		startTime = 0;
	}
	
	/**
	 * @return elapsed time since counter started
	 */
	public long elapsedTime()
	{
		return System.nanoTime() - startTime;
	}
	
	/**
	 * @return elapsed time since last call to this method
	 */
	public long timeInterval()
	{
		long newtime = System.nanoTime();
		long result = newtime - lastTime;
		lastTime = newtime;
		return result;
	}
	
	/**
	 * Create time string from time interval in milliseconds. 
	 * @param timems time interval in milliseconds
	 * @return time string in format hh:mm:ss
	 */
	public static String createTimeString(long timems, boolean outputHours)
	{
		if (outputHours) {
			int hours = (int)timems/3600000;
			timems = timems%3600000;
			int minutes = (int)timems/60000;
			timems = timems%60000;
			int seconds = (int)timems/1000;

			return String.format("%1$02d:%2$02d:%3$02d", hours, minutes, seconds);
		} else {
			int minutes = (int)timems/60000;
			timems = timems%60000;
			int seconds = (int)timems/1000;
		
			return String.format("%1$02d:%2$02d", minutes, seconds);
		}
	}
	
	public static String createTimeString(long timems)
	{
		return createTimeString(timems, true);
	}
	
	public static String createTimeStringNano(long timenano, boolean outputHours)
	{
		return createTimeString(timenano/1000000, outputHours);
	}
	
	/**
	 * Creates time string from time interval in nanoseconds.
	 * @param timenano time interval in nanoseconds
	 * @return time string in format hh:mm:ss
	 */
	public static String createTimeStringNano(long timenano)
	{
		return createTimeString(timenano/1000000, true);
	}
	
	public static StatUtil newInstance()
	{
		return new StatUtil();
	}
	
	public static long getRAM()
	{
		return Runtime.getRuntime().totalMemory();
	}
	
	public static int getCPUCount()
	{
		return Runtime.getRuntime().availableProcessors();
	}	
	
	public static String getJarDir()
	{
		CodeSource codeSource = StatUtil.class.getProtectionDomain().getCodeSource();
		File jarFile;
		try {
			jarFile = new File(codeSource.getLocation().toURI().getPath());
		} catch (URISyntaxException ex) {
			return ".";
		}
	    return jarFile.getParentFile().getPath();
	}
	
	public static void main(String[] arg)
	{
		System.out.println(getJarDir());
	}
}
