package samzip.util;

/**
 * Signals that some option value has incorrect or unknown format or that
 * option key is not recognized.
 */
public class PreferencesException extends Exception
{
	private String optionKey = "";

	public PreferencesException(String message)
	{
		super(message);
	}

	public PreferencesException(String message, String optionKey)
	{
		super(message);
		this.optionKey = optionKey;
	}
	
	public PreferencesException(Throwable cause)
	{
		super(cause);
	}

	public PreferencesException(Throwable cause, String optionKey)
	{
		super(cause);
		this.optionKey = optionKey;
	}
	
	public PreferencesException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public PreferencesException(String message, Throwable cause, String optionKey)
	{
		super(message, cause);
		this.optionKey = optionKey;
	}

	public String getOption()
	{
		return optionKey;
	}
	
	
//	public String toString() {
//        String s = "PreferencesException";//getClass().getName();
//        String message = getLocalizedMessage();
//        return (message != null) ? (s + ": " + message) : s;
//    }
	
}
