package samzip.util;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import net.sf.samtools.SAMFileReader;
import net.sf.samtools.SAMFileReader.ValidationStringency;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.SAMRecord.SAMTagAndValue;
import net.sf.samtools.SAMRecordIterator;

/**
 * Diff for 2 SAM/BAM files. 
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMDiff 
{
	/** Validation stringency - how strict are the errors. */
	private SAMFileReader.ValidationStringency validationStringency = SAMFileReader.ValidationStringency.DEFAULT_STRINGENCY;
	/** True if we want to calculate stats for tags too. */
	private boolean includeTags = true;

	public void setIncludeTags(boolean includeTags)
	{
		this.includeTags = includeTags;
	}
	
	public void setValidationStringency(ValidationStringency validationStringency)
	{
		this.validationStringency = validationStringency;
	}
	
	/** Calculate diff of 2 SAM/BAM files and writes output to {@code outputStream}. */
	public boolean diff(String input1, String input2, OutputStream outputStream) throws FileNotFoundException
	{
		PrintWriter output = new PrintWriter(outputStream);
		
		File file1 = new File(input1);
		if (!file1.exists()) {
			throw new FileNotFoundException("File \"" + input1 + "\" does not exist");
		}
		File file2 = new File(input2);
		if (!file2.exists()) {
			throw new FileNotFoundException("File \"" + input2 + "\" does not exist");
		}
		
		SAMFileReader reader = new SAMFileReader(file1);
		reader.setValidationStringency(validationStringency);
		SAMRecordIterator it1 = reader.iterator();
		reader = new SAMFileReader(file2);
		reader.setValidationStringency(validationStringency);
		SAMRecordIterator it2 = reader.iterator();
		
		SAMRecord rec1 = null;
		SAMRecord rec2 = null;
		boolean identical = true;
		int recordNo = 0;
		
		while (it1.hasNext() || it2.hasNext()) {
			++recordNo;
			if (it1.hasNext()) 
				rec1 = it1.next();
			else
				rec1 = null;
			
			if (it2.hasNext()) 
				rec2 = it2.next();
			else
				rec2 = null;
			
			if (rec1 == null) {
				identical = false;
				output.println("("+recordNo+")<"+rec2.getSAMString());
				continue;
			}
			if (rec2 == null) {
				identical = false;
				output.println("("+recordNo+")>"+rec1.getSAMString());
				continue;
			}
			String res = compareSAMRecords(rec1, rec2);
			if (res != null && !res.isEmpty()) {
				output.println("("+recordNo+") <> "+res);
				identical = false;
			}
		}
		
		output.flush();
		
		return identical;
	}
	
	private String compareSAMRecords(SAMRecord rec1, SAMRecord rec2)
	{
		StringWriter output = new StringWriter();
        // First check all the elements that do not require decoding
		String tmp1 = rec1.getReadName();
		String tmp2 = rec2.getReadName();
		if (tmp1 == null)
			tmp1 = "";
		if (tmp2 == null)
			tmp2 = "";
		if (!tmp1.equals(tmp2))
			output.append("QNAME ");
        if (rec1.getFlags() != rec2.getFlags()) 
			output.append("FLAG ");
		if (rec1.getReferenceIndex() != rec2.getReferenceIndex())
			output.append("RNAME");
		if (rec1.getAlignmentStart() != rec2.getAlignmentStart())
			output.append("POS ");
		if (rec1.getMappingQuality() != rec2.getMappingQuality())
			output.append("MAPQ ");
		if (rec1.getCigar() != null ? !rec1.getCigar().equals(rec2.getCigar()) : rec2.getCigar() != null)
			output.append("CIGAR ");
		if (!Arrays.equals(rec1.getReadBases(), rec2.getReadBases()))
			output.append("SEQ ");
        if (rec1.getMateAlignmentStart() != rec2.getMateAlignmentStart())
			output.append("PNEXT ");
		if (rec1.getMateReferenceIndex() != rec2.getMateReferenceIndex())
			output.append("RNEXT ");
		if (rec1.getInferredInsertSize() != rec2.getInferredInsertSize())
			output.append("TLEN ");
        if (!Arrays.equals(rec1.getBaseQualities(), rec2.getBaseQualities()))
			output.append("QUAL ");
		
		if (includeTags) {
			List<SAMTagAndValue> attrs = rec1.getAttributes();
			for(SAMTagAndValue attr1 : attrs) {
				Object attr2 = rec2.getAttribute(attr1.tag);
				if (attr1.value != null ? !attr1.value.equals(attr2) : attr2 != null)
					output.append("TAG:"+attr1.tag+" ");
			}
		}
        
        return output.toString();
	}
}