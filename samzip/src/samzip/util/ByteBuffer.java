package samzip.util;

/**
 * Dynamic byte buffer.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class ByteBuffer
{
	protected byte[] buf;
	protected int bufPos = 0;

	public final int length()
	{
		return bufPos;
	}

	public final byte[] buffer()
	{
		return buf;
	}

	public final void clear()
	{
		bufPos = 0;
	}

	public ByteBuffer(int capacity)
	{
		this.buf = new byte[capacity];
	}

	public final void add(byte value)
	{
		if (bufPos >= buf.length) {
			byte[] bufnew = new byte[buf.length << 1];
			System.arraycopy(buf, 0, bufnew, 0, bufPos);
			buf = bufnew;
		}
		buf[bufPos++] = value;
	}
	
	public final void add(byte[] values)
	{
		for(byte b : values) {
			add(b);
		}
	}
}
