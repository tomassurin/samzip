package samzip.util;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import samzip.io.FastByteArrayOutputStream;

/**
 * Compression/decompression stats container, calculator and pretty printer.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMZipStats
{
	public static class BlockCompressionStats
	{
		public int alignmentsCount;
		public int rawSAMSize;
		public int blockNo;
		public long filePointer;
		
		public List<Long> compressionTime = new ArrayList<>();
		public List<Long> decompressionTime = new ArrayList<>();
		
		public List<Long> streamSize = new ArrayList<>();
		public List<String> streamName = new ArrayList<>();
		
		public List<String> processorName = new ArrayList<>();
		
		public long totalStreamSize = 0;
		public long lzmaSize;
		public long lzmaTime;
		
		public BlockCompressionStats nextBlock = null;
		
		@Override
		public String toString()
		{
			FastByteArrayOutputStream resultStream = new FastByteArrayOutputStream();
			PrintStream infoWriter = new PrintStream(resultStream, true);
			long total;
			
			if (blockNo != 0) 
				infoWriter.printf("Stats for block %1$d\n",blockNo);
			infoWriter.println("Block offset: "+filePointer);
			infoWriter.println("Alignments count: "+alignmentsCount);
			infoWriter.println("*");
			if (rawSAMSize != 0) {
				infoWriter.printf("Raw SAM data size: %1$6d B\n",rawSAMSize);
				infoWriter.println("*");
			}
			
			if (lzmaTime != 0 || lzmaSize != 0) {
				infoWriter.printf("Raw SAM data with LZMA codec size: %1$6dB (%2$5.2f%%)\n",lzmaSize, (float)lzmaSize/rawSAMSize*100);
				infoWriter.printf("Raw SAM data compression with LZMA time: %1$7.3f ms\n",(float)lzmaTime/1000000);	
				infoWriter.println("*");
			}
					
			if (!compressionTime.isEmpty()) {
				total = 0;
				infoWriter.println("Compression time per processor:");
				for(int i=0; i < compressionTime.size(); ++i) {
					long time = compressionTime.get(i);
					if (processorName.size() <= i) {
						infoWriter.printf("Processor %1$02d: %2$7.3f ms \n",i+1,(float)time/1000000);
					} else {
						infoWriter.printf("Processor %1$02d: %2$7.3f ms (%3$s)\n",i+1,
								            (float)time/1000000, processorName.get(i));
					}
					total += time;
				}
				infoWriter.printf("Total compression time: %1$7.3f ms\n",(float)total/1000000);
				infoWriter.println("*");
			}
			
			if (!decompressionTime.isEmpty()) {
				total = 0;
				infoWriter.println("Decompression time per processor:");
				for(int i=0; i < decompressionTime.size(); ++i) {
					long time = decompressionTime.get(i);
					if (processorName.size() <= i) {
						infoWriter.printf("Processor %1$02d: %2$7.3f ms \n",i+1,(float)time/1000000);
					} else {
						infoWriter.printf("Processor %1$02d: %2$7.3f ms (%3$s)\n",i+1,(float)time/1000000, processorName.get(i));
					}
					total += time;
				}
				infoWriter.printf("Total decompression time: %1$7.3f ms\n",(float)total/1000000);
				infoWriter.println("*");
			}
			if (!streamSize.isEmpty()) {
				total = 0;
				infoWriter.printf("Stream sizes:\n");
				for(int i=0; i < streamSize.size(); ++i) {
					infoWriter.printf("Stream %1$02d: %3$10dB (%2$s)\n",i+1,streamName.get(i),streamSize.get(i));
					total += streamSize.get(i);
				}
				totalStreamSize = total;
				infoWriter.printf("Total size: %1$6dB\n",total);
				if (nextBlock != null) {
					long blockLen = nextBlock.filePointer - filePointer;
					infoWriter.printf("File structures size: %1$dB\n", blockLen - totalStreamSize);
				}
			}

			return new String(resultStream.toByteArray(), 0, resultStream.length());
		}
	}
	
	public void addBlock(BlockCompressionStats block) 
	{
		alignmentsCount += block.alignmentsCount;
		blockCount++;
		
		boolean hasNames = block.streamSize.size() <= block.streamName.size();
		for(int i=0; i < block.streamSize.size(); ++i) {
			if (hasNames && streamName.size() <= i) {
				streamName.add(block.streamName.get(i));
			} 
			
			if (streamSize.size() < i+1) {
				streamSize.add(0L);
			}
			
			Long len = streamSize.get(i)+block.streamSize.get(i);
			totalCompressedSize += block.streamSize.get(i);
			streamSize.set(i, len);
		}
		
		for(int i=0; i < block.processorName.size(); ++i) {
			if (processorName.size() <= i) {
				processorName.add(block.processorName.get(i));
			}
		}
		
		for(int i=0; i < block.compressionTime.size(); ++i) {
			if (compressionTime.size() <= i) {
				compressionTime.add(0L);
			}
			long comTime = compressionTime.get(i)+block.compressionTime.get(i);
			totalProcessorCompressionTime += block.compressionTime.get(i);
			compressionTime.set(i, comTime);
		}
		
		for(int i=0; i < block.decompressionTime.size(); ++i) {
			if (decompressionTime.size() <= i) {
				decompressionTime.add(0L);
			}
			long time = decompressionTime.get(i)+block.decompressionTime.get(i);
			totalProcessorDecompressionTime += block.decompressionTime.get(i);
			decompressionTime.set(i, time);
		}
	}	
	
	public long alignmentsCount;
	public int blockCount;
	
	public long outputFileSize;
	public long inputFileSize;
	public int headerSize;
	public int profileSize;
	public int indexSize;
	public long totalCompressedSize;
	
	public long userCompressionTime;
	public long userDecompressionTime;	
	
	public double compressionRatio;
	public double compressionRate;
	public double decompressionRate;
	
	public List<Long> streamSize = new ArrayList<>();
	public List<String> streamName = new ArrayList<>();
	
	// structures for saving information about processor run times
	public long totalProcessorCompressionTime = 0;
	public long totalProcessorDecompressionTime = 0;
	public List<String> processorName = new ArrayList<>();
	public List<Long> decompressionTime = new ArrayList<>();
	public List<Long> compressionTime = new ArrayList<>();
	
	public void calculate()
	{
		compressionRatio = (double)outputFileSize/inputFileSize*100;
		compressionRate = (double)inputFileSize/userCompressionTime*1000000000/1024;
		decompressionRate = (double)inputFileSize/userDecompressionTime*1000000000/1024;
	}
	
	@Override
	public String toString()
	{
		FastByteArrayOutputStream resultStream = new FastByteArrayOutputStream();
		PrintStream infoWriter = new PrintStream(resultStream, true);
		long total;
		calculate();
		
		infoWriter.println("================================================================");
		infoWriter.printf("Raw size: %1$6dB\n", inputFileSize);
		infoWriter.printf("Compressed size: %1$6dB\n", outputFileSize);
		infoWriter.printf("Compression ratio: %1$5.3f%%\n", compressionRatio);
		infoWriter.printf("SAM records: %1$6d\n", alignmentsCount);
		infoWriter.printf("Compressed blocks: %1$d\n", blockCount);
		infoWriter.printf("*\n");
		infoWriter.printf("Compression time: %2$7.3f ms (%1$s)\n", StatUtil.createTimeStringNano(userCompressionTime),
				(double)userCompressionTime/1000000);
		infoWriter.printf("Decompression time: %2$7.3f ms (%1$s)\n",StatUtil.createTimeStringNano(userDecompressionTime),
				(double)userDecompressionTime/1000000);
		infoWriter.printf("*\n");
		infoWriter.printf("Compression rate: %1$5.3f kb/s\n", 
				compressionRate);
		infoWriter.printf("Decompression rate: %1$5.3f kb/s\n", 
				decompressionRate);
		
		if (!streamName.isEmpty() && !streamSize.isEmpty() && totalCompressedSize != 0) {
			infoWriter.println("================================================================");
			infoWriter.printf("Stream sizes:\n");
			for(int i=0; i < streamSize.size(); ++i) {
				if (streamName.size() > i) {
					infoWriter.printf("Stream %1$02d: %2$10dB (%4$5.2f%%) (%3$s) \n", i+1, 
							streamSize.get(i), streamName.get(i),
							(double)streamSize.get(i)/totalCompressedSize*100);
				} else {
					infoWriter.printf("Stream %1$02d: %2$10dB (%4$5.2f%%) \n", i+1, 
							streamSize.get(i),
							(double)streamSize.get(i)/totalCompressedSize*100);
				}
			}
			infoWriter.printf("Total size: %1$dB\n", totalCompressedSize);
			infoWriter.printf("*\n");
			infoWriter.printf("Header size: %1$6dB\n", headerSize);
			infoWriter.printf("Profile size: %1$6dB\n", profileSize);
			infoWriter.printf("Index size: %1$6dB\n", indexSize);
			infoWriter.printf("File struct size: %1$6dB\n",outputFileSize-headerSize-profileSize-indexSize-totalCompressedSize);
			infoWriter.println("================================================================");
		}
		
		if (!compressionTime.isEmpty() && userCompressionTime != 0) {
			infoWriter.println("Total compression time per processor (min:sec):");
			for(int i=0; i < compressionTime.size(); ++i) {
				long time = compressionTime.get(i);
				if (processorName.size() <= i) {
					infoWriter.printf("Proc %1$02d enc: %4$7.0fms %2$7s (%3$5.2f%%)\n",
							i+1, StatUtil.createTimeStringNano(time, false),
							(double)time/totalProcessorCompressionTime*100, 
							(double)time/1000000);
				} else {
					infoWriter.printf("Proc %1$02d enc: %5$7.0fms %2$7s (%4$5.2f%%) (%3$s)\n",
							i+1, StatUtil.createTimeStringNano(time, false), processorName.get(i),
							(double)time/totalProcessorCompressionTime*100, 
							(double)time/1000000);
				}
			}
			infoWriter.println("================================================================");
		}
		
		
		if (!decompressionTime.isEmpty() && userDecompressionTime != 0) {
			infoWriter.println("Total decompression time per processor (min:sec):");
			for(int i=0; i < decompressionTime.size(); ++i) {
				long time = decompressionTime.get(i);
				if (processorName.size() <= i) {
					infoWriter.printf("Proc %1$02d dec: $3$7.0fms %2$7s (%4$5.2f%%)\n",
							i+1, StatUtil.createTimeStringNano(time, false), 
							(double)time/1000000, 
							(double)time/totalProcessorDecompressionTime*100);
				} else {
					infoWriter.printf("Proc %1$02d dec: %5$7.0fms %2$7s (%4$5.2f%%) (%3$s)\n",
							i+1, StatUtil.createTimeStringNano(time, false), processorName.get(i),
							(double)time/totalProcessorDecompressionTime*100, 
							(double)time/1000000);
				}
			}
			infoWriter.println("================================================================");
		}
		
		
		
		return new String(resultStream.toByteArray(), 0, resultStream.length());
	}
}
