package samzip.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class holding preferences in (key, value) format.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class Preferences
{
	private Map<String, String> opts = new HashMap<>();
	
	public void clear()
	{
		opts.clear();
	}
	
	public Set<String> keySet()
	{
		return opts.keySet();
	}
	
	public String[] getKeys()
	{
		String[] data = new String[0];
		return opts.keySet().toArray(data);
	}
	
	public String getString(String key, String defaultValue)
	{
		if (opts.containsKey(key)) {
			return opts.get(key);
		} else
			return defaultValue;
	}
	
	public int getInteger(String key, int defaultValue)
	{
		if (opts.containsKey(key)) {
			int result;
			try {
				 result = Integer.decode(opts.get(key));
			} catch(NumberFormatException e) {
				result = defaultValue;
			}
			return result;
		} else
			return defaultValue;
	}
	
	public long getLong(String key, long defaultValue)
	{
		if (opts.containsKey(key)) {
			long result;
			try {
				result = Long.decode(opts.get(key));
			} catch (NumberFormatException e) {
				result = defaultValue;
			}
			return result;
		} else
			return defaultValue;
	}
	
	public double getDouble(String key, double defaultValue)
	{
		if (opts.containsKey(key)) {
			double result;
			try {
				result = Double.parseDouble(opts.get(key));
			} catch (NumberFormatException e) {
				result = defaultValue;
			}
			return result;
		} else
			return defaultValue;
	}
	
	public boolean getBoolean(String key, boolean defaultValue)
	{
		if (opts.containsKey(key)) {
			return Boolean.parseBoolean(opts.get(key));
		} else 
			return defaultValue;
	}
	
	public void setString(String key, String value)
	{
		opts.put(key, value);
	}
	
	public void setInteger(String key, int value)
	{
		opts.put(key, String.valueOf(value));
	}
	
	public void setLong(String key, long value)
	{
		opts.put(key, String.valueOf(value));
	}
	
	public void setDouble(String key, double value)
	{
		opts.put(key, String.valueOf(value));
	}

	public boolean contains(String key)
	{
		return opts.containsKey(key);
	}
}