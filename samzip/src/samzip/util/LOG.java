package samzip.util;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/**
 * Exception logging.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class LOG
{
	public final static int CAPACITY = 5000;
	private static int curUsedCapacity = 0;
	public final static String LOG_FILE_NAME = "exception";
	public static int curLogFile = 1;
	private static PrintWriter output = createPrintWriter();
	private final static Formatter format = new SimpleFormatter();

	private static PrintWriter createPrintWriter()
	{	
		try {
			if (curLogFile == 1) {
				curLogFile = 0;
				return new PrintWriter(new BufferedOutputStream(new FileOutputStream(LOG_FILE_NAME+".log")),true);
			}
			else {
				curLogFile = 1;
				return new PrintWriter(new BufferedOutputStream(new FileOutputStream(LOG_FILE_NAME+"."+curLogFile+".log")),true);
			}
		} catch (FileNotFoundException ex) {
			return null;
		}
	}
			
	public static class ConsoleFormatter extends Formatter {
		public synchronized String format(LogRecord record) 
		{
			String message = formatMessage(record);
			return String.format("%1$s %2$s", record.getLevel(), message);
		}
	}

	private static void doLog(LogRecord rec)
	{
		if (output == null)
			output = createPrintWriter();
		output.println(format.format(rec));
//		output.flush();
		++curUsedCapacity;
			if (curUsedCapacity > CAPACITY) {
				output.close();
				output = null;
				curUsedCapacity = 0;
			}
	}
	
	public synchronized static void log(Level level, String msg)
	{
		LogRecord rec =new LogRecord(level, msg);
		doLog(rec);
	}
	
	public synchronized static void log(Level level, String msg, Throwable ex)
	{
		LogRecord rec =new LogRecord(level, msg);
		rec.setThrown(ex);
		doLog(rec);
	}
	
	public synchronized static void info(String msg)
	{
		LogRecord rec =new LogRecord(Level.INFO, msg);
		doLog(rec);
	}
	
	public synchronized static void warning(String msg)
	{
		LogRecord rec =new LogRecord(Level.WARNING, getCallerInfo()+msg);
		doLog(rec);		
	}
	
	public synchronized static String getCallerInfo()
	{
		StackTraceElement caller = new Throwable().getStackTrace()[2];
		
		return "["+caller.getClassName()+"."+caller.getMethodName()+"()//line: "+caller.getLineNumber()+"]\n";
	}
	
	public synchronized static void severe(String msg)
	{
		LogRecord rec =new LogRecord(Level.SEVERE, getCallerInfo()+msg);
		doLog(rec);	
	}
	
	public static void main(String[] args)
	{
		LOG.warning("nieco");
		LOG.warning("nieco2");
		LOG.warning("nieco3");
		LOG.warning("nieco4");
		LOG.warning("nieco");
		LOG.warning("nieco2");
		LOG.warning("nieco3");
		LOG.warning("nieco4");
		LOG.warning("nieco");
		LOG.warning("nieco2");
		LOG.warning("nieco3");
		LOG.warning("nieco4c");
		LOG.warning("nieco");
		LOG.warning("nieco2b");
		LOG.warning("nieco3b");
		LOG.warning("nieco4b");
		LOG.warning("niecoa");
		LOG.warning("nieco2a");
		LOG.warning("nieco3a");
		LOG.warning("nieco4a");
//		output.close();
	}
	
}
