package samzip.io;

import java.io.*;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import samzip.compression.numbercode.NumberCode;
import samzip.compression.numbercode.VByteCode;

/**
 * Implementation of the binary file. File is separated into blocks. 
 * Every block contains 1 or more streams. Blocks are in following format:
 * 
 * <ul>
 * <li>magic string - 2 bytes "BF"</li>
 * <li>stream count as VByte encoded number</li>
 * <li>(stream count)*(stream sizes) as VByte encoded numbers</li>
 * <li>streams data</li>
 * </ul>
 * 
 * Class uses RandomAccessFile for I/O operations.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BinaryBlockFile 
{
	private long mark = 0;
	
	/// block index fields - not used in current version
	private boolean hasBlockIndex = false;
	private List<Long> blockOffsets;
	private MappedByteBuffer blockIndex = null;
	private long blocksCount;
	
	private RandomAccessFile file;
	private String filename;
	private byte[] magic = {(int)'B', (int)'F'};
	
	public BinaryBlockFile(String filename) throws FileNotFoundException
	{
		this(filename, "rw", false);
	}
	
	/** Constructs new binary block file.
	 * 
	 * @param filename path to input/output file
	 * @param mode operation mode (same as mode in {@link RandomAccessFile})
	 * @throws FileNotFoundException 
	 */
	public BinaryBlockFile(String filename, String mode) throws FileNotFoundException
	{
		this(filename, mode, false);
	}
	
	public BinaryBlockFile(String filename, String mode, boolean hasBlockIndex) throws FileNotFoundException
	{
		file = new RandomAccessFile(filename, mode);
		this.filename = filename;
		this.hasBlockIndex = hasBlockIndex;
		blockOffsets = new ArrayList<>();
	}
	
	/** Writes {@code block} to current position in RandomAccessFile. */
	public void writeBlock(OutputBlock block) throws IOException
	{
		if (hasBlockIndex) {
			blockOffsets.add(file.getFilePointer());
		}
		
		file.write(magic);
		BufferedOutputStream outputStream = new BufferedOutputStream(
				                                new FileOutputStream(file.getFD()));
		block.writeBlock(outputStream);
//		block.writeBlock(file);
	}

	/** Creates {@link InputBlock} instance from input stream. */
	public static InputBlock readBlock(InputStream inputStream)
	{
		try {					
			return new InputBlock(inputStream);
		} catch (IOException ex) {
			return null;
		}
	}

	/** @return {@link InputBlock} instance that starts at current position in file. */
	public InputBlock readBlock() throws IOException
	{
		return readBlock(file.getFilePointer());
	}
		
	/** @return  {@link InputBlock} instance that starts at offset {@code offset} in file. */
	public InputBlock readBlock(long offset) throws IOException
	{
//		try {
			file.seek(offset);
		
			/// read header
			byte[] mag = new byte[magic.length];
			if (file.read(mag) < 0 || !Arrays.equals(magic, mag)) {
				//return null;
				throw new IOException("Input file has invalid format (magic string mismatch)");
			}
			
			FileInputStream headerStream = new FileInputStream(file.getFD());
			return new InputBlock(headerStream);
//		} catch (IOException ex) {
//			return null;
//		}
	}
	
	/** 
	 * Works only when block index is present. Don't confuse this with SamZip index. 
	 * Block index only contains array of block offsets. So it's useful for 
	 * access to block with specified number. Currently this index isn't used anymore.
	 * @return  {@link InputBlock} instance that starts at offset {@code offset} in file. 
	 */
	public InputBlock readBlockNo(int blockIdx) throws IOException
	{
		if (!hasBlockIndex)
			return null;
		
		if (blockIndex == null) {
			/// first use of index
			file.seek(file.length()-8);
			long blockIndexOffset = file.readLong();
			long size = file.length()-8-blockIndexOffset;
			blocksCount = size >> 3;
			blockIndex = new RandomAccessFile(filename, "r").getChannel().map(FileChannel.MapMode.READ_ONLY, blockIndexOffset, size);
			file.seek(0);
		}
		if (blockIdx >= blocksCount)
			return null;
		
		return readBlock(blockIndex.getLong(blockIdx << 3));
	}
	
	/** Create new empty instance of {@link OutputBlock}.*/
	public OutputBlock newBlock()
	{
		return new OutputBlock();
	}

	/** @return current offset in file as long. */
	public long getFilePointer() throws IOException
	{
		return file.getFilePointer();
	}
	
	/** Seek to specified offset. */
	public void seek(long offset) throws IOException
	{
		file.seek(offset);
	}
	
	/** Mark position. */
	public void mark() throws IOException
	{
		this.mark = file.getFilePointer();
	}
	
	/** Return to marked position. See: {@see mark()}.*/
	public void reset() throws IOException
	{
		file.seek(mark);
	}
	
	/** @return inner {@link RandomAccessFile} instance */
	public RandomAccessFile getRandomAccessFile()
	{
		return file;
	}

	/** Close file */
	public void close() throws IOException
	{
		if (hasBlockIndex) {
			long blockIndexOffset = file.getFilePointer();
			for(long off : blockOffsets) {
				file.writeLong(off);
			}
			file.writeLong(blockIndexOffset);
		}
		
		file.close();
	}
	
	/** Represents output stream which data are accessible in memory. */
	public static class MemoryStream
	{
		private FastByteArrayOutputStream output;
		private int growFactor = 1;
		private String id = "";
		
		public MemoryStream(int initialCapacity)
		{
			output = new FastByteArrayOutputStream(initialCapacity, growFactor);
		}

		public byte[] getData()
		{
			return output.toByteArray();
		}

		public int getLength()
		{
			return output.length();
		}

		public OutputStream getOutputStream()
		{
			return output;
		}

		public OutputStream getOutputStream(int offset) throws IOException
		{
			output.position(offset);
			return output;
		}

		public void close() throws IOException
		{
			output.close();
		}

		public String getId()
		{
			return id;
		}

		public void setId(String id)
		{
			this.id = id;
		}
	}
	
	/** Class that represent single output block. Contains 0 or more streams. */
	public static final class OutputBlock 
	{			
		private List<MemoryStream> streams = new ArrayList<>();

		public static int blockNO = 0;
		public int thisBlockNo;
		
		public OutputBlock()
		{
			thisBlockNo = blockNO++;
		}
		
		/** Creates new stream with specified id and initial capacity. */
		public OutputStream newStream(String id, int initialCapacity)
		{
			MemoryStream stream = new MemoryStream(initialCapacity);
			stream.setId(id);
			streams.add(stream);
			
			return stream.getOutputStream();
		}	
		
		/** Creates new stream with empty id and default initial capacity (1024 B). */
		public OutputStream newStream()
		{
			return newStream("", 1024);
		}
		
		/** Creates new stream with empty id and specified initial capacity. */
		public OutputStream newStream(int initialCapacity) 
		{
			return newStream("", initialCapacity);
		}
		
		/** Creates new stream with specified id and default initial capacity (1024 B). */
		public OutputStream newStream(String id)
		{
			return newStream(id, 1024);
		}
		
		/** Returns stream with specified index. */
		public OutputStream getStream(int index)
		{
			if (index >= streams.size() || index < 0) 
				throw new IllegalArgumentException("Stream index "+index+" is not valid.");
			
			return streams.get(index).getOutputStream();
		}
		
//		public OutputStream getStream(int index, int atPos) throws IOException
//		{
//			if (index >= streams.size() || index < 0) 
//				throw new IllegalArgumentException("Stream index "+index+" is not valid.");
//			
//			return streams.get(index).getOutputStream(atPos);
//		}
		
		/** @return stream with index {@code index} length in bytes */
		public int getStreamLength(int index)
		{
			if (index >= streams.size() || index < 0) 
				throw new IllegalArgumentException("Stream index "+index+" is not valid.");
			
			return streams.get(index).getLength();
		}
		
		public List<MemoryStream> getStreams()
		{
			return streams;
		}
		
		/** Close all streams in block and write them to specified output stream. */
		public void writeBlock(OutputStream outputStream) throws IOException
		{
//			System.out.println("writing block no: "+thisBlockNo);
//			System.out.flush();
			close();
			
			int[] headerNums = new int[streams.size()+1];
			headerNums[0] = streams.size();

			int i=1;
			for(MemoryStream s : streams) {
				headerNums[i++] = s.getLength();
			}
			
			MemoryStream headerStream = new MemoryStream(512);
			OutputStream header = headerStream.getOutputStream();
					
			for(i=0; i < headerNums.length; ++i) {
				VByteCode.encodeUnsigned(headerNums[i], header);
			}

			if (flags != null) {
				VByteCode.encodeUnsigned(flagsStream.getLength(), header);
				header.write(flagsStream.getData(), 0, flagsStream.getLength());
			} else
				VByteCode.encodeUnsigned(0, header);
			
			header.close();
			
			outputStream.write(headerStream.getData(), 0, headerStream.getLength());

			for(MemoryStream s : streams) {
				outputStream.write(s.getData(), 0, s.getLength());
			}
			
			outputStream.flush();
		}	
		
		/** Close streams */
		public void close() throws IOException
		{
			if (flags != null)
				flags.close();
			
			for(MemoryStream s : streams) {
				s.close();
			}
		}
		
		private BitOutputStream flags;
		private MemoryStream flagsStream;
		
		/** Append binary flag to block header. */
		public void writeFlag(boolean flag) throws IOException
		{
			if (flags == null) {
				flagsStream = new MemoryStream(8);
				flags = new BitOutputStream(flagsStream.getOutputStream());
			}
				
			flags.writeBit(flag ? 1 : 0);
		}
	}
	
	/** Class representing single input block. */
	public static final class InputBlock
	{
		private List<InputStream> streams = new ArrayList<>();
		private int currentStream = 0;
		
		private BitInputStream flags;
		
		/** Read block's flag (set in {@link OutputBlock#writeFlag(boolean)}). */
		public boolean readFlag() throws IOException
		{
			if (flags == null)
				return false;
			
			return flags.readBit() == 1 ? true : false;
		}
		
		/** Create new input block from input stream. */
		public InputBlock(InputStream input) throws IOException
		{
			NumberCode numberCode = new VByteCode(input);
			numberCode.setSigned(false);
			int n = numberCode.decode();
			int[] streamLen = new int[n];
			byte[] buf;
			int res;
			for(int i=0; i < n; ++i) {
				streamLen[i] = numberCode.decode();
			}
			
			n = numberCode.decode();
			if (n != 0) {
				buf = new byte[n];
				res = input.read(buf, 0, n);
				flags = new BitInputStream(new ByteArrayInputStream(buf, 0, res));
			}
			
			for(int i=0; i < streamLen.length; ++i) {
				buf = new byte[streamLen[i]];
				res = input.read(buf);
				streams.add(new ByteArrayInputStream(buf,0,res));
			}
		}
		
		/** @return stream at index {@code index}. */
		public InputStream getStream(int index)
		{
			if (index >= streams.size() || index < 0) 
				throw new IllegalArgumentException("Stream index "+index+" is not valid.");
			
			return streams.get(index);
		}		
		
		/** @return stream from next index and increment this index or {@code null} when index > |streams|. */
		public InputStream getStream()
		{
			if (available() > 0)
				return getStream(currentStream++);
			else
				return null;
		}
		
		/** @return number of streams still available in this input block. */
		public int available()
		{
			int res = streams.size() - currentStream;
			return res < 0 ? 0 : res;
		}
		
		/** Create outputBlock from this input block */
		public OutputBlock createOutputBlock() throws IOException
		{
			OutputBlock block = new OutputBlock();
			
			for(InputStream str : streams) {
				StreamUtils.copyStream(str, block.newStream());
			}
			return block;
		}
		
//		public static InputBlock readBlock(InputStream inputStream) throws IOException
//		{
////			int M = StreamUtils.readUnsignedByte(inputStream);
////			RiceCode numberCode = new RiceCode(inputStream, 1 << M);
//
//			NumberCode numberCode = new VByteCode(inputStream);
//			numberCode.setSigned(false);
//			int n = numberCode.decode();
//			int[] streamsLen = new int[n];
//			for(int i=0; i < n; ++i) {
//				streamsLen[i] = numberCode.decode();
//			}
//			
//			return new InputBlock(inputStream, streamsLen);
//		}
//		
		/** Return streams count */
		public int streamsCount()
		{
			return streams.size();
		}
	}
}