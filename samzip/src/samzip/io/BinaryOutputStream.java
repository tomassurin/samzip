package samzip.io;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

/** 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BinaryOutputStream extends OutputStream
{
	private final Charset charset;
	private final Endian endian;
	private OutputStream output;
	private long bytesWritten;

	public BinaryOutputStream(OutputStream output, Endian endian)
	{
		this(output, endian, Charset.forName("UTF-8"));
	}
	
	public BinaryOutputStream(OutputStream output)
	{
		this(output, Endian.BIG_ENDIAN, Charset.forName("UTF-8"));
	}
	
	public BinaryOutputStream(OutputStream output, Endian endian, Charset charset)
	{
		this.output = output;
		this.endian = endian;
		bytesWritten = 0;
		this.charset = charset;
	}
	
	public Endian getEndian()
	{
		return endian;
	}
	
	public Charset getCharset()
	{
		return charset;
	}
	
	@Override
	public void flush() throws IOException
	{
		output.flush();
	}
	
	/** Closes output stream. */
	@Override
	public void close() throws IOException
	{
		output.close();
	}
	
	@Override
	public final void write(byte[] b, int start, int len) throws IOException
	{
		output.write(b, start, len);
		bytesWritten += len;
	}
	
	@Override
	public final void write(byte[] b) throws IOException
	{
		write(b, 0, b.length);
	}

	@Override
	public final void write(int b) throws IOException
	{
		writeByte(b);
	}
	
	/** Writes specified byte to the stream.
	 * 
	 * @param b The BYTE to be written.
	 * @throws IOException 
	 */
	public final void writeByte(int b) throws IOException
	{
		output.write(b & 0xff);
		++bytesWritten;
	}
	
	/** Writes specified WORD (16bits) to the stream.
	 * 
	 * @param w The WORD to be written.
	 * @throws IOException 
	 */
	public final void writeShort(int w) throws IOException
	{
		if (endian == Endian.BIG_ENDIAN)
		{
			output.write((w & 0xff00) >> 8);
			output.write(w & 0xff);
		} else {
			output.write(w & 0xff);
			output.write((w & 0xff00) >> 8);
		}
		bytesWritten += 2;
	}
	
	/** Writes specified DWORD (32bits) to the stream.
	 * 
	 * @param d The DWORD to be written.
	 * @throws IOException 
	 */
	public final void writeInt(long d) throws IOException
	{
		if (endian == Endian.BIG_ENDIAN) 
		{
			output.write((int) (d &  0xff000000) >> 24);
			output.write((int) (d & 0x00ff0000) >> 16);
			output.write((int) (d & 0x0000ff00) >> 8);
			output.write((int) (d & 0x000000ff));
		} else {
			output.write((int) (d & 0x000000ff));
			output.write((int) (d & 0x0000ff00) >> 8);
			output.write((int) (d & 0x00ff0000) >> 16);
			output.write((int) (d &  0xff000000) >> 24);
		}
		bytesWritten += 4;
	}
		
	/** Write string {@code s} prefixed with it's length (number of bytes of string data)
	 * in single byte. */
	public final void writeLengthPrefixString(String s) throws IOException
	{
		byte[] data = s.getBytes(charset);
		writeByte(data.length);
		for(int i=0; i < data.length; ++i) {
			writeByte(data[i]);
		}
	}
	
	/** Write string {@code s} which is null  terminated. */
	public final void writeNullTerminatedString(String s) throws IOException
	{
		byte[] data = s.getBytes(charset);
		for(int i=0; i < data.length; ++i) {
			writeByte(data[i]);
		}
		writeByte((byte)0);
	}
	
	public final void writeFloat(float v) throws IOException {
        writeInt(Float.floatToIntBits(v));
    }
	
	/** Aligns stream to specified byte boundary. For example if {@code a} 
	 * is 4 then stream is aligned to the next 4 byte boundary. */
	public final void align(int a) throws IOException
	{
		if ((bytesWritten % a) > 0) 
		{
			int count = (int) (a - (bytesWritten % a));
			for(;count > 0; --count) {
				output.write(0);
			}
		}
	}
}
