package samzip.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Flags vector with static size. Not thread safe!
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class FlagsVector
{
	private byte[] flags;
	private short size;
	
	/** Constructs flag vector with capacity {@code sz} bytes ({@code sz*8} flags). */
	public FlagsVector(int sz)
	{
		flags = new byte[sz];
		this.size = (short) sz;
	}
	
	/** Find if flag at position {@code index} is set. */
	public boolean get(int index)
	{
		int bucket = index >> 3; // equivalent to floor(index / 8)
		int offset = index & 7; // equivalent to index % 8
		
		if (bucket >= size)
			return false;
		
		return ((flags[bucket] >> (7-offset)) & 1) != 0;
	}
	
	/** Set flag at position {@code index} to value {@code value}. */
	public void set(int index, boolean value)
	{
		int bucket = index >> 3;
		int offset = index & 7;
		
		if (bucket >= size)
			return;
		
		if (value) {
			flags[bucket] |= 128 >> offset;
		} else {
			flags[bucket] &= ~(128 >> offset);
		}
	}
	
	/** Return internal byte array containing flags data. */
	public byte[] toByteArray()
	{
		return flags;
	}
	
	public void writeToStream(OutputStream output) throws IOException
	{
		output.write(flags);
	}
}
