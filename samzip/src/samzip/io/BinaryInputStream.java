package samzip.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/** 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BinaryInputStream extends InputStream
{	
	private InputStream input;
	private final Charset charset;
	private final Endian endian;

	public BinaryInputStream(InputStream input, Endian endian)
	{
		this(input, endian, Charset.forName("UTF-8"));
	}
	
	public BinaryInputStream(InputStream input)
	{
		this(input, Endian.BIG_ENDIAN, Charset.forName("UTF-8"));
	}
	
	public BinaryInputStream(InputStream input, Endian endian, Charset charset)
	{
		this.input = input;
		this.endian = endian;
		this.charset = charset;
	}
	
	/** Returns used endian encoding. */
	public Endian getEndian()
	{
		return endian;
	}
	
	/** Returns charset which is being used for string decoding. */
	public Charset getCharset()
	{
		return charset;
	}
	
	/** Closes output stream. */
	@Override
	public void close() throws IOException
	{
		input.close();
	}

	/** Reads next byte (unsigned) from stream. */
	@Override
	public int read() throws IOException
	{
		return input.read();
	}

	@Override
	public int available() throws IOException
	{
		return input.available();
	}
	
	/** Reads next byte (signed) of data from stream.
	 * @throws IOException 
	 */
	public byte readByte() throws IOException
	{
		return (byte) input.read();
	}
	
	/** Reads next unsigned byte from stream. */
	public short readUnsignedByte() throws IOException
	{
		return (short) (input.read() & 0xff);
	}
	
	/** Reads next signed WORD from stream. */
	public short readShort() throws IOException
	{
		int result = readUnsignedShort();
		
		/// signed handling
		if ((result & 0x8000) == 0x8000)
			result = -(0x10000 - result);
		
		return (short) result;
	}
	
	/** Reads next unsigned WORD from stream. */
	public int readUnsignedShort() throws IOException
	{
		short a, b;
		int result;
		
		a = readUnsignedByte();
		b = readUnsignedByte();
		
		if (endian == Endian.BIG_ENDIAN) {
			result = ((a << 8) | b);
		} else {
			result = (a | (b << 8));
		}
		
		return result;
	}
	
	/** Reads next unsigned DWORD (32bits) from stream. */
	public long readUnsignedInt() throws IOException
	{
		short a, b, c, d;
		long result;
		
		a = readUnsignedByte();
		b = readUnsignedByte();
		c = readUnsignedByte();
		d = readUnsignedByte();

		if (endian == Endian.BIG_ENDIAN) {
			result = ((a << 24) | (b << 16) | (c << 8) | d);
		} else {
			result = (a | (b << 8) | (c << 16) | (d << 24));
		}
		
		return result;
	}
	
	/** Reads next signed DWORD (32bits) from stream. */
	public int readInt() throws IOException
	{
		long result = readUnsignedInt();
		
		if ((result & 0x80000000L) == 0x80000000L)
			result = -(0x100000000L - result);
		
		return (int) result;
	}
	
	/** Reads string prefixed with it's length (number of bytes of string data). */
	public String readLengthPrefixString() throws IOException
	{
		short len = readUnsignedByte();
		byte[] data = new byte[len];
		for(int i=0; i < len; ++i) {
			data[i] = (byte) input.read();
		}
		
		return new String(data, charset);
	}
	
	public final float readFloat() throws IOException {
        return Float.intBitsToFloat(readInt());
    }
	
	/** Reads null terminated string. */
	public String readNullTerminatedString() throws IOException
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream(256);
		
		int b = input.read();
		while(b != 0) {
			out.write(b);
			b = input.read();
		}
		return new String(out.toByteArray(), charset);
	}
}
