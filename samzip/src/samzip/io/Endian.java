package samzip.io;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public enum Endian
{
	BIG_ENDIAN, LITTLE_ENDIAN
}