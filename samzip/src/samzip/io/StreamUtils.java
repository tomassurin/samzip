package samzip.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;

/**
 * Misc utilities for stream input/output. Also contains
 * routines for unsigned integer types handling.
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class StreamUtils
{
	// everything is in big endian
		
	public static short getUnsignedByte(int b)
	{
		return (short) (b & 0xff);
	}
	
	public static short getShort(byte[] s)
	{
		int result = getUnsignedShort(s);
		
		if ((result & 0x8000) == 0x8000)
			result = -(0x10000 - result);
		
		return (short) result;
	}
	
	public static int getUnsignedShort(byte[] s)
	{
		return ((s[0] & 0xff) << 8) | (s[1] & 0xff);
	}
	
	public static int getInt(byte[] i)
	{
		long result = getUnsignedInt(i);
		
		if ((result & 0x80000000L) == 0x80000000L)
			result = -(0x100000000L - result);
		
		return (int) result;
	}
	
	public static long getUnsignedInt(byte[] i)
	{
		return ((i[0] & 0xff) << 24) | ((i[1] & 0xff) << 16) | ((i[2] & 0xff) << 8) | (i[3] & 0xff);
	}
	
	public static long getLong(byte[] l)
	{
        return (((long) l[0] << 56) +
                ((long)(l[1] & 255) << 48) +
                ((long)(l[2] & 255) << 40) +
                ((long)(l[3] & 255) << 32) +
                ((long)(l[4] & 255) << 24) +
                ((l[5] & 255) << 16) +
                ((l[6] & 255) <<  8) +
                (l[7] & 255));
	}
	
	public static short readUnsignedByte(InputStream input) throws IOException
	{
		return (short) (input.read() & 0xff);
	}
	
	public static short readShort(InputStream input) throws IOException
	{
		int result = readUnsignedShort(input);
		
		if ((result & 0x8000) == 0x8000)
			result = -(0x10000 - result);
		
		return (short) result;
	}
	
	public static int readUnsignedShort(InputStream input) throws IOException
	{
		int a = readUnsignedByte(input);
		int b = readUnsignedByte(input);
		
		return (a << 8) | b;
	}
	
	public static int readInt(InputStream input) throws IOException
	{
		long result = readUnsignedInt(input);
		
		if ((result & 0x80000000L) == 0x80000000L)
			result = -(0x100000000L - result);
		
		return (int) result;
	}
	
	public static long readUnsignedInt(InputStream input) throws IOException
	{
		int a = readUnsignedByte(input);
		int b = readUnsignedByte(input);
		int c = readUnsignedByte(input);
		int d = readUnsignedByte(input);
		
		return (a << 24) | (b << 16) | (c << 8) | d;
	}
	
	public static long readLong(InputStream input) throws IOException
	{
		byte a = (byte) input.read();
		byte b = (byte) input.read();
		byte c = (byte) input.read();
		byte d = (byte) input.read();
		byte e = (byte) input.read();
		byte f = (byte) input.read();
		byte g = (byte) input.read();
		byte h = (byte) input.read();
        return (((long) a << 56) +
                ((long)(b & 255) << 48) +
                ((long)(c & 255) << 40) +
                ((long)(d & 255) << 32) +
                ((long)(e & 255) << 24) +
                ((f & 255) << 16) +
                ((g & 255) <<  8) +
                (h & 255));
	}
	
	public static BigInteger readUnsignedLong(InputStream input) throws IOException
	{
		BigInteger result = BigInteger.ZERO;
		byte a = (byte) input.read();
		byte b = (byte) input.read();
		byte c = (byte) input.read();
		byte d = (byte) input.read();
		byte e = (byte) input.read();
		byte f = (byte) input.read();
		byte g = (byte) input.read();
		byte h = (byte) input.read();
		result = result.add(BigInteger.valueOf(a << 56));
		result = result.add(BigInteger.valueOf((b & 255) << 48));
		result = result.add(BigInteger.valueOf((c & 255) << 40));
		result = result.add(BigInteger.valueOf((d & 255) << 32));
		result = result.add(BigInteger.valueOf((e & 255) << 24));
		result = result.add(BigInteger.valueOf((f & 255) << 16));
		result = result.add(BigInteger.valueOf((g & 255) << 8));
		result = result.add(BigInteger.valueOf(h & 255));
		return result;
	}
	
	public static void writeByte(int b, OutputStream output) throws IOException
	{
		output.write(b & 0xff);
	}
	
	public static void writeShort(short w, OutputStream output) throws IOException
	{
		output.write((w & 0xff00) >> 8);
		output.write(w & 0xff);
	}
	
	public static void writeUnsignedShort(int w, OutputStream output) throws IOException
	{
		output.write((w & 0xff00) >> 8);
		output.write(w & 0xff);
	}
		
	public static void writeInt(int d, OutputStream output) throws IOException
	{
		output.write((int) (d & 0xff000000) >> 24);
		output.write((int) (d & 0x00ff0000) >> 16);
		output.write((int) (d & 0x0000ff00) >> 8);
		output.write((int) (d & 0x000000ff));
	}
	
	public static void writeUnsignedInt(long d, OutputStream output) throws IOException
	{
		output.write((int) (d & 0xff000000) >> 24);
		output.write((int) (d & 0x00ff0000) >> 16);
		output.write((int) (d & 0x0000ff00) >> 8);
		output.write((int) (d & 0x000000ff));
	}
	
	/** untested !!! */
	public static void writeLong(long d, OutputStream output) throws IOException
	{
		output.write((int) (d & 0xff00000000000000L) >> 56);
		output.write((int) (d & 0x00ff000000000000L) >> 48);
		output.write((int) (d & 0x0000ff0000000000L) >> 40);
		output.write((int) (d & 0x000000ff00000000L) >> 32);
		output.write((int) (d & 0x00000000ff000000L) >> 24);
		output.write((int) (d & 0x0000000000ff0000L) >> 16);
		output.write((int) (d & 0x000000000000ff00L) >> 8);
		output.write((int) (d & 0x00000000000000ffL));
	}
	
	public static void copyStream(InputStream input, OutputStream output) throws IOException
	{
		byte[] buffer = new byte[32*1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer, 0, buffer.length)) > 0)
		{
			output.write(buffer, 0, bytesRead);
		}
	}
	
//	public static byte[] intArrayToByteArray(int[] data, int off, int len)
//	{
//		if (data == null) {
//            throw new NullPointerException();
//        } else if ((off < 0) || (off > data.length) || (len < 0) ||
//                   ((off + len) > data.length) || ((off + len) < 0)) {
//            throw new IndexOutOfBoundsException();
//        } else if (len == 0) {
//            return null;
//        }
//		
//		byte[] result = new byte[len];
//		
//		for(int i=0; i < len; ++i) {
//			result[i] = (byte) data[off+i];
//		}
//		
//		return result;
//	}
//	
//	public static int[] byteArrayToIntArray(byte[] data, int off, int len)
//	{
//		if (data == null) {
//            throw new NullPointerException();
//        } else if ((off < 0) || (off > data.length) || (len < 0) ||
//                   ((off + len) > data.length) || ((off + len) < 0)) {
//            throw new IndexOutOfBoundsException();
//        } else if (len == 0) {
//            return null;
//        }
//		
//		int[] result = new int[len];
//		
//		for(int i=0; i < len; ++i) {
//			result[i] = (int) data[off+i];
//		}
//		
//		return result;
//	}

	public static byte[] readStreamData(InputStream input) throws IOException
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream(1 << 16);
		byte[] buf = new byte[1024];
		int res;
		while ((res = input.read(buf)) > 0) {
			out.write(buf,0,res);
		}
		
		out.close();
		input.close();
		return out.toByteArray();
	}
}
