package samzip.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * Representation of bit input stream. Not thread safe!
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BitInputStream
{
	/// Underlying input stream.

	private InputStream input;
	/// Buffered byte from which bits are read.
	private int nextByte = 0;
	/// Offset of next bit in nextByte.
	private short nextBitIndex = -1;
	/// Set to true if all bits have been read.
	private boolean endOfStream = true;

	public BitInputStream(InputStream inputStream) throws IOException
	{
		input = inputStream;
		endOfStream = false;
	}

	public InputStream getInputStream()
	{
		return input;
	}
	
	/** Find if there are available data in the stream. */
	public boolean available() throws IOException
	{
		return input.available() > 0;
	}

	public void close() throws IOException
	{
		endOfStream = true;
		input.close();
	}

	/** Reads {@code n} bits from input stream.
	 * {@code n} should be {@code <=} 31 because return value is of int type.
	 *
	 * @param n Number of read bytes
	 * @return int value of n returned bits.
	 * @throws IOException
	 */
	public long readBit(final int n) throws IOException
	{
		long result = 0;
		int bit;
		for (int i = n - 1; i >= 0; i--) {
			bit = readBit();
			result |= (bit << i);
			if (endOfStream) {
				break;
			}
		}
		return result;
	}
	
	/**
	 * Reads {@code n} bits into array {@code buf}. buf[i] = i'th read bit (0 or 1).
	 * Array buf must have capacity >= {@code n}.
	 * 
	 * @return Number of read bits
	 * @throws IOException 
	 */
	public int readBit(final byte[] buf, final int n) throws IOException
	{
		int bit;
		int i;
		for (i = 0; i < n; ++i) {
			bit = readBit();
			buf[i] = (byte) bit;
			if (endOfStream) {
				break;
			}
		}
		return i;
	}
	
	/** Read the next bit from the input stream.
	 * 
	 * @return The bit value or -1 if end of stream was reached
	 * @throws IOException 
	 */
	public int readBit() throws IOException 
	{
		if (nextBitIndex < 0) {
			if (!readNextByte()) {
				return -1;
			}
		}
		return (nextByte >> nextBitIndex--) & 1;
    }
	
	private boolean readNextByte() throws IOException
	{
		if (endOfStream) {
			return false;
		}
		nextByte = input.read();
		if (nextByte == -1) {
			endOfStream = true;
			return false;
		}
		nextBitIndex = 7;
		return true;
	}
}
