package samzip.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * ByteArrayOutputStream implementation which supports seeking.
 * 
 * <b>Source: </b> Original JDK 1.7 ByteArrayOutputStream source.
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class FastByteArrayOutputStream extends OutputStream
{
	/** The buffer where data is stored. */
    private byte[] buf;
	final protected int growFactor;
	
    /** The number of valid bytes in the buffer. */
    private int length;
	protected int pos;

    /** 
	 * Creates a new byte array output stream. The buffer capacity is
     * initially 32 bytes, though its length increases if necessary. 
	 */
    public FastByteArrayOutputStream() 
	{
        this(32,1);
    }

	public FastByteArrayOutputStream(final int size)
	{
		this(size, 1);
	}
	
    /**
     * Creates a new byte array output stream, with a buffer capacity of
     * the specified length, in bytes.
     *
     * @param capacity   the initial capacity.
	 * @param growFactor how much to grow after buffer is full (in power of 2)
	 * 
     * @exception  IllegalArgumentException if length is negative.
     */
    public FastByteArrayOutputStream(int capacity, int growFactor) 
	{
        if (capacity < 0) {
            throw new IllegalArgumentException("Negative initial size: "
                                               + capacity);
        }
		
		if (growFactor < 0) {
			throw new IllegalArgumentException("Negative grow factor: "+growFactor);
		}
		
		this.growFactor = growFactor;
		
        buf = new byte[capacity];
    }
	
	public FastByteArrayOutputStream(final byte[] data, int growFactor)
	{		
		if (growFactor < 0) {
			throw new IllegalArgumentException("Negative grow factor: "+growFactor);
		}
		
		this.growFactor = growFactor;
		
		buf = data;
	}

    /**
     * Writes the specified byte to this byte array output stream.
     *
     * @param   b   the byte to be written.
     */
	@Override
    public void write(final int b) 
	{
        ensureCapacity(pos + 1);
        buf[pos] = (byte) b;
		++pos;
        if (pos > length) {
			length = pos;
		}
    }

	@Override
	public void write(final byte b[])
	{
		write(b, 0, b.length);
	}
	
	/**
     * Writes <code>len</code> bytes from the specified byte array
     * starting at offset <code>off</code> to this byte array output stream.
     *
     * @param   b     the data.
     * @param   off   the start offset in the data.
     * @param   len   the number of bytes to write.
     */
	@Override
    public void write(final byte b[], final int off, final int len) 
	{
        if ((off < 0) || (off > b.length) || (len < 0) ||
            ((off + len) - b.length > 0)) {
            throw new IndexOutOfBoundsException();
        }
        ensureCapacity(pos + len);
        System.arraycopy(b, off, buf, pos, len);
		pos += len;
		if (pos > length) {
			length = pos;
		}
    }
	
	/**
     * Writes the complete contents of this byte array output stream to
     * the specified output stream argument, as if by calling the output
     * stream's write method using <code>out.write(buf, 0, length)</code>.
     *
     * @param      out   the output stream to which to write the data.
     * @exception  IOException  if an I/O error occurs.
     */
    public void writeTo(OutputStream out) throws IOException 
	{
        out.write(buf, 0, length);
    }
	
	/** Set buffer's writing position. If {@code pos > length()} is specified 
	 * next writing operation will extend written data up to {@code pos} with zeros.
	 * 
	 * @param offset the offset in stream which is used by next write operation for writing
	 */
	public void position(int offset) throws IOException
	{
		if (offset < 0)
			throw new IOException("Stream position can't be smaller than 0.");
			
		ensureCapacity(offset+1);
		pos = offset;
	}

	/** Gets buffer's writing position. */
	public int position()
	{
		return pos;
	}
	
    /**
     * Resets the <code>length</code> field of this byte array output
     * stream to zero, so that all currently accumulated output in the
     * output stream is discarded. Current position is set to zero.
	 * The output stream can be used again, reusing the already allocated buffer space.
     *
     * @see     length
	 * @see		position
     */
    public void reset() 
	{
        length = 0;
		pos = 0;
    }

    /**
	 * Returns internal byte array. Its length can be found with call to {@code length()}.
     * 
	 * <p><b>Do not modify!</b></p>
	 * 
     * @return  the current contents of this output stream, as a byte array.
     * @see     length()
     */
    public byte toByteArray()[] 
	{
        return buf;
    }
	
    /**
     * Returns the current length of the buffer.
     *
     * @return  the value of the <code>length</code> field, which is the number
     *          of valid bytes in this output stream.
     * @see     java.io.ByteArrayOutputStream#length
     */
    public int length() 
	{
        return length;
    }

	public int capacity()
	{
		return buf.length;
	}

    /**
     * Closing a <tt>ByteArrayOutputStream</tt> has no effect. The methods in
     * this class can be called after the stream has been closed without
     * generating an <tt>IOException</tt>.
     * <p>
     *
     */
	@Override
    public void close() throws IOException 
	{
    }
	
	/**
     * Converts the buffer's contents into a string decoding bytes using the
     * platform's default character set. The length of the new <tt>String</tt>
     * is a function of the character set, and hence may not be equal to the
     * length of the buffer.
     *
     * <p> This method always replaces malformed-input and unmappable-character
     * sequences with the default replacement string for the platform's
     * default character set. The {@linkplain java.nio.charset.CharsetDecoder}
     * class should be used when more control over the decoding process is
     * required.
     *
     * @return String decoded from the buffer's contents.
     * @since  JDK1.1
     */
	@Override
    public String toString() 
	{
        return new String(buf, 0, length);
    }

    /**
     * Converts the buffer's contents into a string by decoding the bytes using
     * the specified {@link java.nio.charset.Charset charsetName}. The length of
     * the new <tt>String</tt> is a function of the charset, and hence may not be
     * equal to the length of the byte array.
     *
     * <p> This method always replaces malformed-input and unmappable-character
     * sequences with this charset's default replacement string. The {@link
     * java.nio.charset.CharsetDecoder} class should be used when more control
     * over the decoding process is required.
     *
     * @param  charsetName  the name of a supported
     *              {@linkplain java.nio.charset.Charset </code>charset<code>}
     * @return String decoded from the buffer's contents.
     * @exception  UnsupportedEncodingException
     *             If the named charset is not supported
     * @since   JDK1.1
     */
    public String toString(String charsetName)
        throws UnsupportedEncodingException
    {
        return new String(buf, 0, length, charsetName);
    }
	
	/**
     * Increases the capacity if necessary to ensure that it can hold
     * at least the number of elements specified by the minimum
     * capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     * @throws OutOfMemoryError if {@code minCapacity < 0}.  This is
     * interpreted as a request for the unsatisfiably large capacity
     * {@code (long) Integer.MAX_VALUE + (minCapacity - Integer.MAX_VALUE)}.
     */
    private void ensureCapacity(int minCapacity) 
	{
        // overflow-conscious code
        if (minCapacity - buf.length > 0)
            grow(minCapacity);
    }

    /**
     * Increases the capacity to ensure that it can hold at least the
     * number of elements specified by the minimum capacity argument.
     *
     * @param minCapacity the desired minimum capacity
     */
    private void grow(int minCapacity) 
	{
        // overflow-conscious code
        int oldCapacity = buf.length;
        int newCapacity = oldCapacity << growFactor;
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        if (newCapacity < 0) {
            if (minCapacity < 0) // overflow
                throw new OutOfMemoryError();
            newCapacity = Integer.MAX_VALUE;
        }
        buf = Arrays.copyOf(buf, newCapacity);
    }
}
