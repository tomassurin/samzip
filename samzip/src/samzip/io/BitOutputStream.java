package samzip.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Implementation of bit output stream. Not thread safe!
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class BitOutputStream
{
	/// Underlying output stream.
	private OutputStream output;
	/// Buffered byte for output.
	private int nextByte;
	/// Index of the next bit in nextByte to be written into.
	private int nextBitIndex;
	
	public BitOutputStream(OutputStream outputStream)
	{
		output = outputStream;
		reset();
	}
	
	public OutputStream getOutputStream()
	{
		return output;
	}
	
	/** Writes specified bit (0 or 1) to the output stream.
	 * @param bit Bit to be written.
	 * @throws IOException 
	 */
	public void writeBit(final int bit) throws IOException
	{
		if (nextBitIndex == 0) {
			if (bit == 0) {
				output.write(nextByte);
			} else {
				output.write(nextByte+1);
			}
			reset();
		} else {
			if (bit == 0) {
				nextByte <<= 1;
			} else {
				nextByte = (nextByte + 1) << 1;
			}
			--nextBitIndex;
		}
	}
	
	/** Write {@code n} bits from int value to the stream. The bits are written starting 
	 * at highest bit, going down to the lowest bit.
	 * 
	 * @param bits int value containing bits that will be written to the stream.
	 * @param n how many bits will be written to the stream.
	 * @throws IOException 
	 */
	public void writeBits(final int bits, final int n) throws IOException
	{
		for(int i=n-1; i >= 0; i--) {
			writeBit((bits >> i) & 0x01);
		}
	}
	
	/** Flushes underlying output stream. */
	public void flush() throws IOException
	{
		output.flush();
	}

	/** Closes this {@code BitOutputStream}. 
	 * @throws IOException 
	 */
	public void close() throws IOException
	{
		if (nextBitIndex < 7) {
			output.write(nextByte << nextBitIndex);
		}
		output.close();
	}
	
	private void reset()
	{
		nextByte = 0;
		nextBitIndex = 7;
	}
}

