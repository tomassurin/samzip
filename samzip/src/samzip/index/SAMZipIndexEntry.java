package samzip.index;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
class SAMZipIndexEntry
{
	//		public int blockOverlapStart;
	public int blockOverlapEnd;
	public long blockOffset;

	public SAMZipIndexEntry(int blockOverlapEnd, long blockOffset)
	{
		this.blockOverlapEnd = blockOverlapEnd;
		this.blockOffset = blockOffset;
	}
	
}
