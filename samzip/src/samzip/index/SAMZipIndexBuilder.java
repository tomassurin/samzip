package samzip.index;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import net.sf.samtools.SAMRecord;
import samzip.compression.util.CompressionException;
import samzip.io.StreamUtils;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMZipIndexBuilder
{
	private int curReferenceIndex = 0;
	private ReferenceData refData = null;
	private int lastMaximumAlignmentEnd = 0;
	
	private RandomAccessFile output;
	
	private boolean useCompression = false;
	
	static final byte[] magic = {(int)'S', (int)'Z', (int) 'I'};
	private boolean initialized = false;
	private final int n_ref;
	private int writtenRefIndexes = 0;
	
	private List<Integer> offsets = new LinkedList<>();
	
	public SAMZipIndexBuilder(String indexFile, int n_ref) throws FileNotFoundException, CompressionException
	{
		output = new RandomAccessFile(indexFile, "rw");
		
		this.n_ref = n_ref;
	}
	
	public void addBlockData(final SAMRecord[] data, final int len, final long blockOffset) throws IOException
	{
		if (data.length < len || len < 1 )
			return;
		
		SAMRecord rec;
		int maxEnd = lastMaximumAlignmentEnd;

		int tmpRefIdx;
		for(int i=0; i < len; ++i) {
			rec = data[i];
			tmpRefIdx = rec.getReferenceIndex();
			if (tmpRefIdx == -1) {
				continue;
			}
			if (refData == null) {
				curReferenceIndex = tmpRefIdx;
				refData = new ReferenceData(curReferenceIndex);
			}
			if (tmpRefIdx != curReferenceIndex) {
				if (i > 0) {
					refData.add(blockOffset, maxEnd);
				}
				writeIndexData();
				refData = new ReferenceData(tmpRefIdx);
				curReferenceIndex = tmpRefIdx;
				maxEnd = 0;
				lastMaximumAlignmentEnd = 0;
			}
			if (rec.getAlignmentEnd() > maxEnd) {
				maxEnd = rec.getAlignmentEnd()+1;
			}
		}
		
		if (maxEnd > lastMaximumAlignmentEnd) {
			lastMaximumAlignmentEnd = maxEnd;
		}

		refData.add(blockOffset, maxEnd);
	}
	
	public void close() throws IOException
	{
		if (refData == null) {
			refData = new ReferenceData(0);
			writeIndexData();
		}
		if (!refData.isEmpty()) {
			writeIndexData();
		}
		for(; writtenRefIndexes < n_ref; ++writtenRefIndexes) {
			offsets.add(0);
		}
		long refIndexOffset = output.getFilePointer();
		for(int off : offsets) {
			output.writeInt(off);
		}
		output.seek(magic.length+4);
		output.writeInt((int)refIndexOffset);
		output.close();
	}
	
	public void writeIndexData() throws IOException
	{
		if (!initialized) {
			output.write(magic);
			output.writeInt(n_ref);
			output.writeInt(0);
			initialized = true;
		}
		for(; writtenRefIndexes < refData.referenceIndex; ++writtenRefIndexes) {
			offsets.add(0);
		}
		offsets.add((int)output.getFilePointer());
		refData.writeToFile(output);
		++writtenRefIndexes;
	}
	
	public static class ReferenceData
	{
		public final int referenceIndex;
		private List<SAMZipIndexEntry> data = new ArrayList<>();

		public void add(long blockOffset, int blockOverlapEnd)
		{
			data.add(new SAMZipIndexEntry(blockOverlapEnd, blockOffset));
		}

		public int length()
		{
			return data.size();
		}

		public ReferenceData(int referenceIndex)
		{
			this.referenceIndex = referenceIndex;
		}

		public void writeToStream(OutputStream outputStream) throws IOException
		{
			writeToStream(outputStream, true);
		}

		void writeToStream(OutputStream outputStream, boolean writeLength) throws IOException
		{
			if (writeLength) {
				StreamUtils.writeInt(data.size(), outputStream);
			}
			for (SAMZipIndexEntry e : data) {
				StreamUtils.writeLong(e.blockOffset, outputStream);
				StreamUtils.writeUnsignedInt(e.blockOverlapEnd, outputStream);
			}
		}

		void writeToFile(RandomAccessFile file) throws IOException
		{
			file.writeInt(data.size());
			for(SAMZipIndexEntry e : data) {
				file.writeLong(e.blockOffset);
				file.writeInt(e.blockOverlapEnd);
			}
		}
		public boolean isEmpty()
		{
			return data.isEmpty();
		}
	}

}
