package samzip.index;

import java.io.IOException;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public interface SAMZIPIndex
{
	public static final String EXT = ".idx";
	
	long query(final int refId, final int beg, final int end) throws IOException;
}
