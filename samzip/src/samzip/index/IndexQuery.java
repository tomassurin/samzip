/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samzip.index;

import com.beust.jcommander.ParameterException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class IndexQuery
{		
	public static IndexQuery fromString(String query)
	{
		IndexQuery result = new IndexQuery();
		Pattern pat = Pattern.compile("([^:]+):([0-9]+)(:|\\+)([0-9]+)");
		Matcher mat = pat.matcher(query);
		if (!mat.matches())
			throw new IllegalArgumentException("Malformed index query: "+query);
		
		result.ref = mat.group(1);
		try {
			result.beg = Integer.parseInt(mat.group(2));
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException("Illegal number format in index query: "+mat.group(2));
		}
		switch (mat.group(3)) {
			case ":":
				try {
					result.end = Integer.parseInt(mat.group(4));
					result.count = 0;
				} catch (NumberFormatException ex) {
					throw new IllegalArgumentException("Illegal number format in index query: "+mat.group(4));
				}
				break;
			case "+":
				try {
					result.count = Integer.parseInt(mat.group(4));
					result.end = 0;
				} catch (NumberFormatException ex) {
					throw new IllegalArgumentException("Illegal number format in index query: "+mat.group(4));
				}
				break;
			default: { assert false;}
		}
		return result;
	}
		
	private String ref;
	private int beg;
	private int end;
	private int count;

	public int getBeg()
	{
		return beg;
	}

	public int getCount()
	{
		return count;
	}

	public int getEnd()
	{
		return end;
	}

	public String getRef()
	{
		return ref;
	}
	
	
	public boolean isCountQuery()
	{
		return count != 0;
	}
	
	@Override
	public String toString()
	{
		if (count != 0)
			return ref+":"+beg+"+"+count;
		else
			return ref+":"+beg+":"+end;
	}
}
