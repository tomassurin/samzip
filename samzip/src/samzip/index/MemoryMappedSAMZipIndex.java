package samzip.index;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import samzip.io.FastByteArrayOutputStream;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class MemoryMappedSAMZipIndex implements SAMZIPIndex
{
	private int referenceIndex;
	private SAMZipIndexEntry[] data;
	
	private MappedByteBuffer input;
	/// offset of index data in the input file
	private final long indexOffset;
	
	private int n_ref;
	private int[] refIndexOffsets;
	private int lastRefId = -1;

	
	public MemoryMappedSAMZipIndex(String indexFile, boolean integrated) throws IOException, FileNotFoundException
	{
		RandomAccessFile file = new RandomAccessFile(indexFile,"r");
		if (!integrated) {
			indexOffset = 0;
			input = file.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
		} else {
			// index file is at the end of the samzip binary file
			file.seek(file.length()-8);
			indexOffset = file.readLong();
			input = file.getChannel().map(FileChannel.MapMode.READ_ONLY, indexOffset, file.length()-8-indexOffset);
		}
		
		byte[] magic = new byte[3];
		input.get(magic);
		if (!Arrays.equals(magic, SAMZipIndexBuilder.magic))
			throw new IOException("File "+indexFile+" doesn't contain SAMZip index file: magic string doesn't match.");
		
		n_ref = input.getInt();
		int refIndexOffset = input.getInt();
		refIndexOffsets = new int[n_ref];
		input.position(refIndexOffset);
		for(int i=0; i < n_ref; ++i) {
			refIndexOffsets[i] = input.getInt();
		}
	}
			
	@Override
	public long query(final int refId, final int beg, final int end) throws IOException
	{
		if (data == null || refId != lastRefId ) {
			if (refId < 0 || refId > refIndexOffsets.length) 
				throw new IOException("Wrong reference index "+refId);
			
			if (refIndexOffsets[refId] == 0)
				return -1;
			
			input.position(refIndexOffsets[refId]);
			getReferenceData();
			lastRefId = refId;
		}
		
		int i;
		for (i = 0; i < data.length; ++i) {
			SAMZipIndexEntry elem = data[i];
			if (elem.blockOverlapEnd > beg) {
				break;
			}
		}
		if (i == data.length) {
			--i;
		}
		return data[i].blockOffset;
	}

	@Override
	public String toString()
	{
		FastByteArrayOutputStream resultStream = new FastByteArrayOutputStream();
		PrintStream res = new PrintStream(resultStream, true);
		res.printf("Reference index: %1$d \n", referenceIndex);
		for (SAMZipIndexEntry e : data) {
			res.printf("Block offset: %1$d Block overlap end: %2$d\n", e.blockOffset, e.blockOverlapEnd);
		}
		res.close();
		return new String(resultStream.toByteArray(), 0, resultStream.length());
	}

	private void getReferenceData() throws IOException
	{
		int n = input.getInt();
		data = new SAMZipIndexEntry[n];
		long blockOffset;
		int blockOverlapEnd;
		for (int i = 0; i < n; ++i) {
			blockOffset = input.getLong();
			blockOverlapEnd = input.getInt();
			data[i] = new SAMZipIndexEntry(blockOverlapEnd, blockOffset);
		}
	}
}
