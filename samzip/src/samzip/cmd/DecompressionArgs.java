package samzip.cmd;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
@Parameters(separators=" =")
public class DecompressionArgs extends BaseArgs
{	
	@Parameter(names={"--in"}, description="Input compressed file", required=true)
	public String input;
	
	@Parameter(names={"--out"}, description="Output file for decompression results")
	public String output;
	
	@Parameter(names={"-q"}, description="Query file index for decompression of file region in formats: ref:beg:end or ref:beg+count.")
	public String indexQuery;
	
	@Parameter(names={"-h"}, description="Write SAM header to output")
	public boolean writeHeader = false;
	@Parameter(names={"-H"}, description="Write only SAM header to output")
	public boolean writeHeaderOnly = false;
	@Parameter(names={"-P"}, description="Write only compression profile to output")
	public boolean writeProfileOnly = false;
	@Parameter(names={"-b", "--bam"}, description="Write output as BAM file")
	public boolean outputBAM = false;
	@Parameter(names={"--bam-level"}, description="Set BAM compression level")
	public int bamLevel = 5;
	
	public boolean calculateStats = false;
}
