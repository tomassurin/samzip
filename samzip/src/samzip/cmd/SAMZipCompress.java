package samzip.cmd;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import net.sf.samtools.*;
import samzip.api.SAMZipFileWriter;
import samzip.api.SamZipWriterFactory;
import samzip.util.ConsoleWriter;
import samzip.util.SAMZipProgress;
import samzip.util.SAMZipStats;
import samzip.api.SamzException;

/**
 * Command line compression.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public final class SAMZipCompress
{
	private static final int progressInterval = 1 << 15;
	private static ConsoleWriter console = new ConsoleWriter(System.out, System.err);
	
	private SAMRecordIterator iterator = null;
	private SAMZipFileWriter output = null;
	private SAMZipProgress progress = null;
	
	public SAMZipCompress(CompressionArgs opts) throws IOException
	{
		// create writer factory
		SamZipWriterFactory writerFact = new SamZipWriterFactory();
	
		SAMFileReader input;
		SAMFileHeader samHeader;
		
		// open input file
		try {
			// check if inputFile does exist
			File inputf = new File(opts.input);
			if (!inputf.exists()) {
				throw new FileNotFoundException("Input file \"" + opts.input + "\" does not exist");
			}
			// create SAM/BAM file reader
			input = new SAMFileReader(inputf);
			input.setValidationStringency(opts.validationStringency);
			// save SAM file samHeader
			samHeader = input.getFileHeader();
			// find if we are using 'coordinate' sort order
			if (samHeader.getSortOrder() != SAMFileHeader.SortOrder.coordinate) {
				throw new SamzException("SAM/BAM file must be sorted by coordinate before compression - use samtools for this");
			}
			
			iterator = input.iterator();
			iterator.assertSorted(SAMFileHeader.SortOrder.coordinate);
			
			Path parent = Paths.get(opts.input).getParent();
			if (parent != null)
				writerFact.setCwd(parent.toString());
		} catch (SAMException ex) {
			throw new SamzException(ex);
		}
		
		writerFact.setBlockSize(opts.blockSize);
		writerFact.setWorkerThreads(opts.workerThreads);
		writerFact.setEnvironment(opts.environment);
		
		if (opts.calculateStats) {
			writerFact.setCalculateStats(true);
			if (opts.blockStatsFilename != null)
				writerFact.setBlockStatsOutputStream(new BufferedOutputStream(
						new FileOutputStream(opts.blockStatsFilename)));
		}
		
		writerFact.setStrictValidation(opts.strictValidation);
		output = writerFact.makeSAMZipWriter(samHeader, opts.output, opts.profile);

		// initialize utility class for calculating compression progress
		progress = new SAMZipProgress(console, progressInterval);
		progress.initialize(samHeader);
	}
	
	public void compress() throws IOException
	{
		int recordNo = 0;			
		SAMRecord rec;
		while (iterator.hasNext()) {
			rec = iterator.next();
			output.addAlignment(rec);
			
			++recordNo;
			if (progress != null && ((recordNo & (progressInterval-1)) == 0)) { /// speed hack for power of 2
				recordNo = 0;
				progress.writeProgress(rec);
			}
		}
		
		output.close();
	}

	public SAMZipStats getStatistics()
	{
		return output.getStatistics();
	}
}
