package samzip.cmd;

import java.io.*;
import java.util.Iterator;
import net.sf.samtools.SAMFileHeader;
import net.sf.samtools.SAMFileWriter;
import net.sf.samtools.SAMFileWriterFactory;
import net.sf.samtools.SAMRecord;
import samzip.api.SAMZipFileReader;
import samzip.index.IndexQuery;
import samzip.util.ConsoleWriter;
import samzip.util.SAMZipProgress;
import samzip.util.SAMZipStats;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMZipDecompress implements Closeable
{
	private SAMZipFileReader reader;
	private SAMFileHeader samHeader;
	
	private static final int progressInterval = 1 << 15;
	private ConsoleWriter console = new ConsoleWriter(System.out, System.err);
	private SAMZipProgress progress;
	private int BAM_COMPRESSION_LEVEL = 5;

	public void setConsoleWriter(ConsoleWriter console)
	{
		this.console = console;
	}
	
	public final void setBAMCompressionLevel(int level)
	{
		if (level <=0 || level > 9)
			throw new IllegalArgumentException("BAM compression level must be number from 1 (fastests) to 9 (smallest)");
		
		BAM_COMPRESSION_LEVEL = level;
	}
	
	public SAMZipDecompress(DecompressionArgs opts) throws IOException
	{
		reader = new SAMZipFileReader(opts.input);
		reader.setCalculateStats(opts.calculateStats);
		reader.setValidationStringency(opts.validationStringency);
		reader.setEnvironment(opts.environment);
		setBAMCompressionLevel(opts.bamLevel);
		samHeader = reader.getFileHeader();
		// initialize utility class for calculating compression progress
		progress = new SAMZipProgress(console, progressInterval);
		
		if (!(opts.writeHeaderOnly || opts.writeProfileOnly))
			reader.initialize();
	}
	
	public SAMFileHeader getFileHeader()
	{
		return samHeader;
	}
	
	public String getCompressionProfile()
	{
		return reader.getCompressionProfile();
	}
	
	public void setStatistics(SAMZipStats stats)
	{
		reader.setStatistics(stats);
	}
	
	public void decompress(String outputFile, boolean writeHeader) throws IOException
	{
		OutputStream outputStream;
		if (outputFile == null)
			outputStream = System.out;
		else 
			outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));
		decompress(outputStream, writeHeader);
	}
	
	public void decompress(OutputStream outputStream, boolean writeHeader) throws IOException
	{		
		SAMRecord[] data;
		
		if (console != null) {
			progress = new SAMZipProgress(console, progressInterval);
			progress.initialize(samHeader);
		}
		
		int recordNo = 0;
		SAMRecord rec;
		if (writeHeader) {
			SAMFileWriterFactory fact = new SAMFileWriterFactory();
			SAMFileWriter outputWriter = fact.makeSAMWriter(samHeader, true, outputStream);

			Iterator<SAMRecord> it = reader.iterator();
			while (it.hasNext()) {
				rec = it.next();
				outputWriter.addAlignment(rec);
				
				++recordNo;
				if (progress != null && ((recordNo & (progressInterval-1)) == 0)) { /// speed hack for power of 2
					recordNo = 0;
					progress.writeProgress(rec);
				}
			}

			outputWriter.close();
		} else {
			PrintWriter output = new PrintWriter(outputStream);

			Iterator<SAMRecord> it = reader.iterator();
			while (it.hasNext()) {
				rec = it.next();
				output.write(rec.getSAMString());
				
				++recordNo;
				if (progress != null && ((recordNo & (progressInterval-1)) == 0)) { /// speed hack for power of 2
					recordNo = 0;
					progress.writeProgress(rec);
				}
			}
			output.close();
		}
		progress = null;
	}
	
	public void decompress(String outputFile, final String query, boolean writeHeader) throws IOException
	{
		OutputStream outputStream;
		if (outputFile == null)
			outputStream = System.out;
		else 
			outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));
		decompress(outputStream, query, writeHeader);
	}
	
	public void decompress(OutputStream outputStream, final String query, boolean writeHeader) throws IOException
	{	
		IndexQuery parsedQuery = IndexQuery.fromString(query);
		if (parsedQuery.isCountQuery()) {
			decompressCount(outputStream, parsedQuery.getRef(), parsedQuery.getBeg(),
					parsedQuery.getCount(), writeHeader);
		} else {
			decompressRange(outputStream, parsedQuery.getRef(), parsedQuery.getBeg(),
					parsedQuery.getEnd(), writeHeader);
		}
	}
	
	private void decompressRange(OutputStream outputStream, final String refName, 
			               final int beg, final int end, boolean writeHeader) throws IOException
	{
		if (writeHeader) {
			SAMFileWriterFactory fact = new SAMFileWriterFactory();
			SAMFileWriter outputWriter = fact.makeSAMWriter(samHeader, true, outputStream);
			Iterator<SAMRecord> it = reader.iterator(refName, beg, end);
		
			while(it.hasNext()) {
				SAMRecord rec = it.next();
				outputWriter.addAlignment(rec);
			}
			outputWriter.close();
		} else {
			PrintWriter output = new PrintWriter(outputStream);
			Iterator<SAMRecord> it = reader.iterator(refName, beg, end);
			while (it.hasNext()) {
				SAMRecord rec = it.next();
				output.write(rec.getSAMString());
			}
			output.close();
		}
	}
	
	private void decompressCount(OutputStream outputStream, final String refName, 
			               final int beg, final int count, boolean writeHeader) throws IOException
	{
		if (writeHeader) {
			SAMFileWriterFactory fact = new SAMFileWriterFactory();
			SAMFileWriter outputWriter = fact.makeSAMWriter(samHeader, true, outputStream);
			Iterator<SAMRecord> it = reader.iterator(refName, beg, Integer.MAX_VALUE);
			int k = 0;
			while(it.hasNext()) {
				SAMRecord rec = it.next();
				outputWriter.addAlignment(rec);
				++k;
				if (k >= count)
					break;
			}
			outputWriter.close();
		} else {
			PrintWriter output = new PrintWriter(outputStream);
			Iterator<SAMRecord> it = reader.iterator(refName, beg, Integer.MAX_VALUE);
			int k = 0;
			while (it.hasNext()) {
				SAMRecord rec = it.next();
				output.write(rec.getSAMString());
				++k;
				if (k >= count)
					break;
			}
			output.close();
		}
	}
	
	public void decompressBAM(String outputFile)
	{
		SAMRecord[] data;
		
		if (console != null) {
			progress = new SAMZipProgress(console, progressInterval);
			progress.initialize(samHeader);
		}
		
		int recordNo = 0;
		SAMRecord rec;
		SAMFileWriterFactory fact = new SAMFileWriterFactory();
		SAMFileWriter outputWriter = fact.makeBAMWriter(samHeader, true, 
				new File(outputFile), BAM_COMPRESSION_LEVEL);

		Iterator<SAMRecord> it = reader.iterator();
		while (it.hasNext()) {
			rec = it.next();
			outputWriter.addAlignment(rec);

			++recordNo;
			if (progress != null && ((recordNo & (progressInterval-1)) == 0)) { /// speed hack for power of 2
				recordNo = 0;
				progress.writeProgress(rec);
			}
		}

			outputWriter.close();
	}
	
	public void decompressBAM(String outputFile, final String query)
	{
		IndexQuery parsedQuery = IndexQuery.fromString(query);
		if (parsedQuery.isCountQuery()) {
			decompressCountBAM(outputFile, parsedQuery.getRef(), parsedQuery.getBeg(),
					parsedQuery.getCount());
		} else {
			decompressRangeBAM(outputFile, parsedQuery.getRef(), parsedQuery.getBeg(),
					parsedQuery.getEnd());
		}
	}
	
	private void decompressCountBAM(String outputFile, String ref, int beg, int count)
	{
		SAMFileWriterFactory fact = new SAMFileWriterFactory();
			SAMFileWriter outputWriter = fact.makeBAMWriter(samHeader, true, 
				new File(outputFile), BAM_COMPRESSION_LEVEL);
			Iterator<SAMRecord> it = reader.iterator(ref, beg, Integer.MAX_VALUE);
			int k = 0;
			while(it.hasNext()) {
				SAMRecord rec = it.next();
				outputWriter.addAlignment(rec);
				++k;
				if (k >= count)
					break;
			}
			outputWriter.close();
	}

	private void decompressRangeBAM(String outputFile, String ref, int beg, int end)
	{
		SAMFileWriterFactory fact = new SAMFileWriterFactory();
		SAMFileWriter outputWriter = fact.makeBAMWriter(samHeader, true, 
				new File(outputFile), BAM_COMPRESSION_LEVEL);
		Iterator<SAMRecord> it = reader.iterator(ref, beg, end);

		while(it.hasNext()) {
			SAMRecord rec = it.next();
			outputWriter.addAlignment(rec);
		}
		outputWriter.close();
	}
	
	@Override
	public void close() throws IOException
	{
		reader.close();
	}
}
