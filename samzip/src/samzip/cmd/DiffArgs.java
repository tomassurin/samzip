package samzip.cmd;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import net.sf.samtools.SAMFileReader;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
@Parameters(separators=" =")
public class DiffArgs
{
	@Parameter(names={"--in1"}, description="First file", required=true)
	public String input1;
	
	@Parameter(names={"--in2"}, description="Second file", required=true)
	public String input2;
	
	@Parameter(names={"--diffExcludeTags"}, description="Exclude tags from diff.")
	public boolean excludeTagsFromDiff = false;
	
	@Parameter(names={"--stringency"}, description="Set validation stringency used with SAM/BAM readers -- accepted values: STRICT, LENIENT, SILENT", required=false)
	public SAMFileReader.ValidationStringency validationStringency = SAMFileReader.ValidationStringency.DEFAULT_STRINGENCY;
}
