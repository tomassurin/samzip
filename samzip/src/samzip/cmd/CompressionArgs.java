
package samzip.cmd;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import samzip.util.StatUtil;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
@Parameters(separators=" =")
public class CompressionArgs extends BaseArgs
{
	public static class PositiveInteger implements IParameterValidator
	{
		@Override
		public void validate(String name, String value) throws ParameterException
		{
			int n = Integer.parseInt(value);
			if (n <= 0) {
				throw new ParameterException("Parameter " + name + " should be > 0 (found " + value +")");
			}
		}	
	}

	@Parameter(names={"--in"}, description="Input SAM/BAM file", required=true)
	public String input;
	
	@Parameter(names={"--out"}, description="Output (compressed) file", required=true)
	public String output;
	
	@Parameter(names={"-p"}, 
			   description="Compression profile xml file.",
			   required=true)
	public String profile;
	
	@Parameter(names={"-b"}, 
			   description="Number of alignments to store in single block.",
			   validateWith=PositiveInteger.class)			   
	public int blockSize = 1000;
	
	@Parameter(names={"-s"}, 
			   description="Calculate compression time/size statistics.")
	public boolean calculateStats = false;
	
	@Parameter(names={"-t"}, description="Number of threads used for compression.")
	public int workerThreads = StatUtil.getCPUCount();
	
	public boolean strictValidation = true;
}
