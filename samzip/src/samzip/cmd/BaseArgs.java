package samzip.cmd;

import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.util.HashMap;
import java.util.Map;
import net.sf.samtools.SAMFileReader;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
@Parameters(separators=" =")
public class BaseArgs
{
	@DynamicParameter(names = "-D", description = "Preferences")
	public Map<String, String> environment = new HashMap<>();
	
	public String blockStatsFilename = "stats.block.log";
	public String overallStatsFilename = "stats.overall.log";
	
	@Parameter(names={"--stringency"}, description="Set validation stringency used with SAM/BAM readers -- accepted values: STRICT, LENIENT, SILENT", required=false)
	public SAMFileReader.ValidationStringency validationStringency = SAMFileReader.ValidationStringency.DEFAULT_STRINGENCY;
}
