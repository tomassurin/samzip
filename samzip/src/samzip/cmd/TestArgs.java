package samzip.cmd;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import samzip.util.StatUtil;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
@Parameters(separators=" =", commandDescription="Tests compression and decompression.")
public class TestArgs extends BaseArgs
{
	@Parameter(names={"--in"}, description="Input SAM/BAM file", required=true)
	public String input;
	
	@Parameter(names={"--out"}, description="Output (compressed) file", required=true)
	public String output;
	
	@Parameter(names={"--dec"}, description="Output (decompressed) file", required=false)
	public String decOutput;
	
	@Parameter(names={"-p"}, 
			   description="Compression profile xml file.",
			   required=true)
	public String profile;
	
	@Parameter(names={"-b"}, 
			   description="Number of alignments to store in single block.",
			   validateWith=CompressionArgs.PositiveInteger.class)			   
	public int blockSize = 1000;
	
	@Parameter(names={"--bam"}, description="Write output as BAM file")
	public boolean outputBAM = false;
	
	@Parameter(names={"--bam-level"}, description="Set BAM compression level")
	public int bamLevel = 5;
	
	public boolean calculateStats = true;
	
	@Parameter(names={"-t"}, description="Number of threads used for compression.")
	public int workerThreads = StatUtil.getCPUCount();
	
//	@Parameter(names={"-h"}, description="Write SAM header to output")
	public boolean writeHeader = true;
	
	@Parameter(names={"-d"}, description="Calculate difference between input and decompressed output.")
	public boolean calculateDiff = false;
	
	@Parameter(names={"--diffExcludeTags"}, description="Exclude tags from diff.")
	public boolean excludeTagsFromDiff = false;
	
	@Parameter(names={"--blockStats"}, description="Output block stats")
	public boolean calculateBlockStats = false;
}
