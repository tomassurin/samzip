package samzip.cmd;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
@Parameters(separators=" =")
public class StatsArgs
{
	@Parameter(names={"--in","--input"}, description="Input SAM/BAM file", required=true)
	public String input;
	
	@Parameter(names={"--out","--output"}, description="Output (compressed) file", required=true)
	public String output;
	
	@Parameter(names={"-s"}, description="Output SAM field data to the HDD")
	public boolean samOutput=false;
	@Parameter(names={"-b"}, description="Output BAM field data to the HDD")
	public boolean bamOutput=false;
}
