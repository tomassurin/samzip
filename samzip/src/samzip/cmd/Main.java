package samzip.cmd;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import net.sf.samtools.SAMException;
import samzip.api.SamzException;
import samzip.compression.standard.NullEncoder;
import samzip.processor.SAMZipProfileReader.ProfileException;
import samzip.processor.util.SAMProcessorException;
import samzip.util.*;

/**
 *
 * @author Tomáš Šurín <tomas.surin 
 * at gmail.com>
 */
public class Main
{
//	static Path inputFile = Paths.get("data/NA12878.chrom20.ILLUMINA.bwa.CEU.exome.20111114.bam");
//	static Path inputFile = Paths.get("data/unmapped.bam");
//	static Path inputFile = Paths.get("data/demoS.bam");
	static Path inputFile = Paths.get("testdata/manuel.bam");
//	static Path inputFile = Paths.get("testdata/manuel.bam");
//	static Path inputFile = Paths.get("data/manuel.demo.bam");
	static Path outputFile = Paths.get("testdata/output.samz");

	ConsoleWriter writer = new ConsoleWriter();
	StatUtil stat = StatUtil.newInstance();
	
	public static void main(String[] args) throws Exception
	{
		Main app = new Main();
		app.start(args);
//		app.start(new String[] {"diff", "--in1="+inputFile.toString(), "--in2=output.sam"});
//		app.start(new String[] {"test",  "--diffExcludeTags","--stringency=SILENT", "--bam-level=5","--bam", "-p=@best","-b=1000", "-t=2",
//			"--in="+inputFile.toString(), "--out="+outputFile.toString(),"--dec=output.sam","-Dref=testdata/human.fasta"});
//		app.start(new String[] {"test","-p=profile.Delta+Golomb.xml","-b=5000", "-t=2",
//			"--in="+inputFile.toString(), "--out="+outputFile.toString(), "--dec=output.sam"});// "-Dreference="+reference.toString()});
//		app.start(new String[] {"stats", "-s", "-in="+inputFile.toString(), "-out=stats.stats.txt"});
//		app.start(new String[] {"compress","-p=@best","-b=5000","-t=2",
//			"--in="+inputFile.toString(), "--out="+outputFile.toString()});
//		app.start(new String[] {"decompress", "-b" ,"--in="+outputFile, "--out=output","-q=20:62324:62330"});
//		app.start(new String[] {"decompress", "--in=Manuel.samz", "-q=16:81735256+10"}); //"-Dreference="+reference.toString()});
	}

	static final String mainUsage = 
	"Program: samzip (Tool for SAM/BAM files compression)\n"+
	"Version: 1.0 (original release)\n\n"+ 
	"Usage: java -jar samzip.jar [command] [command options]\n" +
	"\t Recommended switches for java: -Xmsction256m -Xmx1024m -server\n"+
	"\nCommands:\n"+
	"\tcompress       SAM/BAM file compression to SamZip format\n" +      
	"\tdecompress     SamZip file decompression\n"+
	"\ttest           Tests compression\n"+
	"\tdiff           Compare two SAM/BAM files";		
	
	static final String compressUsage = 
	  "Command: compress\n"
	+ "Description: Compression of input file in SAM format.\n\n"
	+ "Required options:\n"
	+ "  --in=file           Input SAM or BAM file (must have coordinate sort order\n"
    + "                      and must contain reference sequence dictionary) \n"
	+ "  --out=file          Output file. \n"
	+ "  -p=file             Compression profile\n\n"
	+ "Optional options:\n"
	+ "  -b=size             Number of SAM records (rows) be stored in a single\n"
	+ "                      compressed block (default: 1000)\n"
	+ "  -s                  Calculate compression statistics\n"
	+ "  -t=number           Sets number of threads used for compression.\n"
	+ "                      (default: "+StatUtil.getCPUCount()+")\n"
	+ "  -Dkey=value         Pass property to SAM processors. Properties depends\n"
	+ "                      on SAM processor implementation. For example '-Dref=path'\n"
	+ "                      sets reference sequence for ReferenceSequenceProcessor.\n"
	+ "  --stringency=value  Sets validation stringency used for SAM/BAM readers and\n"
	+ "                      writers. Accepted values are STRICT (stop reading at\n"
	+ "                      errors), LENIENT (only print errors) and SILENT\n"
	+ "                      (don't print errors). (default: STRICT)\n";
	
	
	static final String decompressUsage = 
	  "Command: decompress\n"
	+ "Description: Decompression of SamZip file. Outputs SAM records in text form.\n\n"
	+ "Required options:\n"
	+ "  --in=file           Input compressed SamZip file\n\n"
	+ "Optional options:\n"
	+ "  --out=file          Output file for decompression results \n"
	+ "                      (default: standard output)\n"
	+ "  -q=query            Query compressed file index.\n"
	+ "                      Queries are in format ref:beg:end (records overlapping\n"
	+ "                      region '(beg,end)' on reference sequence 'ref') or\n"
	+ "                      ref:beg+count ('count' records starting at the first \n" 
	+ "                      record that overlaps position 'beg' on reference \n"
	+ "                      sequence 'ref') (default: empty)\n" 
	+ "  -b                  Output BAM file. Option '--out' must be specified.\n"
	+ "                      (default: false)\n"
	+ "  -h                  Write SAM file header to output (default: false)\n"			
	+ "  -H                  Write only SAM file header to output (default: false)\n"
	+ "  -P                  Write only compression profile to output (default:false)\n"			
	+ "  -Dkey=value         Pass property to SAM processors. Properties depends\n"
	+ "                      on SAM processor implementation. For example '-Dref=path'\n"
	+ "                      sets reference sequence for ReferenceSequenceProcessor.\n"
	+ "  --stringency=value  Sets validation stringency used for SAM/BAM readers and\n"
	+ "                      writers. Accepted values are STRICT (stop reading at\n"
	+ "                      errors), LENIENT (only print errors) and SILENT\n"
	+ "                      (don't print errors). (default: STRICT)\n";
	
	static final String testUsage = 
	  "Command: test\n"
	+ "Description: Tests SamZip compression. Automatically calculates stats.\n\n"
	+ "Required options:\n"
	+ "  --in=file           Input SAM/BAM file\n"
	+ "  --out=file          Output compressed file \n"
	+ "  -p=file             Compression profile \n\n"			
	+ "Optional options:\n"
	+ "  --dec=file          Output file for decompression results\n"
	+ "                      (default: null - discarts results) \n"			
	+ "  -b=size             Number of SAM records (rows) be stored in a single\n"
	+ "                      compressed block (default: 1000)\n"
	+ "  --bam               Output decompressed file as BAM. Option '--dec' must\n"
	+ "                      be specified. (default: false)\n"
	+ "  -t=number           Sets number of threads used for compression\n"
	+ "                      (default: "+StatUtil.getCPUCount()+")\n"
	+ "  -d                  If argument dec is specified calculate difference between\n"
	+ "                      original and decompressed file (default: false)\n"
	+ "  --diffExcludeTags    Exclude optional SAM fields from diff calculation\n"
	+ "                      (default: false)\n"
	+ "  --blockStats        Output detailed compression stats for compressed blocks\n"
	+ "  -Dkey=value         Pass property to SAM processors. Properties depends\n"
	+ "                      on SAM processor implementation. For example '-Dref=path'\n"
	+ "                      sets reference sequence for ReferenceSequenceProcessor.\n"
	+ "  --stringency=value  Sets validation stringency used for SAM/BAM readers and\n"
	+ "                      writers. Accepted values are STRICT (stop reading at\n"
	+ "                      errors), LENIENT (only print errors) and SILENT\n"
	+ "                      (don't print errors). (default: STRICT)\n";
	
	static final String diffUsage = 
	  "Command: diff\n"
	+ "Description: Calculates difference between 2 SAM/BAM files.\n\n"
	+ "Required options:\n"
	+ "  --in1=file          First input file\n"
	+ "  --in2=file          Second input file \n\n"
	+ "Optional options:\n"
	+ "  --diffExludeTags    Exclude optional SAM fields from diff calculation\n"
	+ "                      (default: false)\n"
	+ "  --stringency=value  Sets validation stringency used for SAM/BAM readers and\n"
	+ "                      writers. Accepted values are STRICT (stop reading at\n"
	+ "                      errors), LENIENT (only print errors) and SILENT\n"
	+ "                      (don't print errors). (default: STRICT)\n";
	
	
	public void usage(String command)
	{
		if (command == null) {
			System.out.println(mainUsage);
			return;
		}
		switch (command) {
			case "compress":
				System.out.println(compressUsage);
				break;
			case "decompress":
				System.out.println(decompressUsage);
				break;
			case "test":
				System.out.println(testUsage);
				break;
			case "diff":
				System.out.println(diffUsage);
				break;
			default:
				System.out.println(mainUsage);
		}
	}
	
	public void calculateStats(StatsArgs opts) throws FileNotFoundException, IOException
	{		
		RawStatsCalculator calc = new RawStatsCalculator();
		BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(opts.output));

		calc.outputSAMData = opts.samOutput;
		calc.outputBAMData = opts.bamOutput;
		
		calc.calculate(opts.input, output);
	}
	
	public void start(String[] args) throws IOException
	{
		CompressionArgs comOpts = new CompressionArgs();
		DecompressionArgs decOpts = new DecompressionArgs();
		StatsArgs statsOpts = new StatsArgs();
		TestArgs testOpts = new TestArgs();
		DiffArgs diffOpts = new DiffArgs();
		JCommander jc = new JCommander();
		jc.addCommand("compress", comOpts);
		jc.addCommand("decompress", decOpts);
		jc.addCommand("test", testOpts);
		jc.addCommand("stats", statsOpts);
		jc.addCommand("diff", diffOpts);
		
		if (args.length < 1) {
			usage("main");
			return;
		}
		
		try {
			jc.parse(args);
		} catch (ParameterException ex) {
			System.out.println("ERROR: "+ex.getMessage());
			usage(jc.getParsedCommand());
			return;
		}
		
		try {
			switch (jc.getParsedCommand()) {
				case "compress":
					compress(comOpts);
					break;
				case "decompress":
					decompress(decOpts);
					break;
				case "test":
					test(testOpts);
					break;
				case "stats":
					calculateStats(statsOpts);
					break;
				case "diff":
					SAMDiff diff = new SAMDiff();
					diff.setIncludeTags(!diffOpts.excludeTagsFromDiff);
					diff.setValidationStringency(diffOpts.validationStringency);
					if (diff.diff(diffOpts.input1, diffOpts.input2, System.out))
						System.out.println("Files are identical.");
					System.out.flush();
					break;
				default:
					jc.usage();
					break;
			}
		} catch (IOException ex) {
			writer.error(ex);
			System.exit(1);
		} catch (SamzException ex) {
			if (ex.getCause() instanceof ProfileException) {
				writer.error("Error in profile: ", ex.getCause());
			} else if (ex.getCause() instanceof SAMProcessorException) {
				SAMProcessorException proc = (SAMProcessorException) ex.getCause();
				if (proc.getSAMProcessor() != null) 
					writer.error("SAMProcessor error ("+proc.getSAMProcessor().toString()+"): ", proc, true);
				else
					writer.error("SAMProcessor error ", ex.getCause(), true);
			} else {
				writer.error(ex);
			} 
			System.exit(1);
		} catch (SAMException ex) {
			writer.error("SAM exception: ", ex, false);
		}
	}
	
	public void compress(CompressionArgs opts) throws IOException
	{
		PrintStream statsStream = null;
		if (opts.calculateStats) {
			try {
				statsStream = new PrintStream(new BufferedOutputStream(
							  new FileOutputStream(opts.overallStatsFilename)));
			} catch (FileNotFoundException ex) {
				writer.error("Error with compression stats file.");
				return;
			}
		}
		
		new File(opts.output).delete();

		/// initialization of SAMZIPCompress instance
		SAMZipCompress com = new SAMZipCompress(opts);

		/// compression
		writer.info("Compressing '"+opts.input+"'->'"+opts.output+"' ...");
		writer.increaseLevel();
		stat.startCounter();
		com.compress();
		writer.decreaseLevel();
		writer.info("Compression DONE");
		writer.info("");
		
		/// finalization -> stats output
		SAMZipStats stats = com.getStatistics();
		if (stats == null)
			stats = new SAMZipStats();
		stats.userCompressionTime = stat.elapsedTime();
		try {
			stats.inputFileSize = Files.size(Paths.get(opts.input));
			stats.outputFileSize = Files.size(Paths.get(opts.output));
		} catch (IOException ex) {
			writer.error(null, ex);
		}
		
		writer.info(String.format(Locale.US, "Compression time: %1$7.3f ms",(float)stats.userCompressionTime/1000000));
		writer.info(String.format(Locale.US, "Compression ratio: %1$5.3f%%", (float)stats.outputFileSize/stats.inputFileSize*100));
		
	    //cout.println("Total BinFile header size: "+BinaryBlockFile.headerStreamTotalLength);
		if (opts.calculateStats) {
			statsStream.println(stats.toString());
			statsStream.close();
		}
	}
	
	public void decompress(DecompressionArgs opts) throws IOException
	{
		if (opts.output != null)
			writer.info("Preparing decompression ...");
		
		SAMZipDecompress dec = new SAMZipDecompress(opts);
		if (opts.output == null) {
			dec.setConsoleWriter(null);
		} else {
			dec.setConsoleWriter(writer);
		}
		
		if (opts.outputBAM) {
			if (opts.output == null) {
				throw new SamzException("Output file is required when outputing "
						+ "BAM file. Missing command line argument: --out");
			}
			
			stat.startCounter();
			if (opts.indexQuery != null) {
				/// execute index query
				writer.info("Decompressing \""+opts.input+"\" with query \""+opts.indexQuery+"\" ...");
				writer.increaseLevel();
				dec.decompressBAM(opts.output, opts.indexQuery);
				writer.decreaseLevel();
				writer.info("Decompression DONE");
			} else {
				writer.info("Decompressing \""+opts.input+"\"->\""+opts.output+"\" ...");
				writer.increaseLevel();
				dec.decompressBAM(opts.output);				
				writer.decreaseLevel();
				writer.info("Decompression DONE");
			}
		} else {
			OutputStream outputStream;
			if (opts.output == null) {
				outputStream = System.out;
			} else {
				outputStream = new BufferedOutputStream(new FileOutputStream(opts.output));
			}
			
			stat.startCounter();
			if (opts.writeHeaderOnly) {
				PrintWriter outWriter = new PrintWriter(outputStream);
				outWriter.print(dec.getFileHeader().getTextHeader());
				outWriter.close();
			} else if (opts.writeProfileOnly) {
				PrintWriter outWriter = new PrintWriter(outputStream);
				outWriter.print(dec.getCompressionProfile());
				outWriter.close();
			} else if (opts.indexQuery != null) {
				/// execute index query
				if (opts.output != null) {
					writer.info("Decompressing \""+opts.input+"\" with query \""+opts.indexQuery+"\" ...");
				}
				dec.decompress(outputStream, opts.indexQuery, opts.writeHeader);
				if (opts.output != null) {
					writer.info("Decompression DONE");
				}
			} else {
				if (opts.output != null) {
					writer.info("Decompressing \""+opts.input+"\"->\""+opts.output+"\" ...");
					writer.increaseLevel();

				}
				dec.decompress(outputStream, opts.writeHeader);				
				if (opts.output != null) {
					writer.decreaseLevel();
					writer.info("Decompression DONE");
				}
			}
			outputStream.close();
		}
		if (opts.output != null)
			System.out.printf("Decompression time: %1$7.3f ms\n",(float)stat.elapsedTime()/1000000);	
	}
	
	public void test(TestArgs opts) throws IOException
	{
		PrintStream statsStream;
		try {
			statsStream = new PrintStream(new BufferedOutputStream(
				new FileOutputStream(opts.overallStatsFilename)));
		} catch (FileNotFoundException ex) {
			writer.error("Error with compression stats file.");
			return;
		}
		
		new File(opts.output).delete();

	/// initialization of SAMZIPCompress instance
		writer.info("Preparing compression ...");
		CompressionArgs comop= new CompressionArgs();
		comop.input = opts.input;
		comop.output = opts.output;
		comop.profile = opts.profile;
		comop.blockSize = opts.blockSize;
		comop.workerThreads = opts.workerThreads;
		comop.calculateStats = opts.calculateStats;
		comop.strictValidation = false;
		comop.environment = opts.environment;
		comop.overallStatsFilename = opts.overallStatsFilename;
		comop.validationStringency = opts.validationStringency;
		
		if (opts.calculateBlockStats)
			comop.blockStatsFilename = opts.blockStatsFilename;
		else
			comop.blockStatsFilename = null;
		
		SAMZipCompress com = new SAMZipCompress(comop);
//		com.setProgressInterval(opts.progressInterval);
			
	/// compression
		writer.info("Compressing \""+opts.input+"\"->\""+opts.output+"\" ...");
		writer.increaseLevel();
		stat.startCounter();
		com.compress();
		writer.decreaseLevel();
		writer.info("Compression DONE");
		writer.info("");
		
		SAMZipStats stats = com.getStatistics();
		if (stats == null)
			stats = new SAMZipStats();
		stats.userCompressionTime = stat.elapsedTime();
		try {
			stats.inputFileSize = Files.size(Paths.get(opts.input));
			stats.outputFileSize = Files.size(Paths.get(opts.output));
		} catch (IOException ex) {
			writer.error(null, ex);
		}
		
	/// decompression
		writer.info("Preparing decompression ...");
		DecompressionArgs decop = new DecompressionArgs();
		decop.calculateStats = opts.calculateStats;
		decop.environment = opts.environment;
		decop.input = opts.output;
		decop.output = opts.decOutput;
		decop.overallStatsFilename = opts.overallStatsFilename;
		decop.validationStringency = opts.validationStringency;
		decop.writeHeader = opts.writeHeader;
		decop.bamLevel = opts.bamLevel;

		if (opts.calculateBlockStats)
			decop.blockStatsFilename = opts.blockStatsFilename;
		else
			decop.blockStatsFilename = null;
		
		SAMZipDecompress dec = new SAMZipDecompress(decop);
		dec.setStatistics(stats);
		
		OutputStream outputStream = null;
		if (!opts.outputBAM) {		
			if (opts.decOutput == null) {
				// output to null sink
				outputStream = new NullEncoder();
			} else {
				outputStream = new BufferedOutputStream(new FileOutputStream(opts.decOutput));
			}
		} 
		
		writer.info("Decompressing \""+opts.output+"\"->\""+opts.decOutput+"\" ...");
		writer.increaseLevel();
		stat.startCounter();
		if (opts.outputBAM)
			dec.decompressBAM(opts.decOutput);
		else 
			dec.decompress(outputStream, opts.writeHeader);
		stats.userDecompressionTime = stat.elapsedTime();
		writer.decreaseLevel();
		writer.info("Decompression DONE");
		writer.info("");
		
	/// finalization
		writer.info(String.format(Locale.US, "Compression time: %1$7.3f ms",(float)stats.userCompressionTime/1000000));
		writer.info(String.format(Locale.US, "Decompression time: %1$7.3f ms",(float)stats.userDecompressionTime/1000000));					
		writer.info(String.format(Locale.US, "Compression ratio: %1$5.3f%%", (float)stats.outputFileSize/stats.inputFileSize*100));
	    //cout.println("Total BinFile header size: "+BinaryBlockFile.headerStreamTotalLength);
		
		/// copy decompression stats data
		if (opts.calculateStats) {
			stats.blockCount >>= 1;
			statsStream.println(stats.toString());
			statsStream.close();
		}

		/// calculate diff
		if (opts.calculateDiff & opts.decOutput != null) {
			SAMDiff diff = new SAMDiff();
			diff.setValidationStringency(opts.validationStringency);
			diff.setIncludeTags(!opts.excludeTagsFromDiff);
			if (diff.diff(opts.input, opts.decOutput, System.out)) {
				System.out.println("Original and compressed files are identical.");
			}
		}
		
		writer.close();
	}
}