package samzip.api;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import net.sf.samtools.SAMFileHeader;
import net.sf.samtools.SAMFileReader;
import net.sf.samtools.SAMFileReader.ValidationStringency;
import samzip.compression.CompressionCodec;
import samzip.compression.codecs.LZMACodec;
import samzip.processor.SAMProcessorPipeline;
import samzip.processor.SAMZipProfileReader.ProfileException;
import samzip.processor.util.SAMProcessorException;
import samzip.util.ConsoleWriter;
import samzip.util.SAMZipStats;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMZipBase implements Closeable
{
	/// environment variables that are passed to SAM processors in compression pipeline
	protected Map<String, String> environment = new HashMap<>();
	
	/// codec that is used for compression of raw SAM samHeader and raw profile data
	protected static final CompressionCodec headerCodec = new LZMACodec();
	protected SAMFileReader.ValidationStringency validationStringency = SAMFileReader.ValidationStringency.DEFAULT_STRINGENCY;
	protected ConsoleWriter console = new ConsoleWriter(System.out, System.err);
	
	/// statistics
	protected boolean calculateStats = false;
	protected SAMZipStats stats;
	// writer for block statistics
	protected PrintWriter blockStatsOutput;

	protected SAMFileHeader samHeader;
	protected byte[] profileData;	

	public void setValidationStringency(ValidationStringency validationStringency)
	{
		this.validationStringency = validationStringency;
	}
	
	@Override
	public void close() throws IOException
	{
		if (blockStatsOutput != null)
			blockStatsOutput.close();
	}
	
	/**
	 * Sets SAM file header to use for compression. Mandatory for some SAM processors 
	 * (i.e. those which need access to reference sequence dictionary).
	 * @param header 
	 */
	public void setFileHeader(SAMFileHeader header)
	{
		this.samHeader = header;
	}
	
	public SAMFileHeader getFileHeader()
	{
		return samHeader;
	}
	
	/** Sets input file working directory. */
	public void setInputFileDirectory(String dir)
	{
		environment.put("input_dir", dir);
	}
	
	public void setCurrentWorkingDirectory(String dir)
	{
		environment.put("cwd", dir);
	}
	
	/**
	 * @return String reprezentation of XML compression profile
	 */
	public String getCompressionProfile()
	{
		return new String(profileData, Charset.forName("UTF-8"));
	}
	
	/** @return internal compression/decompression statistics instance */
	public SAMZipStats getStatistics()
	{
		return stats;
	}
	
	/** Set's statistics object instance */
	public void setStatistics(SAMZipStats stats)
	{
		this.stats = stats;
	}
	
	/** if {@code calculateStats} is true then allows stats calculation. */
	public void setCalculateStats(boolean calculateStats)
	{
		this.calculateStats = calculateStats;
		stats = new SAMZipStats();
	}
	
	/** Set's output stream for detailed block stats output. */
	public void setBlockStatsOutputStream(OutputStream stream)
	{
		blockStatsOutput = new PrintWriter(stream);
	}
	
	/** Set method environment */
	public void setEnvironment(Map<String, String> prefs)
	{
		if (prefs == null)
			return;
		for(String key : prefs.keySet()) {
			this.environment.put(key, prefs.get(key));
		}
	}
		
	protected void setPipelineEnvironment(SAMProcessorPipeline pipeline, Map<String, String> prefs)
	{
		for (String key : environment.keySet()) {
			pipeline.setEnvironmentVar(key, environment.get(key));
		}
	}
	
	protected SAMProcessorPipeline newPipeline()
	{
		try {
			SAMProcessorPipeline pipeline;
			pipeline = new SAMProcessorPipeline(profileData);
			pipeline.setCalculateStats(calculateStats);
			pipeline.setSamHeader(samHeader);
			setPipelineEnvironment(pipeline, environment);
			pipeline.initialize();
			return pipeline;
		} catch(ProfileException | IOException | SAMProcessorException ex) {
			throw new SamzException(ex);
		}
	}
	
	protected SAMProcessorPipeline newPipeline(boolean strictValidation)
	{
		try {
			SAMProcessorPipeline pipeline;
			pipeline = new SAMProcessorPipeline(profileData);
			pipeline.setCalculateStats(calculateStats);
			pipeline.setSamHeader(samHeader);
			setPipelineEnvironment(pipeline, environment);
			pipeline.initialize();

			// validate processor pipeline
			if (strictValidation) {
				pipeline.validate();
			} else {
				String mandError = pipeline.getMandatoryFieldsErrors();
				if (!mandError.isEmpty()) {
					String deperr = pipeline.getDependencyErrors();
					String valerr = pipeline.getValidationErrors();
					String unerr = pipeline.getMultipleProcessingErrors();
					if (!(deperr+valerr+unerr).isEmpty())
						pipeline.validate();
					else 
						console.warning(mandError);
				} else {
					pipeline.validate();
				}
			}
			return pipeline;
		} catch(ProfileException | IOException | SAMProcessorException ex) {
			throw new SamzException(ex);
		}
	}
}

