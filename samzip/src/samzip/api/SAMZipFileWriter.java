package samzip.api;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.*;
import java.util.logging.Level;
import net.sf.samtools.SAMFileWriter;
import net.sf.samtools.SAMRecord;
import samzip.compression.EncoderMethod;
import samzip.compression.util.CodecException;
import samzip.index.SAMZIPIndex;
import samzip.index.SAMZipIndexBuilder;
import samzip.io.BinaryBlockFile;
import samzip.io.StreamUtils;
import samzip.processor.SAMProcessorPipeline;
import samzip.processor.util.SAMProcessorException;
import samzip.util.LOG;
import samzip.util.SAMZipStats;

/**
 * Implementation of SamZip file writer. It can be used as substitute for Picard's
 * SAM & BAM file writers as it implements the same interface.
 * 
 * <p> After writer configuration, method {@link initialize(boolean)} must be called. </p>
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SAMZipFileWriter extends SAMZipBase implements SAMFileWriter 
{
	/** Indicates that writer is initialized */
	private boolean initialized = false;
	
	/** Path to output file */
	private String outputFile;
	/** Path to binary output file */
	private BinaryBlockFile output;
	
	/** Block size - number of SAMRecords that are stored in single block */
	private int blockSize = 1000;
	
	/** SamZip index builder. */ 
	private SAMZipIndexBuilder index;
	/** Path to index file */
	private String indexFile;
	
	/// threading
	/** Number of threads that are used for compression. Another thread is being
	 * used for I/O output.  */
	private int workerThreads = 2;
	/** Ratio of workers (structures for compression) to threads. */
	private static final int WORKERS_THREADS_RATIO = 2;
	/** Number of workers (structures for compression). They are aimed 
	 * for improvement of threads usage.*/
	private int workers = 2 * WORKERS_THREADS_RATIO;
	/** Blocking queue used for storing future return values from compression workers. */
	private BlockingQueue<Future<BlockCompressionData>> outputData;
	/** Fixed thread pool */
	private ExecutorService threadPool;
	private CompressionWorker[] compressionWorkers;
	private int curWorkerIdx;
	
	/** Creates SamZip file writer with default configuration.
	 * 
	 * @param profileStream stream containing XML profile data
	 * @throws IOException if error occurs while reading profile data
	 */
	public SAMZipFileWriter(InputStream profileStream, String outputFilename) throws IOException
	{
		readProfile(profileStream);
		this.outputFile = outputFilename;
	}
	
	/** Handles standard profiles (prefixed with @) */
	private InputStream getProfileStream(String profileName) throws FileNotFoundException
	{
		if (profileName.startsWith("@")) {
			String ext = "";
			if (!profileName.endsWith(".xml")) 
				ext = ".xml";
			String data = "profile/"+profileName.substring(1)+ext;
			return getClass().getClassLoader() .getResourceAsStream("profile/"+profileName.substring(1)+ext);
		}
		return new FileInputStream(profileName);
	}
	
	/** Creates SamZip file writer with default configuration.
	 * 
	 * @param profileFile path to fila which contains XML profile data
	 * @throws IOException if error occurs while reading profile data
	 */
	public SAMZipFileWriter(String profileFile, String outputFilename) throws FileNotFoundException, IOException
	{
		InputStream profileStream = getProfileStream(profileFile);
		readProfile(profileStream);
		this.outputFile = outputFilename;
	}
	
	/** Reads profile data from input stream {@code profileStream}. */
	private void readProfile(InputStream profileStream) throws IOException
	{
		ByteArrayOutputStream data = new ByteArrayOutputStream(profileStream.available());
		byte[] buf = new byte[1024];
		int ret;
		while ( (ret = profileStream.read(buf)) > 0) {
			data.write(buf, 0, ret);
		}
		data.close();
		profileData = data.toByteArray();
		profileStream.read(profileData);
	}
	
	/**
	 * Sets number of SAM records to store in a single block while compressing. Defaults to 1000.
	 */
	public void setBlockSize(int blockSize)
	{
		if (initialized)
			return;
		
		if (blockSize < 1) {
			throw new IllegalArgumentException("Block size must be positive, was: " + blockSize);
		}

		this.blockSize = blockSize;
	}

	public int getBlockSize()
	{
		return blockSize;
	}
	
	/**
	 * Configure number of threads which will be used for compression.
	 * @param workerThreads 
	 */
	public void setWorkerThreads(int workerThreads)
	{
		if (initialized)
			return;
		
		if (workerThreads <= 0) {
			throw new IllegalArgumentException("Worker threads count must be positive, was: " + workerThreads);
		}

		this.workerThreads = workerThreads;
		this.workers = workerThreads*WORKERS_THREADS_RATIO;
	}	
	
	/** 
	 * Initialize compression structures. After successful call to this method 
	 * {@code addAlignment} can be used for compression. Uses strict validation for SAM
	 * processors.
	 * 
	 * @throws IOException 
	 * @throws CodecException if there was problem with XML profile configuration
	 * @throws SAMProcessorException if SAM processor configuration validation failed
	 */
	public void initialize()
	{
		initialize(true);
	}
	
	/** 
	 * Initialize compression structures. After successful call to this method 
	 * {@code addAlignment} can be used for compression.
	 * @param strictValidation sets strict validation for SAM processors
	 * 
	 * @throws IOException 
	 * @throws CodecException if there was some problem with XML profile configuration
	 * @throws SAMProcessorException if SAM processor configuration validation failed
	 */
	public void initialize(boolean strictValidation)
	{
		// validate configuration
		if (samHeader == null)
			throw new SamzException("SAM file header was null");
		
		/// initialize compression pipeline
		SAMProcessorPipeline pipeline = newPipeline(strictValidation);
		/// initialize compression workers
		threadPool = Executors.newFixedThreadPool(workerThreads);
		outputData = new LinkedBlockingQueue<>();
		compressionWorkers = new CompressionWorker[workers];
		compressionWorkers[0] = new CompressionWorker(pipeline);
		for (int i = 1; i < workers; ++i) {
			CompressionWorker bcom = new CompressionWorker(newPipeline());
			compressionWorkers[i] = bcom;
		}

		/// create output file
		if (outputFile == null)
			throw new SamzException("Output file wasn't specified.");
		
		try {
			output = new BinaryBlockFile(outputFile, "rw");
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
		
		/// create temporary index file
		Path outputFilePath = Paths.get(outputFile);
		Path parent = outputFilePath.getParent();
		Path fileName = null;
		if (parent != null) {
			fileName = outputFilePath.getFileName();
			if (fileName != null) {
				this.indexFile = parent.resolve("TEMP_" + fileName.toString() + SAMZIPIndex.EXT).toString();
			}
		} 
		
		if (parent == null || fileName == null) {
			this.indexFile = "TEMP_" + outputFile + SAMZIPIndex.EXT;
		}
		
		try {
			index = new SAMZipIndexBuilder(indexFile, samHeader.getSequenceDictionary().size());
		} catch (Exception ex) {
			throw new SamzException(ex);
		} 
		
		/// initialize stats structure
		if (calculateStats) {
			stats = new SAMZipStats();
		}		

		/// write compression profile and SAM header to output
		try {
			BinaryBlockFile.OutputBlock headerBlock = output.newBlock();
			EncoderMethod enc = headerCodec.newEncoderMethod(headerBlock.newStream());
			enc.write(profileData);
			enc.close();
			enc = headerCodec.newEncoderMethod(headerBlock.newStream());
			enc.write(samHeader.getTextHeader().getBytes("UTF-8"));
			enc.close();
			headerBlock.close();
			if (calculateStats) {
				stats.headerSize = headerBlock.getStreamLength(0);
				stats.profileSize = headerBlock.getStreamLength(1);
			}
			output.writeBlock(headerBlock);
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
		
		curWorkerIdx = 0;
		curDataIdx = 0;
		data =  new SAMRecord[blockSize];
		
		initialized = true;
	}
	
	private SAMRecord[] data;
	private int curDataIdx;
	
	/**
	 * Write record to output SamZip file.
	 * 
	 * @param rec SAM record to be written
	 */
	@Override
	public void addAlignment(SAMRecord rec)
	{	
		if (curDataIdx >= blockSize) {
			try {
				compressBlock(data, curDataIdx);
			} catch (IOException ex) {
				throw new SamzException(ex);
			}
			curDataIdx = 0;
			data = new SAMRecord[blockSize];
			data[curDataIdx++] = rec;
		} else {
			data[curDataIdx++] = rec;
		}
	}
	
	/**
	 * Write array of records to output SamZip file.
	 * 
	 * @param rec array of SAM records to be written
	 */
	public void addAlignment(SAMRecord[] alignments)
	{
		if (!initialized)
			throw new SamzException("SAMZipWriter is not initialized");
		
		for(int i=0; i < alignments.length; ++i) {
			if (curDataIdx == blockSize) {
				try {
					compressBlock(data, curDataIdx);
				} catch (IOException ex) {
					throw new SamzException(ex);
				}
				curDataIdx = 0;
				data = new SAMRecord[blockSize];
				data[curDataIdx++] = alignments[i];
			} else {
				data[curDataIdx++] = alignments[i];
			}
		}
	}	
	
	/** Close SamZip file. Without this call resulting file will probably be defective. */
	@Override
	public void close()
	{
		if (!initialized)
			throw new SamzException("SAMZipWriter is not initialized");
		
		/// compress last block
		if (curDataIdx > 0) {
			try {
				compressBlock(data, curDataIdx);
			} catch (IOException ex) {
				throw new SamzException(ex);
			}
		}
		try {
			/// write yet unfinished block to the output file
			writeCompressionResults();
		} catch (IOException ex) {
			throw new SamzException(ex);
		}

		/// shut down thread pool and await it's termination
		threadPool.shutdown();
		while (true) {
			try {
				if (threadPool.awaitTermination(100, TimeUnit.MILLISECONDS)) {
					break;
				}
			} catch (InterruptedException ex) {
			}
		}

		try {
			/// finalize index
			index.close();
			byte[] indexData = StreamUtils.readStreamData(new BufferedInputStream(new FileInputStream(indexFile)));
			RandomAccessFile rout = output.getRandomAccessFile();
			long curPos = rout.getFilePointer();
			rout.write(indexData);
			rout.writeLong(curPos);
			// delete temporary index file
			Files.delete(Paths.get(indexFile));
			if (calculateStats) {
				stats.indexSize = indexData.length;

			}
			/// close output file
			output.close();
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
		initialized = false;
		
		samHeader = null;
		blockCount = 0;
		output = null;
		try {
			super.close();
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
	}
	
	private int blockCount = 0;
	
	private void compressBlock(SAMRecord[] data, int len) throws IOException
	{
		/// block compression
		CompressionWorker com = compressionWorkers[curWorkerIdx++];
		if (len < data.length) {
			SAMRecord[] tmpdata = new SAMRecord[len];
			System.arraycopy(data, 0, tmpdata, 0, len);
			data = tmpdata;
		}
		com.newTask(data, output.newBlock());
		Future<BlockCompressionData> result = threadPool.submit(com);
		outputData.add(result);
		if (curWorkerIdx >= workers) {
			writeCompressionResults();
			curWorkerIdx = 0;
		}
	}
	
	private void writeCompressionResults() throws IOException
	{
		while (true) {
			Future<BlockCompressionData> future = outputData.poll();
			if (future == null) {
				break;
			}
			BlockCompressionData block;
			try {
				block = future.get();
			} catch (InterruptedException | ExecutionException ex) {
				LOG.log(Level.SEVERE, "Error while querying future", ex);
				break;
			}

			if (block.ex != null) {
				throw new SamzException("Exception occured while compressing block", block.ex);
			}

			long filep = output.getFilePointer();
			if (calculateStats) {
				if (block.stats == null) {
					return;
				}
				block.stats.filePointer = filep;
				block.stats.blockNo = 1 + blockCount++;
				stats.addBlock(block.stats);
				if (blockStatsOutput != null) {
					// output block statistics to file
					blockStatsOutput.print(block.stats.toString());
					blockStatsOutput.println("================================================================");
					blockStatsOutput.flush();
				}
			}

			index.addBlockData(block.data, block.data.length, filep);
			output.writeBlock(block.block);
		}
	}	
}

class BlockCompressionData
{

	public BlockCompressionData(BinaryBlockFile.OutputBlock block, SAMZipStats.BlockCompressionStats stats, SAMRecord[] data, Exception ex)
	{
		this.block = block;
		this.stats = stats;
		this.data = data;
		this.ex = ex;
	}
	BinaryBlockFile.OutputBlock block;
	SAMZipStats.BlockCompressionStats stats;
	SAMRecord[] data;
	Exception ex;
}

class CompressionWorker implements Callable<BlockCompressionData>
{
	/// fields for block compression
	private SAMRecord[] data;
	private BinaryBlockFile.OutputBlock block;
	private final SAMProcessorPipeline processorPipeline;
	private BlockCompressionData result = null;

	public CompressionWorker(SAMProcessorPipeline processorPipeline)
	{
		this.processorPipeline = processorPipeline;
	}

	public void newTask(SAMRecord[] data, BinaryBlockFile.OutputBlock block)
	{
		this.data = data;
		this.block = block;
		this.result = null;
	}

	@Override
	public BlockCompressionData call() throws Exception
	{
		/// bug04.05.12: this method was sometimes being called twice 
		//               (when Future.get() was called)
		/// solution: we save return value and return this saved value if called
		if (result != null) {
			return result;
		}

		try {
			processorPipeline.compress(data, block);
			SAMZipStats.BlockCompressionStats stats = processorPipeline.getStatistics();
			result = new BlockCompressionData(block, stats, data, null);
		} catch (Exception ex) {
			result = new BlockCompressionData(null, null, null, ex);
		}
		return result;
	}
}