package samzip.api;

import net.sf.samtools.SAMException;

/**
 * Exception used in high level routines. It's compatible with Picard SDK.
 * It's runtime exception so we don't have to clutter the API with throws clauses.
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SamzException extends SAMException
{
	public SamzException()
	{
	}

	public SamzException(String message)
	{
		super(message);
	}

	public SamzException(Throwable cause)
	{
		super(cause);
	}

	public SamzException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
