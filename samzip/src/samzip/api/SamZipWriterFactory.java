package samzip.api;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.samtools.SAMFileHeader;
import net.sf.samtools.SAMFileReader;
import net.sf.samtools.SAMFileReader.ValidationStringency;

/**
 *
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public class SamZipWriterFactory
{
	/** Block size - number of SAMRecords that are stored in single block */
	private int blockSize = 1000;
	/** Number of threads that are used for compression. Another thread is being
	 * used for I/O output.  */
	private int workerThreads = 2;
	/** Current working directory */
	private String cwd;
	/** Indicates if SamZip file writer is going to calculate statistics */
	private boolean calculateStats = false;
	/** Writer for block statistics */
	private OutputStream blockStatsOutput;
	
	/** SamZip environment variables ((key,value) pairs from -D command line arguments)) */
	private Map<String, String> environment = new HashMap<>();
	
	private SAMFileReader.ValidationStringency validationStringency = SAMFileReader.ValidationStringency.DEFAULT_STRINGENCY;
	private boolean strictValidation;
	
	public void setCalculateStats(boolean calculateStats)
	{
		this.calculateStats = calculateStats;
	}
	
	public void setBlockStatsOutputStream(OutputStream stream)
	{
		blockStatsOutput = stream;
	}
	
	public void setEnvironment(Map<String, String> prefs)
	{		
		this.environment = prefs;
	}
	
	public void setStrictValidation(boolean strictValidation)
	{
		this.strictValidation = strictValidation;
	}
	
	/**
	 * Sets number of SAM records to store in a single block while compressing. Defaults to 1000.
	 */
	public void setBlockSize(int blockSize)
	{
		if (blockSize < 1) {
			throw new IllegalArgumentException("Block size must be positive, was: " + blockSize);
		}

		this.blockSize = blockSize;
	}
	
	/**
	 * Configure number of threads which will be used for compression.
	 * @param workerThreads 
	 */
	public void setWorkerThreads(int workerThreads)
	{		
		if (workerThreads <= 0) {
			throw new IllegalArgumentException("Worker threads count must be positive, was: " + workerThreads);
		}

		this.workerThreads = workerThreads;
	}	
	
	/** Set's current working director */
	public void setCwd(String dir)
	{
		this.cwd = dir;
	}
	
    /**
     * How strict to be when reading a SAM or BAM, beyond bare minimum validation.
	 * See: {@link SAMFileReader.ValidationStringency}.
     */
	public void setValidationStringency(SAMFileReader.ValidationStringency validationStringency)
	{
		this.validationStringency = validationStringency;
	}
	
	/**
     * Create a SAMZipWriter for writing to a compressed stream that is ready to receive SAMRecords.
     * 
     * @param header entire header. Sort order is determined by the sortOrder property of this arg
     * @param outputFile  the file to write records to
	 * @param profileStream stream containing xml profile data
     */
    public SAMZipFileWriter makeSAMZipWriter(final SAMFileHeader header, final String outputFile, final InputStream profileStream) 
	{
		try {
			SAMZipFileWriter output = new SAMZipFileWriter(profileStream, outputFile);
			output.setFileHeader(header);
			output.setBlockSize(blockSize);
			output.setWorkerThreads(workerThreads);
			output.setInputFileDirectory(cwd);
			output.setEnvironment(environment);
			output.setCalculateStats(calculateStats);
			if (blockStatsOutput != null)
				output.setBlockStatsOutputStream(blockStatsOutput);
			output.initialize(strictValidation);
			return output;
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
	}
	
	/**
     * Create a SAMZipWriter for writing to a compressed stream that is ready to receive SAMRecords.
     * 
     * @param header entire header. Sort order is determined by the sortOrder property of this arg
     * @param outputFile  the file to write records to
	 * @param profileFile file that contains xml profile data
     */
	public SAMZipFileWriter makeSAMZipWriter(final SAMFileHeader header, final String outputFile, final String profileFile) 
	{ 
		try {
			SAMZipFileWriter output = new SAMZipFileWriter(profileFile, outputFile);
			output.setFileHeader(header);
			output.setBlockSize(blockSize);
			output.setWorkerThreads(workerThreads);
			output.setInputFileDirectory(cwd);
			output.setEnvironment(environment);
			output.setCalculateStats(calculateStats);
			if (blockStatsOutput != null)
				output.setBlockStatsOutputStream(blockStatsOutput);
			output.initialize(strictValidation);
			return output;
		} catch (FileNotFoundException ex) {
			throw new SamzException("Can't find profile file \""+profileFile+"\"");
		} catch (IOException ex) {
			throw new SamzException(ex);
		} 
	}
}
