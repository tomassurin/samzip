package samzip.api;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import net.sf.samtools.*;
import net.sf.samtools.util.CloseableIterator;
import samzip.index.MemoryMappedSAMZipIndex;
import samzip.index.SAMZIPIndex;
import samzip.io.BinaryBlockFile;
import samzip.io.BinaryBlockFile.InputBlock;
import samzip.processor.SAMProcessorPipeline;
import samzip.processor.SAMZipProfileReader;

/**
 *
 * Implementation of SamZip file reader. It can be used as substitute for Picard's
 * SAM & BAM file readers as it implements iterators with the same interface (via assertableIterator).
 * 
 * <p>TODO: add another query types used in Picard's SAM/BAM readers. </p>
 * 
 * @author Tomáš Šurín <tomas.surin at gmail.com>
 */
public final class SAMZipFileReader extends SAMZipBase implements Iterable<SAMRecord>, Closeable
{
	private SAMProcessorPipeline pipeline;
	
	private String inputFile;
	private BinaryBlockFile input;
	private SAMZIPIndex index;
	
	private long inputFirstBlockOffset;
	private boolean initialized;
		
	/** Constructs SamZip file reader for input file {@code inputFile}. */
	public SAMZipFileReader(String inputFile) 
	{
		open(inputFile, false);
	}
	
	private void open(String inputFile, boolean initialize) 
	{
		this.inputFile = inputFile;
		try {
			input = new BinaryBlockFile(inputFile, "r");
		} catch (FileNotFoundException ex) {
			throw new SamzException("Cannot find input file \""+inputFile+"\".");
		}
		try {
			/// read SamZip samHeader
			InputBlock headerBlock = input.readBlock();
			// read profile
			profileData = SAMZipProfileReader.readProfileData(headerCodec.newDecoderMethod(headerBlock.getStream()));
			// read SAMHeader
			InputStream samHeaderStream = headerCodec.newDecoderMethod(headerBlock.getStream());		
			SAMFileReader headerParser = new SAMFileReader(samHeaderStream);
			samHeader = headerParser.getFileHeader();

			Path parent = Paths.get(inputFile).getParent();
			if (parent != null) {
				setInputFileDirectory(parent.toString());	
			}

			if (initialize) {
				initialize();
			}

			inputFirstBlockOffset = input.getFilePointer();
		} catch (IOException ex) {
			throw new SamzException("Can't initialize SamZip reader",ex);
		}
	}
	
	/** Initialize decompression structures. */
	public void initialize()
	{
		/// create processing pipeline
		pipeline = newPipeline(false);
		initialized = true;
	}
	
	/** Create sequential iterator to compressed file that can assert sort order. */
	public SAMRecordIterator assertableIterator()
	{
		if (!initialized)
			throw new SamzException("Can't create iterator for file reader, that wasn't initialized");
		try {
			return new AssertableIterator(new SequentialIterator(this));
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
	}
	
	/** Create sequential iterator to compressed file. */
	@Override
	public CloseableIterator<SAMRecord> iterator()
	{
		if (!initialized)
			throw new SamzException("Can't create iterator for file reader, that wasn't initialized");
					
		try {
			return new SequentialIterator(this);
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
	}

	/** Create indexed iterator to compressed file that can assert sort order. */
	public SAMRecordIterator assertableIterator(final String refName, final int beg, final int end)
	{
		if (!initialized)
			throw new SamzException("Can't create iterator for file reader, that wasn't initialized");
		
		if (index == null) {
			try {
				// we are using index data at the end of the samzip input file
				index = new MemoryMappedSAMZipIndex(inputFile, true);
			} catch (FileNotFoundException ex) {
				throw new SamzException("Index file for input \""+inputFile+"\" cannot be found.");
			} catch (IOException ex) {
				throw new SamzException(ex);
			}
		}
		
		int refId = samHeader.getSequenceIndex(refName);
		if (refId == -1)
			throw new SamzException("Sequence name "+refName+" wasn't found in the sequence dictionary");
		try {
			return new AssertableIterator(new IndexedIterator(this, refId, beg, end));
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
	}
	
	/** Create indexed iterator to compressed file.  */
	public CloseableIterator<SAMRecord> iterator(final String refName, final int beg, final int end)
	{
		if (!initialized)
			throw new SamzException("Can't create iterator for file reader, that wasn't initialized");
					
		if (index == null) {
			try {
				// we are using index data at the end of the samzip input file
				index = new MemoryMappedSAMZipIndex(inputFile, true);
			} catch (FileNotFoundException ex) {
				throw new SamzException("Index file for input \""+inputFile+"\" cannot be found.");
			} catch (IOException ex) {
				throw new SamzException(ex);
			}
		}
		
		int refId = samHeader.getSequenceIndex(refName);
		if (refId == -1)
			throw new SamzException("Sequence name "+refName+" wasn't found in the sequence dictionary");
		try {
			return new IndexedIterator(this, refId, beg, end);
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
	}
	
	@Override
	public void close()
	{
		if (!initialized)
			return;
					
		try {
			input.close();
			inputFile = null;		
			super.close();
		} catch (IOException ex) {
			throw new SamzException(ex);
		}
	}
	
	private SAMRecord[] decompressBlock(InputBlock block) throws IOException
	{
		SAMRecord[] data = pipeline.decompress(block);
		if (calculateStats) {
			stats.addBlock(pipeline.getStatistics());
		}
		return data;
	}
	
	protected final class IndexedIterator implements CloseableIterator<SAMRecord>
	{
		private SAMRecord[] curBlock;
		private boolean hasNext = true;
		private int curBlockPos = 0;
		private final int refId;
		private final int beg;
		private final int end;
		
		private SAMZipFileReader readerInstance;
		
		public IndexedIterator(SAMZipFileReader reader, final int refId, final int beg, final int end) throws IOException
		{
			if (reader == null)
				throw new IllegalArgumentException("SAMZipFileReader instance was null.");
			readerInstance = reader;
			
			if (refId < 0 || beg < 0 || end < 0) {
				throw new IllegalArgumentException("Coordinates were negative.");
			}
			this.refId = refId;
			this.beg = beg;
			this.end = end;
			
			long startBlockOffset = index.query(refId, beg, end);

			SAMRecord[] data;

			if (startBlockOffset < 0) {
				hasNext = false;
				return;
			}
			input.seek(startBlockOffset);				
		}

		@Override
		public boolean hasNext()
		{
			if (curBlock == null) {
				getNextBlockData();
			}
			return hasNext;
		}

		private void getNextBlockData()
		{
			try {
				InputBlock inputBlock = input.readBlock();
				if (inputBlock == null) {
					hasNext = false;
					return;
				}
				curBlock = decompressBlock(inputBlock);
				curBlockPos = 0;
				hasNext = false;
				for(;curBlockPos < curBlock.length; ++curBlockPos) {
					SAMRecord rec = curBlock[curBlockPos];
					
					int recRefId = rec.getReferenceIndex();
					int alignStart = rec.getAlignmentStart();
					int alignEnd = rec.getAlignmentEnd();
			
					if (recRefId > refId) {
						hasNext = false;
						break;
					}
					if (recRefId == refId && alignStart > end) {
						hasNext = false;
						break;
					}
					if (recRefId == refId && alignEnd >= beg) {
						hasNext = true;
						break;
					}
				}
			} catch (IOException ex) {
				hasNext = false;
			}
		}
		
		@Override
		public SAMRecord next()
		{
			if (curBlock == null) {
				getNextBlockData();
			}

			SAMRecord rec;
			if (curBlockPos < curBlock.length-1) {
				rec = curBlock[curBlockPos++];
			} else {
				rec = curBlock[curBlockPos];
				getNextBlockData();
			}
			
			int recRefId = curBlock[curBlockPos].getReferenceIndex();
			int alignStart = curBlock[curBlockPos].getAlignmentStart();
			if (recRefId > refId || alignStart > end) {
				hasNext = false;
			}
			
			return rec;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("Not supported yet.");
		}
		
		@Override
		public void close()
		{
			readerInstance.close();
		}
		
	}
	
	protected final class SequentialIterator implements CloseableIterator<SAMRecord>
	{

		private SAMRecord[] curBlock;
		private boolean hasNext = true;
		private int curBlockPos = 0;

		private final SAMZipFileReader readerInstance;

		private SequentialIterator(SAMZipFileReader reader) throws IOException
		{
			if (reader == null)
				throw new IllegalArgumentException("SAMZipFileReader instance was null.");
			readerInstance = reader;
			
			input.seek(inputFirstBlockOffset);
		}
		
		@Override
		public final boolean hasNext()
		{
			if (curBlock == null) {
				getNextBlockData();
			}
			return hasNext;
		}

		private void getNextBlockData()
		{
			try {
				InputBlock inputBlock = input.readBlock();
				if (inputBlock == null) {
					hasNext = false;
					return;
				}
				curBlock = decompressBlock(inputBlock);
				curBlockPos = 0;
			} catch (IOException ex) {
				hasNext = false;
			}
		}
		
		@Override
		public final SAMRecord next()
		{			
			if (curBlock == null) {
				getNextBlockData();
			}
			
			if (curBlockPos < curBlock.length-1) {
				return curBlock[curBlockPos++];
			} else {
				SAMRecord rec = curBlock[curBlockPos];
				getNextBlockData();
				return rec;
			}
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("Not supported operation.");
		}

		@Override
		public void close()
		{
			readerInstance.close();
		}
		
	}
	
	/** Copied from Picard SDK */
	protected static class AssertableIterator implements SAMRecordIterator {

        private final CloseableIterator<SAMRecord> wrappedIterator;
        private SAMRecord previous = null;
        private SAMRecordComparator comparator = null;

        public AssertableIterator(CloseableIterator<SAMRecord> iterator) {
            wrappedIterator = iterator;
        }

		@Override
        public SAMRecordIterator assertSorted(SAMFileHeader.SortOrder sortOrder) {

            if (sortOrder == null || sortOrder == SAMFileHeader.SortOrder.unsorted) {
                comparator = null;
                return this;
            }

            comparator = sortOrder.getComparatorInstance();
            return this;
        }

		@Override
        public SAMRecord next() {
            SAMRecord result = wrappedIterator.next();
            if (comparator != null) {
                if (previous != null) {
                    if (comparator.fileOrderCompare(previous, result) > 0) {
                         throw new IllegalStateException("Records " + previous.getReadName() + " (" +
                             previous.getReferenceName() + ":" + previous.getAlignmentStart() + ") " +
                             "should come after " + result.getReadName() + " (" +
                             result.getReferenceName() + ":" + result.getAlignmentStart() +
                             ") when sorting with " + comparator.getClass().getName());
                     }
                }
                previous = result;
            }
            return result;
        }

		@Override
        public void close() { wrappedIterator.close(); }
		@Override
        public boolean hasNext() { return wrappedIterator.hasNext(); }
		@Override
        public void remove() { wrappedIterator.remove(); }
    }
	
	//	public static void main(String[] args)
//	{
//		SAMZipFileReader dec = new SAMZipFileReader("demo.samz");
//		dec.initialize();
//		CloseableIterator<SAMRecord> it = dec.iterator("20", 60006, 60050);
//		while (it.hasNext()) {
//			System.out.println(it.next().getSAMString());
//		}
//		
//		it = dec.iterator("20", 70000, 75000);
//		while (it.hasNext()) {
//			System.out.println(it.next().getSAMString());
//		}
//		
//		it.close();
//	}
}

