/*
 *    Copyright 2010-2011 University of Toronto
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package savant.samzip;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

import savant.api.adapter.DataSourceAdapter;

import savant.plugin.SavantDataSourcePlugin;


public class SamZipPlugin extends SavantDataSourcePlugin {

	@Override
	public void init()
	{
		
	}

	@Override
	public String getTitle()
	{
		return "SamZip Plugin";
	}
	

	@Override
	public DataSourceAdapter getDataSource() throws Exception
	{
		File f = null;
		javax.swing.JFileChooser fileChooser = new JFileChooser();
		if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			f = fileChooser.getSelectedFile();
			return new SamZipDataSource(f.toURI());
		}
		else {
			
		}
		
		return null;
	}

	@Override
	public DataSourceAdapter getDataSource(URI uri)
	{
		try {
			return new SamZipDataSource(uri);
		} catch (IOException ex) {
			Logger.getLogger(SamZipPlugin.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
	
}
