/*
 *    Copyright 2010-2011 University of Toronto
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package savant.samzip;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.samtools.SAMFileHeader;
import net.sf.samtools.SAMFileReader;
import net.sf.samtools.SAMRecord;
import net.sf.samtools.SAMSequenceDictionary;
import net.sf.samtools.SAMSequenceRecord;
import samzip.api.SAMZipFileReader;
import samzip.api.SamzException;
import savant.api.adapter.BAMDataSourceAdapter;
import savant.api.adapter.RangeAdapter;
import savant.api.adapter.RecordFilterAdapter;
import savant.api.data.DataFormat;
import savant.api.util.Resolution;
import savant.controller.LocationController;
import savant.data.sources.DataSource;
import savant.data.types.BAMIntervalRecord;
import savant.util.MiscUtils;

/**
 * 
 * Class to represent a track of BAM intervals. Uses SamZip as data source.
 * Based on source code for representing track from original BAM files in Savant.
 * 
 * @author Tomáš Šurín
 */
public class SamZipDataSource extends DataSource<BAMIntervalRecord> implements BAMDataSourceAdapter 
{
    private SAMFileHeader samFileHeader;
	
	SAMZipFileReader dec;
    private URI uri;

    public SamZipDataSource(URI uri) throws IOException 
	{
        this.uri = uri.normalize();

		File inFile = new File(uri);
		dec = new SAMZipFileReader(inFile.toString());
		dec.setValidationStringency(SAMFileReader.ValidationStringency.SILENT);
		dec.initialize();		
		samFileHeader = dec.getFileHeader();
    }

    @Override
    public List<BAMIntervalRecord> getRecords(String reference, RangeAdapter range, Resolution resolution, RecordFilterAdapter filt) 
	{
        List<BAMIntervalRecord> result = new ArrayList<>();
        try {
            String ref;
            if (getReferenceNames().contains(reference)) {
                ref = reference;
            } else if (getReferenceNames().contains(MiscUtils.homogenizeSequence(reference))) {
                ref = MiscUtils.homogenizeSequence(reference);
            } else {
                ref = guessSequence();
            }
			Iterator<SAMRecord> it;
			it = dec.iterator(ref, range.getFrom(), range.getTo());

            SAMRecord samRecord;
            BAMIntervalRecord record;
            while (it.hasNext()) {
                samRecord = it.next();
                
                // Don't keep unmapped reads
                if (samRecord.getReadUnmappedFlag()) continue;

                record = BAMIntervalRecord.valueOf(samRecord);
                if (filt == null || filt.accept(record)) {
                    result.add(record);
                }
                if (Thread.interrupted()) {
                }
            }
        } catch (Exception ex) {
			Logger.getLogger(SamZipDataSource.class.getName()).log(Level.SEVERE, null, ex);
			savant.api.util.DialogUtils.displayException("SamZip exception", "Exception occured while accessing SamZip file.", ex);
//			throw new SamzException(ex);
		}
        return result;
    }

    /*
     * Use the length of the reference genome to guess which sequence from the dictionary
     * we should search for reads.
     */
    private String guessSequence() {

        // Find out what sequence we're using, by reading the header for sequences and lengths
        LocationController locationController = LocationController.getInstance();
        int referenceSequenceLength = locationController.getMaxRangeEnd() - locationController.getMaxRangeStart();
        assert Math.abs(referenceSequenceLength) < Integer.MAX_VALUE;

        String sequenceName = null;
        SAMSequenceDictionary sequenceDictionary = samFileHeader.getSequenceDictionary();
        // find the first sequence with the smallest difference in length from our reference sequence
        int leastDifferenceInSequenceLength = Integer.MAX_VALUE;
        int closestSequenceIndex = Integer.MAX_VALUE;
        int i = 0;
        for (SAMSequenceRecord sequenceRecord : sequenceDictionary.getSequences()) {
            int lengthDelta = Math.abs(sequenceRecord.getSequenceLength() - (int)referenceSequenceLength);
            if (lengthDelta < leastDifferenceInSequenceLength) {
                leastDifferenceInSequenceLength = lengthDelta;
                closestSequenceIndex = i;
            }
            i++;
        }
        if (closestSequenceIndex != Integer.MAX_VALUE) {
            sequenceName = sequenceDictionary.getSequence(closestSequenceIndex).getSequenceName();
        }
        return sequenceName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() 
	{
		dec.close();
    }

    Set<String> referenceNames;

    @Override
    public Set<String> getReferenceNames() {

        if (referenceNames == null) {
            SAMSequenceDictionary ssd = samFileHeader.getSequenceDictionary();

            List<SAMSequenceRecord> seqs = ssd.getSequences();

            referenceNames = new HashSet<String>();
            for (SAMSequenceRecord ssr : seqs) {
                referenceNames.add(ssr.getSequenceName());
            }
        }
        return referenceNames;
    }

    @Override
    public URI getURI() {
        return uri;
    }

    @Override
    public final DataFormat getDataFormat() {
        return DataFormat.ALIGNMENT;
    }

    @Override
    public final String[] getColumnNames() {
        return new String[] { "Read Name", "Sequence", "Length", "First of Pair", "Position", "Strand +", "Mapping Quality", "Base Qualities", "CIGAR", "Mate Position", "Strand +", "Inferred Insert Size" };
    }

    /**
     * For use by the Data Table plugin, which needs to know the header for export purposes.
     */
    @Override
    public SAMFileHeader getHeader() {
        return samFileHeader;
    }
}